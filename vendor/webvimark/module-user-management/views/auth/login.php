<?php
/**
 * @var $this yii\web\View
 * @var $model webvimark\modules\UserManagement\models\forms\LoginForm
 */

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="outer_container loginPagebBG">
    <div class="inner_container overlayMask">
        <div class="container" id="login-wrapper">
            <div class="row">
                <div class="col-md-12">
                	<h1 class="loginHeading"><?php echo Yii::$app->mycomponent->Get_settings('company_name');?></h1>
                    <div class="panel panel-default loginContainer">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center"><?= UserManagementModule::t('front', 'LOGIN ACCESS') ?></h3>
                        </div>
                        <div class="panel-body">
        
                            <?php $form = ActiveForm::begin([
                                'id'      => 'login-form',
                                'options'=>['autocomplete'=>'off'],
                                'validateOnBlur'=>false,
                                'fieldConfig' => [
                                    'template'=>"{input}\n{error}",
                                ],
                            ]) ?>
        
                            <?= $form->field($model, 'username')
                                ->textInput(['placeholder'=>$model->getAttributeLabel('username'), 'autocomplete'=>'off']) ?>
        
                            <?= $form->field($model, 'password')
                                ->passwordInput(['placeholder'=>$model->getAttributeLabel('password'), 'autocomplete'=>'off']) ?>
        
                            <?= (isset(Yii::$app->user->enableAutoLogin) && Yii::$app->user->enableAutoLogin) ? $form->field($model, 'rememberMe')->checkbox(['value'=>true]) : '' ?>
        
                            <?= Html::submitButton(
                                UserManagementModule::t('front', 'Login'),
                                ['class' => 'btn btn-md btn-success pull-right']
                            ) ?>
        
                            <div class="row registration-block">
                                <div class="col-sm-6">
                                    <?= GhostHtml::a(
                                        UserManagementModule::t('front', "Registration"),
                                        ['/user-management/auth/registration']
                                    ) ?>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <?= GhostHtml::a(
                                        UserManagementModule::t('front', "Forgot password ?"),
                                        ['/user-management/auth/password-recovery']
                                    ) ?>
                                </div>
                            </div>
        
        
        
        
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$css = <<<CSS
html, body {
	background: #eee;
	-webkit-box-shadow: inset 0 0 100px rgba(0,0,0,.5);
	box-shadow: inset 0 0 100px rgba(0,0,0,.5);
	height: 100%;
	min-height: 100%;
	position: relative;
}

.loginPagebBG {
	background-image: url("../../uploads/loginBG.jpg");
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
}

/*center of screen alignment*/
.outer_container {
    display:table;
    height:100%;
    width:100%;
    text-align:center;
}

.inner_container {
    display:table-cell;
    height:100%;
    width:100%;
    vertical-align:middle;
}

.overlayMask {
	background-color: rgba(255, 255, 255, 0.7);
}

#login-wrapper {
	position: relative;
	text-align: left;
	/*top: 30%;*/
}

.loginHeading {
    max-width: 420px;
    margin: 15px auto 30px;
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    color: #0059a0;
    text-shadow: 0px 1px 5px #fff;
}

.loginContainer {
    max-width: 420px;
    margin: 0 auto;
    border-radius: 0;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border: 0 !important;
    background-color: transparent;
    box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.4);
    -webkit-box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.4);
    -moz-box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.4);
}

.loginContainer .panel-heading {
    background: #0059a0;
    border-radius: 0 !important;
    color: #fff;
    border: 0 !important;
}

.loginContainer .panel-body {
    background: rgba(255, 255, 255, 0.6);
}

.loginContainer .panel-body input {
    border-radius: 0;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border: 0;
    background: #dedede;
    color: #000;
}


.loginContainer .panel-body input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0 50px rgb(129, 199, 255) inset;
    -webkit-text-fill-color: #000;
}

.loginContainer .panel-body input:-webkit-autofill:focus {
    -webkit-box-shadow: 0 0 0 0, 0 0 0 50px #dedede inset;
    -webkit-text-fill-color: #333;
}

.loginContainer .panel-body input::-webkit-input-placeholder {
    color: #000 !important;
}
 
.loginContainer .panel-body input:-moz-placeholder { /* Firefox 18- */
    color: #000 !important; 
}
 
.loginContainer .panel-body input::-moz-placeholder {  /* Firefox 19+ */
    color: #000 !important; 
}
 
.loginContainer .panel-body input:-ms-input-placeholder {  
    color: #000 !important;
}

#login-wrapper .registration-block {
	margin-top: 15px;
}
CSS;

$this->registerCss($css);
?>