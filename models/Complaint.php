<?php

namespace app\models;

use Yii;
use app\models\Customer;

/**
 * This is the model class for table "tbl_order".
 *
 * @property integer $order_id
 * @property integer $merchant_id
 * @property integer $client_id
 * @property string $json_details
 * @property string $trans_type
 * @property string $payment_type
 * @property double $sub_total
 * @property double $tax
 * @property string $taxable_total
 * @property double $total_w_tax
 * @property string $status
 * @property integer $stats_id
 * @property string $contact_phone
 * @property string $email_address
 * @property string $door
 * @property string $street
 * @property string $city
 * @property integer $zipcode
 * @property integer $viewed
 * @property double $delivery_charge
 * @property string $delivery_date
 * @property string $delivery_time
 * @property string $delivery_asap
 * @property string $delivery_instruction
 * @property string $voucher_code
 * @property double $voucher_amount
 * @property string $voucher_type
 * @property integer $cc_id
 * @property string $date_created
 * @property string $date_modified
 * @property string $ip_address
 * @property double $order_change
 * @property string $payment_provider_name
 * @property double $discounted_amount
 * @property double $discount_percentage
 */
class Complaint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_complaint';
    }
	
	 public function getjob()
	{
		//return $this->hasOne(Client::className(), ['to_id' => $user_id,'status' => 'pending'])->from(['tbl_job_assign' => Client::tableName()]);
		//return $this->hasOne(Job::className(), ['status' => 'pending'])->from(['tbl_job_assign' => Job::tableName()]);
		 return $this->hasOne(Job::className(), ['complaint_id' => 'complaint_id']);
		//return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
	}
	
	public function getcustomerdetails()
	{
		//return $this->hasOne(Client::className(), ['to_id' => $user_id,'status' => 'pending'])->from(['tbl_job_assign' => Client::tableName()]);
		//return $this->hasOne(Job::className(), ['status' => 'pending'])->from(['tbl_job_assign' => Job::tableName()]);
		 return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
		//return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email_address', 'contact_phone', 'Address', 'date_created', 'status'], 'required'],
            /*[['merchant_id', 'client_id', 'stats_id', 'zipcode', 'viewed', 'cc_id'], 'integer'],
            [['json_details', 'street'], 'string'],
            [['sub_total', 'tax', 'taxable_total', 'total_w_tax', 'delivery_charge', 'voucher_amount', 'order_change', 'discounted_amount', 'discount_percentage'], 'number'],
            [['delivery_date', 'date_created', 'date_modified'], 'safe'],
            [['trans_type', 'payment_type', 'delivery_time', 'voucher_code', 'voucher_type'], 'string', 'max' => 100],
            [['status', 'delivery_instruction', 'payment_provider_name'], 'string', 'max' => 255],
            [['contact_phone'], 'string', 'max' => 20],
            [['email_address', 'ip_address'], 'string', 'max' => 50],
            [['door', 'city'], 'string', 'max' => 30],
            [['delivery_asap'], 'string', 'max' => 14],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'Customer first name',
            'last_name' => 'Last_name',
            'email_address' => 'Email Adress',
            'contact_phone' => 'Mobile',
            'Address' => 'Address',
            'date_created' => 'Date',
			'status' => 'Status',
            
        ];
    }
}
