<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accesories;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Accesoriessearch extends Accesories
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['acc_name','warning_menu'], 'safe'],
			
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Accesories::find()
			;
           /*  $query = Orders::find();
             $query->joinWith(['clients']);*/
	 /* $query = Orders::find()
->innerJoinWith('clients', 'Order::client_id = clients.client_id')
;*/
	//  $query =Orders::find()->joinWith('clients', true, 'INNER JOIN')->where(['tbl_client.client_id' => $this->client_id])->all();
       // $query=Orders::find()->innerJoinWith(['clients']);
		/*$query = Orders::find()->joinWith('clients')
    ->where(['clients.client_id' => $this->client_id])
    ->all();*/
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
        $query->andFilterWhere(['like', 'acc_name', $this->acc_name]);
		$query->andFilterWhere(['like', 'warning_menu', $this->warning_menu]);
            
	   

        return $dataProvider;
    }
}
