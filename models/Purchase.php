<?php

namespace app\models;

use Yii;

class Purchase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 // public $shop;
	//  public $vendor_id;
	  
    public static function tableName()
    {
        return 'tbl_purchase';
    }
	 public function getVendor()
	{
		
		 return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
		
	}
	 public function getShop1()
	{
		
		 return $this->hasOne(Shop::className(), ['id' => 'shop']);
		
	}
	public function getFirstname() 
	{
       return $this->vendor;
    }
	
	
    public function rules()
    {
        return [
            [['vendor_id','purchase_date', 'shop'], 'required'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          
            'purchase_date' => 'Date',
			'shop' => 'Shop',
            
        ];
    }
}
