<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;



class Income_reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	  public $income_type;
	  public $income_name;
	
	 public static function tableName()
    {
        return 'tbl_income';
    }
	 public function getincome_type()
	{
		
		 return $this->hasOne(Income_type::className(), ['id' => 'income_id']);
		
	}
	 

    public function rules()
    {
        return [
            [['description','income_name','exp_date'], 'safe'],
			//[['expense','exp_date','amount'], 'required'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Income_reports::find()->innerJoinWith('income_type', 'Income::income_id = income_type.id')
			 ->select('sum(amount) as amount,exp_date,income_id')
		    ->groupBy(['income_id']);
         //print_r($query);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		 if ( ! is_null($this->exp_date) && strpos($this->exp_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->exp_date);

             $query->andFilterWhere(['between', 'exp_date', $start_date, $end_date]);

          }
		  else
		  {
			  $query->andFilterWhere(['between', 'exp_date', date("Y-m-01"),date("Y-m-t")]);
		  }
		
        $query->andFilterWhere(['like', 'tbl_income_type.income_name', $this->income_name]);
		$query->andFilterWhere(['like', 'description', $this->description]);
            
	   

        return $dataProvider;
    }
}
