<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Purchase;


class Voucher_search extends Purchase
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['purchase_date','vendor_id','total','paid'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		
	    $query = Purchase::find(); //->where('`tbl_purchase`.`total`=`tbl_purchase`.`paid`'); //->where(['=','`tbl_purchase`.`total`',`tbl_purchase`.`paid`]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		
        if ( ! is_null($this->purchase_date) && strpos($this->purchase_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->purchase_date);

             $query->andFilterWhere(['between', 'tbl_purchase.purchase_date', $start_date, $end_date]);

          }
	   

        return $dataProvider;
    }
}
