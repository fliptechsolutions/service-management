<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;



class Income_type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public static function tableName()
    {
        return 'tbl_income_type';
    }
	 

    public function rules()
    {
        return [
            [['income_name','description'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Income_type::find()
			;
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		
        $query->andFilterWhere(['like', 'income_name', $this->income_name]);
		$query->andFilterWhere(['like', 'description', $this->description]);
            
	   

        return $dataProvider;
    }
}
