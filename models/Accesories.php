<?php

namespace app\models;

use Yii;


class Accesories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_accessories';
    }
	
	
    public function rules()
    {
        return [
            [['acc_name'], 'required'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            	'acc_name' => 'Accessories',            
              ];
    }
}
