<?php

namespace app\models;

use Yii;

class Sales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 // public $shop;
	//  public $vendor_id;
	  
    public static function tableName()
    {
        return 'tbl_sales';
    }
	 public function getVendor()
	{
		
		 return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
		
	}
	 public function getcustomer()
	{
		
		 return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
		
	}
	public function getinventory()
	{
		
		 return $this->hasOne(Inventory::className(), ['sales_id' => 'id']);
		
	}
	 public function getShop1()
	{
		
		 return $this->hasOne(Shop::className(), ['id' => 'shop']);
		
	}
	
	
	
   /* public function rules()
    {
        return [
            [['vendor_id','purchase_date', 'shop'], 'required'],
			
        ];
    }*/

    /**
     * @inheritdoc
     */
    /*public function attributeLabels()
    {
        return [
          
            'purchase_date' => 'Date',
			'shop' => 'Shop',
            
        ];
    }*/
}
