<?php

namespace app\models;

use Yii;


class Selling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_selling_price';
    }
	
	
    public function rules()
    {
        return [
            [['selling_name','price'], 'required'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            	'selling_name' => 'Selling name',
				'price' => 'Price',            
              ];
    }
}
