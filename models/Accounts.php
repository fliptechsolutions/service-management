<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class Accounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 // public $shop;
	//  public $vendor_id;
	  
    public static function tableName()
    {
        return 'tbl_accounts';
    }
	
	 public function rules()
    {
		return [
				
				[['account_name','payment_type','bank_name','branch','account_number','customer_name','ifsc','cash_name'], 'safe'],
				
				
			];
	}
	
	public function search($params)
    {
		//,'complaint_id'=>9
		    
		     
		    $query = Accounts::find();
		    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
         if ( ! is_null($this->created_date) && strpos($this->created_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->created_date);

             $query->andFilterWhere(['between', 'created_date', $start_date, $end_date]);

          }
		  
		    $query->andFilterWhere(['like', 'account_name', $this->account_name]);
		    $query->andFilterWhere(['like', 'payment_type', $this->payment_type]);
            $query->andFilterWhere(['like', 'bank_name', $this->bank_name]);
		    $query->andFilterWhere(['like', 'account_number', $this->account_number]);
		    $query->andFilterWhere(['like', 'branch', $this->branch]);
		    $query->andFilterWhere(['like', 'ifsc', $this->ifsc]);
			//$query->andFilterWhere(['like', 'cash_name', $this->cash_name]);
		
		 
	  

        return $dataProvider;
    }
	 
	
}
