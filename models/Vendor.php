<?php

namespace app\models;

use Yii;

class Vendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_vendor';
    }
	
	
    public function rules()
    {
        return [
            [[ 'company_name','contact_phone'], 'required'],
			['contact_phone', 'unique'],
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'Contact Person',
            'last_name' => 'Last name',
            'email_address' => 'Email Adress',
            'contact_phone' => 'Mobile',
            'Address' => 'Address',
			'city' => 'City',
            'date_created' => 'Date',
			'status' => 'Status',
            
        ];
    }
}
