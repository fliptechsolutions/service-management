<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_order".
 *
 * @property integer $order_id
 * @property integer $merchant_id
 * @property integer $client_id
 * @property string $json_details
 * @property string $trans_type
 * @property string $payment_type
 * @property double $sub_total
 * @property double $tax
 * @property string $taxable_total
 * @property double $total_w_tax
 * @property string $status
 * @property integer $stats_id
 * @property string $contact_phone
 * @property string $email_address
 * @property string $door
 * @property string $street
 * @property string $city
 * @property integer $zipcode
 * @property integer $viewed
 * @property double $delivery_charge
 * @property string $delivery_date
 * @property string $delivery_time
 * @property string $delivery_asap
 * @property string $delivery_instruction
 * @property string $voucher_code
 * @property double $voucher_amount
 * @property string $voucher_type
 * @property integer $cc_id
 * @property string $date_created
 * @property string $date_modified
 * @property string $ip_address
 * @property double $order_change
 * @property string $payment_provider_name
 * @property double $discounted_amount
 * @property double $discount_percentage
 */
class Job extends \yii\db\ActiveRecord
{
     public static function tableName()
    {
        return 'tbl_job_assign';
    }
	
     public function getjobid()
	{
		//return $this->hasOne(Client::className(), ['to_id' => $user_id,'status' => 'pending'])->from(['tbl_job_assign' => Client::tableName()]);
		//return $this->hasOne(Job::className(), ['status' => 'pending'])->from(['tbl_job_assign' => Job::tableName()]);
		 return $this->hasOne(Job::className(), ['id' => 'id']);
		//return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
	}
   
}
