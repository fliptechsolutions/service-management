<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sales;


/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Sales_reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public $customer;
	 public $product_name1;
	 public $company_name;
	 public $customer_name;
     public $contact_phone;
	 public $address;
	 public $inventory;
	 public $tin_no;
	 public $cst_no;
	 
	  public static function tableName()
    {
        return 'tbl_sales';
    }
	public function getcustomer()
	{
		
		 return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
		
	}
    public function rules()
    {
        return [
            //[['product_id'], 'integer'],
            [['qty','complaint_id','sales_date','customer','company_name','customer_name','id','address','contact_phone','tin_no','cst_no','sales_sno','service_sno'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }
	

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type)
    {
		//,'complaint_id'=>9
		    
		    // 
		    $query = Sales::find()->innerJoinWith('customer', 'Sales::customer_id = customer.id')->select('customer_id,sales_sno,sum(paid) as paid,sum(total) as total,sales_date')
			->groupBy(['customer_id']);
			  // $query->joinWith(['customer', 'inventory'])->where(['not',[$type=>NULL]])->select('sum(paid) as paid')
			


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
      
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
         if ( ! is_null($this->sales_date) && strpos($this->sales_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->sales_date);

             $query->andFilterWhere(['between', 'sales_date', $start_date, $end_date]);

          }
		  else
		  {
			  $query->andFilterWhere(['between', 'sales_date', date("Y-m-01"),date("Y-m-t")]);
		  }
		  
		 $query->andFilterWhere(['like', 'customer_id', $this->sales_sno]);
        $query->andFilterWhere(['like', 'customer_id', $this->customer_id]);
		$query->andFilterWhere(['like', 'tbl_customer.tin_no', $this->tin_no]);
        $query->andFilterWhere(['like', 'tbl_customer.cst_number', $this->cst_no]);
		$query->andFilterWhere(['like', 'tbl_customer.company_name', $this->company_name]);
		$query->andFilterWhere(['like', 'tbl_customer.first_name', $this->customer_name]);
		$query->andFilterWhere(['like', 'tbl_customer.contact_phone', $this->contact_phone]);
		$query->andFilterWhere(['like', 'tbl_customer.Address', $this->address]);
		 
	  

        return $dataProvider;
    }
}
