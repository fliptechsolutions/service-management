<?php

namespace app\models;

use Yii;

class Updateteam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	  public $password_repeat;
	  public $newPassword;
    public static function tableName()
    {
        return 'user';
    }
	
	
    public function rules()
    {
        return [
            [['firstname','email','contact_phone', 'address','city'], 'required'],
			['contact_phone', 'unique'],
			//['password_hash','alpha'],
			['email', 'unique'],
			['email', 'email','message'=>"The email isn't correct"],
			//[['password_repeat'], 'compare', 'compareAttribute'=>'newPassword', 'message'=>"Passwords don't match" ],
			
         
        ];
    }
	
	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstname' => 'First name',
            'last_name' => 'Last name',
			'date_of_join'=>'Date of join',
            'email' => 'Email Adress',
            'contact_phone' => 'Mobile',
            'address' => 'Address',
			'city' => 'City',
            'date_created' => 'Date',
			'newPassword' => 'Password',
			'password_repeat' => 'Confirm Password',
			'roll' => 'Roll',
            
        ];
    }
}
