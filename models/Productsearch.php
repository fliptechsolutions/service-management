<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Products;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Productsearch extends Products
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
		
	// public $receipt;

    public function rules()
    {
        return [
            
            [['product_name','product_model', 'type','manufacturer','description'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Products::find();
			
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_model', $this->product_model])
			->andFilterWhere(['like', 'type', $this->type])
			->andFilterWhere(['like', 'manufacturer', $this->type]);
			

        return $dataProvider;
    }
}
