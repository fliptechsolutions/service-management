<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Complaint;
//use app\models\Job;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Complaintmerge extends Complaint
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	public $job;
	public $customerdetails;
	public $first_name;
	public $last_name;
	public $email_address;
	public $contact_phone;
	public $address;

    public function rules()
    {
        return [
            [['complaint_id'], 'integer'],
            [['first_name','contact_phone','address','email_address','status','date_created'], 'safe'],
			[['clients'], 'safe'],
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		//,'complaint_id'=>9
		    
		     $user_id=Yii::$app->user->identity->id;
		     $query = Complaint::find()            
->innerJoinWith('job', 'Complaint::complaint_id = job.complaint_id')->where(['tbl_job_assign.to_id' => $user_id,'tbl_job_assign.status' =>'pending'])
->innerJoinWith('customerdetails', 'Complaint::customer_id = customerdetails.id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['first_name' => SORT_ASC],
        'desc' => ['first_name' => SORT_DESC],
		// 'default' => SORT_ASC
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'complaint_id' => $this->complaint_id,
			//'id' => $this->id,
			
        ]);

       $query->andFilterWhere(['like', 'tbl_customer.first_name', $this->first_name])
            ->andFilterWhere(['like', 'tbl_customer.last_name', $this->last_name])
			->andFilterWhere(['like', 'tbl_customer.email_address', $this->email_address])
			->andFilterWhere(['like', 'tbl_customer.contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'tbl_customer.Address', $this->address]);
	   $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
