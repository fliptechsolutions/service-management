<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Complaint;
use app\models\Customer;
use kartik\daterange\DateRangeBehavior;
/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Complaintsearch extends Complaint
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	public $customerdetails;
	public $receipt;
	public $first_name;
	public $last_name;
	public $email_address;
	public $contact_phone;
	public $address;
    public $date_created;
	public $to_dpt;
   // public $createTimeEnd;
    public function rules()
    {
        return [
            [['complaint_id'], 'integer'],
            [['first_name','contact_phone','address','email_address','status','date_created','customerdetails','to_dpt'], 'safe'],
			[['first_name'], 'safe'],
			//[['receipt'], 'safe'],
        ];
    }



    /*public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createTimeRange',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }*/
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		      // $query = Complaint::find();
			   $query = Complaint::find()->innerJoinWith('customerdetails', 'Complaint::customer_id = customerdetails.id')
			   ->innerJoinWith('job', 'Complaint::complaint_id = job.complaint_id');

          
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'complaint_id' => $this->complaint_id,
			
        ]);

        $query->andFilterWhere(['like', 'tbl_customer.first_name', $this->first_name])
            ->andFilterWhere(['like', 'tbl_customer.last_name', $this->last_name])
			->andFilterWhere(['like', 'tbl_customer.email_address', $this->email_address])
			->andFilterWhere(['like', 'tbl_customer.contact_phone', $this->contact_phone])
			->andFilterWhere(['like', 'tbl_job_assign.to_dpt', $this->to_dpt]);
	   $query->andFilterWhere(['like', 'status', $this->status]);
	   $query->andFilterWhere(['like', 'status', $this->status]);
	   //$query->andFilterWhere(['between', 'date_created', $start_date, $end_date]);
	     if ( ! is_null($this->date_created) && strpos($this->date_created, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->date_created);

             $query->andFilterWhere(['between', 'tbl_complaint.date_created', $start_date, $end_date]);

          }
              //->andFilterWhere(['<', 'tbl_complaint.date_created', $this->date_created]);

        return $dataProvider;
    }
}
