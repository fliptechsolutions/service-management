<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Purchase;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Purchasesearch extends Purchase
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
		public $vendor;
		public $shop1;
	// public $receipt;

    public function rules()
    {
        return [
            
            [['vendor','purchase_date','shop1','invoice_number','id'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Purchase::find();
			// $query->joinWith('vendor');
			 $query->joinWith(['vendor', 'shop1']);
			// ->innerJoinWith('vendor', 'Purchase::vendor_id = vendor.id');
           /*  $query = Orders::find();
             $query->joinWith(['clients']);*/
	 /* $query = Orders::find()
->innerJoinWith('clients', 'Order::client_id = clients.client_id')
;*/
	//  $query =Orders::find()->joinWith('clients', true, 'INNER JOIN')->where(['tbl_client.client_id' => $this->client_id])->all();
       // $query=Orders::find()->innerJoinWith(['clients']);
		/*$query = Orders::find()->joinWith('clients')
    ->where(['clients.client_id' => $this->client_id])
    ->all();*/
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
        
        $query->andFilterWhere(['like', 'tbl_vendor.first_name', $this->vendor])
            ->andFilterWhere(['like', 'tbl_shop.shop', $this->shop1]);
		  if ( ! is_null($this->purchase_date) && strpos($this->purchase_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->purchase_date);

             $query->andFilterWhere(['between', 'tbl_purchase.purchase_date', $start_date, $end_date]);

          }
			
			//->andFilterWhere(['like', 'purchase_date', $this->purchase_date]);
			

        return $dataProvider;
    }
}
