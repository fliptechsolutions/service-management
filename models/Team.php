<?php

namespace app\models;

use Yii;

class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	  public $password_repeat;
    public static function tableName()
    {
        return 'user';
    }
	
	
    public function rules()
    {
        return [
            [['firstname', 'password_hash','email','contact_phone', 'address','city','password_repeat'], 'required'],
			['contact_phone', 'unique'],
			['email', 'unique'],
			//[['contact_phone'],'number','min'=>10,'max'=>12],
			['email', 'email','message'=>"The email isn't correct"],
			['password_repeat', 'compare', 'compareAttribute'=>'password_hash', 'message'=>"Passwords don't match" ],
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstname' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email Adress',
            'contact_phone' => 'Mobile',
            'address' => 'Address',
			'city' => 'City',
            'date_created' => 'Date',
			'password_hash' => 'Password',
			'password_repeat' => 'Confirm Password',
			
            
        ];
    }
}
