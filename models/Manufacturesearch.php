<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Manufacture;


class Manufacturesearch extends Manufacture
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['manufacture_name','description'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Manufacture::find()
			;
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		
        $query->andFilterWhere(['like', 'manufacture_name', $this->manufacture_name]);
		//$query->andFilterWhere(['like', 'price', $this->price]);
            
	   

        return $dataProvider;
    }
}
