<?php
namespace app\models;

use Yii;

class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	// public $selling_price;
	 
    public static function tableName()
    {
       return 'tbl_customer';
    }
	 
	
	
    public function rules()
    {
        return [
            [[ 'company_name', 'contact_phone'], 'required'],
			['contact_phone', 'unique'],
            /*[['merchant_id', 'client_id', 'stats_id', 'zipcode', 'viewed', 'cc_id'], 'integer'],
            [['json_details', 'street'], 'string'],
            [['sub_total', 'tax', 'taxable_total', 'total_w_tax', 'delivery_charge', 'voucher_amount', 'order_change', 'discounted_amount', 'discount_percentage'], 'number'],
            [['delivery_date', 'date_created', 'date_modified'], 'safe'],
            [['trans_type', 'payment_type', 'delivery_time', 'voucher_code', 'voucher_type'], 'string', 'max' => 100],
            [['status', 'delivery_instruction', 'payment_provider_name'], 'string', 'max' => 255],
            [['contact_phone'], 'string', 'max' => 20],
            [['email_address', 'ip_address'], 'string', 'max' => 50],
            [['door', 'city'], 'string', 'max' => 30],
            [['delivery_asap'], 'string', 'max' => 14],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'Contact Person',
            //'last_name' => 'Last name',
            'email_address' => 'Email Adress',
            'contact_phone' => 'Mobile',
            'Address' => 'Address',
			'city' => 'City',
            'date_created' => 'Date',
			'status' => 'Status',
			//'selling_id' => 'Selling price',
            
        ];
    }
}
