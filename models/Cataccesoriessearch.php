<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cataccesories;

class Cataccesoriessearch extends Cataccesories
{
   
	 

    public function rules()
    {
        return [
            [['cat_name'], 'safe'],
			
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
       
        return Model::scenarios();
    }

 
    public function search($params)
    {
		     $query = Cataccesories::find()
			;
       
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }

       
        $query->andFilterWhere(['like', 'cat_name', $this->cat_name]);
            
	   

        return $dataProvider;
    }
}
