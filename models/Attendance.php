<?php
namespace app\models;

use yii\base\Model;
//use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

class Attendance extends  \yii\db\ActiveRecord
{

	public $firstname;
	 public static function tableName()
    {
        return 'tbl_attendance';
    }
	public function getteam()
	{
		return $this->hasOne(Team::className(), ['id' => 'user_id']);
		
	}

	
	public function rules()
    {
        return [
            //[['product_id'], 'integer'],
            [['attendance','created_date','firstname'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }
	
	 public function search($params,$id)
    {
		    if($id==1)
			{
			   $query = Attendance::find()->innerJoinWith('team', 'Attendance::user_id = team.id');
			   
			}
			else
			{
				$query = Attendance::find()->where(['user_id'=>$id]);
			}
			          
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $date=date('Y-m-d').'/'.date('Y-m-d');
        
         $query->andFilterWhere(['like', 'user.firstname', $this->firstname]);
	
	     if (!is_null($this->created_date) && strpos($this->created_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->created_date);

             $query->andFilterWhere(['between', 'created_date', $start_date, $end_date]);

          }
		  else
		  { 
		      $query->andFilterWhere(['between', 'created_date', date('Y-m-d'), date('Y-m-d')]);  
		  }
              //->andFilterWhere(['<', 'tbl_complaint.date_created', $this->date_created]);

        return $dataProvider;
    }

}
?>