<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Selling;


class Sellingsearch extends Selling
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['selling_name','price'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Selling::find()
			;
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		
        $query->andFilterWhere(['like', 'selling_name', $this->selling_name]);
		$query->andFilterWhere(['like', 'price', $this->price]);
            
	   

        return $dataProvider;
    }
}
