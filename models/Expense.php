<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;



class Expense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public static function tableName()
    {
        return 'tbl_expense_type';
    }
	 

    public function rules()
    {
        return [
            [['expense','description'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Expense::find()
			;
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		
        $query->andFilterWhere(['like', 'expense', $this->expense]);
		$query->andFilterWhere(['like', 'description', $this->description]);
            
	   

        return $dataProvider;
    }
}
