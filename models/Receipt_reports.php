<?php
namespace app\models;

use yii\base\Model;
//use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
class Receipt_reports extends  \yii\db\ActiveRecord
{
	public $first_name;
	public $trans;

  
	public static function tableName()
    {
        return 'tbl_sales_transaction';
    }
	
    public function rules()
    {
        return [
					[['id'], 'integer'],
					[['first_name','sales_date','pay_type','customer_id'], 'safe'],
			    ];
    }



    public function getcustomerdetails()
	{
		 return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
	}


    public function search($params)
    {
		
	    $query = Receipt_reports::find()
		->innerJoinWith('customerdetails', 'Receipt_reports::customer_id = customerdetails.id')
		->select('count(*) as trans,sum(amount) as amount,sales_date,customer_id')
		->groupBy(['customer_id']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		if ( ! is_null($this->sales_date) && strpos($this->sales_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->sales_date);

             $query->andFilterWhere(['between', 'sales_date', $start_date, $end_date]);

          }
		  else
		  {
			  $query->andFilterWhere(['between', 'sales_date', date("Y-m-01"),date("Y-m-t")]);
		  }
		  
		  $query->andFilterWhere(['like', 'tbl_customer.first_name', $this->first_name]);
		
        $query->andFilterWhere(['like', 'id', $this->id]);
		$query->andFilterWhere(['like', 'customer_id', $this->customer_id]);
            
	   

        return $dataProvider;
    }
  
}
?>