<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventory;
use app\models\Products;
//use app\models\Job;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Stock_reports extends Inventory
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public $product1;
	 public $product_name1;
	 public $product_name;
	 public $product_model;
     public $description;
	 public $manufacturer;
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['qty','type','product','sno','product_name','product_model','description','manufacturer'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$product_id)
    {
		
		     //SUM(IF(DocType = "FP", QTY, 0)) AS FP
		     $query = Inventory::find()->innerJoinWith('product', 'Inventory::product_id = product.pid')
			 //->select('count(sno=1),product_id') 
			 ->where("product_id='$product_id'")
			 ->having('count(sno)=1')
			 ->groupBy(['sno']);
           

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
      
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

      

        $query->andFilterWhere(['like', 'my_sum', $this->my_sum]);
        $query->andFilterWhere(['like', 'product_id', $this->product_id]);
		$query->andFilterWhere(['like', 'tbl_product.product_name', $this->product_name]);
		$query->andFilterWhere(['like', 'tbl_product.product_model', $this->product_model]);
		 $query->andFilterWhere(['like', 'tbl_product.manufacturer', $this->manufacturer]);
		$query->andFilterWhere(['like', 'sno', $this->sno]);
			
	  

        return $dataProvider;
    }
}
