<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Team;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Teamsearch extends Team
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['firstname','last_name', 'password_hash','email','contact_phone', 'address','city','password_repeat','roll','gaurdian_mob'], 'safe'],
			
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Team::find()->where(['auth_key' => ''])
			;
           /*  $query = Orders::find();
             $query->joinWith(['clients']);*/
	 /* $query = Orders::find()
->innerJoinWith('clients', 'Order::client_id = clients.client_id')
;*/
	//  $query =Orders::find()->joinWith('clients', true, 'INNER JOIN')->where(['tbl_client.client_id' => $this->client_id])->all();
       // $query=Orders::find()->innerJoinWith(['clients']);
		/*$query = Orders::find()->joinWith('clients')
    ->where(['clients.client_id' => $this->client_id])
    ->all();*/
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
        // The tables are the ones our relation are configured to
        // in my case they are prefixed with "tbl_"
        'asc' => ['firstname' => SORT_ASC],
        'desc' => ['first_name' => SORT_DESC],
		// 'default' => SORT_ASC
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
			->andFilterWhere(['like', 'roll', $this->roll])
            ->andFilterWhere(['like', 'address', $this->address]);
	   

        return $dataProvider;
    }
}
