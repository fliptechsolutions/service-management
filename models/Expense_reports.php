<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;



class Expense_reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	  public $expense;
	  public $expense_name;
	
	 public static function tableName()
    {
        return 'tbl_expense';
    }
	 public function getexpense()
	{
		
		 return $this->hasOne(Expense::className(), ['id' => 'expense_id']);
		
	}
	 

    public function rules()
    {
        return [
            [['expense','description','expense_name','exp_date'], 'safe'],
			//[['expense','exp_date','amount'], 'required'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		     $query = Expense_reports::find()->innerJoinWith('expense', 'Expense_reports::expense_id = tbl_expense_type.id')
			->select('sum(amount) as amount,exp_date,expense_id')
		    ->groupBy(['expense_id']);
         //print_r($query);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		 if ( ! is_null($this->exp_date) && strpos($this->exp_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->exp_date);

             $query->andFilterWhere(['between', 'exp_date', $start_date, $end_date]);

          }
		  else
		  {
			  $query->andFilterWhere(['between', 'exp_date', date("Y-m-01"),date("Y-m-t")]);
		  }
		
        $query->andFilterWhere(['like', 'tbl_expense_type.expense', $this->expense_name]);
		$query->andFilterWhere(['like', 'description', $this->description]);
            
	   

        return $dataProvider;
    }
}
