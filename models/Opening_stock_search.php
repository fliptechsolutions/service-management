<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventory;
//use app\models\Job;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Opening_stock_search extends Inventory
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public $opening_stock;
	 public $product_name;
	 public $product;
     public $product_model;
	 public $manufacturer;
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['qty','type','customer_details','product_name','manufacturer','product_model'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		//,'complaint_id'=>9
		    
		     
		    $query = Inventory::find()->JoinWith('opening_stock', 'Inventory::stock_id = opening_stock.id')
			->JoinWith('product', 'Inventory::product_id = product.pid')
			->where(['not',['stock_id'=>NULL]])
            ->groupBy(['stock_id']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
      
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       

        $query->andFilterWhere(['like', 'stock_id', $this->stock_id]);
        $query->andFilterWhere(['like', 'product_id', $this->product_id]);
		$query->andFilterWhere(['like', 'tbl_product.product_name', $this->product_name]);
		$query->andFilterWhere(['like', 'tbl_product.product_model', $this->product_model]);
		$query->andFilterWhere(['like', 'tbl_product.manufacturer', $this->manufacturer]);
	//	$query->andFilterWhere(['like', 'tbl_customer.Address', $this->Customer_details]);
			
	  

        return $dataProvider;
    }
}
