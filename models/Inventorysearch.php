<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventory;
//use app\models\Job;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Inventorysearch extends Inventory
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public $customer_details;
	 public $product_name1;
	 public $company_name;
	 public $customer_name;
     public $contact_phone;
	 public $address;
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['qty','type','customer_details','company_name','customer_name','id','address','contact_phone'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		//,'complaint_id'=>9
		    
		     
		    $query = Inventory::find()->innerJoinWith('customer_details', 'Inventory::customer_id = customer_details.id')
			->where(['not',['customer_id'=>NULL]])
            ->groupBy(['sales_id']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
      
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       

        $query->andFilterWhere(['like', 'customer_id', $this->customer_id]);
        $query->andFilterWhere(['like', 'product_id', $this->product_id]);
		$query->andFilterWhere(['like', 'tbl_customer.company_name', $this->company_name]);
		$query->andFilterWhere(['like', 'tbl_customer.first_name', $this->customer_name]);
		$query->andFilterWhere(['like', 'tbl_customer.contact_phone', $this->contact_phone]);
		$query->andFilterWhere(['like', 'tbl_customer.Address', $this->address]);
			
	  

        return $dataProvider;
    }
}
