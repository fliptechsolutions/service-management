<?php
namespace app\models;

use yii\base\Model;
//use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
class Voucher_reports extends  \yii\db\ActiveRecord
{
	public $first_name;
	public $trans;
	public function rules()
    {
        return [
					[['id'], 'integer'],
					[['first_name','purchase_date','pay_type','vendor_id'], 'safe'],
			    ];
    }
  
	public static function tableName()
    {
        return 'tbl_purchase_transaction';
    }



    public function getvendordetails()
	{
		 return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
	}


    public function search($params)
    {
		
	    $query = Voucher_reports::find()
		->innerJoinWith('vendordetails', 'Transaction_purchase::vendor_id = vendordetails.id')
		->select('count(*) as trans,sum(amount) as amount,purchase_date,vendor_id')
		->groupBy(['vendor_id']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		if ( ! is_null($this->purchase_date) && strpos($this->purchase_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->purchase_date);

             $query->andFilterWhere(['between', 'purchase_date', $start_date, $end_date]);

          }
		   else
		  {
			  $query->andFilterWhere(['between', 'purchase_date', date("Y-m-01"),date("Y-m-t")]);
		  }
		  
		  $query->andFilterWhere(['like', 'tbl_vendor.first_name', $this->first_name]);
		
        $query->andFilterWhere(['like', 'id', $this->id]);
		//$query->andFilterWhere(['like', 'customer_id', $this->customer_id]);
            
	   

        return $dataProvider;
    }
  
}
?>