<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventory;
use app\models\Products;
//use app\models\Job;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Stocksearch extends Inventory
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 public $product1;
	 public $product_name1;
	 public $product_name;
	 public $product_model;
     public $description;
	 public $manufacturer;
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['qty','type','product','sno','product_name','product_model','description','manufacturer'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$description)
    {
		//,'complaint_id'=>9
		    
			
		  if ($description!='') 
	     {
			 //$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where product_id='".$product_id."' and qty=1")->queryAll();
              
           /*   $query = Inventory::find()->innerJoinWith('product', 'Inventory::product_id = product.pid', 'MATCH ( `tbl_product`.`product_name`, `tbl_product`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE) AS score 
			  FROM `tbl_product`')			  
			  ->where('MATCH (`tbl_product`.`product_name`, `tbl_product`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE)')
			  //->andwhere('MATCH (`tbl_product`.`product_name`, `tbl_product`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE)')   			  
              ->select(['sum(qty) as my_sum,product_id,sno, select from `tbl_product` MATCH ( `tbl_product`.`product_name`, `tbl_product`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE) where `tbl_product`.`pid`=`product_id` AS score 
			  '])
              ->groupBy(['product_id']);*/
			  
			  
			  $query = Inventory::find();
              $subQuery = Products::find()->select('pid, product_name, description, product_model, manufacturer, MATCH ( `tbl_product`.`product_name`, `tbl_product`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE) AS score 
			  ');
              $query->leftJoin(['tbl_product' => $subQuery], 'tbl_product.pid = tbl_inventory.product_id')
			  		//->where('MATCH (`T`.`product_name`, `T`.`description`) AGAINST ("'.$description.'" IN NATURAL LANGUAGE MODE)')
					->select(['sum(qty) as my_sum,product_id,sno']) 
					->where('`tbl_product`.`score` != 0')
					->groupBy(['product_id'])
					->orderBy(['`tbl_product`.`score`'=>SORT_DESC]);
					
					





          }
		  else
		  {
		     
		     $query = Inventory::find()->innerJoinWith('product', 'Inventory::product_id = product.pid')
             ->select(['sum(qty) as my_sum,product_id,sno'])
             ->groupBy(['product_id']);
		  }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		 $dataProvider->sort->attributes['clients'] = [
      
    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

      

        $query->andFilterWhere(['like', 'my_sum', $this->my_sum]);
        $query->andFilterWhere(['like', 'product_id', $this->product_id]);
		$query->andFilterWhere(['like', 'tbl_product.product_name', $this->product_name]);
		$query->andFilterWhere(['like', 'tbl_product.product_model', $this->product_model]);
		 $query->andFilterWhere(['like', 'tbl_product.manufacturer', $this->manufacturer]);
		$query->andFilterWhere(['like', 'sno', $this->sno]);
			
	  

        return $dataProvider;
    }
}
