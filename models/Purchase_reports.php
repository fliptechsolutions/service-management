<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Purchase;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Purchase_reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 public $first_name;
	
	public static function tableName()
    {
        return 'tbl_purchase';
    }

    public function rules()
    {
        return [
            
            [['vendor','purchase_date','shop1','invoice_number','id'], 'safe'],
			
			//[['receipt'], 'safe'],
        ];
    }

    public function getVendor()
	{
		
		 return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
		
	}
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Purchase_reports::find()->innerJoinWith('vendor', 'Purchase_reports::vendor_id = vendor.id')
			 ->select('vendor_id,sum(paid) as paid,sum(total) as total,purchase_date')
			->groupBy(['vendor_id']);
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
        
        $query->andFilterWhere(['like', 'tbl_vendor.first_name', $this->first_name]);
          
		  if ( ! is_null($this->purchase_date) && strpos($this->purchase_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->purchase_date);

             $query->andFilterWhere(['between', 'purchase_date', $start_date, $end_date]);

          }
		  else
		  {
			  $query->andFilterWhere(['between', 'purchase_date', date("Y-m-01"),date("Y-m-t")]);
		  }
			
			//->andFilterWhere(['like', 'purchase_date', $this->purchase_date]);
			

        return $dataProvider;
    }
}
