<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sales;


class Receipt_search extends Sales
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['sales_date','customer_id','total','paid'], 'safe'],
			
         
        ];
    }

    public function scenarios()
    {
     
        return Model::scenarios();
    }

   
    public function search($params)
    {
		
	    $query = Sales::find()->where(['status'=>1]); //->where('(`total`=`paid` or `total`>`paid`) and `paid`!=0'); //->where(['=','`tbl_purchase`.`total`',`tbl_purchase`.`paid`]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
           
            return $dataProvider;
        }
		if ( ! is_null($this->sales_date) && strpos($this->sales_date, ' - ') !== false ) 
	     {

             list($start_date, $end_date) = explode(' - ', $this->sales_date);

             $query->andFilterWhere(['between', 'sales_date', $start_date, $end_date]);

          }
		
      //  $query->andFilterWhere(['like', 'vat_name', $this->vat_name]);
		//$query->andFilterWhere(['like', 'vat', $this->vat]);
            
	   

        return $dataProvider;
    }
}
