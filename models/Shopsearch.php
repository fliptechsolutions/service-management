<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Shop;

/**
 * MerchantSearch represents the model behind the search form about `app\models\Merchant`.
 */
class Shopsearch extends Shop
{
    /**
     * @inheritdoc
     */
	//  public $Client;
	
	 

    public function rules()
    {
        return [
            [['shop'], 'safe'],
			
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		     $query = Shop::find()
			;
           /*  $query = Orders::find();
             $query->joinWith(['clients']);*/
	 /* $query = Orders::find()
->innerJoinWith('clients', 'Order::client_id = clients.client_id')
;*/
	//  $query =Orders::find()->joinWith('clients', true, 'INNER JOIN')->where(['tbl_client.client_id' => $this->client_id])->all();
       // $query=Orders::find()->innerJoinWith(['clients']);
		/*$query = Orders::find()->joinWith('clients')
    ->where(['clients.client_id' => $this->client_id])
    ->all();*/
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       
        $query->andFilterWhere(['like', 'shop', $this->shop]);
            
	   

        return $dataProvider;
    }
}
