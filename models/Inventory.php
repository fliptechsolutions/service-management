<?php

namespace app\models;

use Yii;

class Inventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	  public $my_sum;
    public static function tableName()
    {
        return 'tbl_inventory';
    }
	
	 public function getproduct()
	{
		
		 return $this->hasOne(Products::className(), ['pid' => 'product_id']);
		
	}
	
	public function getCustomer_details()
	{
		
		 return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
		
	}
	
	public function getopening_stock()
	{
		
		 return $this->hasOne(Opening_stock::className(), ['id' => 'stock_id']);
		
	}

	
	
    
}
