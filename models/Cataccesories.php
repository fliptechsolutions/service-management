<?php

namespace app\models;

use Yii;


class Cataccesories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cat_accessories';
    }
	
	
    public function rules()
    {
        return [
            [['cat_name'], 'required'],
			
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            	'cat_name' => 'Accessories',            
              ];
    }
}
