<?php

namespace app\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\models\Products;
use app\models\Producttype;
use app\models\Manufacture;
 
class Functions extends Component
{
	 public  function UpdateProductType($type_id)
	 {
		 $Products1 = Products::find()->where(['type_id' =>$type_id ])->all(); 
		 foreach($Products1 as $product)
		 {
			 $Products = Products::find()->where(['pid' =>$product['pid'] ])->one(); 
		 $product_type=Producttype::find()->where(['id'=>$Products->type_id])->one(); 
		 $manufacture_name=Manufacture::find()->where(['id'=>$Products->manufacture_id])->one();
		                    $Products->product_name=$manufacture_name->manufacture_name.' '.$product_type->product_type.' '.$Products->product_model;
							$Products->product_model=$Products->product_model;
							$Products->manufacturer=$manufacture_name->manufacture_name;
							$Products->type=$product_type->product_type;
							$Products->type_id=$Products->type_id;
							$Products->manufacture_id=$Products->manufacture_id;
							$Products->save();
		 }
		
	 }
	 
	 public  function UpdateManufact($manufacture_id)
	 {
		 $Products1 = Products::find()->where(['manufacture_id' =>$manufacture_id ])->all(); 
		 foreach($Products1 as $product)
		 {
			 $Products = Products::find()->where(['pid' =>$product['pid'] ])->one(); 
		 $product_type=Producttype::find()->where(['id'=>$Products->type_id])->one(); 
		 $manufacture_name=Manufacture::find()->where(['id'=>$Products->manufacture_id])->one();
		                    $Products->product_name=$manufacture_name->manufacture_name.' '.$product_type->product_type.' '.$Products->product_model;
							$Products->product_model=$Products->product_model;
							$Products->manufacturer=$manufacture_name->manufacture_name;
							$Products->type=$product_type->product_type;
							$Products->type_id=$Products->type_id;
							$Products->manufacture_id=$Products->manufacture_id;
							$Products->save();
		 }
		
	 }
	
	public function createUrl($manager, $route, $params)
    {
        if ($route === 'car/index') {
            if (isset($params['manufacturer'], $params['model'])) {
                return $params['manufacturer'] . '/' . $params['model'];
            } elseif (isset($params['manufacturer'])) {
                return $params['manufacturer'];
            }
        }
        return false; // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database.
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
        }
        return false; // this rule does not apply
    }
	
 public function welcome()
 {
  echo "<h1>Welcome ".ucfirst(Yii::$app->user->identity->username).'<h1>';
 }
 
 public  function menu() 
	 {
		 $base =Yii::$app->request->baseUrl;
	           $array = array(
              /*  ['label'=>'Logout', 'icon' => 'fa fa-user','url'=>'/user-management/auth/logout','active' => 'location,addlocation'],
                ['label'=>'Registration', 'icon' => 'fa fa-user','url'=>'/admin/register','active' => 'register'],
                ['label'=>'Change own password','icon' => 'fa fa-user', 'url'=>'/user-management/auth/change-own-password','active' => 'location,addlocation'],
                ['label'=>'Password recovery', 'icon' => 'fa fa-user','url'=>'/user-management/auth/password-recovery','active' => 'location,addlocation'],
                ['label'=>'E-mail confirmation', 'icon' => 'fa fa-user','url'=>'/user-management/auth/confirm-email','active' => 'location,addlocation'],*/
				['label'=>'Front Office',  'icon' => 'fa fa-user','url'=>'/admin/front','active' => 'location,addlocation'],
				['label'=>'Checking Team',  'icon' => 'fa fa-user','url'=>'/admin/checking','active' => 'location,addlocation'],
				['label'=>'Chiplevel Engineer',  'icon' => 'fa fa-user','url'=>'/admin/chip','active' => 'location,addlocation'],
				['label'=>'BGA Dept',  'icon' => 'fa fa-user','url'=>'/admin/bga','active' => 'location,addlocation'],
				['label'=>'Cardlevel Dept',  'icon' => 'fa fa-user','url'=>'/admin/card','active' => 'location,addlocation'],
				['label'=>'Complaint',  'icon' => 'fa fa-user','url'=>'/complaint/index','active' => 'location,addlocation'],
               // ['label'=>'Change own password', 'url'=>'/user-management/auth/change-own-password','active' => 'location,addlocation'],
			   
			                 /* ['label' => 'Role','icon' => 'fa fa-user', 'url' =>$base.'/admin','active' => ['location,addlocation']],
					          ['label' => 'example','icon' => 'fa fa-user', 'url' =>$base.'/admin','active' => ['trainers,addtrainer,attendance,add_attendance']],*/
							  );
					
				
		 return  $array;
	 }
	 
	 public  function Get_status()
	 {
		 
			 $res=array('0'=>'pending','1'=>'Completed','2'=>'Rejected'); 
		 
		 return  $res;
	 }
	 
	 public function Rupees_words($num)
	 {
		           $points='';
				   $number = $num;
				   $no = round($number);
				   $point = round($number - $no, 2) * 100;
				   $hundred = null;
				   $digits_1 = strlen($no);
				   $i = 0;
				   //echo $points;
				   $str = array();
				   $words = array('0' => '', '1' => 'one', '2' => 'two',
					'3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
					'7' => 'seven', '8' => 'eight', '9' => 'nine',
					'10' => 'ten', '11' => 'eleven', '12' => 'twelve',
					'13' => 'thirteen', '14' => 'fourteen',
					'15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
					'18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
					'30' => 'thirty', '40' => 'forty', '50' => 'fifty',
					'60' => 'sixty', '70' => 'seventy',
					'80' => 'eighty', '90' => 'ninety');
				   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
				    
				   while ($i < $digits_1) {
					 $divider = ($i == 2) ? 10 : 100;
					 $number = floor($no % $divider);
					 $no = floor($no / $divider);
					 $i += ($divider == 10) ? 1 : 2;
					 if ($number) {
						$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
						$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
						$str [] = ($number < 21) ? $words[$number] .
							" " . $digits[$counter] . $plural . " " . $hundred
							:
							$words[floor($number / 10) * 10]
							. " " . $words[$number % 10] . " "
							. $digits[$counter] . $plural . " " . $hundred;
					 } else $str[] = null;
				  }
				  $str = array_reverse($str);
				  $result = implode('', $str);
				 
				  /*$points = ($point) ?
					"." . $words[$point / 10] . " " . 
						  $words[$point = $point % 10] : '';*/
						  if($points!='')
						  {
							$points = $points . " Paise";
						  }
				  return ucfirst($result) . "Rupees  " . $points . " only"; 
	 }
	 
	 public function actionCheck_product_available($product_id)
    {
				
       
		
		$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where product_id='".$product_id."' and qty=1")->queryAll();
	$sales_product=array();
	$purchase_product=array();
	if(!empty($product_all))
	{				
					foreach($product_all as $pro)
					{
						$purchase_product[]=$pro['sno'];
					}
		$product_all1 = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where product_id='".$product_id."' and qty=-1")->queryAll();
		            foreach($product_all1 as $pro)
					{
						$sales_product[]=$pro['sno'];
					}
					
		
		$result = array_diff($purchase_product, $sales_product);
		$keys=array_keys($result);
		//print_r($result);
		//exit();
		if($result)
		{
			
			          $response =$result  ;
									
									
		}
		else
		{
			       $response ='Items unavailable';
		}
		              
	}
	else
	{
		             $response ='Items unavailable';
		//$respose='Items unavailable';
	}
	
	//print_r($_SESSION['selected_sno']);
		return $result;
	}
	 
	 public  function Get_serial_no($group_id)
	 {
		 $s_no='';
			$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where group_id='".$group_id."'")->queryAll();
					
					foreach($product_all as $pro)
					{
						$s_no.=$pro['sno'].',';
						
					}
		 
		 return  rtrim($s_no,',');
	 }
	 
	  public  function Get_sno_sold($s_no)
	 {
		
			$product= Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where sno='".$s_no."' and type='sales'")->queryOne();
					
				if(isset($product['sno']))
				{
					return  $product['sno'];
				}
		 
		 return  false;
	 }
	 
	  public  function Get_serial_number_stock($group_id)
	 {
		 $s_no='';
			$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where group_stock_id='".$group_id."'")->queryAll();
					
					foreach($product_all as $pro)
					{
						$s_no.=$pro['sno'].',';
						
					}
		 
		 return  rtrim($s_no,',');
	 }
	 
	 public  function Checked_accessoried()
	 {
		    $warning_menu='';
			$product_all = Yii::$app->db->createCommand("SELECT id FROM tbl_accessories Where warning_menu=1")->queryAll();
					
		            foreach($product_all as $pro)
					{
						$warning_menu[]=$pro['id'];
						
					}
		 return  $warning_menu;
	 }
	 
	 public  function Get_attendance($user_id)
	 {
		            $date=date('Y-m-d');
		   		    $attendance = Yii::$app->db->createCommand("SELECT * FROM tbl_attendance Where user_id=$user_id and created_date='$date'")->queryOne();//getRawSql();
					//return $attendance;
					if(isset($attendance['attendance']))
					{
						return  $attendance['attendance'];
					}
		            return '0';		 
	 }
	
	 
	 
	  public function UpdateSettings($option_name,$option_value)
	 {
		 
            
			$connection = Yii::$app->db;
			$stmt="SELECT * FROM
				tbl_settings
				WHERE
				option_name='".addslashes($option_name)."'		
							";
		   $rows=$connection->createCommand($stmt)->queryAll(); 
		   $params=array(
		'option_name'=> addslashes($option_name),
		'option_value'=> addslashes($option_value)
		);
		$service_id=Yii::$app->user->identity->id;
		if ( !empty($service_id)){
			$params['service_id']=$service_id;
		}
					
		if (is_array($rows) && count($rows)>=1)
		{
			
				$res = $connection->createCommand()->update('tbl_settings' , $params , 
										 'option_name=:option_name and service_id=:service_id' ,
										 array(
										  ':option_name'=> addslashes($option_name),
										  ':service_id'=>$service_id
										  )
										 )->execute();
					
		   } 
		     else
		    {			
				$connection->createCommand()->insert('tbl_settings',$params)->execute();
				//echo "kk";
			}
           
			
            
        } 
    
	
	 public function Get_settings($option_name)
 {
	 
	          $connection = Yii::$app->db;
			  //$service_id= Yii::$app->user->identity->id;
			
			  $params = [':option_name'=> addslashes($option_name)];
			  			
				 $post = Yii::$app->db->createCommand('SELECT * FROM tbl_settings WHERE option_name=:option_name', $params)->queryOne();
				   if (is_array($post) && count($post)>=1){
			return stripslashes($post['option_value']);
		}
		return '';
 }
	
	 
	 
	 public function job_assign1()
	 {
		                $cid= $_POST['cid'];
		                $notes= $_POST['notes'];
		                $role= $_POST['role'];
						$job_id= $_POST['job_id'];
						if($role=='checking')
						{
							$where_id='checking';
					    }
						elseif($role=='Chip') 
						{
							$where_id='Chip';
						}
						elseif($role=='BGA') 
						{
							$where_id='BGA';
						}
						elseif($role=='Front') 
						{
							$where_id='Front';
						}
						elseif($role=='Card') 
						{
							$where_id='Card';
						}
						
						$Users = Yii::$app->db->createCommand("SELECT * FROM user Where roll='".$where_id."'")->queryAll();
						if(empty($Users))
						{
							return 'unavailable '.$where_id.' users';
						}
						$params=array('status'=>'completed');
		               $res=Yii::$app->db->createCommand()->update('tbl_job_assign',$params,'id="'.$job_id.'"')->execute();
						 
		 	            $from_id=Yii::$app->user->identity->id;
						$Results = Yii::$app->db->createCommand("SELECT to_id,COUNT(*) as tot FROM tbl_job_assign WHERE to_dpt='$role' and status='pending' GROUP BY to_id")->queryAll();
						$Results_all = Yii::$app->db->createCommand("SELECT * FROM tbl_job_assign Where to_dpt='$role'")->queryAll();
						
						$checking_ids='';
						$assigned_ids='';
					    $assigned_user_ids=array();
							foreach($Users as $res)
							{
								$checking_ids[]=$res['id'];
							}
							
							foreach($Results_all as $res)
								{
									$assigned_ids[]=$res['to_id'];
								}
								//print_r($assigned_ids);
								if(!empty($assigned_ids))
								{
								  $assigned_user_ids=array_unique($assigned_ids);
								}
								
								$diff=array_diff($checking_ids,$assigned_user_ids);
								//print_r($diff);
								//exit();
								
							
							if(!empty($diff))
							{
								    foreach($diff as $res)
									{
										$to[]=$res;
									}
								    $to_id=$to[0];
							}
							else
							{
								
									foreach($Results as $res)
									{
										  $total[$res['to_id']]=$res['tot'];
									}
									
									 asort($total);
									foreach($total as $key=>$value)
									   {
									     $less_job[]=$key;
									   }
									  $to_id=$less_job[0];
								
							}
							
						
						
						$params=array('complaint_id'=>$cid,
									  'from_id'=>$from_id,
									  'to_id'=>$to_id,
									  'details'=>$notes,
									  'from_dpt'=>Yii::$app->mycomponent->Get_Roll($from_id),
									  'to_dpt'=>Yii::$app->mycomponent->Get_Roll($to_id),
									  'date_created'=>date('Y-m-d H:i:s'));
		                 Yii::$app->db->createCommand()->insert('tbl_job_assign',$params)->execute();
		 
		 
		return 'updated ';
	
	 }
	 
	 
	  public function job_assign()
	 {
		                $cid= $_POST['cid'];
		                $notes= $_POST['notes'];
		                $role= $_POST['role'];
						$job_id= $_POST['job_id'];
						$team_member= $_POST['team_member'];
						
						
						
									$Users = Yii::$app->db->createCommand("SELECT * FROM auth_assignment Where item_name='".$role."'")->queryAll();
									if(empty($Users))
									{
										return 'unavailable '.$role.' users';
									}
									$params=array('status'=>'completed');
									if($job_id!=0)
									{
									  $res=Yii::$app->db->createCommand()->update('tbl_job_assign',$params,'id="'.$job_id.'"')->execute();
									}
									$from_id=Yii::$app->user->identity->id;
						if($team_member!='')
						{
							$to_id=$team_member;
						}
						else
						{
							
							    
						
								$Results = Yii::$app->db->createCommand("SELECT to_id,COUNT(*) as tot FROM tbl_job_assign WHERE to_dpt='$role' and status='pending' GROUP BY to_id")->queryAll();
								$Results_all = Yii::$app->db->createCommand("SELECT * FROM tbl_job_assign Where to_dpt='$role'")->queryAll();
								
								$checking_ids='';
								$assigned_ids='';
								$assigned_user_ids=array();
									foreach($Users as $res)
									{
										$checking_ids[]=$res['user_id'];
									}
									
									foreach($Results_all as $res)
										{
											$assigned_ids[]=$res['to_id'];
										}
										//print_r($assigned_ids);
										if(!empty($assigned_ids))
										{
										  $assigned_user_ids=array_unique($assigned_ids);
										}
										
										$diff=array_diff($checking_ids,$assigned_user_ids);
										//print_r($diff);
										//exit();
										
							
										if(!empty($diff))
										{
												foreach($diff as $res)
												{
													$to[]=$res;
												}
												$to_id=$to[0];
										}
										else
										{
											
												foreach($Results as $res)
												{
													  $total[$res['to_id']]=$res['tot'];
												}
												
												 asort($total);
												foreach($total as $key=>$value)
												   {
													 $less_job[]=$key;
												   }
												  $to_id=$less_job[0];
											
										}
							
						}
							
						
						
						$params=array('complaint_id'=>$cid,
									  'from_id'=>$from_id,
									  'to_id'=>$to_id,
									  'details'=>$notes,
									  'from_dpt'=>Yii::$app->mycomponent->Get_Roll($from_id),
									  'to_dpt'=>Yii::$app->mycomponent->Get_Roll($to_id),
									  'date_created'=>date('Y-m-d H:i:s'));
		                 Yii::$app->db->createCommand()->insert('tbl_job_assign',$params)->execute();
		 
		 
		return 'updated ';
	
	 }
	 
	 
	 public function GetRolls()
	 {
		 $allRoles = Role::find()
			->asArray()
			//->andWhere('name != :current_name', [':current_name'=>$id])
			->all();
			//print_r(count($allRoles));
			foreach($allRoles as $roll)
			{
				if($roll['name']!='Admin')
				{
				   $Rolls[]=['item_name'=>$roll['name']];
				}
			}
		
		 //$Results = Yii::$app->db->createCommand("SELECT item_name FROM auth_assignment Where item_name!='admin' GROUP BY item_name")->queryAll();
		 return $Rolls ;
	
	 }
	 
	 public function Delete($table_name,$where)
	 {
		Yii::$app->db->createCommand()->delete($table_name, $where)->execute(); 
	 }
	 
	 
	 public function GetUser_list($tbl_name)
	 {
		
		 $Results = Yii::$app->db->createCommand('SELECT * FROM '.$tbl_name.'')->queryAll();
		 return $Results ;
	
	 }
	 public function Get_user_details($where,$table_name)
	 {
		         $post='';
		         $post = Yii::$app->db->createCommand('SELECT * FROM '.$table_name.' WHERE '.$where.'')->queryOne();
			     if($post)
			     {
				    return $post ;
				 }
				 else
				 {
					return false; 
				 }
	
	 }
	 
	 
	 public function Usercreate()
	 {
		 $session = Yii::$app->session;
		$cost = 13;
		$connection = Yii::$app->db;
		$first_name = Yii::$app->request->post('first_name');
		$last_name = Yii::$app->request->post('last_name');
		$address = Yii::$app->request->post('address');
		$contact_phone = Yii::$app->request->post('contact_phone');
		$contact_email = Yii::$app->request->post('contact_email');
		$city = Yii::$app->request->post('city');
		//$username = Yii::$app->request->post('username');
		$password = Yii::$app->request->post('password');
		$code = Yii::$app->request->post('code');
		$tbl_name = Yii::$app->request->post('tbl_name');
		echo $where = Yii::$app->request->post('where');
		
		$ID = Yii::$app->request->post('id');
		if($password)
		{
			$pass=password_hash($password, PASSWORD_DEFAULT,['cost' => $cost]);
		}
		else
		{
			$pass=Yii::$app->request->post('pass');
		}
		if($session->get('cat_img')!='')
		{
			$photo =  $session->get('cat_img');
		}
		else
		{
			$photo =  Yii::$app->request->post('photo');
		}
		$params=[ 'first_name' => $first_name,
				 'last_name' => $last_name,
				'address' => $address,
				'contact_phone' => $contact_phone,
				'email_address' => $contact_email,
				'city' => $city,
				'username' => $contact_email,
				'password' => $pass];
				
				if($ID=='')
			{
			
					$connection->createCommand()->insert($tbl_name,$params)->execute();
					$last_id = Yii::$app->db->getLastInsertID();			
					$connection->createCommand()->insert('user',[
					'username' => $contact_email,
					'firstname' => $first_name,
					'email' => $contact_email,
					'password_hash' => $pass,$where =>$last_id])->execute();
					$insert_id = Yii::$app->db->getLastInsertID();
					$connection->createCommand()->insert('auth_assignment',[
					'item_name' => $code,
					'user_id' => $insert_id,])->execute();
			}
			else
			{
				 $where_con=$where.'='.$ID;
				 //echo $where_con;
				  $res=$connection->createCommand()->update($tbl_name,$params,$where_con)->execute();
				//print_r($res);
					
					$connection->createCommand()->update('user',[
					'username' => $contact_email,
					'email' => $contact_email,
					'password_hash' => $pass],$where_con)->execute();
			}
				
				
	 }
	 
	  public function Complaint_create()
	 {
		 $session = Yii::$app->session;
		$cost = 13;
		$products='';
		$laptop_details='';
		 $phycical='';
		 $problem='';
		 $prsence='';
		  $acc_sno='';
		   $warranty='';
		   $complaint_id='';
		$connection = Yii::$app->db;
		$first_name = Yii::$app->request->post('first_name');
		$last_name = Yii::$app->request->post('last_name');
		$address = Yii::$app->request->post('address');
		$contact_phone = Yii::$app->request->post('contact_phone');
		$contact_email = Yii::$app->request->post('contact_email');
		$city = Yii::$app->request->post('city');
		$company_name = Yii::$app->request->post('company_name');
		$tin_no = Yii::$app->request->post('tin_no');
		$office_no = Yii::$app->request->post('office_no');
		$tbl_name = Yii::$app->request->post('tbl_name');
	    $where = Yii::$app->request->post('where');
		$access_cat = Yii::$app->request->post('access_cat');
		$priority = Yii::$app->request->post('priority');
		//$product_name = Yii::$app->request->post('product_name');
	   //	$product_model = Yii::$app->request->post('product_model');
	     $laptop_details =['lap_sno'=> Yii::$app->request->post('lap_sno'),
		                   'lap_user'=> Yii::$app->request->post('lap_user'),
						   'lap_model'=> Yii::$app->request->post('lap_model'),
						   'lap_pass'=> Yii::$app->request->post('lap_pass'),];
			 $warranty = Yii::$app->request->post('warranty');
		     $prsence = Yii::$app->request->post('prsence');
			 $acc_sno = Yii::$app->request->post('acc_sno');
			 $acc_remarks = Yii::$app->request->post('acc_remarks');
			 $problem = Yii::$app->request->post('problem');
			 $phycical = Yii::$app->request->post('phycical');
			 $physical_type = Yii::$app->request->post('phycical_type');
		                   if($warranty)
						   {
							   $warranty =json_encode($warranty,true);
						   }
		 
		                    if($laptop_details)
						   {
							   $laptop_details =json_encode($laptop_details,true);
						   }
						   if($phycical)
						   {
							   $phycical =json_encode($phycical,true);
						   }
						   if($problem)
						   {
							   $problem =json_encode($problem,true);
						   }
						    if($acc_sno)
						   {
							   $acc_sno =json_encode($acc_sno,true);
						   }
						     if($acc_remarks)
						   {
							   $acc_remarks =json_encode($acc_remarks,true);
						   }
						   if($prsence)
						   {
							   $prsence =json_encode($prsence,true);
						   }
						  // print_r($prsence);
		
		//$old_no=Yii::$app->request->post('contact_phone_old');
		
		/*for($i=0;$i<count($product_name);$i++)
		{
			$pm='';
			if(isset($product_model[$i]))
			{
				$pm=$product_model[$i];
			}
			$pblm='';
			if(isset($problem[$i]))
			{
				$pblm=$problem[$i];
			}
			
			$products[]=['product_name'=>$product_name[$i],
			             'product_model'=>$pm,
						 'problem'=>$pblm,];
		}*/
		//print_r(json_encode($products));
		
		$ID = Yii::$app->request->post('id');
		
		$all_mob='';
		$params=[ //'first_name' => $first_name,
				// 'last_name' => $last_name,
				 //'Address' => $address,
			 	 //'contact_phone' => $contact_phone,
				// 'email_address' => $contact_email,
				 'prsence' => $prsence,
				// 'city' => $city,
				'physical_type'=>$physical_type,
				 'acc_sno' => $acc_sno,
				 'acc_remarks' =>$acc_remarks,
				 'problem' => $problem,
				 'phycical' =>$phycical,
				 'warranty' =>$warranty,
				 'access_cat' =>$access_cat,
				 'priority' => $priority,
				  'laptop_details' =>$laptop_details,
				 'date_created'=>date('Y-m-d H:i:s')
				];
				$all_mob=Yii::$app->mycomponent->customer_all_mob();
				$params1=[ 'first_name' => $first_name,
									 'last_name' => $last_name,
									 'Address' => $address,
									 'contact_phone' => $contact_phone,
									 'email_address' => $contact_email,
									 'city' => $city,									 
									 'company_name' => $company_name,
									 'tin_no' => $tin_no,
									 'office_no' => $office_no,
									  'date_created'=>date('Y-m-d H:i:s')
									];
				//print_r($all_mob);
				//exit();
				if(!empty($all_mob))
				{
						if(!in_array($contact_phone,$all_mob))
						{
							
					        $connection->createCommand()->insert('tbl_customer',$params1)->execute();
							$customer_id = Yii::$app->db->getLastInsertID();
							
							
						}
						else
						{
							$res=$connection->createCommand()->update('tbl_customer',$params1,"contact_phone='".$contact_phone."'")->execute();
							$cust = Yii::$app->db->createCommand("SELECT * FROM tbl_customer Where contact_phone='".$contact_phone."'")->queryOne();
							$customer_id = $cust['id'];
						}
				}
				else
				{
					        $connection->createCommand()->insert('tbl_customer',$params1)->execute();
							$customer_id = Yii::$app->db->getLastInsertID();
				}
				$params['customer_id']=$customer_id;
				
				if($ID=='')
				{
					     
				        
						$connection->createCommand()->insert($tbl_name,$params)->execute();
						$insert_id = Yii::$app->db->getLastInsertID();
						$complaint_id=$insert_id;
						$from_id=Yii::$app->user->identity->id;
						$Results = Yii::$app->db->createCommand("SELECT to_id,COUNT(*) as tot FROM tbl_job_assign WHERE to_dpt='checking' and status='pending' GROUP BY to_id")->queryAll();
						$Results_all = Yii::$app->db->createCommand("SELECT * FROM tbl_job_assign Where to_dpt='checking'")->queryAll();
						$Users = Yii::$app->db->createCommand("SELECT * FROM user Where roll='checking'")->queryAll();
						$checking_ids='';
						$assigned_ids='';
							$assigned_user_ids=array();
							foreach($Users as $res)
							{
								$checking_ids[]=$res['id'];
							}
							
							foreach($Results_all as $res)
								{
									$assigned_ids[]=$res['to_id'];
								}
								//print_r($assigned_ids);
								if(!empty($assigned_ids))
								{
								  $assigned_user_ids=array_unique($assigned_ids);
								}
								
								$diff=array_diff($checking_ids,$assigned_user_ids);
								//print_r($diff);
								//exit();
								
							
							if(!empty($diff))
							{
								    foreach($diff as $res)
									{
										$to[]=$res;
									}
								    $to_id=$to[0];
							}
							else
							{
								
									foreach($Results as $res)
									{
										  $total[$res['to_id']]=$res['tot'];
									}
									
									 asort($total);
									foreach($total as $key=>$value)
									   {
									     $less_job[]=$key;
									   }
									  $to_id=$less_job[0];
								
							}
							
						
						
						$params=array('complaint_id'=>$insert_id,
									  'from_id'=>$from_id,
									  'to_id'=>$to_id,
									  'details'=>'test',
									  'from_dpt'=>Yii::$app->mycomponent->Get_Roll($from_id),
									  'to_dpt'=>Yii::$app->mycomponent->Get_Roll($to_id),
									  'date_created'=>date('Y-m-d H:i:s'));
		                 Yii::$app->db->createCommand()->insert('tbl_job_assign',$params)->execute();
						
						
				}
				else
				{
					 $where_con=$where.'='.$ID;
					 
					// $where_con1= 'contact_phone='.$old_no;
					 //echo $where_con;
					  $res=$connection->createCommand()->update($tbl_name,$params,$where_con)->execute();
					 
					 // $res1=$connection->createCommand()->update('tbl_customer',$params1,$where_con1)->execute();
					//print_r($res);
						
						
				}
				return $complaint_id;
				
	 }
	 
	 public function Get_Roll($user_id)
	 {
		
		 $Results = Yii::$app->db->createCommand("SELECT item_name FROM auth_assignment Where user_id='$user_id'")->queryOne();
		 return $Results['item_name'] ;
	
	 }
	  public function Get_user($user_id)
	 {
		
		 $Results = Yii::$app->db->createCommand("SELECT * FROM user Where id='$user_id'")->queryOne();
		 return $Results;
	
	 }
	 public function Mob_validation($mob)
	 {
		
		 $Results = Yii::$app->db->createCommand("SELECT * FROM tbl_customer Where contact_phone='$mob'")->queryOne();
		 if($Results)
		 {
		    return $Results ;
		 }
	     return false;
	 }
	 
	 public function customer_all_mob()
	 {
		
		 $Results = Yii::$app->db->createCommand("SELECT * FROM tbl_customer")->queryAll();
		 if($Results)
		 {
			 foreach($Results as $res)
			 {
				 $result[]=$res['contact_phone'];
			   
			 }
		    return $result;
		 }
	     return false;
	 }
	 
	 
 
}