<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Expense_reports;
use app\models\Vat;
use app\models\Vat_search;
use app\models\Expense_details;
use app\models\Expense;
use webvimark\modules\UserManagement\models\rbacDB\Role;
class ExpenseController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

	
	 public function actionAdd_expense()
    {
        $model = new Expense_details();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->expense_id = $_POST['expense'];
			$model->exp_date = $_POST['exp_date'];
			$model->amount = $_POST['Expense_details']['amount'];
			$model->description = $_POST['Expense_details']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('expense/expense'));
				 
			}

            
        } else {
            
			return $this->render('add_expense', ['model' => $model]);
        }
    }
	public function actionUpdate_expense()
    {
        $Expense_id = $_GET['id'];
		$model = Expense_details::findOne(['id' =>$Expense_id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->expense_id = $_POST['expense'];
			$model->exp_date = $_POST['exp_date'];
			$model->amount = $_POST['Expense_details']['amount'];
			$model->description = $_POST['Expense_details']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('expense/expense'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('add_expense', ['model' => $model]);
        }
    }
	
	public function actionExpense()
    {
		$searchModel = new Expense_details();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('expense', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionExpensereports()
    {
		$searchModel = new Expense_reports();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		
        return $this->render('expense_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionDelete_expense()
    {
		$Expense_id = $_GET['id'];
		$model = Expense_details::DeleteAll(['id' =>$Expense_id ]);
		return Yii::$app->response->redirect(array('expense/expense'));
      
    }
   
}
