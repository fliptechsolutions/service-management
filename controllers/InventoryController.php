<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Purchase;
use app\models\Vendor;
use app\models\Shop;
use app\models\Selling;
use app\models\Sellingsearch;
use app\models\Shopsearch;
use app\models\Purchasesearch;
use app\models\Products;
use app\models\Productsearch;
use app\models\Inventory;
use app\models\Cataccesories;
use app\models\Cataccesoriessearch;
use app\models\Accesories;
use app\models\Accesoriessearch;
use app\models\Customer;
use app\models\Inventorysearch;
use app\models\Grouppurchase;
use app\models\Producttype;
use app\models\Protypesearch;
use app\models\Manufacture;
use app\models\Manufacturesearch;
use app\models\Sales;
use app\models\Group_sales;
use app\models\Group_stock;
use app\models\Opening_stock;
use app\models\Opening_stock_search;
use app\models\Voucher_search;
use app\models\Receipt_search;
use app\models\Voucher;
use app\models\Receipt;
use app\models\Sales_search;
use app\models\Transaction_sales;
use app\models\Transaction_purchase;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Sales_reports;
use app\models\Purchase_reports;
use app\models\Receipt_reports;
use app\models\Voucher_reports;
use app\models\OpeningBalanceCustomer;
use app\models\OpeningBalanceSupplier;
class InventoryController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
	 
	  public function actionAllvendors()
    {
		//$name=$_GET['q'];
		//$query = "SELECT * FROM `tbl_vendor` where `first_name` LIKE '$name%' ";
       // $result = ArrayHelper::map(Vendor::findBySql($query)->all(), 'id', 'first_name');
		$result = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		//print_r(json_encode($result,true));
		 echo Json::encode($result);
	}
	
	 public function actionAdd_stock()
    {
        $model = new Opening_stock();
		$items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
        if (Yii::$app->request->post()) {
			//exit();
            $main_total=$_POST['total'];
			$model->stock_date = date('Y-m-d');
			$model->total =array_sum($main_total);
			
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$selling_price=$_POST['selling_price'];
			$total=$_POST['subtotal'];
		    
			$sno=$_POST['s_no'];
			//print_r($product_id);
			//exit();
			if ($model->save(false))
			{
			    $last_id=$model->id;
				for($j=0;$j<count($product_id);$j++)
				{
					$total_records=explode(',',$sno[$j]);
					$Group_stock = new Group_stock();
					$Group_stock->stock_id=$last_id;
					$Group_stock->total=$main_total[$j];
					$Group_stock->qty=$quantity[$j];
					$Group_stock->save();
					$last_group_id=$Group_stock->group_id;
					$product_ids=$product_id[$j];
					$new_price=$price[$j];
					$sell_price=$selling_price[$j];
					
						  for($i=0;$i<count($total_records);$i++)
						  {
							$Inventory = new Inventory();
							$Inventory->stock_id =$last_id;
							$Inventory->group_stock_id =$last_group_id;
							$Inventory->product_id =$product_ids;
							$Inventory->price =$new_price;
							$Inventory->selling_price =$sell_price;
							$Inventory->qty =1;
							$Inventory->total =$new_price;
							$Inventory->sno =$total_records[$i];
							//$Inventory->shop =$model->shop;
							$Inventory->type ='opening_stock';
							$Inventory->save();
						  }
					//$Products->isNewRecord = true;
				}
                  return Yii::$app->response->redirect(array('inventory/opening_stock'));
				
			}
			
            
        } else {
            
			return $this->render('add_opening_stock', ['model' => $model,'items'=>$items,'items1'=>$items1,'product_details'=>$product_details]);
        }
    }
	
	
	public function actionOpening_stock()
    {
		$searchModel = new Opening_stock_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('opening_stock', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
		
	public function actionUpdate_stock()
    {
        
        $stock_id = $_GET['id'];
		$model = Opening_stock::findOne(['id' =>$stock_id ]);
		
        $items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
        if (Yii::$app->request->post())  {
            $main_total=$_POST['total'];
			$model->stock_date = date('Y-m-d');
			$model->total =array_sum($main_total);
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$selling_price=$_POST['selling_price'];
			$total=$_POST['subtotal'];
			
			
			$sno=$_POST['s_no'];
			
			
			if ($model->save(false))
			{
				    $last_id=$model->id;
				   //	$Delete_product = Products::find()->where(['=', 'product_id', $parchase_id])->all();
					Inventory::deleteAll(['stock_id' =>$stock_id]);
					Group_stock::deleteAll(['stock_id' =>$stock_id]);
					//$Delete_product->delete();
			   
				$last_id=$model->id;
				for($j=0;$j<count($product_id);$j++)
				{
					$total_records=explode(',',$sno[$j]);
					$Group_stock = new Group_stock();
					$Group_stock->stock_id=$last_id;
					$Group_stock->total=$main_total[$j];
					$Group_stock->qty=$quantity[$j];
					$Group_stock->save();
					$last_group_id=$Group_stock->group_id;
					$product_ids=$product_id[$j];
					$new_price=$price[$j];
					$sell_price=$selling_price[$j];
					
						  for($i=0;$i<count($total_records);$i++)
						  {
							$Inventory = new Inventory();
							$Inventory->stock_id =$last_id;
							$Inventory->group_stock_id =$last_group_id;
							$Inventory->product_id =$product_ids;
							$Inventory->price =$new_price;
							$Inventory->selling_price =$sell_price;
							$Inventory->qty =1;
							$Inventory->total =$new_price;
							$Inventory->sno =$total_records[$i];
						//	$Inventory->shop =$model->shop;
							$Inventory->type ='opening_stock';
							$Inventory->save();
						  }
					//$Products->isNewRecord = true;
				}
				return Yii::$app->response->redirect(array('inventory/opening_stock'));
				
			}
			
            
        } else {
            
			return $this->render('add_opening_stock', ['model' => $model,'items'=>$items,'items1'=>$items1,'product_details'=>$product_details]);
        }
    
    }
	
	public function actionDeletestock()
    {
	    $sold='';
		$stock_id = $_GET['id'];
        $model = Inventory::findAll(['stock_id' =>$stock_id ]);
		foreach($model as $sno)
		{
			$s_no='';
			$s_no=Yii::$app->mycomponent->Get_sno_sold($sno['sno']);
			if($s_no!='')
			{
				$sold=1;
			}
		}
		echo  $sold;
		$_SESSION['warning_product']=1;
		if($sold=='')
		{
			     $_SESSION['warning_product']=2;
		         Opening_stock::deleteAll(['id' =>$stock_id]);
		         Group_stock::deleteAll(['stock_id' =>$stock_id]);
				 Inventory::deleteAll(['stock_id' =>$stock_id]);
				 
		}
         
       return Yii::$app->response->redirect(array('inventory/opening_stock'));
    }
	
	 
	 public function actionNewpurchase()
    {
        $model = new Purchase();
		
        $items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
		$transaction=new Transaction_purchase();
        if ($model->load(Yii::$app->request->post())) {
			//exit();
            $main_total=$_POST['total'];
			$discount=$_POST['discount'];
			$vat=$_POST['vat'];
			$trans=$_POST['trans'];
	        $model->vendor_id = $_POST['Purchase']['vendor_id'];
			$model->purchase_date = $_POST['Purchase']['purchase_date'];
			$model->invoice_number = mysql_real_escape_string($_POST['invoice_number']);
            $model->shop = $_POST['Purchase']['shop'];
			$model->sub_total =array_sum($main_total);
			$model->total =$_POST['total_price'];
			$model->tax_type = $_POST['tax_type'];
			$model->discount = $discount;
			$model->vat =$vat;
			$model->trans =$trans;
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$selling_price=$_POST['selling_price'];
			$total=$_POST['subtotal'];
		    
			$sno=$_POST['s_no'];
			$payment_type=isset($_POST['payment_type'])?$_POST['payment_type']:'';
			 if($payment_type=='credit')
			   {
				 $model->credit_type=$_POST['pay_type'];
			   }
			   /*else
			   {
				   $model->credit_type=0;
			   }*/
			   $model->paid=isset($_POST['mode_amount'])?$_POST['mode_amount']:'';
			if ($model->save(false))
			{
			    $last_id=$model->id;
				
				$transaction->vendor_id=$_POST['Purchase']['vendor_id'];
				$transaction->amount=$_POST['mode_amount'];
				//$transaction->notes=$notes;
				$transaction->pay_type=$_POST['pay_type'];
				$transaction->save(false);
				
				if($payment_type!='' && $payment_type!='credit')
				{
					$Voucher= new Voucher();
					$Voucher->transaction_id=$transaction->id;
					$Voucher->purchase_id=$last_id;
					$Voucher->pay_type=$_POST['pay_type'];
					$Voucher->amount=$_POST['mode_amount'];
					$Voucher->voucher_date=date("Y-m-d h:i:s");
					$Voucher->payon_purchase=1;
					$Voucher->notes=$_POST['pay_notes'];
					$Voucher->save();
					
				}
				
				for($j=0;$j<count($product_id);$j++)
				{
					$total_records=explode(',',$sno[$j]);
					$Grouppurchase = new Grouppurchase();
					$Grouppurchase->purchase_id=$last_id;
					$Grouppurchase->total=$main_total[$j];
					$Grouppurchase->qty=$quantity[$j];
					//$Grouppurchase->discount=$discount[$j];
					//$Grouppurchase->vat=$vat[$j];
					//$Grouppurchase->trans=$trans[$j];
					$Grouppurchase->save();
					$last_group_id=$Grouppurchase->group_id;
					$product_ids=$product_id[$j];
					$new_price=$price[$j];
					$sell_price=$selling_price[$j];
					
						  for($i=0;$i<count($total_records);$i++)
						  {
							$Inventory = new Inventory();
							$Inventory->purchase_id =$last_id;
							$Inventory->group_id =$last_group_id;
							$Inventory->product_id =$product_ids;
							$Inventory->price =$new_price;
							$Inventory->selling_price =$sell_price;
							$Inventory->qty =1;
							$Inventory->total =$new_price;
							$Inventory->sno =$total_records[$i];
							$Inventory->shop =$model->shop;
							$Inventory->type ='purchase';
							$Inventory->save();
						  }
					//$Products->isNewRecord = true;
				}
                  return Yii::$app->response->redirect(array('inventory/purchase'));
				
			}
			
            
        } else {
            
			return $this->render('new_purchase', ['model' => $model,'items'=>$items,'items1'=>$items1,'product_details'=>$product_details]);
        }
    }
	
	
	
	public function actionUpdatepurchase()
    {
        
        $parchase_id = $_GET['id'];
		$model = Purchase::findOne(['id' =>$parchase_id ]);
		
        $items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $main_total=$_POST['total'];
			$model->vendor_id = $_POST['Purchase']['vendor_id'];
			$model->purchase_date = $_POST['Purchase']['purchase_date'];
			$model->invoice_number = mysql_real_escape_string($_POST['invoice_number']);
            $model->shop = $_POST['Purchase']['shop'];
			$model->sub_total =array_sum($main_total);
			$model->total =$_POST['total_price'];
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$selling_price=$_POST['selling_price'];
			$total=$_POST['subtotal'];
			
			$discount=$_POST['discount'];
			$vat=$_POST['vat'];
			$trans=$_POST['trans'];
			$sno=$_POST['s_no'];
			$model->discount = $discount;
			$model->tax_type = $_POST['tax_type'];
			$model->vat =$vat;
			$model->trans =$trans;
			$model->paid=isset($_POST['mode_amount'])?$_POST['mode_amount']:'';
		    $payment_type=isset($_POST['payment_type'])?$_POST['payment_type']:'';
			 if($payment_type=='credit')
			   {
				 $model->credit_type=$_POST['pay_type'];
			   }
			   /*else
			   {
				   $model->credit_type=0;
			   }*/
			if ($model->save(false))
			{
				    $last_id=$model->id;
				  
				 
				  
				  
			     $Voucher_update= Voucher::find()->where(['purchase_id'=>$last_id,'payon_purchase'=>1])->one();
				  if(isset($Voucher_update->transaction_id))
			    {
					$transaction=Transaction_purchase::findOne(['id' =>$Voucher_update->transaction_id]);
				}
				else
				{
					$transaction=new Transaction_purchase();
				}
					$transaction->vendor_id=$_POST['Purchase']['vendor_id'];
					$transaction->amount=$_POST['mode_amount'];
					//$transaction->notes=$notes;
					$transaction->pay_type=$_POST['pay_type'];
					$transaction->save(false);
				
			  
			if($payment_type!='' && empty($Voucher_update) && $payment_type!='credit')
			{
				    $Voucher= new Voucher();
					$Voucher->transaction_id=$transaction->id;
					$Voucher->purchase_id=$last_id;
					$Voucher->pay_type=$_POST['pay_type'];
					$Voucher->amount=$_POST['mode_amount'];
					$Voucher->voucher_date=date("Y-m-d h:i:s");
					$Voucher->payon_purchase=1;
					$Voucher->notes=$_POST['pay_notes'];
					$Voucher->bank_date=$_POST['bank_date'];
					$Voucher->save();
				
			}
			else if($payment_type!='credit' )
			{
				    //$Voucher_update= new Voucher();
					//$Voucher_update->transaction_id=$last_id;
					$Voucher_update->pay_type=$_POST['pay_type'];
					$Voucher_update->amount=$_POST['mode_amount'];
					$Voucher_update->voucher_date=date("Y-m-d h:i:s");
					$Voucher_update->payon_purchase=1;
					$Voucher_update->notes=$_POST['pay_notes'];
					$Voucher_update->bank_date=$_POST['bank_date'];
					$Voucher_update->save();
			}
			else if($payment_type=='credit' && !empty($Voucher_update))
			{
				$Voucher_update->delete();
			}
				  
				  
					Inventory::deleteAll(['purchase_id' =>$parchase_id]);
					Grouppurchase::deleteAll(['purchase_id' =>$parchase_id]);
					//$Delete_product->delete();
			   
				$last_id=$model->id;
				for($j=0;$j<count($product_id);$j++)
				{
					$total_records=explode(',',$sno[$j]);
					$Grouppurchase = new Grouppurchase();
					$Grouppurchase->purchase_id=$last_id;
					$Grouppurchase->total=$main_total[$j];
					$Grouppurchase->qty=$quantity[$j];
					//$Grouppurchase->discount=$discount[$j];
					//$Grouppurchase->vat=$vat[$j];
					//$Grouppurchase->trans=$trans[$j];
					$Grouppurchase->save();
					$last_group_id=$Grouppurchase->group_id;
					$product_ids=$product_id[$j];
					$new_price=$price[$j];
					$sell_price=$selling_price[$j];
					
						  for($i=0;$i<count($total_records);$i++)
						  {
							$Inventory = new Inventory();
							$Inventory->purchase_id =$last_id;
							$Inventory->group_id =$last_group_id;
							$Inventory->product_id =$product_ids;
							$Inventory->price =$new_price;
							$Inventory->selling_price =$sell_price;
							$Inventory->qty =1;
							$Inventory->total =$new_price;
							$Inventory->sno =$total_records[$i];
							$Inventory->shop =$model->shop;
							$Inventory->type ='purchase';
							$Inventory->save();
						  }
					//$Products->isNewRecord = true;
				}
				return Yii::$app->response->redirect(array('inventory/purchase'));
				
			}
			
            
        } else {
            
			return $this->render('new_purchase', ['model' => $model,'items'=>$items,'items1'=>$items1,'product_details'=>$product_details]);
        }
    
    }
	
	
	public function actionSales()
    {
		$searchModel = new Sales_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'sales_sno');
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('sales', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionCheck_product_available()
    {
	
		
        $product_id=$_GET['product_id'];
		$id=$_GET['id'];
		
		
		$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where product_id='".$product_id."' and qty=1")->queryAll();
	$sales_product=array();
	$purchase_product=array();
	if(!empty($product_all))
	{				
					foreach($product_all as $pro)
					{
						$purchase_product[]=$pro['sno'];
					}
		$product_all1 = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where product_id='".$product_id."' and qty=-1")->queryAll();
		            foreach($product_all1 as $pro)
					{
						$sales_product[]=$pro['sno'];
					}
					
		
		$result = array_diff($purchase_product, $sales_product);
		$keys=array_keys($result);
		
		if($result)
		{
			//$product_details = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where sno='".$result[$keys[0]]."' and qty=1")->queryOne();
			//$group = Yii::$app->db->createCommand("SELECT * FROM tbl_group_purchase Where group_id='".$product_details['group_id']."'")->queryOne();
			          $response =  ['status' => 1,
								    'sno' => $result,
									//'sell_price' => $product_details['selling_price'],
									//'disc' => $group['discount'],
									//'vat' => $group['vat'],
									//'trans' => $group['trans'],
									];
									//$_SESSION['selected_sno'][$_GET['product_id']][$id]=$product_details['sno'];
									
									
		}
		else
		{
			       $response = ['status' => 2,
								'results' =>'Items unavailable'];
		}
		              
	}
	else
	{
		             $response = ['status' => 2,
								'results' =>'Items unavailable',
								];
		//$respose='Items unavailable';
	}
	
	//print_r($_SESSION['selected_sno']);
		return json_encode($response);
	}
	
	
	public function actionGet_pay_type()
    {
	
		
       	$type=$_GET['type'];
		
		
		$product_all = Yii::$app->db->createCommand("SELECT * FROM tbl_accounts Where payment_type='".$type."'")->queryAll();

	if(!empty($product_all))
	{
		             foreach($product_all as $account)
					 {
						 $Accounts[$account['id']]=$account['account_name'];
					 }
		
		               $response =  ['status' => 1,
								    'sno' => $Accounts,];
    }
	else
	{
		             $response = ['status' => 2,
								'results' =>'Items unavailable',
								];
		//$respose='Items unavailable';
	}
	
	//print_r($_SESSION['selected_sno']);
		return json_encode($response);
	}
	
	public function actionGet_serial_details()
    {
		
		
        $sno=$_GET['sno'];
		$id=$_GET['id'];
		
		
		if($sno)
		{
			$product_details = Yii::$app->db->createCommand("SELECT * FROM tbl_inventory Where sno='".$sno."' and qty=1")->queryOne();
			$group = Yii::$app->db->createCommand("SELECT * FROM tbl_group_purchase Where group_id='".$product_details['group_id']."'")->queryOne();
			          $response =  ['status' => 1,
								    'sell_price' => $product_details['selling_price'],
									//'disc' => $group['discount'],
									//'vat' => $group['vat'],
									//'trans' => $group['trans'],
									];
									
									
									
		}
		else
		{
			       $response = ['status' => 2,
								'results' =>'Items unavailable'];
		}
		              
	
		return json_encode($response);
	}
	
	
	 public function actionNewsales()
    {
        $model = new sales();
		$Customer=new Customer();
       	$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$sales_table = sales::find('not',['sales_sno' =>NULL])->orderBy(['(id)' => SORT_DESC])->one();
	    $sales_sno=$sales_table['sales_sno']+1;
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
		$transaction=new Transaction_sales();
        if (Yii::$app->request->post()) {
            
			$discount=$_POST['discount'];
			$vat=$_POST['vat'];
			$trans=$_POST['trans'];
			$tax_type=$_POST['tax_type'];
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$total=$_POST['subtotal'];
			$sno=$_POST['s_no'];
			$main_total=$_POST['total'];
			$notes=$_POST['notes'];
			$payment_type=isset($_POST['payment_type'])?$_POST['payment_type']:'';
			
			$Customer->first_name=$_POST['first_name'];
			$Customer->company_name=$_POST['company_name'];
			$Customer->city=$_POST['city'];
			$Customer->Address=$_POST['address'];
			$Customer->contact_phone=$_POST['contact_phone'];
			$Customer->email_address=$_POST['contact_email'];
			$Customer->tin_no=$_POST['tin_no'];
			$Customer->cst_number=$_POST['cst_number'];
			$all_mob=Yii::$app->mycomponent->customer_all_mob();
			//print_r($_POST['notes']);
		//exit();
		
			    if($all_mob)
				{
						if(!in_array($Customer->contact_phone,$all_mob))
						{
							 $Customer->save(false);
							 $last_id=$Customer->id;
						}
						else
						{
							$get_customer = Customer::findOne(['contact_phone' =>$Customer->contact_phone]);
							$last_id=$get_customer->id;
						}
				}
				else
				{
					         $Customer->save(false);
							 $last_id=$Customer->id;
				}
				
				
			$model->customer_id = $last_id;
			$model->sales_date = Date('Y-m-d');
			//$model->total =array_sum($main_total);
			$model->sub_total =array_sum($main_total);
			$model->total =$_POST['total_price'];
			$model->discount = $discount;
			$model->vat =$vat;
			$model->trans =$trans;
			$model->tax_type=$tax_type;
			$model->sales_sno =$sales_sno;
			$model->paid=isset($_POST['mode_amount'])?$_POST['mode_amount']:'';
			
			
			
			   if($payment_type=='credit')
			   {
				 $model->credit_type=$_POST['pay_type'];
			   }
			   else
			   {
				   $model->credit_type='';
			   }
			  if ($model->save(false))
			  {
				 $sales_id=$model->id;
				
				if($payment_type!='' && $payment_type!='credit')
				{
					$transaction->customer_id=$last_id;
					$transaction->amount=$_POST['mode_amount'];
					//$transaction->notes=$notes;
					$transaction->pay_type=$_POST['pay_type'];
					$transaction->save(false);
					$Receipt= new Receipt();
					$Receipt->transaction_id=$transaction->id;
					$Receipt->sales_id=$sales_id;
					$Receipt->pay_type=$_POST['pay_type'];
					$Receipt->amount=$_POST['mode_amount'];
					$Receipt->sales_date=date("Y-m-d h:i:s");
					$Receipt->payon_sales=1;
					$Receipt->notes=$_POST['pay_notes'];
					$Receipt->bank_date=$_POST['bank_date'];
					$Receipt->save();
					
				}
				
				for($i=0;$i<count($product_id);$i++)
				{
					
					$Group_sales = new Group_sales();
					$Group_sales->sales_id=$sales_id;
					$Group_sales->total=$main_total[$i];
					$Group_sales->notes=$notes[$i];
					$Group_sales->qty=1;
					//$Group_sales->discount=$discount[$i];
					//$Group_sales->vat=$vat[$i];
					//$Group_sales->trans=$trans[$i];
					$Group_sales->save();
					$last_group_id=$Group_sales->group_id;
					
					$Inventory = new Inventory();
					$Inventory->sales_id =$sales_id;
					$Inventory->customer_id =$last_id;
				    $Inventory->product_id =$product_id[$i];
					$Inventory->price =$price[$i];
					$Inventory->total =$total[$i];
					$Inventory->sno =$sno[$i];
					$Inventory->qty ='-1';
					$Inventory->group_id_sales =$last_group_id;
					//$Inventory->shop =1;
					$Inventory->type ='sales';
					$Inventory->save();
					//$Products->isNewRecord = true;
				}
                  return Yii::$app->response->redirect(array('inventory/sales'));
				
				} else {
				
				return $this->render('new_sales', ['items1'=>$items1,'product_details'=>$product_details]);
			}
			
            
        } else {
            
			return $this->render('new_sales', ['items1'=>$items1,'product_details'=>$product_details]);
        }
    }
	
	
	public function actionUpdatesales()
    {
        
        $sales_id = $_GET['id'];
		$Customer=new Customer();
		$model = Sales::findOne(['id' =>$_GET['id']]);
		$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$transaction=new Transaction_sales();
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
        if (Yii::$app->request->post()) {
            $notes=$_POST['notes'];
			$main_total=$_POST['total'];
			$product_id=$_POST['product_id'];
			$quantity=$_POST['qty'];
			$price=$_POST['price'];
			$total=$_POST['subtotal'];
			$sno=$_POST['s_no'];
			$discount=$_POST['discount'];
			$vat=$_POST['vat'];
			$trans=$_POST['trans'];
			$tax_type=$_POST['tax_type'];
			$payment_type=isset($_POST['payment_type'])?$_POST['payment_type']:'';
			$Receipt_update= Receipt::find()->where(['sales_id'=>$sales_id,'payon_sales'=>1])->one();
			if(isset($Receipt_update->transaction_id))
			{
			   $transaction_update=Transaction_sales::find()->where(['id'=>$Receipt_update->transaction_id])->one();
			}
			   if($payment_type=='credit')
			   {
				 $model->credit_type=$_POST['pay_type'];
			   }
			   else
			   {
				   $model->credit_type='';
			   }
			
			//exit();
			//Sales::deleteAll(['id' =>$sales_id]);
			Group_sales::deleteAll(['sales_id' =>$sales_id]);
			Inventory::deleteAll(['sales_id' =>$sales_id]);
			$Customer->first_name=$_POST['first_name'];
			$Customer->company_name=$_POST['company_name'];
			$Customer->city=$_POST['city'];
			$Customer->Address=$_POST['address'];
			$Customer->contact_phone=$_POST['contact_phone'];
			$Customer->email_address=$_POST['contact_email'];
			$Customer->tin_no=$_POST['tin_no'];
			$Customer->cst_number=$_POST['cst_number'];
			$all_mob=Yii::$app->mycomponent->customer_all_mob();
			//print_r($all_mob);
		//	exit();
		
			    if($all_mob)
				{
						if(!in_array($Customer->contact_phone,$all_mob))
						{
							 $Customer->save(false);
							 $last_id=$Customer->id;
						}
						else
						{
							$get_customer = Customer::findOne(['contact_phone' =>$Customer->contact_phone]);
							$last_id=$get_customer->id;
						}
				}
				else
				{
					         $Customer->save(false);
							 $last_id=$Customer->id;
				}
				
		    $model->customer_id = $last_id;
			$model->sales_date = Date('Y-m-d');
			//$model->total =array_sum($main_total);
			$model->sub_total =array_sum($main_total);
			$model->total =$_POST['total_price'];
			$model->discount = $discount;
			$model->vat =$vat;
			$model->trans =$trans;
			$model->tax_type=$tax_type;
			$model->paid=isset($_POST['mode_amount'])?$_POST['mode_amount']:'';
			
			
			
			if($payment_type!='' && empty($Receipt_update) && $payment_type!='credit')
			{
				    $transaction->customer_id=$last_id;
					$transaction->amount=$_POST['mode_amount'];
					//$transaction->notes=$notes;
					$transaction->pay_type=$_POST['pay_type'];
					$transaction->save(false);
				    
					
				
				$Receipt= new Receipt();
				$Receipt->transaction_id=$transaction->id;
				$Receipt->sales_id=$sales_id;
				$Receipt->pay_type=$_POST['pay_type'];
				$Receipt->amount=$_POST['mode_amount'];
				$Receipt->sales_date=date("Y-m-d h:i:s");
				$Receipt->payon_sales=1;
				$Receipt->notes=$_POST['pay_notes'];
				$Receipt->bank_date=$_POST['bank_date'];
				$Receipt->save();
				
			}
			else if($payment_type!='credit' )
			{
				//$Receipt_update->transaction_id=$sales_id;
				$transaction_update->amount=$_POST['mode_amount'];
				$transaction_update->pay_type=$_POST['pay_type'];
				$transaction_update->save(false);
				$Receipt_update->pay_type=$_POST['pay_type'];
				$Receipt_update->amount=$_POST['mode_amount'];
				$Receipt_update->sales_date=date("Y-m-d h:i:s");
				$Receipt_update->payon_sales=1;
				$Receipt_update->notes=$_POST['pay_notes'];
				$Receipt_update->bank_date=$_POST['bank_date'];
				$Receipt_update->save();
			}
			else if($payment_type=='credit' && !empty($Receipt_update))
			{
				$Receipt_update->delete();
			}
			
			   if ($model->save(false))
			  {
				$sales_id=$model->id;
				for($i=0;$i<count($product_id);$i++)
				{
					
					$Group_sales = new Group_sales();
					$Group_sales->sales_id=$sales_id;
					$Group_sales->total=$main_total[$i];
					$Group_sales->notes=$notes[$i];
					$Group_sales->qty=1;
				//	$Group_sales->discount=$discount[$i];
					//$Group_sales->vat=$vat[$i];
					//$Group_sales->trans=$trans[$i];
					$Group_sales->save();
					$last_group_id=$Group_sales->group_id;
					
					$Inventory = new Inventory();
					$Inventory->sales_id =$sales_id;
					$Inventory->customer_id =$last_id;
				    $Inventory->product_id =$product_id[$i];
					$Inventory->price =$price[$i];
					$Inventory->total =$total[$i];
					$Inventory->sno =$sno[$i];
					$Inventory->qty ='-1';
					$Inventory->group_id_sales =$last_group_id;
					//$Inventory->shop =1;
					$Inventory->type ='sales';
					$Inventory->save();
					//$Products->isNewRecord = true;
				}
                  return Yii::$app->response->redirect(array('inventory/sales'));
                
			  }
			
			
            
        } else {
            
			return $this->render('new_sales', ['items1'=>$items1,'product_details'=>$product_details,'customer_id'=>$model->customer_id,'model'=>$model]);
        }
    
    }
	
	
	
	public function actionNewproduct()
    {
        $model = new Products();
		
        $product_details = ArrayHelper::map(Producttype::find()->all(), 'id', 'product_type');
		$manufacture_details = ArrayHelper::map(Manufacture::find()->all(), 'id', 'manufacture_name');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $product_type=Producttype::find()->where(['id'=>$_POST['Products']['type']])->one(); 
			$manufacture_name=Manufacture::find()->where(['id'=>$_POST['Products']['manufacturer']])->one();
			$product_model=$_POST['Products']['product_model'];
			
			//$model->product_name=$_POST['Products']['manufacturer'].' '.$_POST['Products']['type'].' '.$_POST['Products']['product_model'];
			//$model->product_model=$_POST['Products']['product_model'];
			$manuf_id=$_POST['Products']['manufacturer'];
			$type_id=$_POST['Products']['type'];
			$type=$product_type->product_type;
			$manufacturer=$manufacture_name->manufacture_name;
			$description=$_POST['Products']['description'];
			
			             for($i=0;$i<count($product_model);$i++)
						  {
							$Products = new Products();
							$Products->product_name=$manufacturer.' '.$type.' '.$product_model[$i];
							$Products->product_model=$product_model[$i];
							$Products->manufacturer=$manufacturer;
							$Products->type=$type;
							$Products->description=$description[$i];
							$Products->type_id=$type_id;
							$Products->manufacture_id=$manuf_id;
							$Products->save();
						  }
			              return Yii::$app->response->redirect(array('inventory/products'));
			
			/*if ($model->save(false))
			{
			   
                  return Yii::$app->response->redirect(array('inventory/products'));
				
			}*/
			
            
        } else {
            
			return $this->render('new_product', ['model' => $model,'product_details'=>$product_details,'manufacture_details'=>$manufacture_details]);
        }
    }
	
	public function actionUpdateproducts()
    {
        
        $product_id = $_GET['id'];
		$model = Products::findOne(['pid' =>$product_id ]);
		$product_details = ArrayHelper::map(Producttype::find()->all(), 'id', 'product_type');
		$manufacture_details = ArrayHelper::map(Manufacture::find()->all(), 'id', 'manufacture_name');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$product_type=Producttype::find()->where(['id'=>$_POST['Products']['type']])->one(); 
			$manufacture_name=Manufacture::find()->where(['id'=>$_POST['Products']['manufacturer']])->one();
			$type=$product_type->product_type;
			$manufacturer=$manufacture_name->manufacture_name;
			$model->product_name=mysql_real_escape_string($manufacture_name->manufacture_name.' '.$product_type->product_type.' '.$_POST['Products']['product_model']);
			$model->product_model=$_POST['Products']['product_model'];
			$model->type=$product_type->product_type;
			$model->type_id=$_POST['Products']['type'];
			$model->manufacture_id=$_POST['Products']['manufacturer'];
			$model->manufacturer=$manufacture_name->manufacture_name;
			$model->description=$_POST['Products']['description'];
			
			if ($model->save(false))
			{
			   
                  return Yii::$app->response->redirect(array('inventory/products'));
				
			}
			
            
        } else {
            
			return $this->render('new_product', ['model' => $model,'product_details'=>$product_details,'manufacture_details'=>$manufacture_details]);
        }
    
    }
	
	
	public function actionNewshop()
    {
        $model = new Shop();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->shop = $_POST['Shop']['shop'];
			$model->description = $_POST['Shop']['description'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/shop'));
				
			}

            
        } else {
            
			return $this->render('new_shop', ['model' => $model]);
        }
    }
	
	public function actionAddselling()
    {
        $model = new Selling();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->selling_name = $_POST['Selling']['selling_name'];
			$model->price = $_POST['Selling']['price'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/selling'));
				
			}

            
        } else {
            
			return $this->render('add_selling_price', ['model' => $model]);
        }
    }
	
	public function actionAddproduct()
    {
        $model = new Producttype();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$product_type = $_POST['Producttype']['product_type'];
			$description = $_POST['Producttype']['description'];
			//print_r($product_type);
			//exit();
			for($i=0;$i<count($product_type);$i++)
			{
				$model = new Producttype();
				$model->product_type = $product_type[$i];
			    $model->description = $description[$i];
				$model->save(false);
			}
            
				return Yii::$app->response->redirect(array('inventory/product_type'));
			

            
        } else {
            
			return $this->render('add_product_type', ['model' => $model]);
        }
    }
	public function actionProduct_type()
    {
		$searchModel = new Protypesearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('product_type', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionUpdateprotype()
    {
         $id = $_GET['id'];
		$model = Producttype::findOne(['id' =>$id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->product_type = $_POST['Producttype']['product_type'];
			$model->description = $_POST['Producttype']['description'];
            
			if ($model->save(false))
			{
				Yii::$app->mycomponent->UpdateProductType($id);
				return Yii::$app->response->redirect(array('inventory/product_type'));
				
			}

            
        } else {
            
			return $this->render('add_product_type', ['model' => $model]);
        }
    }
	
	
	
	public function actionDeleteprotype()
    {
		$id = $_GET['id'];
        $model = Producttype::findOne(['id' =>$id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/product_type'));
    }
	
	public function actionManufacture()
    {
		$searchModel = new Manufacturesearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('manufacture', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	
	
	public function actionAdd_manufact()
    {
        $model = new Manufacture();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$manufacture_name = $_POST['Manufacture']['manufacture_name'];
			for($i=0;$i<count($manufacture_name);$i++)
			{
				$model = new Manufacture();
				$model->manufacture_name = $manufacture_name[$i];
			   // $model->description = $manufacture_name[$i];
				$model->save(false);
			}
            
			//$model->description = $_POST['Manufacture']['description'];
                      return Yii::$app->response->redirect(array('inventory/manufacture'));
				
			

            
        } else {
            
			return $this->render('add_manufacture', ['model' => $model]);
        }
    }
	
	public function actionUpdatemanufact()
    {
          $id = $_GET['id'];
		  $model = Manufacture::findOne(['id' =>$id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->manufacture_name = $_POST['Manufacture']['manufacture_name'];
			$model->description = $_POST['Manufacture']['description'];
            
			if ($model->save(false))
			{
				Yii::$app->mycomponent->UpdateManufact($id);
				return Yii::$app->response->redirect(array('inventory/manufacture'));
				
			}

            
        } else {
            
			return $this->render('add_manufacture', ['model' => $model]);
        }
    }
	
	public function actionUpdate_typeids()
    {
         $Products1 = Producttype::find()->all(); 
		 foreach($Products1 as $product)
		 {
			                $Products_type = Products::find()->where(['type' =>$product['product_type'] ])->all(); 
							foreach($Products_type as $prd)
		                   {
							   $Products = Products::find()->where(['pid' =>$prd['pid'] ])->one();
								if($Products)
								{
									if(isset($product['id']))
									{
										$Products->type_id=$product['id'];
										$Products->save();
									}
								}
						   }
		 }
    }
	public function actionUpdate_manufacids()
    {
         $Products1 = Manufacture::find()->all(); 
		 foreach($Products1 as $product)
		 {
			                $manufacturer = Products::find()->where(['manufacturer' =>$product['manufacture_name'] ])->all(); 
							foreach($manufacturer as $manu)
		                   {
							   $Products = Products::find()->where(['pid' =>$manu['pid'] ])->one(); 
								if($Products)
								{
									if(isset($product['id']))
									{
										$Products->manufacture_id=$product['id'];
										$Products->save();
									}
								}
						   }
		 }
    }
	
	public function actionDeletemanufact()
    {
		$id = $_GET['id'];
        $model = Manufacture::findOne(['id' =>$id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/manufacture'));
    }
	
	public function actionSelling()
    {
		$searchModel = new Sellingsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('selling', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionUpdateselling()
    {
        $selling_id = $_GET['id'];
		$model = Selling::findOne(['id' =>$selling_id ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->selling_name = $_POST['Selling']['selling_name'];
			$model->price = $_POST['Selling']['price'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/selling'));
				
			}

            
        } else {
            
			return $this->render('add_selling_price', ['model' => $model]);
        }
    }
	public function actionDeleteselling()
    {
		$selling_id = $_GET['id'];
        $model = Selling::findOne(['id' =>$selling_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/selling'));
    }
	
	public function actionNewaccesories()
    {
        $model = new Accesories();
		$categories =Cataccesories::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->acc_name = $_POST['Accesories']['acc_name'];
			$model->description = $_POST['Accesories']['description'];
			$model->warning_menu = $_POST['warning'];
			if(isset($_POST['category']))
			{
                $model->category =json_encode($_POST['category']);
			}
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/accessories'));
				
			}

            
        } else {
            
			return $this->render('new_accessories', ['model' => $model,'categories'=>$categories]);
        }
    }
	
	
	public function actionNewcataccesories()
    {
        $model = new Cataccesories();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->cat_name = $_POST['Cataccesories']['cat_name'];
			$model->description = $_POST['Cataccesories']['description'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/cataccessories'));
				
			}

            
        } else {
            
			return $this->render('new_cat_accessories', ['model' => $model]);
        }
    }
	
	public function actionCataccessories()
    {
		$searchModel = new Cataccesoriessearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('cat_accesories', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionAccessories()
    {
		$searchModel = new Accesoriessearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('accesories', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionUpdatecateaccessories()
    {
        $cat_id = $_GET['id'];
		$model = Cataccesories::findOne(['id' =>$cat_id ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->cat_name = $_POST['Cataccesories']['cat_name'];
			$model->description = $_POST['Cataccesories']['description'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/cataccessories'));
				
			}

            
        } else {
            
			return $this->render('new_cat_accessories', ['model' => $model]);
        }
    }
	
	public function actionUpdateaccessories()
    {
        $shop_id = $_GET['id'];
		$model = Accesories::findOne(['id' =>$shop_id ]);
		$categories =Cataccesories::find()->all();
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if(isset($_POST['category']))
			{
                $model->category =json_encode($_POST['category']);
			}
				$model->acc_name = $_POST['Accesories']['acc_name'];
				$model->description = $_POST['Accesories']['description'];
				$model->warning_menu = $_POST['warning'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/accessories'));
				
			}

            
        } else {
            
			return $this->render('new_accessories', ['model' => $model,'categories'=>$categories]);
        }
    }
	
	
	public function actionUpdateshop()
    {
        $shop_id = $_GET['id'];
		$model = Shop::findOne(['id' =>$shop_id ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->shop = $_POST['Shop']['shop'];
			$model->description = $_POST['Shop']['description'];
            
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('inventory/shop'));
				
			}

            
        } else {
            
			return $this->render('new_shop', ['model' => $model]);
        }
    }
	
	public function actionShop()
    {
		$searchModel = new Shopsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('shop', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionPurchase()
    {
		$searchModel = new Purchasesearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('purchase', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionProducts()
    {
		$searchModel = new Productsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['pid'=>SORT_DESC]]);
        return $this->render('products', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	
	
	
	
	
	
	public function actionDeletecataccess()
    {
		$cat_id = $_GET['id'];
		$model = Cataccesories::findOne(['id' =>$cat_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/cataccessories'));
    }
	
	public function actionDeleteshop()
    {
		$shop_id = $_GET['id'];
        $model = Shop::findOne(['id' =>$shop_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/shop'));
    }
	public function actionDeleteaccess()
    {
		$ac_id = $_GET['id'];
        $model = Accesories::findOne(['id' =>$ac_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('inventory/accessories'));
    }
	
	public function actionDeleteparchase()
    {
		$sold='';
		$purchase_id = $_GET['id'];
        $model = Inventory::findAll(['purchase_id' =>$purchase_id ]);
		foreach($model as $sno)
		{
			$s_no='';
			$s_no=Yii::$app->mycomponent->Get_sno_sold($sno['sno']);
			if($s_no!='')
			{
				$sold=1;
			}
		}
		echo  $sold;
		$_SESSION['warning_product']=1;
		if($sold=='')
		{
			     $_SESSION['warning_product']=2;
		         Purchase::deleteAll(['id' =>$purchase_id]);
		         Grouppurchase::deleteAll(['purchase_id' =>$purchase_id]);
				 Inventory::deleteAll(['purchase_id' =>$purchase_id]);
				 
		}
    	
       return Yii::$app->response->redirect(array('inventory/purchase'));
    }
	
	public function actionSold_check()
    {
		    $response='';
			$model=explode(',',$_GET['sno']);
			
			foreach($model as $sno)
			{
				$s_no=Yii::$app->mycomponent->Get_sno_sold($sno);
				if($s_no!='')
				{
						$response =1;
				}
			}
			
			
			echo $response;
				
	}
	
	public function actionDeletesales()
    {
		$customer_id = $_GET['id'];
        Inventory::deleteAll(['sales_id' =>$customer_id]);
       return Yii::$app->response->redirect(array('inventory/sales'));
    }
	
	public function actionCancel_sales()
    {
		$sales_id = $_GET['id'];
		$model = Sales::findOne(['id' =>$sales_id ]);
		$model->status =2;
			         
			if ($model->save(false))
			{
				
			}
		$connection=Yii::$app->db;
		$connection->createCommand('INSERT tbl_cancelled SELECT * FROM tbl_inventory WHERE sales_id='.$sales_id.'')->execute();
        Inventory::deleteAll(['sales_id' =>$sales_id]);
       return Yii::$app->response->redirect(array('complaint/estimate'));
    }
	
	public function actionReturn_sales()
    {
		$sales_id = $_GET['id'];
		$model = Sales::findOne(['id' =>$sales_id ]);
		$model->status =1;
			         
			if ($model->save(false))
			{
				
			}
		$connection=Yii::$app->db;
		$connection->createCommand('INSERT tbl_inventory SELECT * FROM tbl_cancelled  WHERE sales_id='.$sales_id.'')->execute();
		$connection->createCommand()->delete('tbl_cancelled', ['sales_id' => $sales_id])->execute();
       return Yii::$app->response->redirect(array('complaint/estimate'));
    }
	
	public function actionDeleteproducts()
    {
		//session_start();
		$product_id = $_GET['id'];
		$model1 = Inventory::findOne(['product_id' =>$product_id ]);
		$_SESSION['warning_product']=1;
		if(empty($model1))
		{
			$_SESSION['warning_product']=2;
			$model = Products::findOne(['pid' =>$product_id ]);
			$model->delete();
		}
		
         return Yii::$app->response->redirect(array('inventory/products'));
		
    }
	
	public function actionVoucher()
    {
		$searchModel=new Transaction_purchase();
		//$searchModel = new Voucher_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('voucher_transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	
	
	
	public function actionAdd_voucher()
    {
        $model = new Purchase();
		$items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		$transaction=new Transaction_purchase();
        if (Yii::$app->request->post()) {
			$product_id=$_POST['purchase_id'];
			$total=$_POST['total'];
			$paid=$_POST['paid'];
			$pay=$_POST['pay'];
			$notes=$_POST['pay_notes'];
			//$pay_type=$_POST['pay_type'];
			//print_r($pay_type);
			//exit();
			$transaction->vendor_id=$_POST['vendor_id'];
			$transaction->amount=$_POST['amount'];
			$transaction->notes=$notes;
			$transaction->pay_type=$_POST['pay_type'];
			$transaction->save(false);
			
			    for($i=0;$i<count($product_id);$i++)
				{
					
					if($product_id[$i]=='opening_balance')
					{
						$customer =Vendor::findOne(['id' =>$_POST['vendor_id']]);
					    $customer->paid=$paid[$i]+$pay[$i];
						$customer->save(false);
						        
						    if($pay[$i]!='')
							{
								$OpeningBalance = new OpeningBalanceSupplier();
								$OpeningBalance->transaction_id=$transaction->id;
								$OpeningBalance->amount=$pay[$i];
								$OpeningBalance->supplier_id=$_POST['vendor_id'];
								$OpeningBalance->save();
							}
					}
				  else
				 {			
						$Purchase =Purchase::findOne(['id' =>$product_id[$i] ]);
						$Purchase->paid=$paid[$i]+$pay[$i];
					   // $Purchase->total=$total[$i];
						$Purchase->save(false);
						//exit();
						if($pay[$i]!='')
						{
							$voucher = new Voucher();
							$voucher->purchase_id=$product_id[$i];
							$voucher->transaction_id=$transaction->id;
							$voucher->pay_type=$_POST['pay_type'];
							$voucher->amount=$pay[$i];
							$voucher->notes=$notes;
							$voucher->bank_date=isset($_POST['bank_date'])?$_POST['bank_date']:'';
							$voucher->voucher_date=date('Y-m-d h:i:s');
							$voucher->save();
						}
					
				 }
					
				}
            
			    return Yii::$app->response->redirect(array('inventory/voucher'));
				
			

            
        } else {
            
			return $this->render('add_voucher', ['model' => $model,'items'=>$items]);
        }
    }
	
	
	public function actionUpdate_voucher()
    {
        $model = new Purchase();
		$items = ArrayHelper::map(Vendor::find()->all(), 'id', 'first_name');
		
        if (Yii::$app->request->post()) {
			$voucher_id=$_POST['voucher_id'];
			$purchase_id=$_POST['purchase_id'];
			
			$product_id=$_POST['purchase_id'];
			$pay=$_POST['pay'];
			$notes=$_POST['pay_notes'];
			//$trans_id=$_POST['trans_id'];
			
			$transaction_id=$_POST['transaction_id'];
			$transaction=Transaction_purchase::findOne(['id' =>$transaction_id]);
			//$transaction->vendor_id=$_POST['vendor_id'];
			$transaction->amount=$_POST['amount'];
			$transaction->notes=$notes;
			$transaction->pay_type=$_POST['pay_type'];
			$transaction->save(false);
			
			    for($i=0;$i<count($voucher_id);$i++)
				{
					
					if($voucher_id[$i]=='opening_balance')
					{
						
					   
						        
						    if($pay[$i]!='')
							{
								$OpeningBalance =OpeningBalanceSupplier::findOne(['id' =>$_POST['opening_id']]);
								//print_r($OpeningBalance);
								$OpeningBalance->transaction_id=$transaction->id;
								$OpeningBalance->amount=$pay[$i];
								//$OpeningBalance->customer_id=$_POST['customer_id'];
								$OpeningBalance->save(false);
							}
							$opening =OpeningBalanceSupplier::find()->where(['id' =>$_POST['opening_id']])->select('sum(amount) as amount')->one();
							$customer =Vendor::findOne(['id' =>$OpeningBalance->supplier_id]);
					        $customer->paid=$opening->amount;
						    $customer->save(false);
							//exit();
							
					}
					
					else
					{
					
					
							//exit();
							if($pay[$i]!='')
							{
								
								$voucher =Voucher::findOne(['id'=>$voucher_id[$i]]);;
								//$voucher->transaction_id=$product_id;
								$voucher->pay_type=isset($_POST['pay_type'])?$_POST['pay_type']:'0';
								$voucher->amount=$pay[$i];
								$voucher->notes=$notes;
								$voucher->voucher_date=date('Y-m-d h:i:s');
								$voucher->bank_date=isset($_POST['bank_date'])?$_POST['bank_date']:'';
								$voucher->save();
							}
							
							$voucher_paid = Voucher::find()->where(['purchase_id'=>$purchase_id[$i]])->select('sum(amount) as amount')->one();
							echo $voucher_paid->amount;
						
							
							$Purchase =Purchase::findOne(['id' =>$purchase_id[$i]]);
							$Purchase->paid=$voucher_paid->amount;
							$Purchase->save(false);
					}
					
				}
            
			    return Yii::$app->response->redirect(array('inventory/voucher'));
				
			

            
        } else {
            
			return $this->render('update_voucher');
        }
    }
	
	 
	 public function actionReceipt()
    {
		//$searchModel = new Receipt_search();
		$searchModel=new Transaction_sales();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('receipt_transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	
	public function actionAdd_receipt()
    {
        $model = new Purchase();
		$items = ArrayHelper::map(Customer::find()->all(), 'id', 'company_name');
		$transaction=new Transaction_sales();
        if (Yii::$app->request->post()) {
			$product_id=$_POST['sales_id'];
			
			//$total_amount=$_POST['total_amount'];
			$paid=$_POST['paid'];
			$pay=$_POST['pay'];
			$notes=$_POST['pay_notes'];
			$transaction->customer_id=$_POST['customer_id'];
			$transaction->amount=$_POST['amount'];
			$transaction->notes=$notes;
			$transaction->pay_type=$_POST['pay_type'];
			$transaction->save(false);
			    for($i=0;$i<count($product_id);$i++)
				{
					if($product_id[$i]=='opening_balance')
					{
						$customer =Customer::findOne(['id' =>$_POST['customer_id']]);
					    $customer->paid=$paid[$i]+$pay[$i];
						$customer->save(false);
						        
						    if($pay[$i]!='')
							{
								$OpeningBalance = new OpeningBalanceCustomer();
								$OpeningBalance->transaction_id=$transaction->id;
								$OpeningBalance->amount=$pay[$i];
								$OpeningBalance->customer_id=$_POST['customer_id'];
								$OpeningBalance->save();
							}
					}
					else
					{
					
							$Sales =Sales::findOne(['id' =>$product_id[$i] ]);
							$Sales->paid=$paid[$i]+$pay[$i];
							//echo $paid[$i]+$pay[$i].'<br>' ;
							//$Sales->total=$total[$i];
							//exit();
							$Sales->save(false);
							//exit();
							if($pay[$i]!='')
							{
								$Receipt = new Receipt();
								$Receipt->transaction_id=$transaction->id;
								$Receipt->sales_id=$product_id[$i];
								$Receipt->pay_type=$_POST['pay_type'];
								$Receipt->amount=$pay[$i];
								$Receipt->notes=$notes;
								//$Receipt->notes=$_POST['pay_notes'];
								$Receipt->bank_date=isset($_POST['bank_date'])?$_POST['bank_date']:'';
								$Receipt->sales_date=date('Y-m-d h:i:s');
								$Receipt->save();
							}
					}
					
				}
            
			    return Yii::$app->response->redirect(array('inventory/receipt'));
				
			

            
        } else {
            
			return $this->render('add_receipt', ['model' => $model,'items'=>$items]);
        }
    }
	
	public function actionUpdate_receipt()
    {
        $model = new Sales();
		
		
        if (Yii::$app->request->post()) {
			if(isset($_POST['receipt_id']))
			{
			   $receipt_id=$_POST['receipt_id'];
			}
			$product_id=$_POST['sales_id'];
			//$total_amount=$_POST['total_amount'];
			$paid=$_POST['paid'];
			$pay=$_POST['pay'];
			$notes=$_POST['pay_notes'];
			$transaction_id=$_POST['transaction_id'];
			$transaction=Transaction_sales::findOne(['id' =>$transaction_id]);
			//$transaction->customer_id=$_POST['customer_id'];
			$transaction->amount=$_POST['amount'];
			$transaction->notes=$notes;
			$transaction->pay_type=$_POST['pay_type'];
			$transaction->save(false);
			    for($i=0;$i<count($product_id);$i++)
				{
					if($product_id[$i]=='opening_balance')
					{
						
					   
						        
						    if($pay[$i]!='')
							{
								$OpeningBalance =OpeningBalanceCustomer::findOne(['id' =>$_POST['opening_id']]);
								print_r($OpeningBalance);
								$OpeningBalance->transaction_id=$transaction->id;
								$OpeningBalance->amount=$pay[$i];
								//$OpeningBalance->customer_id=$_POST['customer_id'];
								$OpeningBalance->save(false);
							}
							$opening =OpeningBalanceCustomer::find()->where(['id' =>$_POST['opening_id']])->select('sum(amount) as amount')->one();
							$customer =Customer::findOne(['id' =>$OpeningBalance->customer_id]);
					        $customer->paid=$opening->amount;
						    $customer->save(false);
							//exit();
							
					}
					
					else
					{
							if($pay[$i]!='')
							{
								$Receipt = Receipt::findOne(['id'=>$receipt_id[$i]]);
								//$Receipt->transaction_id=$transaction->id;
								//$Receipt->sales_id=$product_id[$i];
								$Receipt->pay_type=$_POST['pay_type'];
								$Receipt->amount=$pay[$i];
								$Receipt->notes=$notes;
								$Receipt->bank_date=isset($_POST['bank_date'])?$_POST['bank_date']:'';
								$Receipt->sales_date=date('Y-m-d h:i:s');
								$Receipt->save();
							}
							$Receipt_paid = Receipt::find()->where(['sales_id'=>$product_id[$i]])->select('sum(amount) as amount')->one();
							echo $Receipt_paid->amount;
							/*foreach ($Receipt_paid as $key=>$val)
							{
								echo $val['amount'];
							}*/
							$Sales =Sales::findOne(['id' =>$product_id[$i] ]);
							$Sales->paid=$Receipt_paid->amount;
							$Sales->save(false);
					}
					
				}
            
			    return Yii::$app->response->redirect(array('inventory/receipt'));
				
			

            
        } else {
            
			return $this->render('update_receipt');
        }
    }
	
	public function actionGet_paylist()
    {
	
		 $response='';
       	$customer_id=$_GET['customer_id'];
		
	$customer_details=Customer::find()->where(['id'=>$customer_id])->one();	
	$sales_all = \app\models\Sales::find()
					->where(" `tbl_sales`.`total`>`tbl_sales`.`paid` and customer_id=$customer_id")->all();
					
					if($customer_details['opening_balance']!='' && $customer_details['opening_balance']>$customer_details['paid'])
					{
						$result[] =  ['status' => 1,
								        'customer_id' => $_GET['customer_id'],
										'id' => 'opening_balance',
										'bill_no' => 'Opening balance',
										'sales_date' => date('Y'),
										'total' => $customer_details['opening_balance'],
										'paid' => isset($customer_details['paid'])?$customer_details['paid']:'0',
										'remaining' => ($customer_details['opening_balance']-$customer_details['paid']),];
					}

	if(!empty($sales_all))
	{
		             foreach($sales_all as $product)
					 {
						 $result[] =  ['status' => 1,
								        'customer_id' => $product['customer_id'],
										'id' => $product['id'],
										'bill_no' => isset($product['sales_sno'])?'SALE'.$product['sales_sno']:'SER'.$product['service_sno'],
										'sales_date' => $product['sales_date'],
										'total' => $product['total'],
										'paid' => $product['paid'],
										'remaining' => ($product['total']-$product['paid']),];
					 }
		
		             
    }
	$response= $result; 
	
	//print_r($response);
		return json_encode($response);
	}
	 
	 
	public function actionGet_purchaselist()
    {
	
		 $response='';
       	$vendor_id=$_GET['vendor_id'];
		
		
	$purchase_all = \app\models\Purchase::find()
					->where("(`tbl_purchase`.`total`>`tbl_purchase`.`paid`) and vendor_id=".$vendor_id)->all();
	$supplier_details=Vendor::find()->where(['id'=>$vendor_id])->one();	
					if($supplier_details['opening_balance']!='' && $supplier_details['opening_balance']>$supplier_details['paid'])
					{
						$result[] =  ['status' => 1,
								        'vendor_id' => $_GET['vendor_id'],
										'id' => 'opening_balance',
										'bill_no' => 'Opening balance',
										'purchase_date' => date('Y'),
										'total' => $supplier_details['opening_balance'],
										'paid' => isset($supplier_details['paid'])?$supplier_details['paid']:'0',
										'remaining' => ($supplier_details['opening_balance']-$supplier_details['paid']),];
					}

	if(!empty($purchase_all))
	{
		             foreach($purchase_all as $product)
					 {
						  $result[] =  ['status' => 1,
								        'vendor_id' => $product['vendor_id'],
										'id' => $product['id'],
										'bill_no' =>'pur'.$product['id'],
										'purchase_date' => $product['purchase_date'],
										'total' => $product['total'],
										'paid' => $product['paid'],
										'remaining' => ($product['total']-$product['paid']),];
					 }
		
		              
    }
	$response= $result;
	
	//print_r($response);
		return json_encode($response);
	}
	
	
	
	public function actionSalesreports()
    {
		$searchModel = new Sales_reports();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'sales_sno');
        $dataProvider->pagination->pageSize=10;
		/*$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);*/
        return $this->render('sales_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionPurchasereports()
    {
		$searchModel = new Purchase_reports();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		/*$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);*/
        return $this->render('purchase_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionReceiptreports()
    {
		//$searchModel = new Receipt_search();
		$searchModel=new Receipt_reports();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		
        return $this->render('receipt_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionVoucherreports()
    {
		$searchModel=new Voucher_reports();
		//$searchModel = new Voucher_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		
        return $this->render('voucher_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
   
}
