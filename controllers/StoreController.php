<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Purchase;
use app\models\Vendor;
use app\models\Shop;
use app\models\Shopsearch;
use app\models\Purchasesearch;
use app\models\Products;
use app\models\Productsearch;
use app\models\Inventory;
use app\models\Accesories;
use app\models\Stock_reports;
use app\models\Complaint;
use app\models\Complaintsearch;
use app\models\Stocksearch;
use app\models\Sales;
use app\models\Group_sales;
use yii\helpers\ArrayHelper;

class StoreController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
	  public function actionIndex()
    {
       	$searchModel = new Complaintsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['priority'=>SORT_DESC],]);
        return $this->render('complaint', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	  public function actionStock()
    {
       	$searchModel = new Stocksearch();
		$description='';
		if(isset($_GET['description']))
		{
		 $description=$_GET['description'];
		}
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$description);
        $dataProvider->pagination->pageSize=10;
		/*$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);*/
        return $this->render('stock', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'description' => $description,
        ]);
    }
	
	 public function actionStockreports()
    {
       	$searchModel = new Stock_reports();
		$product_id='';
		if(isset($_GET['id']))
		{
		 $product_id=$_GET['id'];
		}
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$product_id);
        $dataProvider->pagination->pageSize=10;
		
        return $this->render('stock_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			//'description' => $description,
        ]);
    }
	 
	 
	 public function actionNewsales()
    {
				$model = new Purchase();
				
				$items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
				$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
				if ($model->load(Yii::$app->request->post()))
				 {
					$model->shop = $_POST['Purchase']['shop'];
					$product_id=$_POST['product_id'];
					$quantity=$_POST['qty'];
					$price=$_POST['price'];
					  $complaint_id=$_POST['complaint_id'];
					  Inventory::deleteAll(['complaint_id' =>$complaint_id]);
						for($i=0;$i<count($product_id);$i++)
						{
							$Inventory = new Inventory();
							$Inventory->complaint_id =$complaint_id;
							$Inventory->product_id =$product_id[$i];
							$Inventory->sold_price =$price[$i];
							$Inventory->qty ='-'.$quantity[$i];
							$Inventory->shop =$model->shop;
							$Inventory->type ='sales';
							$Inventory->save();
							//$Products->isNewRecord = true;
						}
						  return Yii::$app->response->redirect(array('store/index'));
				
				} 
				else {
					
					  return $this->render('new_purchase', ['model' => $model,'items1'=>$items1,'product_details'=>$product_details]);
					}
          }
		 public function actionUpdatesales()
         {
        
        $complaint_id = $_GET['id'];
	//	$model1 = Inventory::findOne(['complaint_id' =>$complaint_id ]);
	    
		$model = new Purchase();
        $items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
		
		
	
		
		 if ($model->load(Yii::$app->request->post())) 
		{
			/*$Is_insert_table = Sales::find(['complaint_id' =>$complaint_id])->one();
			echo $Is_insert_table->createCommand()->getRawSql();
			*/
			/*echo $complaint_id;
			$query = Sales::find()->where(['complaint_id' =>$complaint_id]);
           echo $query->createCommand()->sql;*/
			//print_r($Is_insert_table);
			$Is_insert_table = Sales::findOne(['complaint_id' =>$complaint_id]);
			if(isset($Is_insert_table->complaint_id))
			{
				$Sales = Sales::findOne(['complaint_id' =>$complaint_id]);
			    $Sales->service_sno =$Sales['service_sno'];
			}
			else
			{
				$Sales=new Sales();
				$sales_table = Sales::find('not',['service_sno' =>NULL])->orderBy(['(id)' => SORT_DESC])->one();
				$service_sno=$sales_table['service_sno']+1;
				$Sales->service_sno =$service_sno;
			}
			//exit();
            
		            $model->shop = $_POST['Purchase']['shop'];
					$product_id=$_POST['product_id'];
					$quantity=$_POST['qty'];
					$price=$_POST['price'];
					$complaint_id=$_POST['complaint_id'];
					$action=$_POST['action'];
					$total=$_POST['subtotal'];
			        $sno=$_POST['s_no'];
					$main_total=$_POST['total'];
					$discount=$_POST['discount'];
					$vat=$_POST['vat'];
					$trans=$_POST['trans'];
					$customer_id=$_POST['customer_id'];
				
							Inventory::deleteAll(['complaint_id' =>$complaint_id]);
							//Sales::deleteAll(['complaint_id' =>$complaint_id]);
							Group_sales::deleteAll(['complaint_id' =>$complaint_id]);
							//$Delete_product->delete();
					   $Sales->complaint_id = $complaint_id;
					   $Sales->customer_id = $customer_id;
					   $Sales->discount = $discount;
			           $Sales->vat =$vat;
			           $Sales->trans =$trans;
			           $Sales->sales_date = Date('Y-m-d');
			           $Sales->total =array_sum($main_total);
					   
					   if ($Sales->save(false))
					  {
						     $sales_id=$Sales->id;
							 for($i=0;$i<count($product_id);$i++)
							 {
								    $Group_sales = new Group_sales();
									$Group_sales->sales_id=$sales_id;
									$Group_sales->total=$main_total[$i];
									$Group_sales->qty=1;
									$Group_sales->complaint_id =$complaint_id;
									//$Group_sales->customer_id =$customer_id;
									$Group_sales->save();
									$last_group_id=$Group_sales->group_id;
								 
									$Inventory = new Inventory();
									$Inventory->sales_id =$sales_id;
									$Inventory->complaint_id =$complaint_id;
									$Inventory->customer_id =$customer_id;
									$Inventory->product_id =$product_id[$i];
									$Inventory->price =$price[$i];
									$Inventory->total =$total[$i];
					                $Inventory->sno =$sno[$i];
									$Inventory->qty ='-1';
									$Inventory->shop =$model->shop;
									$Inventory->type ='sales';
									$Inventory->group_id_sales =$last_group_id;
									$Inventory->save();
									//$Products->isNewRecord = true;
								}
								if(isset($action))
								{
									return Yii::$app->response->redirect(array('/complaint/complaint'));
								}
								else
								{
								  return Yii::$app->response->redirect(array('store/index'));
								}
								  
					  }
				
            
        } 
		else {
            
			  return $this->render('new_purchase', ['model' => $model,'items1'=>$items1,'product_details'=>$product_details]);
           }
     }
   
        }
