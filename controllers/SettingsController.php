<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Accounts;
use app\models\ContactForm;
use app\models\Vat;
use app\models\Vat_search;
use app\models\Income_type;
use app\models\Expense;
use webvimark\modules\UserManagement\models\rbacDB\Role;
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
	 public function actionAdd_vat()
    {
        $model = new Vat();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->vat_name = $_POST['Vat']['vat_name'];
			$model->vat = $_POST['Vat']['vat'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/vat'));
				 
			}

            
        } else {
            
			return $this->render('add_vat', ['model' => $model]);
        }
    }
	
	public function actionUpdate_vat()
    {
        $vat_id = $_GET['id'];
		$model = Vat::findOne(['id' =>$vat_id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->vat_name = $_POST['Vat']['vat_name'];
			$model->vat = $_POST['Vat']['vat'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/vat'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('add_vat', ['model' => $model]);
        }
    }
	
	
	public function actionSettings()
    {
        
		  
          if (Yii::$app->request->post()) 
		  {
			Yii::$app->mycomponent->UpdateSettings('company_name',Yii::$app->request->post('company_name'));
		    Yii::$app->mycomponent->UpdateSettings('address',Yii::$app->request->post('address'));
			Yii::$app->mycomponent->UpdateSettings('tin_number',Yii::$app->request->post('tin_number'));
			Yii::$app->mycomponent->UpdateSettings('cst_number',Yii::$app->request->post('cst_number'));
		    Yii::$app->mycomponent->UpdateSettings('contact_number1',Yii::$app->request->post('contact_number1'));
			Yii::$app->mycomponent->UpdateSettings('contact_number2',Yii::$app->request->post('contact_number2'));
			Yii::$app->mycomponent->UpdateSettings('contact_number3',Yii::$app->request->post('contact_number3'));
			Yii::$app->mycomponent->UpdateSettings('city',Yii::$app->request->post('city'));
			Yii::$app->mycomponent->UpdateSettings('state',Yii::$app->request->post('state'));
			Yii::$app->mycomponent->UpdateSettings('title',Yii::$app->request->post('title'));
			Yii::$app->mycomponent->UpdateSettings('email',Yii::$app->request->post('email'));
			Yii::$app->mycomponent->UpdateSettings('website',Yii::$app->request->post('website'));
			
			Yii::$app->mycomponent->UpdateSettings('bank_name',Yii::$app->request->post('bank_name'));
			Yii::$app->mycomponent->UpdateSettings('branch',Yii::$app->request->post('branch'));
			Yii::$app->mycomponent->UpdateSettings('account_number',Yii::$app->request->post('account_number'));
			Yii::$app->mycomponent->UpdateSettings('ifsc',Yii::$app->request->post('ifsc'));

			return Yii::$app->response->redirect(array('settings/settings'));
			
		  }
		  else
		    {			
				return $this->render('settings');
			}
	}
	
	
	
	public function actionAdd_accounts()
    {
        
		  $model = new Accounts();
          if (Yii::$app->request->post()) 
		  {
			$model->account_name = $_POST['account_name'];
		    $model->payment_type = $_POST['payment_type'];
			$model->days = $_POST['days'];
			$model->bank_name = $_POST['bank_name'];
			$model->account_number = $_POST['account_number'];
			$model->branch = $_POST['branch'];
			$model->ifsc = $_POST['ifsc'];

			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/accounts'));
				 
			}
			
		  }
		  else
		    {			
				return $this->render('accounts');
			}
	}
	
	public function actionUpdate_accounts()
    {
        $id = $_GET['id'];
		$accounts = Accounts::findOne(['id' =>$id ]);
		
        if (Yii::$app->request->post()) {
			if($accounts->payment_type==$_POST['payment_type'])
			{
				$model = Accounts::findOne(['id' =>$id ]);
			}
			else
			{
				$model = new Accounts();
			}
			if($_POST['payment_type']=='bank')
			{
				$model->bank_name = $_POST['bank_name'];
				$model->account_number = $_POST['account_number'];
				$model->branch = $_POST['branch'];
				$model->ifsc = $_POST['ifsc'];
			}
		
            $model->account_name = $_POST['account_name'];
			$model->payment_type = $_POST['payment_type'];
			$model->days = $_POST['days'];
			

			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/accounts'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('accounts',['accounts'=>$accounts]);
        }
    }
	
	public function actionAccounts()
    {
		$searchModel = new Accounts();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('accounts_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }

    public function actionDelete_accounts()
    {
		$account_id = $_GET['id'];
		$model = Accounts::DeleteAll(['id' =>$account_id ]);
		return Yii::$app->response->redirect(array('settings/accounts'));
      
    }
	
	
	
	public function actionVat()
    {
		$searchModel = new Vat_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('vat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionDelete_vat()
    {
		$vat_id = $_GET['id'];
		$model = Vat::DeleteAll(['id' =>$vat_id ]);
		return Yii::$app->response->redirect(array('settings/vat'));
      
    }
	
	
	 public function actionAdd_expense()
    {
        $model = new Expense();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->expense = $_POST['Expense']['expense'];
			$model->description = $_POST['Expense']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/expense'));
				 
			}

            
        } else {
            
			return $this->render('add_expense', ['model' => $model]);
        }
    }
	public function actionUpdate_expense()
    {
        $Expense_id = $_GET['id'];
		$model = Expense::findOne(['id' =>$Expense_id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->expense = $_POST['Expense']['expense'];
			$model->description = $_POST['Expense']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/expense'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('add_expense', ['model' => $model]);
        }
    }
	
	public function actionExpense()
    {
		$searchModel = new Expense();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('expense', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionDelete_expense()
    {
		$Expense_id = $_GET['id'];
		$model = Expense::DeleteAll(['id' =>$Expense_id ]);
		return Yii::$app->response->redirect(array('settings/expense'));
      
    }
	
	
	 public function actionAdd_income()
    {
        $model = new Income_type();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->income_name = $_POST['Income_type']['income_name'];
			$model->description = $_POST['Income_type']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/income'));
				 
			}

            
        } else {
            
			return $this->render('add_income', ['model' => $model]);
        }
    }
	
	
	public function actionUpdate_income()
    {
        $income_id = $_GET['id'];
		$model = Income_type::findOne(['id' =>$income_id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->income_name = $_POST['Income_type']['income_name'];
			$model->description = $_POST['Income_type']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('settings/income'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('add_income', ['model' => $model]);
        }
    }
	
	public function actionIncome()
    {
		$searchModel = new Income_type();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('income', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionDelete_income()
    {
		$Expense_id = $_GET['id'];
		$model = Income_type::DeleteAll(['id' =>$Expense_id ]);
		return Yii::$app->response->redirect(array('settings/income'));
      
    }
	
   
}
