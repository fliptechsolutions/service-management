<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Vat;
use app\models\Income_reports;
use app\models\Income;
use app\models\Expense;
use webvimark\modules\UserManagement\models\rbacDB\Role;
class IncomeController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

	
	 public function actionAdd_income()
    {
        $model = new Income();
	    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->income_id = $_POST['income_id'];
			$model->exp_date = $_POST['exp_date'];
			$model->amount = $_POST['Income']['amount'];
			$model->description = $_POST['Income']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('income/income'));
				 
			}

            
        } else {
            
			return $this->render('add_income', ['model' => $model]);
        }
    }
	public function actionUpdate_income()
    {
        $Expense_id = $_GET['id'];
		$model = Income::findOne(['id' =>$Expense_id ]);
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->income_id = $_POST['income_id'];
			$model->exp_date = $_POST['exp_date'];
			$model->amount = $_POST['Income']['amount'];
			$model->description = $_POST['Income']['description'];
           
			
			if ($model->save(false))
			{
				return Yii::$app->response->redirect(array('income/income'));
				 
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('add_income', ['model' => $model]);
        }
    }
	
	public function actionIncome()
    {
		$searchModel = new Income();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('income', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionIncomereports()
    {
		$searchModel = new Income_reports();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		
        return $this->render('income_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionDelete_income()
    {
		$Expense_id = $_GET['id'];
		$model = Income::DeleteAll(['id' =>$Expense_id ]);
		return Yii::$app->response->redirect(array('income/income'));
      
    }
   
}
