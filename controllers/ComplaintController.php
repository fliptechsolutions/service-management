<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Sales_search;
use app\models\Complaint;
use app\models\Complaintsearch;
use app\models\Complaintmerge;
use app\models\Job;
use app\models\Customer;
use app\models\Customersearch;
use app\models\Customvalidation;
use app\models\Vendor;
use app\models\Vendorsearch;
use app\models\Purchase;
use app\models\Products;
use app\models\Stocksearch;
use app\models\Sales;
use app\models\Group_sales;
use app\models\Shop;

use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use app\models\Inventory;

class ComplaintController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
	 
	 
	 public function actionReport() {
		 $this->layout = false;
		// get your HTML raw content without any layouts or scripts
	    $htmlContent = $this->renderPartial('reportView', ['model'=>$_GET['id']]);
				// $htmlContent = 'kumar';
			$pdf = Yii::$app->pdf;
		$pdf->content = $htmlContent;
		return $pdf->render();
       }
	   
    public function actionGet_team_member() 
	{
		$team_users='';
		$team_name=$_GET['team_name'];
		$Results = Yii::$app->db->createCommand("SELECT user_id FROM auth_assignment Where item_name='$team_name'")->queryAll(); 
		foreach($Results as $user)
		{
		  $result=Yii::$app->mycomponent->Get_user($user['user_id']);
		  $team_users[$user['user_id']]=$result['firstname'];
		}
		               $response =  ['status' => 1,
								    'team_users' => $team_users,];
		return json_encode($response);
    }
	   
	    public function actionBill() {
		 $this->layout = false;
		// get your HTML raw content without any layouts or scripts
	    $htmlContent = $this->renderPartial('billView');
				// $htmlContent = 'kumar';
			$pdf = Yii::$app->pdf;
		$pdf->content = $htmlContent;
		return $pdf->render();
       }		   
	 
    public function actionIndex()
    {
       	$searchModel = new Complaintsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['priority'=>SORT_DESC],]);
        return $this->render('complaint', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionEstimate()
    {
		$searchModel = new Sales_search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,'service_sno');
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('sales', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionRegister()
    {
		$searchModel = new Customvalidation();
       return $this->render('register',['model'=>$searchModel]);
    }
	public function actionView_complaint()
    {
		//$searchModel = new Customvalidation();
       return $this->render('view_complaint');
    }
	public function actionJobassign()
    {
		$type=$_GET['type'];
		$complaint_id=$_GET['complaint_id'];
		//Yii::$app->mycomponent->Complaint_create();
       return $_GET['type'];
    }
	public function actionCreate()
    {
		 $redirect=Yii::$app->request->post('render');
        $complaint_id=Yii::$app->mycomponent->Complaint_create();
		if($complaint_id!='')
		{
	      return $this->redirect([$redirect,'id'=>$complaint_id]);
		}
		else
		{
			return $this->redirect([$redirect]);
		}
		
    }
	
	public function actionMob_validation()
    {
		
        $result=Yii::$app->mycomponent->Mob_validation($_GET['mob']);
		if($result)
						{
						$response = ['status' => 1,
								'results' => $result,
								];
						}
						else
						{
							$response = ['status' => 2,
								'results' =>'No results found',
								];
						}
	    return json_encode($response);
    }

	public function actionComplaint()
    {
		$searchModel = new Complaintsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['priority'=>SORT_DESC],]);
        return $this->render('complaint', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionNewcustomer()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // valid data received in $model
			$model->first_name = $_POST['Customer']['first_name'];
            $model->last_name = $_POST['Customer']['last_name'];
            $model->Address = $_POST['Customer']['Address'];
            $model->city = $_POST['Customer']['city'];
			$model->contact_phone = $_POST['Customer']['contact_phone'];
			$model->email_address = $_POST['Customer']['email_address'];
			$model->company_name = $_POST['Customer']['company_name'];
			$model->tin_no = $_POST['Customer']['tin_no'];
			$model->office_no = $_POST['Customer']['office_no'];
			$model->date_created = date('Y-m-d H:i:s');
			$model->pincode = $_POST['Customer']['pincode'];
			$model->state = $_POST['Customer']['state'];
			$model->cst_number = $_POST['Customer']['cst_number'];
			$model->opening_balance = $_POST['Customer']['opening_balance'];
            $model->selling_id = $_POST['selling_id'];
			$model->notes = $_POST['Customer']['notes'];
            // do something meaningful here about $model ...
			if ($model->save())
			{
				//Yii::$app->response->redirect(array('complaint/customers'));
				return Yii::$app->response->redirect(array('complaint/customers'));
			}

            
        } else {
            // either the page is initially displayed or there is some validation error
            //return $this->render('customer', ['model' => $model]);
			return $this->render('new', ['model' => $model]);
        }
    }
	
	public function actionNewvendor()
    {
        $model = new Vendor();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // valid data received in $model
			$model->first_name = $_POST['Vendor']['first_name'];
           // $model->last_name = $_POST['Vendor']['last_name'];
            $model->Address = $_POST['Vendor']['Address'];
            $model->city = $_POST['Vendor']['city'];
			$model->contact_phone = $_POST['Vendor']['contact_phone'];
			$model->email_address = $_POST['Vendor']['email_address'];
			$model->company_name = $_POST['Vendor']['company_name'];
			$model->tin_no = $_POST['Vendor']['tin_no'];
			$model->office_no = $_POST['Vendor']['office_no'];
			$model->state = $_POST['Vendor']['state'];
			$model->pincode = $_POST['Vendor']['pincode'];
			$model->cst_number = $_POST['Vendor']['cst_number'];
			$model->opening_balance = $_POST['Vendor']['opening_balance'];
			$model->office_no = $_POST['Vendor']['pincode'];
			$model->notes = $_POST['Vendor']['notes'];
			if ($model->save())
			{
				//Yii::$app->response->redirect(array('complaint/customers'));
				return Yii::$app->response->redirect(array('complaint/vendor'));
			}

            
        } else {
            // either the page is initially displayed or there is some validation error
            //return $this->render('customer', ['model' => $model]);
			//var_dump($model->errors);
			//print_r($model->getErrors());
			return $this->render('new_vendor', ['model' => $model]);
        }
    }
	
	public function actionDeletecustomer()
    {
		$customer_id = $_GET['id'];
        $model = Customer::findOne(['id' =>$customer_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('complaint/customers'));
    }
	
	public function actionDeletevendor()
    {
		$customer_id = $_GET['id'];
        $model = Vendor::findOne(['id' =>$customer_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('complaint/vendor'));
    }
	public function actionDelete()
    {
		$complaint_id = $_GET['id'];
        $model = Complaint::findOne(['complaint_id' =>$complaint_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('complaint/complaint'));
    }
	
	
	
	public function actionUpdatecustomer()
    {
        $customer_id = $_GET['id'];
		$model = Customer::findOne(['id' =>$customer_id ]);
          //$model = User::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())&& $model->validate()) {
            // valid data received in $model
			$model->first_name = $_POST['Customer']['first_name'];
            $model->last_name = $_POST['Customer']['last_name'];
            $model->Address = $_POST['Customer']['Address'];
            $model->city = $_POST['Customer']['city'];
			$model->contact_phone = $_POST['Customer']['contact_phone'];
			$model->email_address = $_POST['Customer']['email_address'];
			$model->date_created = date('Y-m-d H:i:s');
			$model->company_name = $_POST['Customer']['company_name'];
			$model->tin_no = $_POST['Customer']['tin_no'];
			$model->office_no = $_POST['Customer']['office_no'];
			
			$model->pincode = $_POST['Customer']['pincode'];
			$model->state = $_POST['Customer']['state'];
			$model->cst_number = $_POST['Customer']['cst_number'];
			$model->opening_balance = $_POST['Customer']['opening_balance'];
			$model->notes = $_POST['Customer']['notes'];
			$model->selling_id = $_POST['selling_id'];

            // do something meaningful here about $model ...
			if ($model->save())
			{
				//Yii::$app->response->redirect(array('complaint/customers'));
				return Yii::$app->response->redirect(array('complaint/customers'));
			}

            
        } else {
            // either the page is initially displayed or there is some validation error
            //return $this->render('customer', ['model' => $model]);
			return $this->render('new', ['model' => $model]);
        }
    }
	
	public function actionUpdatevendor()
    {
        $customer_id = $_GET['id'];
		$model = Vendor::findOne(['id' =>$customer_id ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
		{
            // valid data received in $model
			$model->first_name = $_POST['Vendor']['first_name'];
            $model->last_name = $_POST['Vendor']['last_name'];
            $model->Address = $_POST['Vendor']['Address'];
            $model->city = $_POST['Vendor']['city'];
			$model->contact_phone = $_POST['Vendor']['contact_phone'];
			$model->email_address = $_POST['Vendor']['email_address'];
			$model->company_name = $_POST['Vendor']['company_name'];
			$model->tin_no = $_POST['Vendor']['tin_no'];
			$model->office_no = $_POST['Vendor']['office_no'];
			$model->pincode = $_POST['Vendor']['pincode'];
			$model->state = $_POST['Vendor']['state'];
			$model->cst_number = $_POST['Vendor']['cst_number'];
			$model->opening_balance = $_POST['Vendor']['opening_balance'];
			$model->office_no = $_POST['Vendor']['pincode'];
			$model->notes = $_POST['Vendor']['notes'];
	
			if ($model->save())
			{
				//Yii::$app->response->redirect(array('complaint/customers'));
				return Yii::$app->response->redirect(array('complaint/vendor'));
			}

            
        } 
		else 
		  {
         
			//print_r($model->getErrors());
			return $this->render('new_vendor', ['model' => $model]);
         }
    }
	
	
	
	public function actionCustomers()
    {
		$searchModel = new Customersearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('customer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionVendor()
    {
		$searchModel = new Vendorsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('vendor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	
	public function actionNewcomplaint()
    {
		$searchModel = new Complaintmerge();
		// $searchModel = new Job();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
        return $this->render('complaint_new', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      //  return $this->render('orders');
    }
	
	public function actionStatus_update()
    {
		$params = ['status' =>$_GET['status']];
		$params1 = ['status' =>$_GET['status'],
		            'complaint_id'=>$_GET['complaint_id'],
					'notes'=>$_GET['notes'],
					'created_date'=>date('Y-m-d H:i:s'),
					'user_id'=>Yii::$app->user->identity->id];
		$res=Yii::$app->db->createCommand()->update('tbl_complaint',$params,"complaint_id='".$_GET['complaint_id']."'")->execute();
		$res=Yii::$app->db->createCommand()->insert('tbl_status_history',$params1)->execute();
	}
	
	 public function actionStatus_change()
    {
		$status1=Yii::$app->mycomponent->Get_status();
	    
		 $params = [':complaint_id' =>$_GET['complaint_id']];
				 $post = Yii::$app->db->createCommand('SELECT * FROM tbl_complaint WHERE complaint_id=:complaint_id', $params)  
				   ->queryOne();
				 
             $Status='<option value="0">Please select</option>';?>
                                <?php
								 foreach($status1 as $st): 
								$selected='';
								if($post['status']==$st)
								{
									 $selected='selected="selected"';
								}
								
								?>
                          <?php     $Status.=' <option value='.$st.' '.$selected.'>'. ucfirst($st).'</option>';?>
                                <?php endforeach; 
                            
		
	return $Status;
    }
	
	
	
	     public function actionSpares()
         {
        
        $complaint_id = $_GET['id'];
	//	$model1 = Inventory::findOne(['complaint_id' =>$complaint_id ]);
	    
		$model = new Purchase();
        $items1 = ArrayHelper::map(Shop::find()->all(), 'id', 'shop');
		$product_details = ArrayHelper::map(Products::find()->all(), 'pid', 'product_name');
		
		
	
		
		 if ($model->load(Yii::$app->request->post())) 
		{
			/*$Is_insert_table = Sales::find(['complaint_id' =>$complaint_id])->one();
			echo $Is_insert_table->createCommand()->getRawSql();
			*/
			/*echo $complaint_id;
			$query = Sales::find()->where(['complaint_id' =>$complaint_id]);
           echo $query->createCommand()->sql;*/
			//print_r($Is_insert_table);
			$Is_insert_table = Sales::findOne(['complaint_id' =>$complaint_id]);
			if(isset($Is_insert_table->complaint_id))
			{
				$Sales = Sales::findOne(['complaint_id' =>$complaint_id]);
			    $Sales->service_sno =$Sales['service_sno'];
			}
			else
			{
				$Sales=new Sales();
				$sales_table = Sales::find('not',['service_sno' =>NULL])->orderBy(['(id)' => SORT_DESC])->one();
				$service_sno=$sales_table['service_sno']+1;
				$Sales->service_sno =$service_sno;
			}
			//exit();
            
		            $model->shop = $_POST['Purchase']['shop'];
					$product_id=$_POST['product_id'];
					$quantity=$_POST['qty'];
					$price=$_POST['price'];
					$complaint_id=$_POST['complaint_id'];
					$action=$_POST['action'];
					$total=$_POST['subtotal'];
			        $sno=$_POST['s_no'];
					$main_total=$_POST['total'];
					$discount=$_POST['discount'];
					$vat=$_POST['vat'];
					$trans=$_POST['trans'];
					$customer_id=$_POST['customer_id'];
				
							Inventory::deleteAll(['complaint_id' =>$complaint_id]);
							//Sales::deleteAll(['complaint_id' =>$complaint_id]);
							Group_sales::deleteAll(['complaint_id' =>$complaint_id]);
							//$Delete_product->delete();
					   $Sales->complaint_id = $complaint_id;
					   $Sales->customer_id = $customer_id;
					   $Sales->discount = $discount;
			           $Sales->vat =$vat;
			           $Sales->trans =$trans;
			           $Sales->sales_date = Date('Y-m-d');
			           $Sales->total =array_sum($main_total);
					   
					   if ($Sales->save(false))
					  {
						     $sales_id=$Sales->id;
							 for($i=0;$i<count($product_id);$i++)
							 {
								    $Group_sales = new Group_sales();
									$Group_sales->sales_id=$sales_id;
									$Group_sales->total=$main_total[$i];
									$Group_sales->qty=1;
									$Group_sales->complaint_id =$complaint_id;
									//$Group_sales->customer_id =$customer_id;
									$Group_sales->save();
									$last_group_id=$Group_sales->group_id;
								 
									$Inventory = new Inventory();
									$Inventory->sales_id =$sales_id;
									$Inventory->complaint_id =$complaint_id;
									$Inventory->customer_id =$customer_id;
									$Inventory->product_id =$product_id[$i];
									$Inventory->price =$price[$i];
									$Inventory->total =$total[$i];
					                $Inventory->sno =$sno[$i];
									$Inventory->qty ='-1';
									$Inventory->shop =$model->shop;
									$Inventory->type ='sales';
									$Inventory->group_id_sales =$last_group_id;
									$Inventory->save();
									//$Products->isNewRecord = true;
								}
							
									return Yii::$app->response->redirect(array('/complaint/complaint'));
								
								  
					  }
				
            
        } 
		else {
            
			  return $this->render('spares_add', ['model' => $model,'items1'=>$items1,'product_details'=>$product_details]);
           }
     }
	
}
