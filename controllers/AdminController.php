<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Team;
use app\models\Teamsearch;
use app\models\Updateteam;
use app\models\Roll;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use app\models\UploadForm;
use app\models\Attendance;
use yii\web\UploadedFile;
use kartik\growl\Growl;
class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
	// public $layout=false;
    public function behaviors()
    {
       return [
        'ghost-access'=> [
            'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
        ],
    ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
	 
	public function actionUpload_settings()
	{
	    

		$model = new UploadForm();
		$images=$_FILES['imageFiles'];
		
		$i=0;
		$filenames = $images['name'];

// loop and process files
		for($i=0; $i < count($filenames); $i++)
		{
					 $ext = explode('.', basename($filenames[$i]));
					 $t_path=  md5(uniqid()) . "." . array_pop($ext);
					 $target = "documents" . DIRECTORY_SEPARATOR . $t_path;
					 $model->user_id=$_GET['id'];
					 $model->document=$t_path;
					if(move_uploaded_file($images['tmp_name'][$i], $target)) {
						//echo $target;
						$success = true;
						$paths[] = $target;
						$model->save(false);
					} else {
						$success = false;
						break;
					}
          }
		  
		if ($success === true) {
		
		$output = ['sucess'=>'File uploaded'];
		} elseif ($success === false) {
		$output = ['error'=>'Error while uploading images. Contact the system administrator'];
		
		foreach ($paths as $file) {
		unlink($file);
		}
		} else {
		$output = ['error'=>'No files were processed.'];
		}
		
		// return a json encoded response for plugin to process successfully
		echo json_encode($output);
	}
 
	 public function actionNewuser()
    {
        $model = new Team();
		$model1 = new Roll();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->username = $_POST['Team']['email'];
			$model->firstname = $_POST['Team']['firstname'];
           // $model->last_name = $_POST['Team']['last_name'];
            $model->address = $_POST['Team']['address'];
            $model->city = $_POST['Team']['city'];
			$model->contact_phone = $_POST['Team']['contact_phone'];
			$model->email = $_POST['Team']['email'];
			//$model->password_hash =Yii::$app->security->generatePasswordHash($_POST['Team']['password_hash']);
			$model->password_hash =password_hash($_POST['Team']['password_hash'], PASSWORD_DEFAULT);
			$model->roll = $_POST['roll'];
			$model->auth_key = '';
			$model->qulification = $_POST['Team']['qulification'];
			$model->state = $_POST['Team']['state'];
			$model->gaurdian_mob = $_POST['Team']['gaurdian_mob'];
			$model->aathar_no = $_POST['Team']['aathar_no'];
			$model->blood_group = $_POST['Team']['blood_group'];
			//$model->password_hash =Yii::$app->security->generatePasswordHash(['newPassword']);
			//echo $_POST['Updateteam']['newPassword'];
			//exit();
			$profile= UploadedFile::getInstance($model, 'profile');
			
          
			if (isset($profile) && $profile!='') 
			{
				$img_name = rand(1,1000);
			    $model->profile = UploadedFile::getInstance($model, 'profile'); 
			               
			   $model->profile->saveAs('uploads/' . $img_name . $model->profile->baseName. '.' . $model->profile->extension);
			   $model->profile = $img_name.$model->profile->baseName. '.' . $model->profile->extension; 
			}
			
			if ($model->save(false))
			{
				$model1->user_id =$model->id;
				$model1->item_name =$model->roll;
				  if($model1->save(false))
				  {
					return Yii::$app->response->redirect(array('admin/users'));
				  }
			}

            
        } else {
            
			return $this->render('new_user', ['model' => $model]);
        }
    }
	
	public function actionUpdateuser()
    {
        $user_id = $_GET['id'];
		$model = Updateteam::findOne(['id' =>$user_id ]);
		$model1 = Roll::findOne(['user_id' =>$user_id ]);
          //$model = User::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
			$model->username = $_POST['Updateteam']['email'];
			$model->firstname = $_POST['Updateteam']['firstname'];
            $model->last_name = $_POST['Updateteam']['last_name'];
            $model->address = $_POST['Updateteam']['address'];
            $model->city = $_POST['Updateteam']['city'];
			$model->contact_phone = $_POST['Updateteam']['contact_phone'];
			$model->email = $_POST['Updateteam']['email'];
			
			$model->qulification = $_POST['Updateteam']['qulification'];
			$model->state = $_POST['Updateteam']['state'];
			$model->gaurdian_mob = $_POST['Updateteam']['gaurdian_mob'];
			$model->aathar_no = $_POST['Updateteam']['aathar_no'];
			$model->blood_group = $_POST['Updateteam']['blood_group'];
			//$model->password_hash =Yii::$app->security->generatePasswordHash(['newPassword']);
			//echo $_POST['Updateteam']['newPassword'];
			//exit();
			$profile= UploadedFile::getInstance($model, 'profile');
			
          
			if (isset($profile) && $profile!='') 
			{
				$img_name = rand(1,1000);
			    $model->profile = UploadedFile::getInstance($model, 'profile'); 
			               
			   $model->profile->saveAs('uploads/' . $img_name . $model->profile->baseName. '.' . $model->profile->extension);
			   $model->profile = $img_name.$model->profile->baseName. '.' . $model->profile->extension; 
			}
			
			if(isset($_POST['Updateteam']['newPassword']))
			{
		      $model->password_hash =password_hash($_POST['Updateteam']['newPassword'], PASSWORD_DEFAULT);
			}
			$model->roll = $_POST['roll'];
			
			if ($model->save())
			{
				
				$model1->item_name =$model->roll;
				  if($model1->save(false))
				  {
				
				       return Yii::$app->response->redirect(array('admin/users'));
				  }
			}

            
        } else {
			//print_r($model->getErrors());
            
			return $this->render('update_user', ['model' => $model]);
        }
    }
	
	
	public function actionUsers()
    {
		$searchModel = new Teamsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionAdminattendance()
    {
		$searchModel = new Teamsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('attendance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionAttendance_reports()
    {
		$searchModel = new Attendance();
		$id=Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('attendance_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	public function actionReportsattanance()
    {
		$searchModel = new Attendance();
		$id=Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination->pageSize=10;
		$dataProvider->setSort([
        'defaultOrder' => ['id'=>SORT_DESC],]);
        return $this->render('attendance_reports_user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
    }
	
	public function actionSaveattendance()
    {
		$date = date('Y-m-d');
		if(isset($_POST['att_date']))
		{
		  $date = $_POST['att_date'];
		}
		 $new_att = new Attendance();
        $model = Attendance::findOne(['user_id' =>$_POST['user_id'],'created_date' =>date('Y-m-d')]);
		if(!empty($model))
		{
			$model->user_id=$_POST['user_id'];
			$model->attendance_date=$date;
			$model->created_date=date('Y-m-d');
			$model->attendance=$_POST['att'];
			$model->save();
		}
		else
		{
			$new_att->user_id=$_POST['user_id'];
			$new_att->attendance_date=$date;
			$new_att->created_date=date('Y-m-d');
			$new_att->attendance=$_POST['att'];
			$new_att->save();
		}
			echo Growl::widget([
			'type' => Growl::TYPE_SUCCESS,
			'title' => 'Success!',
			'icon' => 'glyphicon glyphicon-ok-sign',
			'body' => 'This product deleted',
			'showSeparator' => true,
			'delay' => 1000,
			'pluginOptions' => [
			'showProgressbar' => true,
			'placement' => [
			'from' => 'top',
			'align' => 'right',
			]
			]
			]);
    	//return Yii::$app->response->redirect(array('admin/users'));
    }
	
	
	public function actionUserattendance()
    {
	
    	 return $this->render('user_attendance');
    }
	
	
	public function actionDeleteuser()
    {
		$user_id = $_GET['id'];
        $model = Team::findOne(['id' =>$user_id ]);
    	$model->delete();
       return Yii::$app->response->redirect(array('admin/users'));
    }
	
	 
    public function actionIndex()
    {
	  return $this->render('index');
    }
	public function actionRegister()
    {
       return $this->render('register');
    }
	public function actionRole()
    {
       return $this->render('role');
    }
	
	public function actionFront()
    {
       return $this->render('front_list');
    }
	public function actionFrontadd()
    {
       return $this->render('front_desc');
    }
	
	public function actionCheckingadd()
    {
       return $this->render('checking');
    }
	public function actionChecking()
    {
       return $this->render('checking_list');
    }
	
	public function actionChip()
    {
       return $this->render('chip_engi_list');
    }
	public function actionChipadd()
    {
       return $this->render('chiplevel_add');
    }
	
	public function actionBga()
    {
       return $this->render('bga');
    }
	public function actionBgaadd()
    {
       return $this->render('bga_add');
    }
	public function actionRoleassign()
    {
		/*echo $job_id= $_POST['job_id'];
		$params=array('status'=>'completed');
		$res=Yii::$app->db->createCommand()->update('tbl_job_assign',$params,'id="'.$job_id.'"')->execute();
		print_r($res);*/
		return Yii::$app->mycomponent->job_assign();
    }
	
	public function actionCard()
    {
       return $this->render('card');
    }
	public function actionCardadd()
    {
       return $this->render('card_add');
    }
	
	
     public function actionUsercreate()
    {
		
         Yii::$app->mycomponent->Usercreate();
		 $redirect=Yii::$app->request->post('render');
	     return $this->redirect($redirect);
    }
	public function actionDeleteusers()
    {
		  $where=[$_GET['where_name']=>$_GET['where_value']];
		 // $where1=['user_id'=>$_GET['where_value']];
		  Yii::$app->mycomponent->Delete($_GET['tbl_name'],$where);
		  Yii::$app->mycomponent->Delete('user',$where);
			//return $this->render($_GET['render']);
			$this->redirect($_GET['render']);
			
			
	//	return $this->render('settings');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
