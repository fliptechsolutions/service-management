<?php
use kartik\mpdf\Pdf;
$params = require(__DIR__ . '/params.php');


$config = [
    'defaultRoute' => 'admin',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
		
	 'pdf' => [
        'class' => Pdf::classname(),
        'format' => Pdf::FORMAT_A4,
        'orientation' => Pdf::ORIENT_PORTRAIT,
        'destination' => Pdf::DEST_BROWSER,
        // refer settings section for all configuration options
    ],
		
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'i1EcvwV8IqkEMNiC1ptLk6fm8I52lCu1',
        ],
		'mycomponent' => [
 
            'class' => 'app\components\Functions',
 
            ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
		'urlManager' => [
			  //'class' => 'yii\web\UrlManager',
					'enablePrettyUrl' => true,
					'showScriptName' => false,
					 'rules' => ['admin/frontadd/<id:\d+>' => 'admin/frontadd/', 
					             'admin/checkingadd/<id:\d+>' => 'admin/checkingadd/', 
								 'admin/chipadd/<id:\d+>' => 'admin/chipadd/',
								 'inventory/update_receipt/<id:\d+>' => 'inventory/update_receipt/',
								  'complaint/register/<id:\d+>' => 'complaint/register/',
								  'complaint/view_complaint/<id:\d+>' => 'complaint/view_complaint/',
								  'complaint/updatecustomer/<id:\d+>' => 'complaint/updatecustomer/',
								  'complaint/updatevendor/<id:\d+>' => 'complaint/updatevendor/',
								  'inventory/updateshop/<id:\d+>' => 'inventory/updateshop/',
								  'settings/update_accounts/<id:\d+>' => 'settings/update_accounts/',
								  'inventory/updatesales/<id:\d+>' => 'inventory/updatesales/',
								  'inventory/updatepurchase/<id:\d+>' => 'inventory/updatepurchase/',
								  'inventory/update_voucher/<id:\d+>' => 'inventory/update_voucher/',
								   'store/stockreports/<id:\d+>' => 'store/stockreports/',
								//  '<inventory:[\w-]+>/<voucherreports:[\w-]+>'=>'inventory/voucherReports',
								    //'inventory/voucherreports'=>'VoucherReports/<action>',
								  'store/newsales/<id:\d+>' => 'store/newsales/',],
					   
			       
            ],
		
		'user' => [
        'class' => 'webvimark\modules\UserManagement\components\UserConfig',

        // Comment this if you don't want to record user logins
        'on afterLogin' => function($event) {
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
            }
    ],
	
	'view' => [
         'theme' => [
             'pathMap' => [
                //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
				'@app/views' => '@app/views/layouts'
             ],
         ],
    ],
	


		
    ],
	
	
	'modules'=>[
	'gridview' =>  [
        'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
    ],
    'user-management' => [
        'class' => 'webvimark\modules\UserManagement\UserManagementModule',

        // 'enableRegistration' => true,

        // Add regexp validation to passwords. Default pattern does not restrict user and can enter any set of characters.
        // The example below allows user to enter :
        // any set of characters
        // (?=\S{8,}): of at least length 8
        // (?=\S*[a-z]): containing at least one lowercase letter
        // (?=\S*[A-Z]): and at least one uppercase letter
        // (?=\S*[\d]): and at least one number
        // $: anchored to the end of the string

        //'passwordRegexp' => '^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$',


        // Here you can set your handler to change layout for any controller or action
        // Tip: you can use this event in any module
        'on beforeAction'=>function(yii\base\ActionEvent $event) {
                if ( $event->action->uniqueId == 'user-management/auth/login' )
                {
                    $event->action->controller->layout = 'loginLayout.php';
                };
            },
    ],
],
	
    'params' => $params,
	
];

/*if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
*/
return $config;
