<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Accounts;
 use app\models\Customer;
 

$this->title = 'Voucher | '.Yii::$app->mycomponent->Get_settings('company_name'); 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_voucher'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['voucher'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Payments'],
    'columns' => [
	     
       
		 
		 [
            'label'=>'Transaction ID',
			'attribute' => 'id',
			'value' => 'id'
           
         ],
		  
		 
		 [
            'label'=>'Suppliers Name',
			'attribute' => 'first_name',
			'value' => 'vendordetails.first_name'
           
         ],
    
		
		 
		 [
            'label'=>'Transaction Amount',
			//'attribute' => 'customer_details',
			'value' => 'amount'
           
         ],
		 
		  
		   [
            'label'=>'Payment Mode',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->pay_type))
				{
					$account=Accounts::find()->where(['id'=>$model->pay_type])->one();
					return $account['payment_type'];
				}
				return '0';
			}
           
          ],
		 
		 [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Purchase date',
				
				'attribute'=>'purchase_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'purchase_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit}  {print}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
				 
				/* 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },*/
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/update_voucher', 'id' =>$model->id]);
                }
                
				if ($action === 'print') {
                    $url = Url::to(['/complaint/bill', 'sales_id'=>$model->id]);
                }                       
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
