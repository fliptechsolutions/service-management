<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
 use kartik\checkbox\CheckboxX;
$this->title = 'Accesories | '.Yii::$app->mycomponent->Get_settings('company_name'); 
//print_r($categories);
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/inventory/newaccesories'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['inventory/accessories'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Accessories</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'acc_name', ['label'=>'Accesories', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'acc_name',['showLabels'=>false])->textInput(['placeholder'=>'Accesories']); ?>
    </div>
    </div>
    
     <div class="form-group">
    <label class='col-sm-2 control-label'>Warning menu</label>
    <div class="col-sm-4">
        <?php  echo 	CheckboxX::widget([
						'name'=>'warning',
						'value'=>$model->warning_menu,
						//'initInputType' => CheckboxX::INPUT_CHECKBOX,
						//'options'=>$options,
						'pluginOptions'=>['threeState'=>false]
					]); ?>
    </div>
    </div>
    
         <div><h4>Categories</h4></div>
     <br />
     
  
           <table style="max-width:420px;" class="table table-striped">
        <thead>
         
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		    if(isset($model->category))
			{
				$res=json_decode($model->category,true);
				
			}
		 foreach($categories as $value): 
		 
		 if($i%2 == 0)
		 {
			 $style='style="float:right;width:49%;text-align:right;"';
		 }
		 else
		 {
			$style='style="float:left;width:49%;text-align:left;"'; 
		 }
		 ?>
       
       
          <tr>
            <td><?php echo $i ?></td>
             <td><?php echo ucfirst($value['cat_name']); ?></td>
            <td>
			<?php
			$val=0;
			if(isset($res[$value['id']]))
			{
				
				$val=$res[$value['id']];
			}
			
			
			 echo 	CheckboxX::widget([
						'name'=>'category['.$value['id'].']',
						'value'=>$val,
						//'options'=>$options,
						'pluginOptions'=>['threeState'=>false]
					]);
			
			 ?></td>
          
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
  
    
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'description', ['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'description',['showLabels'=>false])->textArea(); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
