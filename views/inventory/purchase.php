<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
  use kartik\select2\Select2;
  use app\models\Vendor;
use kartik\growl\Growl;

$this->title = 'Purchase | '.Yii::$app->mycomponent->Get_settings('company_name'); 

if(isset($_SESSION['warning_product']) && $_SESSION['warning_product']==1)
{
	echo Growl::widget([
    'type' => Growl::TYPE_WARNING,
    'title' => 'Warning!',
    'icon' => 'glyphicon glyphicon-exclamation-sign',
    'body' => 'Product already Soldout',
    'showSeparator' => true,
    'delay' => 1000,
    'pluginOptions' => [
        'showProgressbar' => true,
        'placement' => [
            'from' => 'top',
            'align' => 'right',
			]
		]
	]);
}
else if(isset($_SESSION['warning_product']) && $_SESSION['warning_product']==2)
{
	echo Growl::widget([
    'type' => Growl::TYPE_SUCCESS,
    'title' => 'Success!',
    'icon' => 'glyphicon glyphicon-ok-sign',
    'body' => 'This Purchase id is deleted',
    'showSeparator' => true,
    'delay' => 1000,
    'pluginOptions' => [
        'showProgressbar' => true,
        'placement' => [
            'from' => 'top',
            'align' => 'right',
			]
		]
	]);
	
}


unset($_SESSION['warning_product']);


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['newpurchase'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Purchase'],
    'columns' => [
	     
       
		 
		 [
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->id!='')
				 {
                    $model = Inventory::find()->where(['=', 'purchase_id', $model->id])->groupBy(['group_id'])->all();
					
                    return Yii::$app->controller->renderPartial('purchase_list', ['model'=>$model]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			
		],
		
		 [
            'label'=>'Purchase id',
			'value' => 'id',
			'attribute' => 'id'
		
         ],
		 [
            'label'=>'Invoice number',
			'value' => 'invoice_number',
			'attribute' => 'invoice_number'
		
         ],
		
		 [
            'label'=>'Supplier Name',
			'attribute' => 'vendor',
			 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Vendor::find()->orderBy('first_name')->asArray()->all(), 'first_name', 'first_name'),
			'filterInputOptions'=>['placeholder'=>'Select Supplier'],
			'value' => 'vendor.first_name',
			'format'=>'raw'
		
         ],
		 [
            'label'=>'Shop',
			'attribute' => 'shop1',
			'value' => 'shop1.shop'
           
         ],
		 
		 [
            'label'=>'Total',
			//'attribute' => 'customer_details',
			'value' => 'total'
           
         ],
		 
		  [
            'label'=>'Paid',
			//'attribute' => 'customer_details',
			'value' => 'paid'
           
         ],
		 [
            'label'=>'Remaining',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid) && isset($model->total))
				{
					return ($model->total-$model->paid);
				}
				return '0';
			}
           
          ],
		  
		 
		
		[

				//'class'=>'kartik\grid\EditableColumn',
				
				'attribute'=>'purchase_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'purchase_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
		
		
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit} {delete}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/updatepurchase', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/inventory/deleteparchase', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
<?php //\yii\widgets\Pjax::end(); ?>
<?php
$Rolls=Yii::$app->mycomponent->GetRolls();

?>
 
