<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
 use kartik\select2\Select2;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Product | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/newproduct'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/products'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>MODEL</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
    <?php /*?><div class="form-group">
    <?= Html::activeLabel($model, 'product_name',['label'=>'Product name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
       <?= $form->field($model, 'product_name',['showLabels'=>false])->textInput() ?>
        
    </div>
    </div><?php */?>
    
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'type',['label'=>'Product Type', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
       <?php /*?><?= $form->field($model, 'type',['showLabels'=>false])->textInput() ?><?php */?>
       
       
        <?php
				echo Select2::widget([
			'name' => 'Products[type]',
			'value' => isset($model->type_id)?$model->type_id:'',
			'data' => $product_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Product type ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
    </div>
    </div>
    
  
     
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'manufacturer',['label'=>'Manufacturer', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
      <?php /*?> <?= $form->field($model, 'manufacturer',['showLabels'=>false])->textInput() ?><?php */?>
       
        <?php
				echo Select2::widget([
			'name' => 'Products[manufacturer]',
			'value' => isset($model->manufacture_id)?$model->manufacture_id:'',
			'data' => $manufacture_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Product type ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
    </div>
    </div>
     
  <!--  <div style="float:right;margin: 0 35px  0 0;" > 
    
       <p>
					<a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a>				</p>
        
        </div>
    -->
    
    
     <div class="form-group">
    <?= Html::activeLabel($model, 'product_model',['label'=>'Product model', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
    <?php if(isset($_GET['id']))  {  ?>
    <?= $form->field($model, 'product_model',['showLabels'=>false])->textInput() ?>
    <?php } else { ?>
    
       <?= $form->field($model, 'product_model[]',['showLabels'=>false])->textInput() ?>
       <?php } ?>
        
    </div>
    <div class="col-sm-4">
   <?php if(!isset($_GET['id']))  {  ?> <a class="btn btn-success" id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a> <?php } ?>
    </div>
    </div>
     <div class="form-group">
    <?= Html::activeLabel($model, 'description',['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
   <?php if(isset($_GET['id']))  {  ?>
    <?= $form->field($model, 'description',['showLabels'=>false])->textArea() ?>
    <?php } else { ?>
    <?= $form->field($model, 'description[]',['showLabels'=>false])->textArea() ?>
    <?php } ?>
        
    </div>
    </div>
    
    <div class="inputs"></div>
 
   
    
   
   
    <div class="form-group" style="margin-top:10px;">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>

function remove(id)
	{
		//alert(id);
		$('#remove'+id).remove();
	}
	 
 $(document).ready(function(){
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
var i = $('.field').size() + 1;
var result = <?php echo json_encode($product_details)?>;

$('#add').click(function() {
	
	$('<br> <div id="remove'+i+'" class="field"><div class="form-group" ><label class="col-sm-2 control-label">&nbsp;</label><div class="col-sm-4">'+
	'<input type="text" class="form-control"  name="Products[product_model][]" required="required" placeholder="product model"/></div>'+
	'<div class="col-sm-4"><a onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></div></div>'+
	'<div class="form-group" ><label class="col-sm-2 control-label">&nbsp;</label><div class="col-sm-4">'+
	'<textarea class="form-control"  name="Products[description][]" placeholder="Description"/></textarea></div></div>'+
	
	'</div>').fadeIn('slow').appendTo('.inputs');



i++;
});



$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
