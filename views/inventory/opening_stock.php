<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
 use app\models\Products;
 use kartik\growl\Growl;

$this->title = 'Opening stock | '.Yii::$app->mycomponent->Get_settings('company_name');

if(isset($_SESSION['warning_product']) && $_SESSION['warning_product']==1)
{
	echo Growl::widget([
    'type' => Growl::TYPE_WARNING,
    'title' => 'Warning!',
    'icon' => 'glyphicon glyphicon-exclamation-sign',
    'body' => 'Product already Soldout',
    'showSeparator' => true,
    'delay' => 1000,
    'pluginOptions' => [
        'showProgressbar' => true,
        'placement' => [
            'from' => 'top',
            'align' => 'right',
			]
		]
	]);
}
else if(isset($_SESSION['warning_product']) && $_SESSION['warning_product']==2)
{
	echo Growl::widget([
    'type' => Growl::TYPE_SUCCESS,
    'title' => 'Success!',
    'icon' => 'glyphicon glyphicon-ok-sign',
    'body' => 'This Opening stock is deleted',
    'showSeparator' => true,
    'delay' => 1000,
    'pluginOptions' => [
        'showProgressbar' => true,
        'placement' => [
            'from' => 'top',
            'align' => 'right',
			]
		]
	]);
	
}

unset($_SESSION['warning_product']);

$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_stock'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['opening_stock'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Opening Stock'],
    'columns' => [
	     
       
		 
		 [
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->id!='')
				 {
                    $model = Inventory::find()->where(['=', 'stock_id', $model->stock_id])->groupBy(['group_stock_id'])->all();
					
                    return Yii::$app->controller->renderPartial('purchase_list', ['model'=>$model]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			
		],
		[
            'label'=>'ID',
			'value' => 'stock_id'
		
         ],
		
		
		 [
            'label'=>'Product Name',
			'value' => 'product.product_name'
		
         ],
		 
		 [
            'label'=>'Product Model',
			'value' => 'product.product_model'
		
         ],
		 
		[
            'label'=>'Product Manufacturer',
			'value' => 'product.manufacturer'
		
         ],
		
		 
		 
		
		  
		 [
		   'label'=>'Date',  
            'attribute'=>'opening_stock.stock_date',
			
           
        ],
		
		
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit} {delete}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/update_stock', 'id' =>$model->stock_id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/inventory/deletestock', 'id' =>$model->stock_id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);
?>
