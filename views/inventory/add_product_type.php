<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
 use kartik\select2\Select2;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Product type | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/inventory/addproduct'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['inventory/product_type'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Product Type</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'product_type', ['label'=>'Product type', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
    <?php if(isset($_GET['id']))  {  ?>
    <?= $form->field($model, 'product_type',['showLabels'=>false])->textInput() ?>
    <?php } else { ?>
    
       <?= $form->field($model, 'product_type[]',['showLabels'=>false])->textInput() ?>
       <?php } ?>
        
    </div>
    <div class="col-sm-4">
   <?php if(!isset($_GET['id']))  {  ?> <a class="btn btn-success" id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a> <?php } ?>
    </div>
    </div>
    
   
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'description', ['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
       <?php if(isset($_GET['id']))  {  ?>
    <?= $form->field($model, 'description',['showLabels'=>false])->textArea() ?>
    <?php } else { ?>
    <?= $form->field($model, 'description[]',['showLabels'=>false])->textArea() ?>
    <?php } ?>
    </div>
    </div>
    
    <div class="inputs"></div>
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>

function remove(id)
	{
		//alert(id);
		$('#remove'+id).remove();
	}
	 
 $(document).ready(function(){

var i = $('.inputs').size() + 1;


$('#add').click(function() {
	
	$(' <div id="remove'+i+'" class="field"><div class="form-group" ><label class="col-sm-2 control-label">&nbsp;</label><div class="col-sm-4">'+
	'<input type="text" class="form-control"  name="Producttype[product_type][]" required="required" placeholder="product type"/></div>'+
	'<div class="col-sm-4"><a onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></div></div>'+
	'<div class="form-group" ><label class="col-sm-2 control-label">&nbsp;</label><div class="col-sm-4">'+
	'<textarea class="form-control"  name="Producttype[description][]" placeholder="Description"/></textarea></div></div>'+
	
	'</div>').fadeIn('slow').appendTo('.inputs');



i++;
});



$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>