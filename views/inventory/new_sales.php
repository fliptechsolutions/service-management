<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$Rolls=Yii::$app->mycomponent->GetRolls();
 use yii\helpers\Url;
 use kartik\select2\Select2;
 use app\models\Vat;
 use yii\helpers\ArrayHelper;
 use kartik\editable\Editable;
$this->title = 'Sales | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/newsales'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/sales'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <h3>Sales</h3>
<div class="user-form">
<?php 
if(isset($_GET['id']))
{
	$Complaint = \app\models\Customer::findOne(['id' =>$customer_id]);
	$Receipt_update= \app\models\Receipt::find()->where(['sales_id'=>$_GET['id'],'payon_sales'=>1])->one();
	if(isset($Receipt_update->pay_type))
	{
		$pay_type = \app\models\Accounts::findOne(['id' =>$Receipt_update->pay_type]);
	}
							
	
}

    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
   <h3>Customer Details</h3>
   
   
    <div class="form-group">
     <?= Html::label('Company Name','restphone', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'company_name',isset($Complaint['company_name'])?$Complaint['company_name']:'',['class' => ['form-control'],'id'=>'company_name'] ) ?>
      </div>
      </div>
    
     <div class="form-group  required" >
     <?= Html::label('Contact person', 'restname',['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'first_name',isset($Complaint['first_name'])?$Complaint['first_name']:'',['class' => ['form-control'],'id'=>'fname'] ) ?>
     <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
     
      <?= Html::Input('hidden', 'tbl_name','tbl_complaint','' ) ?>
      <?= Html::Input('hidden', 'where','complaint_id','' ) ?>
      <?= Html::Input('hidden', 'render','index','' ) ?>
    </div>
     </div>
     <div class="form-group  required" style="display:none;">
     <?= Html::label('Last Name','restphone', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'last_name',isset($Complaint['last_name'])?$Complaint['last_name']:'',['class' => ['form-control'],'id'=>'lname'] ) ?>
      </div>
      </div>
      
      <div class="form-group  required">
     <?= Html::label('Vat Tin Number','Tin', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'tin_no',isset($Complaint['tin_no'])?$Complaint['tin_no']:'',['class' => ['form-control'],'id'=>'tin_no'] ) ?>
      </div>
      </div>
      
        <div class="form-group  required">
     <?= Html::label('Cst Number','Cst', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'cst_number',isset($Complaint['cst_number'])?$Complaint['cst_number']:'',['class' => ['form-control'],'id'=>'cst_no'] ) ?>
      </div>
      </div>
  
   <div class="form-group  required" >
   <?= Html::label('Address', 'contactname', ['class'=>'control-label col-sm-2']) ?>
   <div class="col-sm-4">
   <?= Html::Input('text', 'address',isset($Complaint['Address'])?$Complaint['Address']:'',['class' => ['form-control'],'id'=>'address']) ?>
   </div>
   </div>
 
     <div class="form-group  required" >
     <?= Html::label('City', 'country', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'city',isset($Complaint['city'])?$Complaint['city']:'',['class' => ['form-control'],'id'=>'city'] ) ?>
     </div>
     </div>
 
      <div class="form-group  required" >
      <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
     <?= Html::Input('text', 'contact_phone',isset($Complaint['contact_phone'])?$Complaint['contact_phone']:'',$options = ['class' => ['form-control'],'required'=>'required','id'=>'mob'] ) ?>
     <?= Html::Input('hidden', 'contact_phone_old',isset($Complaint['contact_phone'])?$Complaint['contact_phone']:'','' ) ?>
     </div>
     <button type="button" onclick="check_phone()" style="margin-left:-17px;" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
     <?php /*?><img src="<?php echo Yii::$app->request->baseUrl. '/uploads/ajax-loader.gif' ?>" /><?php */?>
   <?php  // echo Url::to('@web/uloads/ajax-loader.gif', true); ?>
  <?= Html::img('@web/uploads/ajax-loader.gif', ['id'=>'loading','style'=>'display:none;']);?>
  <span id="error_msg" style="color:#F00; display:none;">Unavailable!</span>
     </div>
     
     <div class="form-group  required" >
     <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-2']) ?>
      <div class="col-sm-4">
     <?= Html::Input('text', 'contact_email',isset($Complaint['email_address'])?$Complaint['email_address']:'',['class' => ['form-control'],'id'=>'email_address'] ) ?>
     </div>
     </div>
     
   <h3>Tax and discount Details</h3> 
   
   <div class="form-group">
     <?= Html::label('Tax Type', 'Tax', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <input type="radio" name="tax_type" class="tax_type" value="1" <?php if(isset($model->tax_type) && ($model->tax_type=='1' )) { echo 'checked="checked"'; }?> onclick="tax_enable(1)" checked="checked"  />Exclusive Tax
      <input type="radio" name="tax_type" class="tax_type" value="2" <?php if(isset($model->tax_type) && ($model->tax_type=='2' )) { echo 'checked="checked"'; }?> onclick="tax_enable(2)"/>Inclusive Tax
    </div>
    </div>
      
     
       <div class="form-group">
    <label  class='col-sm-2 control-label'>Discount</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="discount" id="discount"  class="form-control" value="<?php echo isset($model->discount)?$model->discount:''?>" onkeyup="Total_price()"/>
    
    </div>
    </div>
    
       
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Vat (%)</label>
   
    <div class="col-sm-4">
    <?php $vat_details = ArrayHelper::map(Vat::find()->all(), 'vat', 'vat_name');  ?> 
    
     <?php
	// print_r($product_details);
	 
				echo Select2::widget([
			'name' => 'vat',
			'data' => $vat_details,
			'value'=>isset($model->vat)?$model->vat:'',
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Vat ...','id'=>'vat','onchange'=>'Vat_calc()'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
 
     <?php /*?><input type="text" name="vat" id="vat" class="form-control" onkeyup="Vat_calc()" value="<?php echo isset($model->vat)?$model->vat:''?>" required="required"/><?php */?>
    
    </div>
    </div>
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Tansport</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="trans" id="trans" class="form-control" value="<?php echo isset($model->trans)?$model->trans:''?>" onkeyup="Total_price()" />
    
    </div>
    </div>
     
    
    <div><h3>Product Details</h3></div>
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Total</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="total_price" id="total_price" class="form-control" value="" style="height: 45px; width: 200px;" readonly="readonly" />
    
    </div>
    </div>
    
    
  <?php ///////////////////////////////Payment mode on  Sales ////////////////////////// ?>
    
     <?php 
	$pay_id='';
	if(isset($model->credit_type)) 
	{
		$pay_id=$model->credit_type;
	}
	else if(isset($Receipt_update->pay_type))
	{
		$pay_id=$Receipt_update->pay_type;
	}
	
	?>
    
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Payment</label>
     <div class="col-sm-4">
                <input type="radio" name="payment_type" value="bank"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='bank' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('bank')">Bank
                <input type="radio" name="payment_type" value="cash"   <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cash' )) { echo 'checked="checked"'; }?>   onclick="ac_enable('cash')"/>Cash
                <input type="radio" name="payment_type" value="cheque"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cheque' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('cheque')"/>Cheque
               <input type="radio" name="payment_type" value="credit"  <?php if(isset($model->credit_type)) { echo 'checked="checked"'; }?>  onclick="ac_enable('credit')"/>Credit
                <select name="pay_type"  class="form-control pay_mode" >
			
                </select>
    </div>
   
   <div class="col-sm-4" style="margin-top: 19px; display:none;" id="pay_notes" >
 
     <input type="text" name="pay_notes" class="form-control pay_notes" value="" style="height: 45px; width: 200px;" />
    
    </div>
    
    
    <div class="col-sm-4" style="margin-top: 19px; display:none;" id="bank_date" >
       <?php
	   
	   echo DatePicker::widget([
	'name' => 'bank_date', 
	'value'=>isset($Receipt_update->bank_date)?$Receipt_update->bank_date:'',
	'options' => ['placeholder' => 'Select  date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);
	   ?>
       
       
 
    
    
    </div>
    
    </div>
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Pay amount</label>
    
     <div class="col-sm-4">
 
     <input type="text" name="mode_amount" id="mode_amount" class="form-control" value="<?php  echo isset($Receipt_update->amount)?$Receipt_update->amount:'';?>" style="height: 45px; width: 200px;" />
     <input type="hidden" id="pay_id" class="form-control" value="<?php  echo $pay_id; ?>"  />
    
    </div>
    </div>
    
    
     <?php ///////////////////////////////END ////////////////////////// ?>
    
    
       <div style="float:right;margin: 0 35px  0 0;" > 
       <p><a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a></p>
       </div>
  
  

     
        <?php
	
		if(isset($_GET['id']))
		{
			if ($_GET['id']!='')
				 {
                    $product_all = \app\models\Inventory::find()->where(['=', 'sales_id', $_GET['id']])->all();
					
                    
				 } 
		
		
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
          <tr>
           <th width="30%">Product</th>
              <th width="20%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Qty</th>
              
              <th width="10%">Sub Total</th>
              <th width="10%">VAT Amt</th>
              <th width="20%">Total</th>
          </tr>
        </thead>
        <tbody class="inputs">
        <?php foreach($product_all as $product): ?>
         <?php   $group_table = \app\models\Group_sales::find()->where(['=', 'group_id', $product['group_id_sales']])->one(); ?>
         <?php
		 
		
		
		 $sub_total=($group_table['qty']*$product['total']);
		//$vat_total=$sub_total-($sub_total* $group_table['vat']/100);
		$vat_amt=($sub_total* $model->vat/100);
		  ?>
       
       
          <tr id="remove<?php echo $i; ?>" class="field">
            <td>
            
             <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'value'=>$product['product_id'],
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...','class'=>'product_0','onchange'=>'get_serial_number(0)'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
            
           </td>
        <?php $serial_num=Yii::$app->mycomponent->actionCheck_product_available($product['product_id'])?>
            
          
           <td><select name="s_no[]" id="s_no_<?php echo $i;?>" class="form-control s_no_<?php echo $i;?>"  placeholder="S.NO." style="width:250px;" onchange='Get_serial_details(0)'>
           <option value="<?php echo $product['sno']; ?>" ><?php echo $product['sno']; ?></option>
		   <?php foreach($serial_num as $key=>$val)
		
		{ ?>
			<option value="<?php echo $val; ?>" ><?php echo $val; ?></option>
		<?php }?>
           </select></td>
            <td> <input type="text" name="price[]" class="form-control price_<?php echo $i;?>" value="<?php echo $product['price'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
           
            <td> <input type="text" name="qty[]" data-id='<?php echo $i;?>' class="form-control qty_<?php echo $i;?> check_qty" value="1" onkeyup="get_subtotal(<?php echo $i; ?>)" readonly="readonly"></td>
           
            <td><input type="text" name="subtotal[]" class="form-control total_<?php echo $i;?>" onkeyup="get_price(<?php echo $i; ?>)" value="<?php echo $sub_total;?>"  required="required"></td>
           
            <td> <input type="text" name="vat_amt[]"  class="form-control vat_amt_<?php echo $i;?>" value="<?php echo $vat_amt; ?>"  readonly="readonly" ></td>
            
            <td><input type="text" name="total[]" onkeyup="total_edit(<?php echo $i;?>)"   class="form-control all_total_<?php echo $i;?> total_calc"  value="<?php echo $group_table['total'];?>" readonly="readonly"></td>
            
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
            <tr id="notes_<?php echo $i;?>" ><td><textarea name="notes[]" class="form-control notes_<?php echo $i;?>"   placeholder="notes" ><?php echo $group_table['notes'];?></textarea></td></tr>
          </tr>
           
          
           

		
		
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { 
		
		//print_r($product_details);
		?>
        
        
     <table class="table table-striped">
    <thead>
       <tr>
            <th width="30%">Product</th>
              <th width="20%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Qty</th>
              
              <th width="10%">Sub Total</th>
              <th width="10%">VAT Amt</th>
              <th width="20%">Total</th>
              
              
          </tr>
    </thead>
    <tbody class="inputs">
   
      <tr id="remove0" class="field">
        <td>
        <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...','class'=>'product_0','onchange'=>'get_serial_number(0)'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
       </td>
         <td><select name="s_no[]" id="s_no_0" class="form-control s_no_0"  placeholder="S.NO." style="width:200px;" onchange='Get_serial_details(0)'></select></td>
      <!-- <td><textarea name="s_no[]" class="form-control s_no_0"  placeholder="S.NO." required="required"></textarea></td>-->
           
          
          <td> <input type="text" name="price[]" class="form-control price_0" onkeyup="get_subtotal(0)" required="required" ></td>
          <td><input  name="qty[]" value="1" data-id='0' class="form-control qty_0 check_qty"  onkeyup="get_subtotal(0)" required="required" readonly="readonly"></td>
         
          <td><input type="text" name="subtotal[]" class="form-control total_0" onkeyup="get_price(0)" required="required"></td>
         
          <td><input type="text" name="vat_amt[]"  class="form-control vat_amt_0"  readonly="readonly"></td>
          
          <td><input type="text" name="total[]"  class="form-control all_total_0 total_calc"  required="required" readonly="readonly"></td>
         
        <td><a class="btn btn-danger" onclick="remove(0)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
       <tr id="notes_0" ><td><textarea name="notes[]" class="form-control notes_0"  placeholder="notes" ></textarea></td></tr>
      </tr>
       
      
     </tbody>
  </table>


        
        <?php } ?>

    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>
function total_edit(id)
{
	exclusive_price(id);
}

function ac_enable(type)
{
	$('#pay_notes').hide();
	$('#bank_date').hide();
	$('.pay_notes').val(' ');
	 $('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		   //alert(type);
		   var pay_id=$('#pay_id').val();
		   
		   if(type=='cheque')
		   {
			   $('#pay_notes').show();
		   }
		     if(type=='bank')
		   {
			   $('#bank_date').show();
		   }
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_pay_type' ?>',
	data:'type='+type,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		 //console.log(response);
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			var x=2;
	       		
	 
			for(var item in result)
			{
				 if(result[item]!='')
				 {
					 check='';
					 if(pay_id==item)
					 {
						 var check="selected"
					 }
					   $('<option value="'+item+'" '+check+'>'+result[item]+'</option>').appendTo('.pay_mode');
					 
				 }
			 
			}
			
		  
		}
		
	
	}
	});

		   
	
}

	function tax_enable(tax_type)
	{
		
		if(tax_type==2)
		{
			$('.total_calc').removeAttr('readonly');
		}
		else
		{
			$('.total_calc').prop('readonly', true);
		}
		$('.check_qty').each(function(){
     var id=$(this).data('id'); //
	 var qty=$(this).val();
	 var price=$('.price_'+id).val();
				if(qty!='' && price!='')
				{
					Get_serial_details(id);
				}
			
    
      });
		
	}


  function Total_price()
  {
   
    var discount=0;
	 var trans=0; 
	if ($(".total_calc")[0])
	{
		 var total = 0;
		$('.total_calc').each(function(){
			if($(this).val()=='')
			{
			  tot=0;	
			}
			else
			{
				tot=$(this).val();
			}
		   
		    total += parseFloat(tot);
				
		  });
		  if($('#trans').val()!='')
		  {
			  trans=parseFloat($('#trans').val());
		  }
		  if($('#discount').val()!='' && total!=0)
		  {
			  discount=parseFloat($('#discount').val());
		  }
		  
		  $('#total_price').val(total.toFixed(2));
		  //$('#mode_amount').val(total.toFixed(2));
		   var all_total=total+trans-discount;
		  
		  $('#total_price').val(all_total.toFixed(2));	
		  //$('#mode_amount').val(all_total.toFixed(2));	
	} 
	else 
	{
	   $('#total_price').val(0);
	  // $('#mode_amount').val(0);	
	}

 }


 function Vat_calc()
  {
	 // var tax_type=$('input[name=tax_type]:checked').val(); 
   var tax_type=$('input[name=tax_type]:checked').val(); 
    $('.check_qty').each(function(){
     var id=$(this).data('id'); //
	 var qty=$(this).val();
	 var price=$('.price_'+id).val();
				if(qty!='' && price!='')
				{
					if(tax_type==1)
					{
						 get_price(id);
						 get_subtotal(id);
					}
					else
					{
						exclusive_price(id)
					}
				}
			
    
      });

  }
 

    function serial_check(sno)
	{
		var available=1;
		$('.check_qty').each(function(){
         var id=$(this).data('id'); //
	     var qty=$(this).val();
	     var s_no=$('#s_no_'+id).val();
		// alert(s_no);
		 if(s_no==sno)
		 {
			 
			available=2;
		 }
			
    
      });
	  return available;
		
	}


function get_serial_number(id)
{
	//console.log($('textarea[name*="s_no[]"]').val());
	
	//JSON.parse();
	var product_id=$('.product_'+id).val();
	     // alert(id);
		  
		  $('.s_no_'+id+' option').each(function() {
    
           $(this).remove();
    
           });
		  $('.price_'+id).val('');
		  $('.discont_'+id).val('');
		  $('.vat_'+id).val('');
		  $('.trans_'+id).val('');
		  $('.total_'+id).val(''); 
		  $('.all_total_'+id).val('');
		  $('.vat_amt_'+id).val('');

	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/check_product_available' ?>',
	data:'product_id='+product_id+'&id='+id,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	success: function(response){
		
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			  var x=2;
			for(var item in result)
			{
			  var sn=serial_check(result[item]);
			//  alert(sn);
			  if(sn==1)
			  {
				x=1;
			    $('<option value="'+result[item]+'">'+result[item]+'</option>').appendTo('#s_no_'+id);
			  }
			}
			
			setTimeout(
			  function() 
			  {
				        if(x==1)
						{
							Get_serial_details(id)
						}
						else if(x==2)
						{
							//$('.product_'+id+' option:selected').removeAttr('selected');
							$('.product_'+id).val(null).trigger("change"); 
							//alert('Items unavailable');
						}
			
			
			  },500);

			
			
		  
		}
		else
		{
			alert(response.results);
		}
	
	}
	});

	
}


function Get_serial_details(id)
{
	//console.log($('textarea[name*="s_no[]"]').val());
	
	//JSON.parse();
	var sno=$('.s_no_'+id).val();
	 var tax_type=$('input[name=tax_type]:checked').val(); 
	 //alert(tax_type);
		  $('.price_'+id).val('');
		  $('.total_'+id).val(''); 
		  $('.all_total_'+id).val('');
		  $('.vat_amt_'+id).val('');

	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_serial_details' ?>',
	data:'sno='+sno+'&id='+id,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	  success: function(response){
		
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var tot=parseFloat(response.sell_price);
			if(parseFloat(tax_type)==1)
			{
				
				$('.all_total_'+id).val(tot.toFixed(2));
				$('.price_'+id).val(tot.toFixed(2));
				get_price(id);
				get_subtotal(id);
			}
			else if(parseFloat(tax_type)==2)
			{
				$('.all_total_'+id).val(tot.toFixed(2));
				$('.price_'+id).val(tot.toFixed(2));
				exclusive_price(id);
				Total_price();
			}
		  
		  
		}
		else
		{
			alert('Items unavilable');
		}
	
	}
	});

	
}


function exclusive_price(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.all_total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			var price=(Math.abs(parseFloat(total))*Math.abs(parseFloat(qty)));
			//var d_price=Math.round(price * 100) / 100;
			$('.all_total_'+row).val(price);
			//$('.price_'+row).val(total);
			  if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				   // total=parseFloat(total)+parseFloat(vat_amt);
					//var calculatedTaxRate=((total-d_price)/d_price)*100; 
					var ini_price=(100*total)/(100+parseFloat(vat)); 
				    $('.vat_amt_'+row).val((total-ini_price).toFixed(2));
					$('.total_'+row).val(ini_price.toFixed(2));
					$('.price_'+row).val(ini_price.toFixed(2));
					
				}
				else
				{
					$('.total_'+row).val(price.toFixed(2));
					$('.price_'+row).val(price.toFixed(2));
				}
				
		}
		else if(qty!='' && total=='' && price!='')
		{
			    var total=parseFloat(qty)*parseFloat(price);
				$('.all_total_'+row).val(total);
				
				
				
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt);
					$('.all_total_'+row).val(total);
				}
				
		}
		Total_price();
	 
}

function get_subtotal(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
	if (qty > 0 && price>0)
	 {
			if(qty!='' && price!='')
			{
				var total=parseFloat(qty)*parseFloat(price);
				
				$('.total_'+row).val(total.toFixed(2));
				$('.all_total_'+row).val(total.toFixed(2));
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt.toFixed(2));
					$('.all_total_'+row).val(total.toFixed(2));
				}
				
				
			}
	 }
	 else
	 {
		// $('.qty_'+row).val('');
		 $('.total_'+row).val('');
	 }
	 Total_price();
}

function get_price(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			var price=(Math.abs(parseFloat(total))/Math.abs(parseFloat(qty)));
			var d_price=Math.round(price * 100) / 100;
			$('.price_'+row).val(d_price);
			$('.all_total_'+row).val(total);
			  if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=parseFloat(total)+parseFloat(vat_amt);
				    $('.vat_amt_'+row).val(vat_amt);
					$('.all_total_'+row).val(total.toFixed(2));
					
				}
				
		}
		else if(qty!='' && total=='' && price!='')
		{
			    var total=parseFloat(qty)*parseFloat(price);
				$('.all_total_'+row).val(total);
				
				
				
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt);
					$('.all_total_'+row).val(total);
				}
				
		}
		Total_price();
	 
}



 function remove(id)
{
	//alert(id);
	$('#remove'+id).remove();
	$('#notes_'+id).remove();
	
	Total_price();
}
 
 $(document).ready(function(){
	 Total_price();
	 var payment_type=$('input[name=payment_type]:checked').val(); 
	 var tax_type=$('input[name=tax_type]:checked').val(); 
	// alert(payment_type);
	 ac_enable(payment_type);
	 tax_enable(tax_type);
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
var i = $('.field').size();
var result = <?php echo json_encode($product_details)?>;


$('#add').click(function() {
	
	$('<tr id="remove'+i+'" class="field"><td><select id="product_options'+i+'" name="product_id[]" onchange="get_serial_number('+i+')" class="form-control product_'+i+'"></select></td>'+
	'<td><select name="s_no[]" id="s_no_'+i+'" class="form-control s_no_'+i+'"  onchange="Get_serial_details('+i+')"></select></td>'+
	'<td><input type="text" class="form-control price_'+i+'" name="price[]" required="required" onkeyup="get_subtotal('+i+')"/></td>'+
	'<td><input type="text" class="form-control qty_'+i+' check_qty" value="1" readonly="readonly" data-id="'+i+'" name="qty[]" required="required" onkeyup="get_subtotal('+i+')" /></td>'+
	//'<td><input type="text" class="form-control discont_'+i+'" name="discount[]"  onkeyup="get_discount('+i+')"  /></td>'+
	'<td><input type="text" class="form-control total_'+i+'" name="subtotal[]" required="required" onkeyup="get_price('+i+')"  /></td>'+
	//'<td><input type="text" class="form-control vat_'+i+'" name="vat[]"  onkeyup="get_vat('+i+')"  /></td>'+
	'<td><input type="text" class="form-control vat_amt_'+i+'"  name="vat_amt[]" readonly="readonly"  /></td>'+
	//'<td><input type="text" class="form-control trans_'+i+'" style="width:40px;" name="trans[]"  onkeyup="get_trans('+i+')"  /></td>'+
	'<td><input type="text" class="form-control all_total_'+i+' total_calc"  name="total[]" readonly="readonly"  /></td>'+
	'<td><a  onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>'+
	'<tr id="notes_'+i+'"><td><textarea name="notes[]" class="form-control notes_'+i+'"  placeholder="notes" ></textarea></td></tr>').fadeIn('slow').appendTo('.inputs');
//append_pop(i)
$("#product_options"+i).select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Products ...",
 
  allowClear: true
});



$("#s_no_"+i).select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Sno ...",
 
  allowClear: true
});
var previous_id=(i-1);
var product_id=$('.product_'+previous_id).val();	
//alert(i);
if(product_id!='')
{
		for(var item in result)
		{
			 check='';
			 if(product_id==item)
			 {
				 var check="selected"
			 }
		   
		  $('<option value="'+item+'" '+check+'>'+result[item]+'</option>').appendTo('#product_options'+i);
		}
		get_serial_number(i);
}
else
{
	for(var item in result)
		{
					   
		  $('<option value=""></option>'+'<option value="'+item+'" '+check+'>'+result[item]+'</option>').appendTo('#product_options'+i);
		}
}
 
/*setTimeout(
  function() 
  {
	 alert(i);
     get_serial_number(i);
	 
  },5000);
*/

i++;
});
 
/*$('#remove').click(function() {
if(i > 1) {
$('.field:last').remove();
i--;
}
});*/


$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
 <script>
function check_phone() {
	var mob=$('#mob').val();
	      $('#fname').val('');
		  $('#lname').val('');
		  $('#address').val('');
		  $('#city').val('');
		  $('#city').val('');
		  $('#email_address').val('');
//alert(mob);
    $('#loading').show();
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/mob_validation' ?>',
	data:'mob='+mob,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	success: function(response){
		//var first_name=response.results.first_name;
		 //alert(JSON.stringify(response));
		 $('#loading').hide();
		var status=response.status;
		if(parseFloat(status)==1)
		{
			
		  $('#fname').val(response.results.first_name);
		  $('#company_name').val(response.results.company_name);
		   $('#tin_no').val(response.results.tin_no);
		    $('#cst_no').val(response.results.cst_number);
		  $('#lname').val(response.results.last_name);
		  $('#address').val(response.results.Address);
		  $('#city').val(response.results.city);
		  $('#city').val(response.results.city);
		  $('#email_address').val(response.results.email_address);
		  
		}
		else
		{
			$("#error_msg").show();
           setTimeout(function() { $("#error_msg").hide(); }, 5000);
		}
	
	}
	});
}
</script>
 