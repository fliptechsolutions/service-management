<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
 use yii\helpers\Url;
 use kartik\select2\Select2;
  use app\models\Vat;
 use yii\helpers\ArrayHelper;
$Rolls=Yii::$app->mycomponent->GetRolls();
use app\models\Accounts;
$this->title = 'Receipt | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/add_receipt'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/receipt'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Receipt details</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
					// 'options' => ['onsubmit'=>'return Assign_Schedule()'],
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
	
	
	
	 ?>
     
     
     <input type="hidden" name="sales_id"  value="<?php echo $_GET['id'] ?>"  />
      <?php
		
		
                    $purchase = \app\models\Sales::find()
					->where(['id'=>$_GET['id']])->one();
				
		  ?>
           <table class="table table-striped">
        <thead>
          <tr>
               <th width="20%">Sales ID</th>
               <th width="20%">Amount</th>
                <th width="20%">Paid</th>
                      </tr>
        </thead>
        <tbody class="inputs">
       
          <tr>
             
            <td><?php echo $_GET['id']; ?></td>
            <td><?php echo $purchase['total']; ?></td>
            <td><?php echo $purchase['paid']; ?></td>
          
          </tr>
          
         </tbody>
           </table>
    
   
   
    <h3>Paid Details</h3>
    
        <?php
		
		
                    $product_all = \app\models\Receipt::find()
					->where(['transaction_id'=>$_GET['id']])->all();
					$accounts_details = Accounts::find()->all();
					// print_r($accounts_details);
					 foreach($accounts_details as $account)
					 {
						 $Accounts[$account['id']]=$account['account_name'];
					 }
				
		  ?>
           <table class="table table-striped">
        <thead>
          <tr>
               <th width="20%">Voucher date</th>
               <th width="20%">Amount</th>
                <th width="20%">Notes</th>
               <th width="20%">Pay mode</th>
              
            
          </tr>
        </thead>
        <tbody class="inputs">
        <?php
	$i=0;
		 foreach($product_all as $product): ?>
          <tr class="remove_<?php echo $i; ?>" >
                    
            <td><?php echo $product['sales_date']; ?></td>
            <input type="hidden" value="<?php echo $product['id'] ?>" name="trans_id[]" />
           
            <td><input type="text" name="pay[]"   value="<?php echo $product['amount'] ?>" required="required" ></td>
            <td><textarea name="notes[]" > <?php echo $product['notes']; ?></textarea></td>
            <td><input type="radio" name="payment_type" value="bank" checked="checked"  onclick="ac_enable('bank')">Bank
                <input type="radio" name="payment_type" value="cash"   onclick="ac_enable('cash')"/>Cash
                <input type="radio" name="payment_type" value="cheque" onclick="ac_enable('cheque')"/>Cheque
                <select name="pay_type"  class="form-control pay_mode" style="width:100%;" >
			    </select>
            
            <?php foreach($Accounts as $key=>$value): ?>
            
            <input type="radio" name="pay_type_<?php echo $product['id'] ?>"   value="<?php echo $key;?>" <?php if($product['pay_type']==$key) echo 'checked="checked"'; ?>><?php echo $value;?>
       
                <?php endforeach; ?>
          </td>
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
          </tr>
           
          
           

		
		
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>



 
 

 <script>
 function ac_enable(type)
{
	 $('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		   //alert(type);
		   
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_pay_type' ?>',
	data:'type='+type,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		 //console.log(response);
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			  var x=2;
			 
			for(var item in result)
			{
				 if(result[item]!='')
				 {			
					$('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('.pay_mode');
				 }
			 
			}
			
		  
		}
		
	
	}
	});

		   
	
}
 
   function remove(id)
	{
		//alert(id);
		$('.remove_'+id).remove();
	}
 

</script>
