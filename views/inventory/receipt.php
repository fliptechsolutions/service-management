<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
 use app\models\Customer;

$this->title = 'Receipt | MR infotech'; 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_receipt'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['receipt'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Receipt'],
    'columns' => [
	     
       
		 
		 [
            'label'=>'Sales ID',
			'attribute' => 'id',
			'value' => 'id'
           
         ],
		  
		 
		 [
            'label'=>'Customer Name',
			'attribute' => 'customer_id',
			'value' => function ($model) 
			{
				$model = Customer::find()->where(['=', 'id', $model->customer_id])->one();
				if(isset($model->first_name))
				{
				  return $model->first_name.' '.$model->last_name;
				}
				else
				{
					return false;
				}
			},
           
         ],
    
		
		 
		 [
            'label'=>'Total',
			//'attribute' => 'customer_details',
			'value' => 'total'
           
         ],
		 
		  [
            'label'=>'Paid',
			//'attribute' => 'customer_details',
			'value' => 'paid'
           
          ],
		   [
            'label'=>'Remainig Balance',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid) && isset($model->total))
				{
					return ($model->total-$model->paid);
				}
				return '0';
			}
           
          ],
		 
		 [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Sales date',
				
				'attribute'=>'sales_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'sales_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit}  {print}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
				 
				 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/update_receipt', 'id' =>$model->id]);
                }
                
				if ($action === 'print') {
                    $url = Url::to(['/complaint/bill', 'sales_id'=>$model->id]);
                }                       
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
