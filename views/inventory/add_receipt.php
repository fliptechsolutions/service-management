<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
 use yii\helpers\Url;
 use kartik\select2\Select2;
  use app\models\Vat;
 use yii\helpers\ArrayHelper;
$Rolls=Yii::$app->mycomponent->GetRolls();
use app\models\Accounts;
$this->title = 'Receipt | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/add_receipt'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/receipt'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Receipt</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
					 //'options' => ['onsubmit'=>'return Assign_Schedule()'],
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
	
	
	
	 ?>
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'customer_id',['label'=>'Customer Name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
   
  
    
     <?php
			echo  Select2::widget([
			'name' => 'customer_id',
			'data' => $items,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Customer Name ...','id'=>'customer','onchange'=>'view_list(this.val)'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
    </div>
    </div>
   
    <h3>Sales Details</h3>
    
       
          
          <table class="table table-striped">
        <thead>
          <tr id="pay_enabled" style="display:none;">
            
              <th><input type="text" name="amount" class="pay name_class form-control pay_amount" placeholder="Enter Amount"  value="" onkeyup="get_amount(this.value)" ></th>
              <th><textarea class="notes name_class form-control" name="pay_notes"  placeholder="Notes for sales"></textarea></th>
               <th><input type="radio" name="payment_type" value="bank" checked="checked"  onclick="ac_enable('bank')">Bank
                <input type="radio" name="payment_type" value="cash"   onclick="ac_enable('cash')"/>Cash
                <input type="radio" name="payment_type" value="cheque" onclick="ac_enable('cheque')"/>Cheque
                <select name="pay_type"  class="form-control pay_mode" style="width:100%;" >
			    </select>
                </th> 
                
                <th style="display:none;" id="bank_date"><?php
	   
	   echo DatePicker::widget([
	'name' => 'bank_date', 
	'value'=>isset($voucher_update->bank_date)?$voucher_update->bank_date:'',
	'options' => ['placeholder' => 'Select  date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);
	   ?></th>
            
          </tr>
        </thead>
        
         </tbody>
           </table>
          
          
           <table class="table table-striped">
        <thead>
          <tr>
              <th>Bill No</th>
              <th>Sales date</th>
              <th>Total</th>
               <th>paid</th>
               <th>Remaining</th>
               <th>pay</th>
               <!--<th>Notes</th>
               <th>Payment mode</th>  -->
              
            
          </tr>
        </thead>
        <tbody class="customer_list">
      
         </tbody>
           </table>
		
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>


 <script>
 
 function get_amount()
 {
	 var pay_amount=parseFloat($('.pay_amount').val());
	 var customer_id= $('#customer').val();
	 
	 $('.payrow').each(function(){
		var remaining=parseFloat($(this).find(".remaining").val());
		if(pay_amount > 0)
		{
		  if(remaining >= pay_amount)
		  {
			  $(this).find(".pay").val(pay_amount);
			  pay_amount = 0;
		  }
		  else
		  {
			  $(this).find(".pay").val(remaining);
			  pay_amount = pay_amount - remaining;
		  }		  
		}
		else
		{
			  $(this).find(".pay").val("");
		}		
		  //var id=$(this).data('id');
		  //price =price-pay_amount;
	      //alert(price);    
      });
	  
	  if(pay_amount > 0)
	  {
		  alert("Exceeds total remaining");
   	  	  var pay_original_amount=parseFloat($('.pay_amount').val());
		  $('.pay_amount').val(pay_original_amount - pay_amount);
	  }
 }
 
 $(document).ready(function(){
	
	 $.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
	
});
function ac_enable(type)
{
	//$('#pay_notes').hide();
	$('#bank_date').hide();
	 $('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		  /* if(type=='cheque')
		   {
			   $('#pay_notes').show();
		   }*/
		     if(type=='bank')
		   {
			   $('#bank_date').show();
		   };
		   
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_pay_type' ?>',
	data:'type='+type,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		 //console.log(response);
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			  var x=2;
			 
			for(var item in result)
			{
				 if(result[item]!='')
				 {			
					$('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('.pay_mode');
				 }
			 
			}
			
		  
		}
		
	
	}
	});

		   
	
}
 
 
   function view_list()
  {
	  var payment_type=$('input[name=payment_type]:checked').val(); 
	  ac_enable('bank');
  
    var customer_id= $('#customer').val();
	
	$('#pay_enabled').hide();
	
	$('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		   //alert(type);
		   		    var container = '';
					 var str='';
       $('.customer_list').empty();
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_paylist' ?>',
	data:'customer_id='+customer_id,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		    $.each(response, function(i, e)
			{
				
               var bill_no = response[i].bill_no;
               
			   str+='<tr id="remove'+response[i].id+'" class="payrow">'+
	                                '<td>'+bill_no+'</td>'+
	                                '<td>'+response[i].sales_date+'</td>'+
									'<td><input type="text" class="form-control " value="'+response[i].total+'" name="total[]" required="required" readonly="readonly" /></td>'+
									'<td><input type="text" class="form-control " value="'+response[i].paid+'" name="paid[]" required="required" readonly="readonly" /></td>'+
									'<td><input type="text" data-id="'+response[i].id+'" class="form-control remaining" value="'+response[i].remaining+'"  required="required" readonly="readonly" /></td>'+
									'<td><input type="text" class="form-control pay pay_'+response[i].id+'"  name="pay[]" required="required" readonly="readonly" /></td>'+
									'<input type="hidden"  value="'+response[i].id+'" name="sales_id[]" required="required" readonly="readonly" /></tr>';
	
               //console.log(bill_no);
			   $('#pay_enabled').show();
			
			});
		
	       //console.log(str);
	       $('.customer_list').append(str);
	}
	});
	
	//$('.customer_list').html(container);
   
    /*$('.customer_'+customer_id).show();
	$(".remaining_"+customer_id).addClass('check_qty');
	$(".total_"+customer_id).attr("name", "total[]");
	$(".paid_"+customer_id).attr("name", "paid[]");
	$(".sales_id_"+customer_id).attr("name", "sales_id[]");
	$(".notes_"+customer_id).attr("name", "notes[]");
	$(".pay_"+customer_id).attr("name", "pay[]");*/
  }
 

</script>
