<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
 use yii\helpers\Url;
 use kartik\select2\Select2;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Opening stock | '.Yii::$app->mycomponent->Get_settings('company_name');; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/add_stock'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/opening_stock'], ['class'=>'btn btn-success']) ?> 
            
        </div>
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
					// 'options' => ['onsubmit'=>'return Assign_Schedule()'],
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
	
	
	
	 ?>

    <h3>Product Details</h3>
       
         <div style="float:right;margin: 0 35px  0 0;" > 
    
       <p>
					<a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a>				</p>
        
        </div>
   
        
        
        <?php
		
		if(isset($model->id))
		{
			if ($model->id!='')
				 {
                    $product_all = \app\models\Inventory::find()->where(['=', 'stock_id', $model->id])->groupBy(['group_stock_id'])->all();
							                   
				 } 
		
		
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
          <tr>
               <th width="20%">Product</th>
               <th width="30%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Sell Price</th>
               <th width="5%">Qty</th>
               <th width="10%">Sub Total</th>
               <th width="20%">Total</th>
            
          </tr>
        </thead>
        <tbody class="inputs">
        <?php
		$sub_total='';
		 foreach($product_all as $product): ?>
         <?php   $group_table = \app\models\Group_stock::find()->where(['=', 'group_id', $product['group_stock_id']])->one(); ?>
         <?php
		 
		 $sub_total=$group_table['qty']*$product['total'];
		
		  ?>
       
     
          <tr id="remove<?php echo $i; ?>" class="field">
          
         
        
          
            <td>  <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
		    'value'=>$product['product_id'],
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?></td>
            
            <td><input type="text" name="s_no[]" class="form-control s_no_<?php echo $i;?>"  value="<?php echo Yii::$app->mycomponent->Get_serial_number_stock($product['group_stock_id']); ?>" required="required" ></td>
            <td> <input type="text" name="price[]" class="form-control price_<?php echo $i;?>" value="<?php echo $product['price'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
            <td> <input type="text" name="selling_price[]" class="form-control " value="<?php echo $product['selling_price'] ?>"  required="required"></td>
            <td> <input type="text" name="qty[]" class="form-control qty_<?php echo $i;?> check_qty" value="<?php echo $group_table['qty'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
            <?php /*?><td> <input type="text" name="discount[]" class="form-control discont_<?php echo $i;?>" value="<?php echo $group_table['discount'] ?>" onkeyup="get_discount(<?php echo $i; ?>)" required="required"></td><?php */?>
            <td><input type="text" name="subtotal[]" class="form-control total_<?php echo $i;?>" onkeyup="get_price(<?php echo $i; ?>)" value="<?php echo $sub_total;?>"  required="required"></td>
            <?php /*?><td> <input type="text" name="vat[]" style="width:40px;" class="form-control vat_<?php echo $i;?>" value="<?php echo $group_table['vat']; ?>" onkeyup="get_vat(<?php echo $i; ?>)" ></td><?php */?>
            
            
            <td><input type="text" name="total[]" style="width:77px;" class="form-control all_total_<?php echo $i;?>"  value="<?php echo $group_table['total'];?>" readonly="readonly"></td>
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
            
          </tr>
           
          
           

		
		
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { 
		
		//print_r($product_details);
		?>
        
      
     <table class="table table-striped">
    <thead>
       <tr>
               <th width="30%">Product</th>
               <th width="30%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Sell Price</th>
               <th width="5%">Qty</th>
               <th width="10%">Sub Total</th>
               <th width="20%">Total</th>
            
          </tr>
    </thead>
    <tbody class="inputs">
   
      <tr id="remove0" class="field">
        <td>
        <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
       </td>
          <td><textarea name="s_no[]" class="form-control s_no_0"  placeholder="S.NO."></textarea></td>
          <td> <input type="text" name="price[]" class="form-control price_0" onkeyup="get_subtotal(0)" required="required" ></td>
          <td><input type="text" name="selling_price[]" class="form-control"  required="required" ></td>
          <td><input  name="qty[]" data-id='0' class="form-control qty_0 check_qty"  onkeyup="get_subtotal(0)" required="required"></td>
          <td><input type="text" name="subtotal[]" class="form-control total_0" onkeyup="get_price(0)" required="required"></td>
          <td><input type="text" name="total[]" style="width:77px;" class="form-control all_total_0"  required="required" readonly="readonly"></td>
         
        <td><a class="btn btn-danger" onclick="remove(0)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
      </tr>
       
      
     </tbody>
  </table>

        
        <?php } ?>

    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>



 
 <script>
 
   function Assign_Schedule()
  {
    var test='yes';
	var arra_val=[];
  // var qty=$('.check_qty').val();
    $('.check_qty').each(function(){
    var id=$(this).data('id'); //
	var qty=$(this).val();
	var sno=$('.s_no_'+id).val();
	var arr = sno.split(',');
	var snl=arr.length;
	if(snl!=qty)
	{
		  alert('kk');
		  test='yes';
		  return false;
		  arra_val.push("yes");
	}
	else
	{
		 test='no';
		 arra_val.push('no');
		
	}
	//if(test==='yes') { break; }
	
    
});
alert(arra_val);

	/*if(test=='yes')
	{
		return false;
	}
	else
	{
		return false;
	}*/
	//return false;
  }
 

</script>
 


 


<script>

function get_subtotal(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
	if (qty > 0 && price>0)
	 {
			if(qty!='' && price!='')
			{
				var total=parseFloat(qty)*parseFloat(price);
				//alert(total);
				$('.total_'+row).val(total.toFixed(2));
				$('.all_total_'+row).val(total.toFixed(2));
				
				
			}
	 }
	 else
	 {
		 $('.qty_'+row).val('');
		 $('.total_'+row).val('');
	 }
}

function get_price(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			var price=(Math.abs(parseFloat(total))/Math.abs(parseFloat(qty)));
			var d_price=Math.round(price * 100) / 100;
			var total=parseFloat(qty)*parseFloat(price);
			$('.price_'+row).val(d_price);
			  $('.all_total_'+row).val(total.toFixed(2));
				
		}
		else if(qty!='' && total=='' && price!='')
		{
			    var total=parseFloat(qty)*parseFloat(price);
				$('.all_total_'+row).val(total.toFixed(2));
				
				
		}
	 
}





    function remove(id)
	{
		var sno =$('.s_no_'+id).val();
		
		$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/sold_check' ?>',
	data:'sno='+sno,
	// contentType: "application/json; charset=utf-8",
      dataType: "text",
	  success: function(response){
		
		//var status=response.status;
		 
		if(parseFloat(response)==1)
		{
			
		  alert('Sold out this product');
		 // $.notify(responseData.growl.title, responseData.growl);
		  
		}
		else
		{
			$('#remove'+id).remove();
		   // Total_price();
		}
	
	}
	});
		
		//$('#remove'+id).remove();
	}
	 
 $(document).ready(function(){
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
var i = $('.field').size() + 1;
var result = <?php echo json_encode($product_details)?>;

$('#add').click(function() {
	
	$('<tr id="remove'+i+'" class="field"><td><select style="width: 100%;" id="product_options'+i+'" name="product_id[]" class="form-control"></select></td>'+
	'<td><textarea class="form-control s_no_'+i+'" name="s_no[]" value="" placeholder="S.NO."></textarea></td>'+
	'<td><input type="text" class="form-control price_'+i+'" name="price[]" required="required" onkeyup="get_subtotal('+i+')"/></td>'+
	'<td><input type="text" class="form-control" name="selling_price[]" required="required" ></td>'+
	'<td><input type="text" class="form-control qty_'+i+' check_qty" data-id="'+i+'" name="qty[]" required="required" onkeyup="get_subtotal('+i+')" /></td>'+
	'<td><input type="text" class="form-control total_'+i+'" name="subtotal[]" required="required" onkeyup="get_price('+i+')"  /></td>'+
	'<td><input type="text" class="form-control all_total_'+i+'" style="width:77px;" name="total[]" readonly="readonly"  /></td>'+
	'<td><a  onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>').fadeIn('slow').appendTo('.inputs');

$("#product_options"+i).select2({
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Products ...",
 
  allowClear: true
});
for(var item in result)
{
	
  $('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('#product_options'+i);
}
i++;
});

/*$('#remove').click(function() {
if(i > 1) {
$('.field:last').remove();
i--;
}
});*/


$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
 