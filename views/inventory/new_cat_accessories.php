<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Accesories | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/inventory/newcataccesories'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['inventory/cataccessories'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Category</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'cat_name', ['label'=>'Category Name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'cat_name',['showLabels'=>false])->textInput(['placeholder'=>'Category']); ?>
    </div>
    </div>
    <div class="form-group">
    <?= Html::activeLabel($model, 'description', ['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'description',['showLabels'=>false])->textArea(); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
