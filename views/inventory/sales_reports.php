<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
 use app\models\Sales;
   use kartik\select2\Select2;
  use app\models\Customer;

$this->title = 'Sales Reports| '.Yii::$app->mycomponent->Get_settings('company_name'); 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['salesreports'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Sales Reports'],
    'columns' => [
	     
       
		
		
		 
		 [
            'label'=>'Company Name',
			'attribute' => 'company_name',
			 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Customer::find()->orderBy('company_name')->asArray()->all(), 'company_name', 'company_name'),
			 'filterInputOptions'=>['placeholder'=>'Select company'],
				
			'value'=> function($model)
			{
			  if(isset($model->customer_id))
              { 
			  
			       $customer= Customer::find()->where(['id'=>$model->customer_id])->one();
				    $search_date=date("Y-m-01").' - '.date("Y-m-t");
				   if(isset($_GET['Sales_reports']['sales_date']) && $_GET['Sales_reports']['sales_date']!='')
				   {
					  $search_date= $_GET['Sales_reports']['sales_date'];
				   }
				  
                   return  Html::a('<span>'.$customer->company_name.'</span>', 
				   ['/inventory/sales','Sales_search[company_name]'=>$customer->company_name,'Sales_search[sales_date]'=>$search_date]);      
              }
			},
			// 'customer.company_name',
			'format'=>'raw'
           
         ],
		 
		 [
            'label'=>'Contact person',
			'attribute' => 'customer_name',
			 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Customer::find()->orderBy('first_name')->asArray()->all(), 'first_name', 'first_name'),
			 'filterInputOptions'=>['placeholder'=>'Select customer'],
			'value' =>'customer.first_name',
			'format'=>'raw'
		
         ],
		 
		 [
            'label'=>'Mobile No.',
			'attribute' => 'contact_phone',
			'value' => 'customer.contact_phone'
           
         ],
		
		 
		  [
            'label'=>'Total',
			//'attribute' => 'customer_details',
			'value' => 'total'
           
         ],
		 
		  [
            'label'=>'Paid',
			//'attribute' => 'customer_details',
			'value' => 'paid'
           
          ],
		   [
            'label'=>'Remaining',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid) && isset($model->total))
				{
					return ($model->total-$model->paid);
				}
				return '0';
			}
           
          ],
    
		[

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Sales date',
				
				'attribute'=>'sales_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'sales_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	  
	 
		
		
	
		
		
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
<?php //\yii\widgets\Pjax::end(); ?>
<?php
$Rolls=Yii::$app->mycomponent->GetRolls();

?>
 

