<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Shop | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/inventory/addselling'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['inventory/selling'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Discount</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'Selling', ['label'=>'Discount name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'selling_name',['showLabels'=>false])->textInput(['placeholder'=>'Selling name']); ?>
    </div>
    </div>
    <div class="form-group">
    <?= Html::activeLabel($model, 'Price', ['label'=>'Discount(%)', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'price',['showLabels'=>false])->textInput(['placeholder'=>'Price']); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
