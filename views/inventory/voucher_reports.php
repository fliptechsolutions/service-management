<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Accounts;
 use app\models\Vendor;
 

$this->title = 'Voucher Reports | '.Yii::$app->mycomponent->Get_settings('company_name'); 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_voucher'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['voucher'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Payments'],
    'columns' => [
	     
       
	
		  
		 
		 [
            'label'=>'Suppliers Name',
			'attribute' => 'first_name',
			'value' => function($model)
			{
			  if(isset($model->vendor_id))
              { 
			  
			       $Vendor= Vendor::find()->where(['id'=>$model->vendor_id])->one();
				    $search_date=date("Y-m-01").' - '.date("Y-m-t");
				   if(isset($_GET['Voucher_reports']['purchase_date']) && $_GET['Voucher_reports']['purchase_date']!='')
				   {
					  $search_date= $_GET['Voucher_reports']['purchase_date'];
				   }
				  
                   return  Html::a('<span>'.$Vendor->first_name.'</span>', 
				   ['/inventory/voucher','Transaction_purchase[first_name]'=>$Vendor->first_name,'Transaction_purchase[purchase_date]'=>$search_date]);      
              }
			},
			'format'=>'raw'
			
           
         ],
    
		
		 
		 [
            'label'=>'Transaction Amount',
			//'attribute' => 'customer_details',
			'value' => 'amount'
           
         ],
		 
		  [
            'label'=>'Number Of Transaction',
			'width' => '10%',
			'value' =>'trans'
           
         ],
		   
		 
		 [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Purchase date',
				
				'attribute'=>'purchase_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'purchase_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	
		
		
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
