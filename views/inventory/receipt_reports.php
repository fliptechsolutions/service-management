<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Accounts;
 use app\models\Customer;
 

$this->title = 'Receipt Reports | '.Yii::$app->mycomponent->Get_settings('company_name'); 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
           
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['receipt'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Receipt Reports'],
    'columns' => [
	     
       
		
		  
		 
		 [
            'label'=>'Customer Name',
			'attribute' => 'first_name',
			'value' => function($model)
			{
			  if(isset($model->customer_id))
              { 
			  
			       $Customer= Customer::find()->where(['id'=>$model->customer_id])->one();
				    $search_date=date("Y-m-01").' - '.date("Y-m-t");
				   if(isset($_GET['Receipt_reports']['sales_date']) && $_GET['Receipt_reports']['sales_date']!='')
				   {
					  $search_date= $_GET['Receipt_reports']['sales_date'];
				   }
				  
                   return  Html::a('<span>'.$Customer->first_name.'</span>', 
				   ['/inventory/receipt','Transaction_sales[first_name]'=>$Customer->first_name,'Transaction_sales[sales_date]'=>$search_date]);      
              }
			},
			'format'=>'raw'
           
         ],
    
		
		 
		 [
            'label'=>'Transaction Amount',
			//'attribute' => 'customer_details',
			'value' => 'amount'
           
         ],
		 
		 [
            'label'=>'Number Of Transaction',
			'width' => '10%',
			'value' =>'trans'
           
         ],
		 
		  
		  
		 
		 [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Sales date',
				
				'attribute'=>'sales_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'sales_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	
		
		
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
