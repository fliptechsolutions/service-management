<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Product type | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['addproduct'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['product_type'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Product Type'],
    'columns' => [
       
		  [
		    'label'=>'Product type',
		    'attribute'=>'product_type',
			'value'=>'product_type',
			 'width'=>'50%',
          ],
		 
		  [
		    'label'=>'Description',
            'attribute'=>'description',
			 'width'=>'20%',
           
          ],
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
			  'width'=>'30%',
              'template' => '{edit} {delete}',
              'buttons' => [
                
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure want to delete this type?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/updateprotype', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/inventory/deleteprotype', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);


?>
 
 

