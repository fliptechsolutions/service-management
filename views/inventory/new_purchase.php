<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
 use yii\helpers\Url;
 use kartik\select2\Select2;
  use app\models\Vat;
 use yii\helpers\ArrayHelper;
 use kartik\growl\Growl;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Purchase | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<?php 
if(isset($_GET['id']))
{
	$voucher_update= \app\models\Voucher::find()->where(['purchase_id'=>$_GET['id'],'payon_purchase'=>1])->one();
	if(isset($voucher_update->pay_type))
	{
		$pay_type = \app\models\Accounts::findOne(['id' =>$voucher_update->pay_type]);
	}
							
	
}
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/newpurchase'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/purchase'], ['class'=>'btn btn-success']) ?> 
            
        </div>
       <h3>Purchase</h3>
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
					 'options' => ['onsubmit'=>'return Assign_Schedule()'],
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
	
	
	
	 ?>
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'vendor_id',['label'=>'Supplier Name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
   
    <?= $form->field($model, 'vendor_id',['showLabels'=>false])->dropDownList($items) ?>
    
     <?php
			 Select2::widget([
			'name' => 'test',
			'data' => $items,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
    </div>
    </div>
    
    
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'purchase_date', ['label'=>'Invoice Date', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
    <?= $form->field($model, 'purchase_date',['showLabels'=>false])->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter Purchase date ...'],
    'pluginOptions' => [
    'format' => 'yyyy-mm-dd',
    'todayHighlight' => true
    ]
    ]); ?>
    </div>
    </div>
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'Shop',['label'=>'Warehouse', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
 
     <?= $form->field($model, 'shop',['showLabels'=>false])->dropDownList($items1) ?>
    
    </div>
    </div>
    
    <div class="form-group">
     <?= Html::label('Tax Type', 'Tax', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <input type="radio" name="tax_type" class="tax_type" value="1" <?php if(isset($model->tax_type) && ($model->tax_type=='1' )) { echo 'checked="checked"'; }?> onclick="tax_enable(1)" checked="checked"  />Exclusive Tax
      <input type="radio" name="tax_type" class="tax_type" value="2" <?php if(isset($model->tax_type) && ($model->tax_type=='2' )) { echo 'checked="checked"'; }?> onclick="tax_enable(2)"/>Inclusive Tax
    </div>
    </div>
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Invoice Number</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="invoice_number" class="form-control" value="<?php echo isset($model->invoice_number)?$model->invoice_number:''?>" />
    
    </div>
    </div>
    
    
    
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Discount</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="discount" id="discount"  class="form-control"  onkeyup="Total_price()" value="<?php echo isset($model->discount)?$model->discount:''?>" />
    
    </div>
    </div>
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Vat (%)</label>
   
    <div class="col-sm-4">
 
      <?php $vat_details = ArrayHelper::map(Vat::find()->all(), 'vat', 'vat_name');  ?> 
    
     <?php
	// print_r($product_details);
	 
				echo Select2::widget([
			'name' => 'vat',
			'data' => $vat_details,
			'value'=>isset($model->vat)?$model->vat:'',
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Vat ...','id'=>'vat','onchange'=>'Vat_calc()'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
    
    </div>
    </div>
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Tansport</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="trans" id="trans" class="form-control" value="<?php echo isset($model->trans)?$model->trans:''?>" onkeyup="Total_price()"  />
    
    </div>
    </div>
    
   
    
     
    
    <h3>Product Details</h3>
    
    
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Total</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="total_price" id="total_price" class="form-control" value="" style="height: 45px; width: 200px;" readonly="readonly" />
    
    </div>
    </div>
    
    
    
  <?php ///////////////////////////////Payment mode on  Purchase ////////////////////////// ?>
    
     <?php 
	$pay_id='';
	if(isset($model->credit_type)) 
	{
		$pay_id=$model->credit_type;
	}
	else if(isset($voucher_update->pay_type))
	{
		$pay_id=$voucher_update->pay_type;
	}
	
	?>
    
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Payment</label>
     <div class="col-sm-4">
                <input type="radio" name="payment_type" value="bank"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='bank' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('bank')">Bank
                <input type="radio" name="payment_type" value="cash"   <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cash' )) { echo 'checked="checked"'; }?>   onclick="ac_enable('cash')"/>Cash
                <input type="radio" name="payment_type" value="cheque"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cheque' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('cheque')"/>Cheque
               <input type="radio" name="payment_type" value="credit"  <?php if(isset($model->credit_type)) { echo 'checked="checked"'; }?>  onclick="ac_enable('credit')"/>Credit
                <select name="pay_type"  class="form-control pay_mode" >
			
                </select>
    </div>
   
   <div class="col-sm-4" style="margin-top: 19px; display:none;" id="pay_notes" >
 
     <input type="text" name="pay_notes" class="form-control pay_notes" value="<?php  echo isset($voucher_update->notes)?$voucher_update->notes:'';?>" style="height: 45px; width: 200px;" />
    
    </div>
       <div class="col-sm-4" style="margin-top: 19px; display:none;" id="bank_date" >
       <?php
	   
	   echo DatePicker::widget([
	'name' => 'bank_date', 
	'value'=>isset($voucher_update->bank_date)?$voucher_update->bank_date:'',
	'options' => ['placeholder' => 'Select  date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);
	   ?>
       
       
 
    
    
    </div>
    
    </div>
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Pay amount</label>
    
     <div class="col-sm-4">
 
     <input type="text" name="mode_amount" id="mode_amount" class="form-control" value="<?php  echo isset($voucher_update->amount)?$voucher_update->amount:'';?>" style="height: 45px; width: 200px;" />
     <input type="hidden" id="pay_id" class="form-control" value="<?php  echo $pay_id; ?>"  />
    
    </div>
    </div>
    
    
     <?php ///////////////////////////////END ////////////////////////// ?>
    
    
       
         <div style="float:right;margin: 0 35px  0 0;" > 
    
       <p>
					<a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a>				</p>
        
        </div>
   
        
        
        <?php
		
		if(isset($model->id))
		{
			if ($model->id!='')
				 {
                    $product_all = \app\models\Inventory::find()->where(['=', 'purchase_id', $model->id])->groupBy(['group_id'])->all();
							                   
				 } 
		
		
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
          <tr>
             <th width="20%">Product</th>
              <th width="20%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Sell Price</th>
               <th width="10%">Qty</th>
              
             
              <th width="10%">Sub Total</th>
             
              <th width="10%">VAT Amt</th>
            
              <th width="10%">Total</th>
              <th ></th>
            
          </tr>
        </thead>
        <tbody class="inputs">
        <?php
		$sub_total='';
		 foreach($product_all as $product): ?>
         <?php   $group_table = \app\models\Grouppurchase::find()->where(['=', 'group_id', $product['group_id']])->one(); ?>
         <?php
		 
		 $sub_total=$group_table['qty']*$product['total'];
		//$vat_total=$sub_total-($sub_total* $group_table['vat']/100);
		$vat_amt=($sub_total* $model->vat/100);
		  ?>
       
     
          <tr id="remove<?php echo $i; ?>" class="field">
          
         
        
          
            <td>  <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
		    'value'=>$product['product_id'],
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?></td>
            
            <td><input type="text" name="s_no[]" class="form-control s_no_<?php echo $i;?>"  value="<?php echo Yii::$app->mycomponent->Get_serial_no($product['group_id']); ?>" required="required" ></td>
            <td> <input type="text" name="price[]" class="form-control price_<?php echo $i;?>" value="<?php echo $product['price'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
            <td> <input type="text" name="selling_price[]" class="form-control" value="<?php echo $product['selling_price'] ?>"  required="required"></td>
            <td> <input type="text" data-id='<?php echo $i;?>' name="qty[]" class="form-control qty_<?php echo $i;?> check_qty" value="<?php echo $group_table['qty'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
            <?php /*?><td> <input type="text" name="discount[]" class="form-control discont_<?php echo $i;?>" value="<?php echo $group_table['discount'] ?>" onkeyup="get_discount(<?php echo $i; ?>)" required="required"></td><?php */?>
            <td><input type="text" name="subtotal[]" class="form-control total_<?php echo $i;?>" onkeyup="get_price(<?php echo $i; ?>)" value="<?php echo $sub_total;?>"  required="required"></td>
            <?php /*?><td> <input type="text" name="vat[]" style="width:40px;" class="form-control vat_<?php echo $i;?>" value="<?php echo $group_table['vat']; ?>" onkeyup="get_vat(<?php echo $i; ?>)" ></td><?php */?>
            <td> <input type="text" name="vat_amt[]"  class="form-control vat_amt_<?php echo $i;?>" value="<?php echo $vat_amt; ?>"  readonly="readonly" ></td>
            <?php /*?><td> <input type="text" name="trans[]" style="width:40px;" class="form-control trans_<?php echo $i;?>" value="<?php echo $group_table['trans']; ?>" onkeyup="get_trans(<?php echo $i; ?>)" ></td><?php */?>
            
            <td><input type="text" name="total[]" style="width:77px;" class="form-control all_total_<?php echo $i;?> total_calc"  value="<?php echo $group_table['total'];?>" onkeyup="exclusive_price(0)"></td>
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
            
          </tr>
           
          
           

		
		
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { 
		
		//print_r($product_details);
		?>
        
      
     <table class="table table-striped">
    <thead>
       <tr>
             <th width="20%">Product</th>
              <th width="20%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="10%">Sell Price</th>
               <th width="10%">Qty</th>
              
             
              <th width="10%">Sub Total</th>
             
              <th width="10%">VAT Amt</th>
            
              <th width="10%">Total</th>
              <th ></th>
            
          </tr>
    </thead>
    <tbody class="inputs">
   
      <tr id="remove0" class="field">
        <td>
        <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
       </td>
          <td><textarea name="s_no[]" class="form-control s_no_0"  placeholder="S.NO."></textarea></td>
          <td> <input type="text" name="price[]" class="form-control price_0" onkeyup="get_subtotal(0)" required="required" ></td>
          <td><input type="text" name="selling_price[]" class="form-control"  required="required" ></td>
          <td><input  name="qty[]" data-id='0' class="form-control qty_0 check_qty"  onkeyup="get_subtotal(0)" required="required"></td>
         <!-- <td><input type="text" name="discount[]" class="form-control discont_0" onkeyup="get_discount(0)" ></td>   -->
          <td><input type="text" name="subtotal[]" class="form-control total_0" onkeyup="get_price(0)" required="required"></td>
          <!--<td><input type="text" name="vat[]"  style="width:40px;" class="form-control vat_0" onkeyup="get_vat(0)" ></td>-->
          <td><input type="text" name="vat_amt[]"  class="form-control vat_amt_0"  readonly="readonly"></td>
         <!-- <td><input type="text" name="trans[]" style="width:40px;" class="form-control trans_0" onkeyup="get_trans(0)"></td>-->
          <td><input type="text" name="total[]" style="width:77px;" class="form-control all_total_0 total_calc"  required="required" onkeyup="exclusive_price(0)" ></td>
         
        <td><a class="btn btn-danger" onclick="remove(0)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
      </tr>
       
      
     </tbody>
  </table>

        
        <?php } ?>

    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>



 
 <script>
 
 function ac_enable(type)
{
	$('#pay_notes').hide();
	$('#bank_date').hide();
	 $('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		   //alert(type);
		   var pay_id=$('#pay_id').val();
		   
		   if(type=='cheque')
		   {
			   $('#pay_notes').show();
		   }
		    if(type=='bank')
		   {
			   $('#bank_date').show();
		   }
		   
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_pay_type' ?>',
	data:'type='+type,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		 //console.log(response);
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			var x=2;
	       		
	 
			for(var item in result)
			{
				 if(result[item]!='')
				 {
					 check='';
					 if(pay_id==item)
					 {
						 var check="selected"
					 }
					   $('<option value="'+item+'" '+check+'>'+result[item]+'</option>').appendTo('.pay_mode');
					 
				 }
			 
			}
			
		  
		}
		
	
	}
	});

		   
	
}
 
 
 
  function Total_price()
  {
   
    var discount=0;
	 var trans=0; 
	if ($(".total_calc")[0])
	{
		 var total = 0;
		$('.total_calc').each(function(){
			if($(this).val()=='')
			{
			  tot=0;	
			}
			else
			{
				tot=$(this).val();
			}
		   
		    total += parseFloat(tot);
				
		  });
		  if($('#trans').val()!='')
		  {
			  trans=parseFloat($('#trans').val());
		  }
		  if($('#discount').val()!='' && total!=0)
		  {
			  discount=parseFloat($('#discount').val());
		  }
		  
		  $('#total_price').val(total.toFixed(2));
		   var all_total=total+trans-discount;
		  
		  $('#total_price').val(all_total.toFixed(2));		
	} 
	else 
	{
	   $('#total_price').val(0);	
	}

 }
 
 
 function tax_enable(tax_type)
	{
		$('.check_qty').each(function(){
     var id=$(this).data('id'); //
	 var qty=$(this).val();
	 var price=$('.price_'+id).val();
				
					if(tax_type==2)
					{
					     $('.price_'+id).attr("readonly", "readonly"); 
					     $('.all_total_'+id).removeAttr("readonly");
					}
					else if(tax_type==1)
					{
						 $('.all_total_'+id).attr("readonly", "readonly"); 
					     $('.price_'+id).removeAttr("readonly");
					}
				
			
    
      });
		
	}
 
 
 function exclusive_price(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.all_total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			//alert(total);
			var price=(Math.abs(parseFloat(total))*Math.abs(parseFloat(qty)));
			//var d_price=Math.round(price * 100) / 100;
			$('.all_total_'+row).val(price);
			//$('.price_'+row).val(total);
			  if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				   // total=parseFloat(total)+parseFloat(vat_amt);
					//var calculatedTaxRate=((total-d_price)/d_price)*100; 
					var ini_price=(100*total)/(100+parseFloat(vat)); 
				    $('.vat_amt_'+row).val((total-ini_price).toFixed(2));
					$('.total_'+row).val(ini_price.toFixed(2));
					$('.price_'+row).val(ini_price.toFixed(2));
					
				}
				else
				{
					$('.total_'+row).val(price.toFixed(2));
					$('.price_'+row).val(price.toFixed(2));
				}
				
		}
		
		Total_price();
	 
}

 
 
   function Assign_Schedule()
  {
    var test=true;
	var arra_val=[];
 
    $('.check_qty').each(function(){
    var id=$(this).data('id'); //
	var qty=$(this).val();
	var sno=$('.s_no_'+id).val();
	var arr = sno.split(',');
	var snl=arr.length;
	if(snl!=qty)
	{
		  
		  test=false;
		  //return false;
		 
	}
	else
	{
		 
		
	}
	//if(test==='yes') { break; }
	
    
});
//alert(test);
if(test==false) 
{ 

   alert('Count mismatch Sno. and Qty');

}
	
	return test;
  }
 

</script>
 


 <script>
 
   function Vat_calc()
  {
    var tax_type=$('input[name=tax_type]:checked').val(); 
    $('.check_qty').each(function(){
     var id=$(this).data('id'); //
	 var qty=$(this).val();
				if(qty!='')
				{
					if(tax_type==1)
					{
						 get_price(id);
						 get_subtotal(id);
					}
					else
					{
						exclusive_price(id)
					}
				}
				else
				{
					
					
				}
	
	
	
    
      });

  }
 

</script>


<script>

function get_subtotal(row)
{ 
  var tax_type=$('input[name=tax_type]:checked').val();     
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	if(tax_type==1)
	{
	
			if (qty > 0 && price>0)
			 {
					if(qty!='' && price!='')
					{
						var total=parseFloat(qty)*parseFloat(price);
						//alert(total);
						$('.total_'+row).val(total.toFixed(2));
						$('.all_total_'+row).val(total.toFixed(2));
						if(vat!='')
						{
							
							var vat_amt=(total*(parseFloat(vat) / 100));
							total=total+vat_amt;
							$('.vat_amt_'+row).val(vat_amt.toFixed(2));
							$('.all_total_'+row).val(total.toFixed(2));
						}
						
						
					}
			 }
			 else
			 {
				// $('.qty_'+row).val('');
				 $('.total_'+row).val('');
			 }
	}
	else if(tax_type==2)
	{
		exclusive_price(row);
	}
	
	 Total_price();
}

function get_price(row)
{ 
    Total_price();
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			var price=(Math.abs(parseFloat(total))/Math.abs(parseFloat(qty)));
			var d_price=Math.round(price * 100) / 100;
			$('.price_'+row).val(d_price);
			$('.all_total_'+row).val(price.toFixed(2));
			  if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=parseFloat(total)+parseFloat(vat_amt.toFixed(2));
				    $('.vat_amt_'+row).val(vat_amt.toFixed(2));
					$('.all_total_'+row).val(total.toFixed(2));
					
				}
				
		}
		else if(qty!='' && total=='' && price!='')
		{
			    var total=parseFloat(qty)*parseFloat(price);
				$('.all_total_'+row).val(total.toFixed(2));
				
				
				
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt.toFixed(2));
					$('.all_total_'+row).val(total.toFixed(2));
				}
				
		}
		Total_price();
	 
}



function Sold_check(sno)
{
	var x='false';
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/sold_check' ?>',
	data:'sno='+sno,
	// contentType: "application/json; charset=utf-8",
      dataType: "text",
	  success: function(response){
		
		//var status=response.status;
		 alert(response);
		if(parseFloat(status)==1)
		{
			
		   x='true';
		  
		  
		}
		else
		{
			
		}
	
	}
	});

	return x;
}

    function remove(id)
	{
		var sno =$('.s_no_'+id).val();
		
		$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/sold_check' ?>',
	data:'sno='+sno,
	// contentType: "application/json; charset=utf-8",
      dataType: "text",
	  success: function(response){
		
		//var status=response.status;
		 
		if(parseFloat(response)==1)
		{
			
		  alert('Sold out this product');
		 // $.notify(responseData.growl.title, responseData.growl);
		  
		}
		else
		{
			$('#remove'+id).remove();
		    Total_price();
		}
	
	}
	});
		
		
		
		
	}
	 
 $(document).ready(function(){
	 Total_price();
	  var tax_type=$('input[name=tax_type]:checked').val(); 
	  tax_enable(tax_type);
	   var payment_type=$('input[name=payment_type]:checked').val(); 
	   ac_enable(payment_type);
	  
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
var i = $('.field').size() + 1;
var result = <?php echo json_encode($product_details)?>;

$('#add').click(function() {
	
	
	$('<tr id="remove'+i+'" class="field"><td><select style="width: 100%;" id="product_options'+i+'" name="product_id[]" class="form-control"></select></td>'+
	'<td><textarea class="form-control s_no_'+i+'" name="s_no[]" value="" placeholder="S.NO."></textarea></td>'+
	'<td><input type="text" class="form-control price_'+i+'" name="price[]" required="required" onkeyup="get_subtotal('+i+')"/></td>'+
	'<td><input type="text" class="form-control" name="selling_price[]" required="required" ></td>'+
	'<td><input type="text" class="form-control qty_'+i+' check_qty" data-id="'+i+'" name="qty[]" required="required" onkeyup="get_subtotal('+i+')" /></td>'+
	//'<td><input type="text" class="form-control discont_'+i+'" name="discount[]"  onkeyup="get_discount('+i+')"  /></td>'+
	'<td><input type="text" class="form-control total_'+i+'" name="subtotal[]" required="required" onkeyup="get_price('+i+')"  /></td>'+
	//'<td><input type="text" class="form-control vat_'+i+'" name="vat[]"  onkeyup="get_vat('+i+')"  /></td>'+
	'<td><input type="text" class="form-control vat_amt_'+i+'"  name="vat_amt[]" readonly="readonly"  /></td>'+
	//'<td><input type="text" class="form-control trans_'+i+'" style="width:40px;" name="trans[]"  onkeyup="get_trans('+i+')"  /></td>'+
	'<td><input type="text" class="form-control all_total_'+i+' total_calc" style="width:77px;" name="total[]" onkeyup="exclusive_price('+i+')" /></td>'+
	'<td><a  onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>').fadeIn('slow').appendTo('.inputs');
//var data = jQuery.parseJSON(result);
//var json = $.getJSON(result);
var tax_type=$('input[name=tax_type]:checked').val(); 
	  tax_enable(tax_type);

$("#product_options"+i).select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Products ...",
 
  allowClear: true
});
for(var item in result)
{
	
  $('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('#product_options'+i);
}
i++;
});

/*$('#remove').click(function() {
if(i > 1) {
$('.field:last').remove();
i--;
}
});*/


$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
 