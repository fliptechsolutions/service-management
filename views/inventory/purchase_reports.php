<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
  use kartik\select2\Select2;
  use app\models\Vendor;
use kartik\growl\Growl;

$this->title = 'Purchase Reports | '.Yii::$app->mycomponent->Get_settings('company_name'); 



$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
           
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['purchasereports'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Purchase Reports'],
    'columns' => [
	     
       
		
		 [
            'label'=>'Supplier Name',
			'attribute' => 'first_name',
			 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Vendor::find()->orderBy('first_name')->asArray()->all(), 'first_name', 'first_name'),
			'filterInputOptions'=>['placeholder'=>'Select Supplier'],
			'value' => function($model)
			{
			  if(isset($model->vendor_id))
              { 
			  
			       $vendor= Vendor::find()->where(['id'=>$model->vendor_id])->one();
				    $search_date=date("Y-m-01").' - '.date("Y-m-t");
				   if(isset($_GET['Purchase_reports']['purchase_date']) && $_GET['Purchase_reports']['purchase_date']!='')
				   {
					  $search_date= $_GET['Purchase_reports']['purchase_date'];
				   }
				  
                   return  Html::a('<span>'.$vendor->first_name.'</span>', 
				   ['/inventory/purchase','Purchasesearch[vendor]'=>$vendor->first_name,'Purchasesearch[purchase_date]'=>$search_date]);      
              }
			},
			'format'=>'raw'
		
         ],
		 
		 
		 [
            'label'=>'Total',
			//'attribute' => 'customer_details',
			'value' =>function ($model) {
				if(isset($model->total))
				{
					return number_format($model->total,2);
				}
				return '0';
			}
         ],
		 
		  [
            'label'=>'Paid',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid))
				{
					return number_format($model->paid,2);
				}
				return '0';
			}
         ],
		 [
            'label'=>'Remaining',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid) && isset($model->total))
				{
					return number_format(($model->total-$model->paid),2);
				}
				return '0';
			}
           
          ],
		  
		 
		
		[

				//'class'=>'kartik\grid\EditableColumn',
				
				'attribute'=>'purchase_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'purchase_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
		
		
		
		
	]
		
		
]);
?>

 
