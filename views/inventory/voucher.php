<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
 use app\models\Vendor;

$this->title = 'Voucher | MR infotech'; 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_voucher'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Voucher'],
    'columns' => [
	     
       
		 
		 [
            'label'=>'Purchase ID',
			'attribute' => 'id',
			'value' => 'id'
           
         ],
		  [
            'label'=>'Invoice number',
			//'attribute' => 'customer_details',
			'value' => 'invoice_number'
           
         ],
		 
		 [
            'label'=>'Vendor Name',
			'attribute' => 'vendor_id',
			'value' => function ($model) 
			{
				$model = Vendor::find()->where(['=', 'id', $model->vendor_id])->one();
				if(isset($model->first_name))
				{
				  return $model->first_name.' '.$model->last_name;
				}
				else
				{
					return false;
				}
			},
           
         ],
    
		
		 
		 [
            'label'=>'Total',
			//'attribute' => 'customer_details',
			'value' => 'total'
           
         ],
		 
		  [
            'label'=>'Paid',
			//'attribute' => 'customer_details',
			'value' => 'paid'
           
         ],
		 [
            'label'=>'Remainig Balance',
			//'attribute' => 'customer_details',
			'value' => function ($model) {
				if(isset($model->paid) && isset($model->total))
				{
					return ($model->total-$model->paid);
				}
				return '0';
			}
           
          ],
		   [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Purcahse date',
				
				'attribute'=>'purchase_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'purchase_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	
		 
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit}  {print}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
				 
				 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/inventory/update_voucher', 'id' =>$model->id]);
                }
                
				if ($action === 'print') {
                    $url = Url::to(['/complaint/bill', 'sales_id'=>$model->id]);
                }                       
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
