<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\Typeahead;
 use yii\helpers\Url;
 use kartik\select2\Select2;
  use app\models\Vat;
 use yii\helpers\ArrayHelper;
$Rolls=Yii::$app->mycomponent->GetRolls();
use app\models\Accounts;
use app\models\Transaction_sales;
use app\models\Customer;
use app\models\Receipt;
use app\models\Sales;
$this->title = 'Receipt | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/inventory/add_receipt'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/inventory/receipt'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Receipt</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
		'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
					 //'options' => ['onsubmit'=>'return Assign_Schedule()'],
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
	
	$transaction=Transaction_sales::find()->where(['id'=>$_GET['id']])->one();
    $customer=Customer::find()->where(['id'=>$transaction['customer_id']])->one()
	
	 ?>
    
     <div class="form-group ">
   <?= Html::label('Customer Name:', 'contactphone', ['class'=>' col-sm-2']) ?>
    <div class="col-sm-4">
   <input type="text" readonly="readonly" class="form-control" value="<?php echo  $customer->first_name.' '.$customer->last_name; ?>" />
    <?php
			 Select2::widget([
			'name' => 'customer_id',
			//'data' => $items,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Customer Name ...','id'=>'customer','onchange'=>'view_list(this.val)'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
    </div>
    </div>
    
  
   
    <h3>Sales Details</h3>
    
        <?php
		
		
                  
					 $accounts_details = Accounts::find()->all();
					// print_r($accounts_details);
					 foreach($accounts_details as $account)
					 {
						 $Accounts[$account['id']]=$account['account_name'];
					 }
					 
					 $Receipt_all=Receipt::find()->where(['transaction_id'=>$_GET['id']])->all();
					 
					 $openingBalnce=\app\models\OpeningBalanceCustomer::find()->where(['transaction_id'=>$_GET['id'],'customer_id'=>$transaction['customer_id']])->one();
					 $customer_details=Customer::find()->where(['id'=>$transaction['customer_id']])->one();	
					
					 
					if(isset($transaction->pay_type))
					{
						$pay_type = \app\models\Accounts::findOne(['id' =>$transaction->pay_type]);
					}
					
				
		  ?>
          <input type="hidden" id="pay_id" class="form-control" value="<?php  echo $transaction->pay_type; ?>"  />
           <input type="hidden" name="transaction_id" class="form-control" value="<?php  echo $_GET['id']; ?>"  />
          
          <table class="table table-striped">
        <thead>
          <tr id="pay_enabled">
            
              <th><input type="text" name="amount" class="pay name_class form-control pay_amount" placeholder="Enter Amount"  value="<?php echo $transaction['amount'] ?>" onkeyup="get_amount(this.value)" ></th>
              <th><textarea class="notes name_class form-control" name="pay_notes"  placeholder="Notes for sales"><?php echo $transaction['notes'] ?> </textarea></th>
               <th><input type="radio" name="payment_type" value="bank"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='bank' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('bank')">Bank
                <input type="radio" name="payment_type" value="cash"   <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cash' )) { echo 'checked="checked"'; }?>   onclick="ac_enable('cash')"/>Cash
                <input type="radio" name="payment_type" value="cheque"  <?php if(isset($pay_type->payment_type) && ($pay_type->payment_type=='cheque' )) { echo 'checked="checked"'; }?>  onclick="ac_enable('cheque')"/>Cheque
                <select name="pay_type"  class="form-control pay_mode" style="width:100%;" >
			    </select></th> 
                
                <th style="display:none;" id="bank_date"><?php
	   
	   echo DatePicker::widget([
	'name' => 'bank_date', 
	'value'=>isset($voucher_update->bank_date)?$voucher_update->bank_date:'',
	'options' => ['placeholder' => 'Select  date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);
	   ?></th>
            
            
          </tr>
        </thead>
        
         </tbody>
           </table>
          
          
           <table class="table table-striped">
        <thead>
          <tr>
              <th>Bill No</th>
              <th>Sales date</th>
              <th>Total</th>
               <th>paid</th>
               <th>Remaining</th>
               <th>pay</th>
              
            
          </tr>
        </thead>
        <tbody class="customer_list">
       <?php if($openingBalnce)
	  {
			  
	  ?>
     <tr class="payrow">
     <td><input type="text" readonly="readonly"  value="<?php echo 'Opening Balance' ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo date('Y'); ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo $customer_details['opening_balance']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo $customer_details['paid']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly" name="paid[]" value="<?php echo ($customer_details['opening_balance']-$customer_details['paid']+$openingBalnce['amount']); ?>" class="form-control remaining" /></td>
     <td><input type="text" readonly="readonly" name="pay[]" value="<?php echo $openingBalnce['amount']; ?>" class="form-control pay" />
     <input type="hidden"  value="opening_balance" name="sales_id[]" required="required" readonly="readonly" />
     <input type="hidden"  value="<?php echo $openingBalnce['id']; ?>" name="opening_id" required="required" readonly="readonly" />
     <input type="hidden"  value="<?php echo $openingBalnce['id']; ?>" name="receipt_id[]" required="required" readonly="readonly" />
     </td>
     </tr>
      
      <?php  }?>
      
      <?php foreach($Receipt_all as $Receipt)
	  {
		$product=Sales::find()->where(['id'=>$Receipt['sales_id']])->one();  	  
	  ?>
     <tr class="payrow">
     <td><input type="text" readonly="readonly"  value="<?php echo isset($product['sales_sno'])?'SALE'.$product['sales_sno']:'SER'.$product['service_sno']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo $product['sales_date']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo $product['total']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly"  value="<?php echo $product['paid']; ?>" class="form-control" /></td>
     <td><input type="text" readonly="readonly" name="paid[]" value="<?php echo ($product['total']-$product['paid']+$Receipt['amount']); ?>" class="form-control remaining" /></td>
     <td><input type="text" readonly="readonly" name="pay[]" value="<?php echo $Receipt['amount']; ?>" class="form-control pay" />
     <input type="hidden"  value="<?php echo $Receipt['id']; ?>" name="receipt_id[]" required="required" readonly="readonly" />
     <input type="hidden"  value="<?php echo $Receipt['sales_id']; ?>" name="sales_id[]" required="required" readonly="readonly" /></td>
     </tr>
      
      <?php  }?>
         </tbody>
           </table>
		
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>


 <script>
 
 function get_amount()
 {
	 var pay_amount=parseFloat($('.pay_amount').val());
	 var customer_id= $('#customer').val();
	 
	 $('.payrow').each(function(){
		var remaining=parseFloat($(this).find(".remaining").val());
		if(pay_amount > 0)
		{
		  if(remaining >= pay_amount)
		  {
			  $(this).find(".pay").val(pay_amount);
			  pay_amount = 0;
		  }
		  else
		  {
			  $(this).find(".pay").val(remaining);
			  pay_amount = pay_amount - remaining;
		  }		  
		}
		else
		{
			  $(this).find(".pay").val("");
		}		
		  //var id=$(this).data('id');
		  //price =price-pay_amount;
	      //alert(price);    
      });
	  
	  if(pay_amount > 0)
	  {
		  alert("Exceeds total remaining");
   	  	  var pay_original_amount=parseFloat($('.pay_amount').val());
		  $('.pay_amount').val(pay_original_amount - pay_amount);
	  }
 }
 
 $(document).ready(function(){
	 var payment_type=$('input[name=payment_type]:checked').val(); 
	  ac_enable(payment_type);
	
	 $.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
	
});
function ac_enable(type)
{
	 $('.pay_mode option').each(function() {
    
           $(this).remove();
    
           });
		   //alert(type);
		    var pay_id=$('#pay_id').val();
		   $.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_pay_type' ?>',
	data:'type='+type,
	contentType: "application/json; charset=utf-8",
    dataType: "json",
	success: function(response){
		 //console.log(response);
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.sno;
			  var x=2;
			 
			for(var item in result)
			{
				 if(result[item]!='')
				 {
					 if(pay_id==item)
					 {
						 var check="selected"
					 }
					   $('<option value="'+item+'" '+check+'>'+result[item]+'</option>').appendTo('.pay_mode');
					 
				 }
			 
			}
			
		  
		}
		
	
	}
	});

		   
	
}
 
 
 
 

</script>
