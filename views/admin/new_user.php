<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$Rolls=Yii::$app->mycomponent->GetRolls();
use kartik\date\DatePicker;
$this->title = 'Staffs | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/admin/newuser'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/users'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Add Staff</h3></div>
        <br />  
<div class="user-form">
<?php $form = ActiveForm::begin([
           
            'options' => [
                'class' => 'form-horizontal'
             ]
        ]); ?>
        
        
        
        <div class="form-group" >
<label class='control-label col-sm-3'>Roll</label>

<div class="col-sm-6">

        <select name="roll" id="roll" class="form-control">
               <?php foreach($Rolls as $roll) { 
			  // if($roll_name!=$roll['item_name'])
			   {
			   ?>
                  <option value="<?php echo $roll['item_name'] ?>"><?php echo ucfirst($roll['item_name']) ?></option>
                  
                  <?php } }?>
               </select>
        </div>
        </div>
 <div class="form-group" >
<?= Html::activeLabel($model, 'Name', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'firstname',['class' => ['form-control']]); ?>
<?= Html::error($model, 'first_name',['style' => 'color:red;']); ?>
</div>
</div>
 <div class="form-group">
    <?= Html::activeLabel($model, 'date_of_join', ['class'=>'control-label col-sm-3']) ?>
    <div class="col-sm-6">
    <?= DatePicker::widget([
	'name' => 'date_of_join', 
	'value' => date('Y-m-d'),
	'options' => ['placeholder' => 'Select issue date ...'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]); ?>
    </div>
    </div>
    


<div class="form-group" >
<?= Html::activeLabel($model, 'qulification', ['class'=>'control-label col-sm-3'],false); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'qulification',['class' => ['form-control']]); ?>
<?= Html::error($model, 'qulification',['style' => 'color:red;']); ?>
</div>
</div>

<?php

if(isset($model->profile))
{
	$url =Yii::$app->request->baseUrl.'/uploads/'.$model->profile;
}
else
{
	$url =Yii::$app->request->baseUrl.'/uploads/no-image.png';
}
 ?>

<div class="form-group" >
<?= Html::activeLabel($model, 'profile', ['class'=>'control-label col-sm-3'],false); ?>
<div class="col-sm-3">
<?= Html::activeFileInput($model, 'profile',['class' => ['form-control'],'id'=>'profile']); ?>
<?= Html::error($model, 'profile',['style' => 'color:red;']); ?>
</div>
<img id="image"  style="border: 2px solid red;height: 98px; width: 100px;" src="<?php echo $url;?>" />
</div>



<div class="form-group" >
<?= Html::activeLabel($model, 'state', ['class'=>'control-label col-sm-3'],false); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'state',['class' => ['form-control']]); ?>
<?= Html::error($model, 'state',['style' => 'color:red;']); ?>
</div>
</div>
<div class="form-group" style="display:none;" >
<?= Html::activeLabel($model, 'last_name', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'last_name',['class' => ['form-control']]); ?>
<?= Html::error($model, 'last_name',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'address', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextArea($model, 'address',['class' => ['form-control']]); ?>
<?= Html::error($model, 'address',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'city', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'city',['class' => ['form-control']]); ?>
<?= Html::error($model, 'city',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'contact_phone', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'contact_phone',['class' => ['form-control']]); ?>
<?= Html::error($model, 'contact_phone',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'gaurdian_mobile', ['class'=>'control-label col-sm-3'],false); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'gaurdian_mob',['class' => ['form-control']]); ?>
<?= Html::error($model, 'gaurdian_mob',['style' => 'color:red;']); ?>
</div>
</div>


<div class="form-group" >
<?= Html::activeLabel($model, 'aathar_no', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'aathar_no',['class' => ['form-control']]); ?>
<?= Html::error($model, 'aathar_no',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'blood_group', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'blood_group',['class' => ['form-control']]); ?>
<?= Html::error($model, 'blood_group',['style' => 'color:red;']); ?>
</div>
</div>


<div class="form-group" >
<?= Html::activeLabel($model, 'Email', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activeTextInput($model, 'email',['class' => ['form-control']]); ?>
<?= Html::error($model, 'email',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'password_hash', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activepasswordInput($model, 'password_hash',['class' => ['form-control']]); ?>
<?= Html::error($model, 'password_hash',['style' => 'color:red;']); ?>
</div>
</div>

<div class="form-group" >
<?= Html::activeLabel($model, 'password_repeat', ['class'=>'control-label col-sm-3']); ?>
<div class="col-sm-6">
<?= Html::activepasswordInput($model, 'password_repeat',['class' => ['form-control']]); ?>
<?= Html::error($model, 'password_repeat',['style' => 'color:red;']); ?>
</div>
</div>



    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>
document.getElementById("profile").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>

