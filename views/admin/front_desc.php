<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
 

$this->title = 'Front Office | Add';

if(isset($_GET['id']))
{
	/*$id=$_GET['id'];
	$Front=Yii::$app->mycomponent->Get_front_user_details($id);*/
	$where_con='front_id='.$id;
	$Front=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_front_office');
	//print_r($Front);
}
 ?>
 
 <h1>Front Office</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/frontadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/front'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
<div class="user-form">
 <?= Html::beginForm(['admin/usercreate'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control']];
 ?>

 <div class="form-group  required" >
 <?= Html::label('First Name', 'restname',['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'first_name',isset($Front['first_name'])?$Front['first_name']:'',$options ) ?>
 <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
 <?= Html::Input('hidden', 'photo',isset($Front['restaurant_slug'])?$Front['restaurant_slug']:'','' ) ?>
  <?= Html::Input('hidden', 'code','Front','' ) ?>
  <?= Html::Input('hidden', 'tbl_name','tbl_front_office','' ) ?>
  <?= Html::Input('hidden', 'where','front_id','' ) ?>
  <?= Html::Input('hidden', 'render','front','' ) ?>
</div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Last Name','restphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'last_name',isset($Front['last_name'])?$Front['last_name']:'',$options ) ?>
  </div>
  </div>
   <div class="form-group  required" >
 <?= Html::label('Address', 'contactname', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'address',isset($Front['Address'])?$Front['Address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >
 
 <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'contact_phone',isset($Front['contact_phone'])?$Front['contact_phone']:'',$options ) ?>
 </div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-3']) ?>
  <div class="col-sm-6">
 <?= Html::Input('text', 'contact_email',isset($Front['email_address'])?$Front['email_address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >

 <?= Html::label('City', 'country', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'city',isset($Front['city'])?$Front['city']:'',$options ) ?>
 </div>
 </div>
 
  <?php /*?><div class="form-group  required" >

 <?= Html::label('Username', 'Username', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('email', 'username',isset($Front['username'])?$Front['username']:'',$options ) ?>
 </div>
 </div><?php */?>
 
  <div class="form-group  required" >

 <?= Html::label('Password', 'Password', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'password','',$options ) ?>
 
 <?= Html::Input('hidden', 'pass',isset($Front['password'])?$Front['password']:'','' ) ?>
 </div>
 </div>

 <?= Html::submitButton('Submit' , ['class'=>'btn btn-success']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>