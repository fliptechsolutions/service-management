<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
  use kartik\select2\Select2;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Staffs | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/
 foreach($Rolls as $rolle)
  {
	 $All_rolls[$rolle['item_name']]= ucfirst($rolle['item_name']); 
  }
$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['newuser'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Staffs'],
    'columns' => [
	
	
       
		  [
		    'label'=>'Name',
		    'attribute'=>'firstname',
			'value'=> function($model)
              { 
			  
                   return  Html::a('<span>'.$model->firstname.'</span>', ['/admin/updateuser','id'=>$model->id]);      
              },
            'format' => 'raw'
		
          ],
		  [
            'attribute'=>'roll',
			'filter'=>$All_rolls,
            'format' => 'raw'
        ],
	
		  [
            'attribute'=>'contact_phone',
		  ],
		  
		  [
            'attribute'=>'address',
           
          ],
		  
		  
		  
		  [
            'attribute'=>'email',
			//'contentOptions' => ['class' => 'text-center'],
			// 'headerOptions' => ['class' => 'text-center']
          ],
		  
		[  
		'label' => 'Photo',
		'format' => 'raw',
         'value'=>function($model) {
			         if(isset($model->profile))
					{
						$url =Yii::$app->request->baseUrl.'/uploads/'.$model->profile;
					}
					else
					{
						$url =Yii::$app->request->baseUrl.'/uploads/no-image.png';
					}
			  return Html::img($url, ['alt'=>'myImage','width'=>'70','height'=>'50']);
			   },
		 'width'=>'10%',
		 ],
		
		  [
		    'label'=>'Gaurdian Phone',
            'attribute'=>'gaurdian_mob',
           
          ],
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit} {delete}',
              'buttons' => [
                
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this Customer?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/admin/updateuser', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/admin/deleteuser', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);


?>
 
 

<div id="state-list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
	<script>
    function assign_complaint(complaint_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(complaint_id);
		$(".assign_to").trigger('click');
        
    }
    </script>


<script>
function status_change(order_id) {
//alert(order_id);
//var x=val.split(',');
//alert(x[1]);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/status_change' ?>',
	data:'order_id='+order_id,
	success: function(data){
	//alert(data);
		$("#state-list").html(data);
		$("#stc").trigger('click');
	}
	});
}
</script>


<script>
function edit_complaint(order_id) {

//alert(order_id);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/register' ?>',
	data:'order_id='+order_id,
	success: function(data){
	alert(data);
		$("#receipt").html(data);
		$(".receipt_trigg").trigger('click');
	}
	});
}
</script>

<script>
function st_change(val) {

var status=$('#st').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/status_update' ?>',
	data:'id='+status+'&order_id='+val,
	success: function(data){
	//alert("#status_"+val);
	
		$("#status_"+val).html(data);
		$("#cls").trigger('click');
	}
	});
}


</script>


<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Assign to </label>
               <div class="col-sm-8"> 
			   <select name="role" id="role" class="form-control" >
               <?php foreach($Rolls as $rolle) { ?>
                  <option value="<?php echo $rolle['item_name'] ?>"><?php echo ucfirst($rolle['item_name']) ?></option>
                  
                  <?php } ?>
               </select>
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="notes" id="notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function st_assign()
 {

var cid=$('#cid').val();
var notes=$('#notes').val();
var role=$('#role').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/roleassign' ?>',
	data:'cid='+cid+'&notes='+notes+'&role='+role,
	success: function(data){
	//alert(data);
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->