<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
  use kartik\select2\Select2;
  use kartik\date\DatePicker;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Attendance | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/
 foreach($Rolls as $rolle)
  {
	 $All_rolls[$rolle['item_name']]= ucfirst($rolle['item_name']); 
  }
$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            //Html::a('<i class="glyphicon glyphicon-plus"></i>', ['newuser'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Attendance Reports'],
    'columns' => [
	
	
	 [
            'label'=>'Staff name',
			'attribute'=>'firstname',
			'value'=>'team.firstname',
			
        ],
	
	   
        
		  [
		    'label'=>'Attendance',
		    //'attribute'=>'firstname',
			'value'=> function($model)
              { 
			     if($model->attendance==1)
				 {
					 $status_class = "label-success";
					 $att='Present';
				 }
				 else
				 {
					 $status_class = "label-danger";
					 $att='Absent';
				 }
                   return  Html::a('<span class="label '.$status_class.'">'.$att.'</span>','');      
              },
            'format' => 'raw'
		
          ],
		
	
		  [
		     'label'=>'In Time',
            'value'=>'attendance_date',
			//'filter'=>$All_rolls,
           // 'format' => 'raw'
        ],
		  
		 [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Date filter',
				
				'attribute'=>'created_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'created_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
			     
				 'value'=>date('Y-m-d'),
				
				'opens'=>'left'
				
				]
				
				]),
				
				/*'editableOptions'=> [
				
				'header' => 'term start',
				
				'inputType' => \kartik\editable\Editable::INPUT_TEXT

],
*/



	            'format' => 'raw'		,
			
		
      ],
		
    ],
]);


?>
 
 

<div id="state-list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
	<script>
    function attendance(user_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(user_id);
		$(".assign_to").trigger('click');
        
    }
    </script>


<script>
function status_change(order_id) {
//alert(order_id);
//var x=val.split(',');
//alert(x[1]);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/status_change' ?>',
	data:'order_id='+order_id,
	success: function(data){
	//alert(data);
		$("#state-list").html(data);
		$("#stc").trigger('click');
	}
	});
}
</script>




<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Date</label>
               <div class="col-sm-8"> 
			  <?php
			  
				echo DatePicker::widget([
				'name' => 'check_issue_date', 
				'value' => date('Y-m-d h:i:s'),
				'options' => ['placeholder' => 'Select date ...','id'=>'att_date'],
				'pluginOptions' => [
				'format' => 'yyyy-mm-dd '.date('h:i:s'),
				'todayHighlight' => true
				]
				]);
			   ?>
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4" >Attendance</label>
               <div class="col-sm-8"> 
			   <input type="radio" name="attend" id="present" value="1" />Present
               <input type="radio" name="attend" id="present" value="0" />Absent
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="OK" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function st_assign()
 {

var user_id=$('#cid').val();
var att_date=$('#att_date').val();
var type=$('input[name=attend]:checked').val();
//alert(type);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/saveattendance' ?>',
	data:'user_id='+user_id+'&att_date='+att_date+'&att='+type,
	success: function(data){
	//alert(data);
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
		$.pjax.reload({container:'#w0'});
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->