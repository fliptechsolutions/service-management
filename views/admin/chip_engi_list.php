<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 
 ?>
  <?php $this->title = 'Chiplevel Team'; ?>
<?php 

$chiplevel=Yii::$app->mycomponent->GetUser_list('tbl_chiplevel_team');

?>
 <h1>Chiplevel Team</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/chipadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/chip'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
          <th>Firstname</th>
          <th>Last Name</th>
          <th>Contact Email</th>
          <th>Contact Phone</th>
          <th>Address</th>
          <th>City</th>
          <th>Edit/Delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($chiplevel as $user)
	{
		
		
		
		?>
      <tr>
         <td><?php echo ucfirst($user['first_name']); ?> </td>
         <td><?php echo $user['last_name']; ?></td>
         <td><?php echo $user['email_address']; ?></td>
         <td><?php echo $user['contact_phone']; ?></td>
         <td><?php echo $user['Address']; ?></td>
         <td><?php echo $user['city']; ?></td>
         <td> <?= Html::a('<b>Edit</b>', 
        ['admin/chipadd', 'id' =>$user['chip_id']], 
        ['class' => 'profile-link']);?>/
          <?= Html::a('<b>Delete</b>', 
        ['admin/deleteuser', 'id' =>$user['chip_id'], 'tbl_name' =>'tbl_chiplevel_team','where_name' =>'chip_id','where_value' =>$user['chip_id'],'render' =>'chip'],
		 
        ['class' => 'profile-link',
		
		'data' => [
            'confirm' => 'Are you sure you want to delete this user?',
            'method' => 'post',
        ], 
		]);?></td>
      </tr>
     <?php } ?>
    </tbody>
  </table>
</div>
</div></div>
