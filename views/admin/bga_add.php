<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
 

$this->title = 'B G A Dept | Add';

if(isset($_GET['id']))
{
	$id=$_GET['id'];
	$where_con='bga_id='.$id;
	$BGA=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_bga_team');
	//print_r($BGA);
}
 ?>
 
 <h1>B G A Dept</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/bgaadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/bga'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
<div class="user-form">
 <?= Html::beginForm(['admin/usercreate'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control']];
 ?>

 <div class="form-group  required" >
 <?= Html::label('First Name', 'restname',['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'first_name',isset($BGA['first_name'])?$BGA['first_name']:'',$options ) ?>
 <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
 <?= Html::Input('hidden', 'photo',isset($BGA['restaurant_slug'])?$BGA['restaurant_slug']:'','' ) ?>
  <?= Html::Input('hidden', 'code','BGA','' ) ?>
  <?= Html::Input('hidden', 'tbl_name','tbl_bga_team','' ) ?>
  <?= Html::Input('hidden', 'where','bga_id','' ) ?>
  <?= Html::Input('hidden', 'render','bga','' ) ?>
</div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Last Name','restphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'last_name',isset($BGA['last_name'])?$BGA['last_name']:'',$options ) ?>
  </div>
  </div>
   <div class="form-group  required" >
 <?= Html::label('Address', 'contactname', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'address',isset($BGA['Address'])?$BGA['Address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >
 
 <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'contact_phone',isset($BGA['contact_phone'])?$BGA['contact_phone']:'',$options ) ?>
 </div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-3']) ?>
  <div class="col-sm-6">
 <?= Html::Input('text', 'contact_email',isset($BGA['email_address'])?$BGA['email_address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >

 <?= Html::label('City', 'country', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'city',isset($BGA['city'])?$BGA['city']:'',$options ) ?>
 </div>
 </div>
 
  <?php /*?><div class="form-group  required" >

 <?= Html::label('Username', 'Username', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('email', 'username',isset($BGA['username'])?$BGA['username']:'',$options ) ?>
 </div>
 </div><?php */?>
 
  <div class="form-group  required" >

 <?= Html::label('Password', 'Password', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'password','',$options ) ?>
 
 <?= Html::Input('hidden', 'pass',isset($BGA['password'])?$BGA['password']:'','' ) ?>
 </div>
 </div>

 <?= Html::submitButton('Submit' , ['class'=>'btn btn-success']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>