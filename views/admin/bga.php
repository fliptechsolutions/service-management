<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 
 ?>
  <?php $this->title = 'B G A Dept'; ?>
<?php 

$BGA=Yii::$app->mycomponent->GetUser_list('tbl_bga_team');

?>
 <h1>B G A Dept</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/bgaadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/bga'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
          <th>Firstname</th>
          <th>Last Name</th>
          <th>Contact Email</th>
          <th>Contact Phone</th>
          <th>Address</th>
          <th>City</th>
          <th>Edit/Delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($BGA as $user)
	{
		
		
		
		?>
      <tr>
         <td><?php echo ucfirst($user['first_name']); ?> </td>
         <td><?php echo $user['last_name']; ?></td>
         <td><?php echo $user['email_address']; ?></td>
         <td><?php echo $user['contact_phone']; ?></td>
         <td><?php echo $user['Address']; ?></td>
         <td><?php echo $user['city']; ?></td>
         <td> <?= Html::a('<b>Edit</b>', 
        ['admin/bgaadd', 'id' =>$user['bga_id']], 
        ['class' => 'profile-link']);?>/
          <?= Html::a('<b>Delete</b>', 
        ['admin/deleteuser', 'id' =>$user['bga_id'], 'tbl_name' =>'tbl_bga_team','where_name' =>'bga_id','where_value' =>$user['bga_id'],'render' =>'bga'],
		 
        ['class' => 'profile-link',
		
		'data' => [
            'confirm' => 'Are you sure you want to delete this user?',
            'method' => 'post',
        ], 
		]);?></td>
      </tr>
     <?php } ?>
    </tbody>
  </table>
</div>
</div></div>
