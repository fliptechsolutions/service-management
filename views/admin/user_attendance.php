<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
$Rolls=Yii::$app->mycomponent->GetRolls();
use kartik\date\DatePicker;
$this->title = 'Attendance | '.Yii::$app->mycomponent->Get_settings('company_name'); 
$attendance=Yii::$app->mycomponent->Get_attendance(Yii::$app->user->identity->id);
?>
<div class="panel panel-default">
<div class="panel-body">

        <div> <h3>Attendance</h3></div>
        <br />  
<div class="user-form">
 <?= Html::beginForm(['admin/add_attendance'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
$options = ['class' => ['form-control'],'required'=>'required'];?>
 <input type="hidden" name="" id="att_date" value="<?php echo date('Y-m-d h:i:s'); ?>" />
    <div class="form-group" >
    <?= Html::label('Present', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?php
	
	echo 	CheckboxX::widget([
						'name'=>'attendance',
						'value'=>$attendance,
						'options'=>['class'=>'myCheckbox'],
						'initInputType' => CheckboxX::INPUT_CHECKBOX,
						'pluginOptions'=>['threeState'=>false]
					]);
	 ?>
    </div>
    </div>
    
     <?= Html::Button('SAVE' , ['class'=>'btn btn-success','onclick'=>'st_assign('.Yii::$app->user->identity->id.')']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>
 <script>
function st_assign(id)
 {

var user_id=id;
var att_date=$('#att_date').val();
var type=$('#w0').val();
//alert(att_date);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/saveattendance' ?>',
	data:'user_id='+user_id+'&att_date='+att_date+'&att='+type,
	success: function(data){
	
	}
	});
}


</script>

