<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 
 ?>
  <?php $this->title = 'Front Office'; ?>
<?php 

$Front=Yii::$app->mycomponent->GetUser_list('tbl_front_office');

?>
 <h1>Front Office</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/frontadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/front'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
          <th>Firstname</th>
          <th>Last Name</th>
          <th>Contact Email</th>
          <th>Contact Phone</th>
          <th>Address</th>
          <th>City</th>
          <th>Edit/Delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($Front as $items)
	{
		
		
		
		?>
      <tr>
         <td><?php echo ucfirst($items['first_name']); ?> </td>
         <td><?php echo $items['last_name']; ?></td>
         <td><?php echo $items['email_address']; ?></td>
         <td><?php echo $items['contact_phone']; ?></td>
         <td><?php echo $items['Address']; ?></td>
         <td><?php echo $items['city']; ?></td>
         <td> <?= Html::a('<b>Edit</b>', 
        ['admin/frontadd', 'id' =>$items['front_id']], 
        ['class' => 'profile-link']);?>/
          <?= Html::a('<b>Delete</b>', 
        ['admin/deleteuser', 'id' =>$items['front_id'], 'tbl_name' =>'tbl_front_office','where_name' =>'front_id','where_value' =>$items['front_id'],'render' =>'front'],
		 
        ['class' => 'profile-link',
		
		'data' => [
            'confirm' => 'Are you sure you want to delete this user?',
            'method' => 'post',
        ], 
		]);?></td>
      </tr>
     <?php } ?>
    </tbody>
  </table>
</div>
</div></div>
