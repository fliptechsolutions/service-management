<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
 

$this->title = 'Checking | Add';

if(isset($_GET['id']))
{
	
	$where_con='checking_id='.$id;
	$Checking=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_front_office');
	//print_r($Checking);
}
 ?>
 
 <h1>Checking Team</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/checkingadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/checking'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
<div class="user-form">
 <?= Html::beginForm(['admin/usercreate'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control']];
 ?>

 <div class="form-group  required" >
 <?= Html::label('First Name', 'restname',['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'first_name',isset($Checking['first_name'])?$Checking['first_name']:'',$options ) ?>
 <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
 <?= Html::Input('hidden', 'photo',isset($Checking['restaurant_slug'])?$Checking['restaurant_slug']:'','' ) ?>
  <?= Html::Input('hidden', 'code','checking','' ) ?>
  <?= Html::Input('hidden', 'tbl_name','tbl_checking_team_list','' ) ?>
  <?= Html::Input('hidden', 'where','checking_id','' ) ?>
   <?= Html::Input('hidden', 'render','checking','' ) ?>
</div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Last Name','restphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'last_name',isset($Checking['last_name'])?$Checking['last_name']:'',$options ) ?>
  </div>
  </div>
   <div class="form-group  required" >
 <?= Html::label('Address', 'contactname', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'address',isset($Checking['Address'])?$Checking['Address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >
 
 <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'contact_phone',isset($Checking['contact_phone'])?$Checking['contact_phone']:'',$options ) ?>
 </div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-3']) ?>
  <div class="col-sm-6">
 <?= Html::Input('text', 'contact_email',isset($Checking['email_address'])?$Checking['email_address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >

 <?= Html::label('City', 'country', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'city',isset($Checking['city'])?$Checking['city']:'',$options ) ?>
 </div>
 </div>
 
  <?php /*?><div class="form-group  required" >

 <?= Html::label('Username', 'Username', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('email', 'username',isset($Checking['username'])?$Checking['username']:'',$options ) ?>
 </div>
 </div><?php */?>
 
  <div class="form-group  required" >

 <?= Html::label('Password', 'Password', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'password','',$options ) ?>
 
 <?= Html::Input('hidden', 'pass',isset($Checking['password'])?$Checking['password']:'','' ) ?>
 </div>
 </div>

 <?= Html::submitButton('Submit' , ['class'=>'btn btn-success']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>