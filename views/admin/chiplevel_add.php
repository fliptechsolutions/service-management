<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
 

$this->title = 'Chiplevel Engi | Add';

if(isset($_GET['id']))
{
	$id=$_GET['id'];
	$where_con='chip_id='.$id;
	$Chip=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_chiplevel_team');
	//print_r($Chip);
}
 ?>
 
 <h1>Chiplevel Engineer</h1>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Addnew', ['/admin/chipadd'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/admin/chip'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />  
<div class="user-form">
 <?= Html::beginForm(['admin/usercreate'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control']];
 ?>

 <div class="form-group  required" >
 <?= Html::label('First Name', 'restname',['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'first_name',isset($Chip['first_name'])?$Chip['first_name']:'',$options ) ?>
 <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
 <?= Html::Input('hidden', 'photo',isset($Chip['restaurant_slug'])?$Chip['restaurant_slug']:'','' ) ?>
  <?= Html::Input('hidden', 'code','Chip','' ) ?>
  <?= Html::Input('hidden', 'tbl_name','tbl_chiplevel_team','' ) ?>
  <?= Html::Input('hidden', 'where','chip_id','' ) ?>
  <?= Html::Input('hidden', 'render','chip','' ) ?>
</div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Last Name','restphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'last_name',isset($Chip['last_name'])?$Chip['last_name']:'',$options ) ?>
  </div>
  </div>
   <div class="form-group  required" >
 <?= Html::label('Address', 'contactname', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'address',isset($Chip['Address'])?$Chip['Address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >
 
 <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'contact_phone',isset($Chip['contact_phone'])?$Chip['contact_phone']:'',$options ) ?>
 </div>
 </div>
 <div class="form-group  required" >
 <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-3']) ?>
  <div class="col-sm-6">
 <?= Html::Input('text', 'contact_email',isset($Chip['email_address'])?$Chip['email_address']:'',$options ) ?>
 </div>
 </div>
  <div class="form-group  required" >

 <?= Html::label('City', 'country', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'city',isset($Chip['city'])?$Chip['city']:'',$options ) ?>
 </div>
 </div>
 
  <?php /*?><div class="form-group  required" >

 <?= Html::label('Username', 'Username', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('email', 'username',isset($Chip['username'])?$Chip['username']:'',$options ) ?>
 </div>
 </div><?php */?>
 
  <div class="form-group  required" >

 <?= Html::label('Password', 'Password', ['class'=>'control-label col-sm-3']) ?>
<div class="col-sm-6">
 <?= Html::Input('text', 'password','',$options ) ?>
 
 <?= Html::Input('hidden', 'pass',isset($Chip['password'])?$Chip['password']:'','' ) ?>
 </div>
 </div>

 <?= Html::submitButton('Submit' , ['class'=>'btn btn-success']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>