<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;


$this->title = 'Suppliers | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['newvendor'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Suppliers'],
    'columns' => [
	       [
		    'label'=>'Company name',
		    'attribute'=>'company_name',
			'value'=>'company_name',
          ],
       
		  [
		    'label'=>'Contact Person',
		    'attribute'=>'first_name',
			'value'=>'first_name',
          ],
		  
		  [
            'attribute'=>'contact_phone',
		  ],
		
		  [
            'attribute'=>'Address',
           
          ],
		  
		 
		  
		  [
		  'label'=>'Email',
            'attribute'=>'email_address',
			'contentOptions' => ['class' => 'text-center'],
			 'headerOptions' => ['class' => 'text-center']
          ],
		
		
		
		
		
		
		
		[
            'attribute'=>'date_created',
            //'pageSummary'=>'Page Summary',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit} {delete}',
              'buttons' => [
                
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/complaint/updatevendor', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/complaint/deletevendor', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);


?>
 
 
