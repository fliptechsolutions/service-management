<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Supplier | '.Yii::$app->mycomponent->Get_settings('company_name'); 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/complaint/newvendor'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/complaint/vendor'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Supplier</h3></div>
        <br />  
<div class="user-form">
<?php $form = ActiveForm::begin([
           
            'options' => [
                'class' => 'form-horizontal'
             ]
        ]); ?>
        
    <div class="form-group" >
    <?= Html::activeLabel($model, 'company_name', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'company_name',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'company_name',['style' => 'color:red;']); ?>
    </div>
    </div>
    <div class="form-group" >
    <?= Html::activeLabel($model, 'tin_no', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'tin_no',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'tin_no',['style' => 'color:red;']); ?>
    </div>
    </div>
    
     <div class="form-group" >
    <?= Html::activeLabel($model, 'cst_number', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'cst_number',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'cst_number',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    

        
    <div class="form-group" >
    <?= Html::activeLabel($model, 'first_name', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'first_name',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'first_name',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    <div class="form-group" style="display:none;" >
    <?= Html::activeLabel($model, 'last_name', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'last_name',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'last_name',['style' => 'color:red;']); ?>
    </div>
    </div>
     <div class="form-group" >
    <?= Html::activeLabel($model, 'city', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'city',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'city',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    <div class="form-group" >
    <?= Html::activeLabel($model, 'Address', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'Address',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'Address',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    <div class="form-group" >
    <?= Html::activeLabel($model, 'state', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'state',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'state',['style' => 'color:red;']); ?>
    </div>
    </div>
    
     <div class="form-group" >
    <?= Html::activeLabel($model, 'pincode', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'pincode',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'pincode',['style' => 'color:red;']); ?>
    </div>
    </div>
    
   
    
    <div class="form-group" >
    <?= Html::activeLabel($model, 'contact_phone', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'contact_phone',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'contact_phone',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    <div class="form-group" >
    <?= Html::activeLabel($model, 'office_no', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'office_no',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'office_no',['style' => 'color:red;']); ?>
    </div>
    </div>
    
    <div class="form-group" >
    <?= Html::activeLabel($model, 'Email', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'email_address',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'email_address',['style' => 'color:red;']); ?>
    </div>
    </div>
    
     <div class="form-group" >
    <?= Html::activeLabel($model, 'opening_balance', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextInput($model, 'opening_balance',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'opening_balance',['style' => 'color:red;']); ?>
    </div>
    </div>

 <div class="form-group" >
    <?= Html::activeLabel($model, 'Notes', ['class'=>'control-label col-sm-3']); ?>
    <div class="col-sm-6">
    <?= Html::activeTextArea($model, 'notes',['class' => ['form-control']]); ?>
    <?= Html::error($model, 'notes',['style' => 'color:red;']); ?>
    </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>
</div>
</div>