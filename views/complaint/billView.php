<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
use app\models\Accesories;
use app\models\Customer;

 $customer_details = Customer::findOne(['id' =>$_GET['customer_id']]);
 $product_all = \app\models\Inventory::find()->where(['=', 'sales_id', $_GET['sales_id']])->all();
 
 $Sales = \app\models\Sales::find()->where(['=', 'id', $_GET['sales_id']])->one();
 $sales_number='';
 if(isset($Sales['service_sno']))
 {
	 $sales_number=$Sales['service_sno'];
 }
 else if(isset($Sales['sales_sno']))
 {
	 $sales_number=$Sales['sales_sno'];
 }

?>

<div style="border:0px solid #000; text-align: center; width: 100%;">
    <h3 style="border:0px solid #000; margin:0 auto; font-family: Arial; font-size:18px; font-weight: bold;">INVOICE</h3>
</div>

 <table cellspacing="0" cellpadding="5" style="border:0px solid #000; width: 100%; text-align: left;">    
    <tbody>
      <tr>
        <td rowspan="3" style="border:1px solid #000; border-right:0; width: 50%;">
            <hgroup>
                <h2 style="border:0px solid #F00; font-family: Times New Roman; font-size:15px; font-weight: bold;"><?php echo Yii::$app->mycomponent->Get_settings('company_name'); ?></h2>
                <h3 style="border:0px solid #F00; font-family: Arial; font-size:10px; font-weight: bold; text-transform: uppercase;"><?php echo Yii::$app->mycomponent->Get_settings('title'); ?></h3>
               <!-- <h3 style="border:0px solid #F00; font-family: Arial; font-size:10px; font-weight: bold;">(Used Laptop & Accessories Sale)</h3>-->
            </hgroup>
            <hr style="line-height:1; margin:0px auto 0; color:#fff;" />
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo Yii::$app->mycomponent->Get_settings('address'); ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">MOBILE  : <?php echo Yii::$app->mycomponent->Get_settings('contact_number1'); ?>, <?php echo Yii::$app->mycomponent->Get_settings('contact_number2'); ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Service : <?php echo Yii::$app->mycomponent->Get_settings('contact_number3'); ?></p>            
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo Yii::$app->mycomponent->Get_settings('website'); ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">e-mail:<?php echo Yii::$app->mycomponent->Get_settings('email'); ?></p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-bottom:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Invoice No.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;"><?php echo $Sales['id'].$sales_number; ?></p>
        </td>
        <td style="border:1px solid #000; border-bottom:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Dated.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;"><?php $date=date_create($Sales['sales_date']);
echo date_format($date,"d-M-Y");?></p>
        </td>
      </tr>  
        
      <tr>
        <td style="border:1px solid #000; border-right:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Delivery Note</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;  </p>
        </td>
        <td style="border:1px solid #000; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Mode/Terms of Payment</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-top:0; border-right:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">&nbsp;</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
        <td style="border:1px solid #000; border-top:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">&nbsp;</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
      </tr>
        
      <tr>
        <td rowspan="3" style="border:1px solid #000; border-top:0; border-right:0; width: 50%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Buyer</p>
            <hgroup>
                <h2 style="border:0px solid #F00; font-family: Times New Roman; font-size:15px; font-weight: bold;"><?php echo strtoupper($customer_details['company_name']); ?></h2>
            </hgroup>
            <hr style="line-height:1; margin:0px auto 0; color:#fff;" />
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo $customer_details['Address']; ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo $customer_details['email_address']; ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">MOBILE  : <?php echo $customer_details['contact_phone']; ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo $customer_details['pincode']; ?></p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;"><?php echo $customer_details['state']; ?></p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-top:0; border-bottom:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Buyer's Order No.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
        <td style="border:1px solid #000; border-top:0; border-bottom:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Dated.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-right:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Despatch Document No.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
        <td style="border:1px solid #000; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Delivery Note Date</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-right:0; border-top:0; width: 25%; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Despatched through</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
        <td style="border:1px solid #000; width: 25%; border-top:0; vertical-align: top;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Destination</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:15px; font-weight:bold;">&nbsp;</p>
        </td>
      </tr>
     
    </tbody>
  </table>
  
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <thead>
            <tr>
                <th style="border: 1px solid #000; border-top:0; border-right:0; border-bottom: 1px solid #000; font-size:13px; font-weight: normal; padding: 5px; text-align: left; width: 30px;">Sl No.</th>
                <th style="border: 1px solid #000; border-top:0; border-right:0; font-size:13px; padding: 5px 0; text-align: center; font-weight: normal;">Description of Goods</th>
                <th style="border: 1px solid #000; border-top:0; border-right:0; font-size:13px; padding: 5px 0; text-align: center; font-weight: normal; width: 80px;">Quantity</th>
                <th style="border: 1px solid #000; border-top:0; border-right:0; font-size:13px; padding: 5px 0; text-align: center; font-weight: normal; width: 80px;">Rate</th>
                <th style="border: 1px solid #000; border-top:0; font-size:13px; padding: 5px 0; text-align: center; width: 120px; font-weight: normal;">Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php
		$sno=1;
		$vat_amt='';
		foreach($product_all as $product)
		{ 
		
		   $price[]=$product['price'];
		   $Qty[]=1;
		   $vat_amt[]=($product['price']* $Sales['vat']/100);
		   $product_name = \app\models\Products::findOne(['pid' =>$product['product_id'] ]);
		   for($i=0;$i<4;$i++)
		   {
			 $cell1='&nbsp;';
			 $cell2='&nbsp;';
			 $cell3='&nbsp;';
			 $cell4='&nbsp;';
			 $cell5='&nbsp;';
		     if($i==0)
			 {
				 $cell1=$sno;
				 $cell2=strtoupper($product_name['product_name']);
			 }
			 else if($i==1)
			 {
				 $cell2='S.No : '.$product['sno'];
				 $cell3=1;
				 $cell4=$product['price'];
				 $cell5=$product['price'];
			 }
			 else if($i==2)
			 {
				  $notes = \app\models\Group_sales::findOne(['group_id' =>$product['group_id_sales'] ]);
				 $cell2=$notes['notes'];
				// $cell3=1;
				// $cell4=$product['price'];
				// $cell5=$product['price'];
			 }
		   ?>
           
			<?php 
			$product_array[]=['cell1'=> $cell1,
			                                        'cell2'=>$cell2,
			                                        'cell3'=>$cell3,
													'cell4'=>$cell4,
													'cell5'=>$cell5,] ?>
			
		<?php }
		$sno++;
		 } ?>
         
         <?php 
		// print_r($product_array);
		 //exit();
		 
		 foreach($product_array as $product_row){ ?>
            <tr>
                <td style="border: 1px solid #000; border-top:0; border-right:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;"><?php echo $product_row['cell1']; ?></td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; line-height:1;"><b><?php echo $product_row['cell2'];?></b></td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; text-align: right; width: 80px;"><?php echo $product_row['cell3'];?></td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; text-align: right; width: 80px;"><?php echo $product_row['cell4']; ?></td>
                <td style="border: 1px solid #000; border-top:0; font-size:13px; border-bottom:0; padding: 2px 5px; text-align: right; width: 120px;"><?php echo $product_row['cell5']; ?></td>
            </tr>
            <?php } ?>
		<tr>
                <td style="border: 1px solid #000; border-top:0; border-right:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; text-align: right;">Output vat @ <?php echo $Sales['vat']; ?> %</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-top:0; font-size:13px; border-bottom:0; padding: 2px 5px; text-align: right; width: 120px;"><?php echo number_format(array_sum($vat_amt),2);?></td>
            </tr>
            <?php if(isset($Sales['trans']) && $Sales['trans']!='') { ?>
            <tr>
                <td style="border: 1px solid #000; border-top:0; border-right:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; text-align: right;">Package and forwarding</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-top:0; font-size:13px; border-bottom:0; padding: 2px 5px; text-align: right; width: 120px;"><?php echo number_format($Sales['trans'],2);?></td>
            </tr>
		<?php } ?>
        
        <?php if(isset($Sales['discount']) && $Sales['discount']!='') { ?>
            <tr>
                <td style="border: 1px solid #000; border-top:0; border-right:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px; text-align: right;">Discount</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-top:0; font-size:13px; border-bottom:0; padding: 2px 5px; text-align: right; width: 120px;"><?php echo number_format($Sales['discount'],2);?></td>
            </tr>
		<?php } ?>
            <?php for($x=count($product_array); $x<16; $x++) { ?>
            <tr>
                <td style="border: 1px solid #000; border-top:0; border-right:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 5px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-top:0; font-size:13px; border-bottom:0; padding: 2px 5px 0; text-align: center; width: 120px;">&nbsp;</td>
            </tr>
            <?php } ?>
            <tr>
                <td style="border: 1px solid #000; border-right:0; font-size:13px; padding: 2px 0; text-align: center; width: 30px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-right:0; font-size:13px; padding: 2px 5px; text-align: right;">Total</td>
                <td style="border: 1px solid #000; border-right:0; font-size:13px; padding: 2px 0; text-align: center; width: 80px;"><?php echo array_sum($Qty);?></td>
                <td style="border: 1px solid #000; border-right:0; font-size:13px; padding: 2px 5px; text-align: center; width: 80px;">&nbsp;</td>
                <td style="border: 1px solid #000; font-size:13px; padding: 2px 5px; text-align: right; width: 120px;"><?php $total=array_sum($vat_amt)+array_sum($price)+$Sales['trans']-$Sales['discount']; 
				$round_total= round($total);
				echo number_format($round_total,2);
				?></td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td style="border: 1px solid #000; border-right:0; border-top:0; border-bottom:0; font-size:12px; padding: 5px;">Amount Chargeable (in words)</td>
                <td style="width:100px; text-align: right; border: 1px solid #000; border-left:0; border-bottom:0; border-top:0; font-size:12px; padding: 5px; font-style: italic;">E. & O.E</td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight:bold; border: 1px solid #000; border-bottom:0; border-top:0; font-size:13px; padding: 5px 0 20px 5px;">    
                    <?php 
					$Paise='';
					 $number=$round_total;
                        /*$number=explode('.',$total);
						if(isset($number[1]) && $number[1]!=00)
						{
							
							$num=$number[1];
							if($number[1]<9)
							{
								$num=$number[1].'0';
							}
							$Paise=' and '.Yii::$app->formatter->asSpellout($num).' Paise';
						}*/
						
						//echo strtoupper(Yii::$app->formatter->asSpellout($number));
						
						//echo Yii::$app->formatter->asSpellout($number);
                        echo strtoupper(Yii::$app->mycomponent->Rupees_words($number));
                    ?>
                </td>
            </tr>
        </tbody>
    </table>

  <table cellspacing="0" cellpadding="0" style="border:0px solid #000; width: 100%; text-align: left;">    
    <tbody>
      <tr>
        <td style="border:1px solid #000; border-right:0; border-top:0; border-bottom:0; vertical-align: top; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">&nbsp;</p>
        </td>
        <td style="border:1px solid #000; border-left:0; border-right:0; border-top:0; border-bottom:0; vertical-align:top; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">&nbsp;</p>
        </td>
        <td colspan="2" style="border:1px solid #000; border-left:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Company's Bank Details</p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-right:0; border-top:0; border-bottom:0; width: 20%; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Company's VAT TIN</p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; width: 30%; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo Yii::$app->mycomponent->Get_settings('tin_number'); ?></p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; width: 20%; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Bank Name</p>
        </td>
        <td style="border:1px solid #000; border-bottom:0; border-left:0; border-top:0; width: 30%; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo Yii::$app->mycomponent->Get_settings('bank_name'); ?></p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-right:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Company's CST No.</p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo Yii::$app->mycomponent->Get_settings('cst_number'); ?></p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">A/c No.</p>
        </td>
        <td style="border:1px solid #000; border-bottom:0; border-left:0; border-top:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo Yii::$app->mycomponent->Get_settings('account_number'); ?></p>
        </td>
      </tr>
        
      <tr>
        <td style="border:1px solid #000; border-right:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Buyer's VAT TIN</p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo $customer_details['tin_no']; ?></p>
        </td>
        <td style="border:1px solid #000; border-right:0; border-left:0; border-top:0; border-bottom:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">Branch & IFS Code</p>
        </td>
        <td style="border:1px solid #000; border-bottom:0; border-left:0; border-top:0; vertical-align: middle; padding:0 5px;">
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight:bold;"> : <?php echo Yii::$app->mycomponent->Get_settings('branch'); ?> & <?php echo Yii::$app->mycomponent->Get_settings('ifsc'); ?></p>
        </td>
      </tr>
        
      <tr>
        <td colspan="2" style="border:1px solid #000; border-right:0; border-top:0; vertical-align: middle; padding:0 5px;">
            <p style="border-bottom:1px solid #000; font-family: Arial; font-size:12px; width:120px;">Declaration</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">We declare that this invoice show the actual price of the goods described and that all particulars are true and correct.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">CHECK BOUNCE CHARGES Rs.500</p>
        </td>
        <td colspan="2" style="border:1px solid #000; text-align: right; vertical-align: top; padding:0 5px;">
            <p style="border:0px solid #F00; text-align: right; font-family: Arial; font-size:12px; font-weight:bold;">for <?php echo Yii::$app->mycomponent->Get_settings('company_name'); ?></p>
            <p style="border:0px solid #F00; text-align: right; font-family: Arial; font-size:30px; font-weight:bold;">&nbsp;</p>
            <p style="border:0px solid #F00; text-align: right; font-family: Arial; font-size:12px;">Authorised Signature</p>
        </td>
      </tr>
     
    </tbody>
  </table>

<div style="border:0px solid #000; text-align: center; width: 100%;">
    <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">This is a Computer Generated Invoice</p>
</div>