<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use app\models\Inventory;
 use app\models\Sales;
   use kartik\select2\Select2;
  use app\models\Customer;

$this->title = 'Sales | MR infotech'; 


$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            //Html::a('<i class="glyphicon glyphicon-plus"></i>', ['newsales'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Complaint estimates'],
    'columns' => [
	     
       
		 
		 [
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->id!='')
				 {
                    $model = Inventory::find()->where(['=', 'sales_id', $model->id])->all();
					
                    return Yii::$app->controller->renderPartial('purchase_list', ['model'=>$model]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			
		],
		
		  [
		     'label' => 'Bill Number',
            'attribute'=>'service_sno',
			
         ],
		 
		  [
		     'label' => 'Jobcard Number',
            'attribute'=>'complaint_id',
			
         ],
		 [
            'label'=>'Company Name',
			'attribute' => 'company_name',
			'value' => 'customer.company_name',
			/* 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Customer::find()->orderBy('company_name')->asArray()->all(), 'company_name', 'company_name'),
			 'filterInputOptions'=>['placeholder'=>'Select company'],
			'value' => 'customer.company_name',
			'format'=>'raw'*/
           
         ],
		 
		/* [
            'label'=>'Contact person',
			'attribute' => 'customer_name',
			 'filterType'=>GridView::FILTER_SELECT2,
             'filter'=>ArrayHelper::map(Customer::find()->orderBy('first_name')->asArray()->all(), 'first_name', 'first_name'),
			 'filterInputOptions'=>['placeholder'=>'Select customer'],
			'value' => 'customer.first_name',
			'format'=>'raw'
		
         ],*/
		  /*[
            'label'=>'Tin No.',
			'attribute' => 'tin_no',
			'value' => 'customer.tin_no'
           
         ],
		 [
            'label'=>'CST No.',
			'attribute' => 'cst_no',
			'value' => 'customer.cst_number'
           
         ],*/
		
		 [
            'label'=>'Mobile No.',
			'attribute' => 'contact_phone',
			'value' => 'customer.contact_phone'
           
         ],
		 [
            'label'=>'Address',
			'attribute' => 'address',
			'value' => 'customer.Address'
           
         ],
    
		[

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Sales date',
				
				'attribute'=>'sales_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'sales_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
	  
	  [
    'attribute'=>'status',
	//'filter'=>array('pending'=>'pending','Completed'=>'Completed','Rejected'=>'Rejected'),
    'value'=> function($model)
              { 
			   if($model->status == "1")
              {
                 $status_class = "label-success";
				 $string='Cancel';
				 $URL=['inventory/cancel_sales','id'=>$model->id];
              }
              else if($model->status == "2")
              {
                 $status_class = "label-success";
				 $string='Return';
				  $URL=['inventory/return_sales','id'=>$model->id];
              }
                 return  Html::a('<span class="label '.$status_class.'">'.$string.'</span>', $URL);      
              },
              'format' => 'raw'
         ],
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{edit}  {print}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 
				 
				/* 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },*/
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

               
                if ($action === 'edit') {
                    $url = Url::to(['/complaint/spares', 'id' =>$model->complaint_id,'customer_id' =>$model->customer_id,'action'=>'complaint']);
                }
                
				if ($action === 'print') {
                    $url = Url::to(['/complaint/bill', 'id' =>$model->complaint_id,'sales_id'=>$model->id,'customer_id' =>$model->customer_id]);
                }                       
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
<?php //\yii\widgets\Pjax::end(); ?>
<?php
$Rolls=Yii::$app->mycomponent->GetRolls();

?>
 

<?php
$this->registerJs("$(function() {
   $('.popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});"); ?>
 

<div id="state_list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
<input type="hidden" data-toggle="modal" data-target="#stc" class="stc"/>
	<script>
    function assign_complaint(complaint_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(complaint_id);
		$(".assign_to").trigger('click');
        
    }
    </script>


<script>
function status_change(complaint_id) {
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_change' ?>',
	data:'complaint_id='+complaint_id,
	success: function(data){
	//alert(data);
		$("#state_list").html(data);
		$("#stc").trigger('click');
	}
	});
}
</script>


<script>
function edit_complaint(order_id) {

//alert(order_id);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/register' ?>',
	data:'order_id='+order_id,
	success: function(data){
	alert(data);
		$("#receipt").html(data);
		$(".receipt_trigg").trigger('click');
	}
	});
}
</script>

<script>
function assign_status(val) {

var status=$('#st').val();
//alert(status);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_update' ?>',
	data:'status='+status+'&complaint_id='+val,
	success: function(data){
	//alert("#status_"+val);
	
		$('#modal').modal('hide');
		$.pjax.reload({container:'#w0'});
		
	}
	});
}


</script>


<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Assign to </label>
               <div class="col-sm-8"> 
			   <select name="role" id="role" class="form-control" >
               <?php foreach($Rolls as $rolle) { ?>
                  <option value="<?php echo $rolle['item_name'] ?>"><?php echo ucfirst($rolle['item_name']) ?></option>
                  
                  <?php } ?>
               </select>
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="notes" id="notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function st_assign()
 {

var cid=$('#cid').val();
var notes=$('#notes').val();
var role=$('#role').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/roleassign' ?>',
	data:'cid='+cid+'&notes='+notes+'&role='+role,
	success: function(data){
	//alert(data);
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->