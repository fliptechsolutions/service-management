<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use yii\widgets\Pjax;


$this->title = 'Complaint | MR'; 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus=''; 
//print_r($dataProvider);?>
<?php \yii\widgets\Pjax::begin(); ?>
<?php echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Complaints'],
    'columns' => [
        //'payment_type',
      //  'contact_phone',
       /* 'Title',
        'Published:date',*/
		 /*['class'=>'kartik\grid\SerialColumn',
		 'header' => 'S No',],*/
		 
		/* [
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->complaint_id!='')
				 {
					 
                    $model = \app\models\Complaint::findOne($model->complaint_id);
					
					$model1 = \app\models\Job::findOne($model->complaint_id);
				
                    return Yii::$app->controller->renderPartial('products', ['model'=>$model]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			
		],*/
		
		/*[
			'class'=>'kartik\grid\ExpandRowColumn',
			'header' => 'Job assign',
			//'expandTitle'=>'Job assign',
			//'bootstrap'=>false,
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->complaint_id!='')
				 {
                    $model = \app\models\Complaint::findOne($model->complaint_id);
					
                    return Yii::$app->controller->renderPartial('job_assign', ['model'=>$model]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				//return Yii::$app->controller->renderPartial('products', ['model'=>$model,'id'=>1]);
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			//'expandOneOnly'=>true,
		],*/
		
		 [
            'attribute'=>'complaint_id',
            //'pageSummary'=>'Complaint id',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
         ],
		 [
		     'label' => 'Customer name',
            'attribute'=>'first_name',
			'value'=>'customerdetails.first_name',
            //'pageSummary'=>'Page Summary',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],
		[
		    'label' => 'Address',
			'attribute'=>'address',
            'value'=>'customerdetails.Address',
          
         ],
		  [
		     'label' => 'Contact phone',
            'attribute'=>'contact_phone',
			'value'=>'customerdetails.contact_phone',
			
        ],
		 /*[
		    'label' => 'Email Address',
			'attribute'=>'email_address',
            'value'=>'customerdetails.email_address',
			
        ],*/
		
		
		
		
		[
		/*'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'status',*/
			'attribute'=>'status',
           'format' => 'raw',
			'value' => function ($model)  {
				
			  if($model->status == "Pending")
              {
                 $status_class = "label-warning";
              }
              else if($model->status == "Accepted")
              {
                 $status_class = "label-success";
              }
              else if($model->status == "Canceled")
              {
                 $status_class = "label-danger";
              }
              else
              {
                 $status_class = "label-default";
              }
				
				return '<div id="status_'.$model->complaint_id.'">'.Html::tag('span',$model->status,['class'=>'label '.$status_class,'style'=>'cursor:pointer;','onclick'=>'status_change('.$model->complaint_id.')','data-ds'=>'123']).'</div>';
			},
        ],
		
		[
            'attribute'=>'date_created',
			'value'=>function ($model, $key, $index, $column) {
				$row = \app\models\Job::findOne(['complaint_id' => $model->complaint_id,'status'=>'pending']);
				 return $row->date_created;
			},
            //'pageSummary'=>'Page Summary',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],
		/*[
            'attribute'=>'id',
			'value'=>'job.id',
            //'pageSummary'=>'Page Summary',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],*/
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{assign}',
              'buttons' => [
                 'assign' => function ($url, $model) {
					 $row = \app\models\Job::findOne(['complaint_id' => $model->complaint_id,'status'=>'pending']);
                   //echo"kk".  $row->id;
					 //$model = \app\models\Complaintmerge::findOne($model->complaint_id);
                   return Html::a('<span class="glyphicon glyphicon-eye-open"></span> ', $url,[
                    //'onclick' => 'edit_complaint('.$model->complaint_id.')',
					
					 'onclick' => 'assign_complaint('.$model->complaint_id.','.$row->id.')',
					/*'data-target'=>"#myModal",
					'data-toggle'=>"modal",
					','.$model->job->id.
					*/
                   
                ]);
                 },
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                                 
                return $url;
            }
          ],
		
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>
<?php $Rolls=Yii::$app->mycomponent->GetRolls();
$from_id=Yii::$app->user->identity->id;
$roll_name=Yii::$app->mycomponent->Get_Roll($from_id)
?>
 
 

<div id="state-list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
	<script>
    function assign_complaint(complaint_id,job_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(complaint_id);
		$("#job_id").val(job_id);
		$(".assign_to").trigger('click');
        
    }
    </script>


<script>
function status_change(order_id) {
//alert(order_id);
//var x=val.split(',');
//alert(x[1]);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/status_change' ?>',
	data:'order_id='+order_id,
	success: function(data){
	//alert(data);
		$("#state-list").html(data);
		$("#stc").trigger('click');
	}
	});
}
</script>


<script>
function edit_complaint(order_id) {

//alert(order_id);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/register' ?>',
	data:'order_id='+order_id,
	success: function(data){
	alert(data);
		$("#receipt").html(data);
		$(".receipt_trigg").trigger('click');
	}
	});
}
</script>

<script>
function st_change(val) {

var status=$('#st').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/status_update' ?>',
	data:'id='+status+'&order_id='+val,
	success: function(data){
	//alert("#status_"+val);
	
		$("#status_"+val).html(data);
		$("#cls").trigger('click');
	}
	});
}


</script>


<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                 <input type="hidden" id='job_id' name="job_id"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Assign to </label>
               <div class="col-sm-8"> 
			   <select name="role" id="role" class="form-control" id="week-list">
               <?php foreach($Rolls as $roll) { 
			   if($roll_name!=$roll['item_name'])
			   {
			   ?>
                  <option value="<?php echo $roll['item_name'] ?>"><?php echo ucfirst($roll['item_name']) ?></option>
                  
                  <?php } }?>
               </select>
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="notes" id="notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function st_assign()
 {

var cid=$('#cid').val();
var notes=$('#notes').val();
var role=$('#role').val();
var job_id=$('#job_id').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/roleassign' ?>',
	data:'cid='+cid+'&notes='+notes+'&role='+role+'&job_id='+job_id,
	success: function(data){
	//alert(data);
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
		 $.pjax.reload({container:'#w0'});
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->