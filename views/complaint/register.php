<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use app\models\Accesories;
use app\models\Customer;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use app\models\Cataccesories;
//yii\db\BaseActiveRecord; 

$this->title = 'COMPLAINT | '.Yii::$app->mycomponent->Get_settings('company_name');
$Accessories_list = Accesories::find()->all();
$warranty_details=array('TRC','Warranty','AMC');
$category_accessories = ArrayHelper::map(Cataccesories::find()->all(), 'id', 'cat_name');
//print_r($model[0]['acc_name']);
//$acc_sno='';
if(isset($_GET['id']))
{
	$id=$_GET['id'];
	$where_con='complaint_id='.$id;
	$Complaint=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_complaint');
	if($Complaint['laptop_details'])
	{
	  $laptop_details=json_decode($Complaint['laptop_details'],true);
	}
	if($Complaint['prsence'])
	{
	  $prsence=json_decode($Complaint['prsence'],true);
	}
	if($Complaint['acc_sno'])
	{
	  $acc_sno=json_decode($Complaint['acc_sno'],true);
	}
	if($Complaint['acc_remarks'])
	{
	  $acc_remarks=json_decode($Complaint['acc_remarks'],true);
	}
	if($Complaint['warranty'])
	{
	  $warranty=json_decode($Complaint['warranty'],true);
	}
	
	$customer_details = Customer::findOne(['id' =>$Complaint['customer_id']]);
}

 ?>
 
  
 
 <h2>COMPLAINT</h2>
<div class="panel panel-default">
<div class="panel-body">
        <div><?= Html::a('Create', ['/complaint/register'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/complaint/complaint'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Customer Details</h3></div>
        <br />  
<div class="user-form">
 <?= Html::beginForm(['complaint/create'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control'],'required'=>'required'];
 ?>
 
    <div class="form-group" >
    <?= Html::label('Name', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'company_name',isset($customer_details['company_name'])?$customer_details['company_name']:'',['class' => ['form-control'],'required'=>'required','id'=>'company_name']) ?>
    </div>
    
    <?= Html::label('Tin No.', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'tin_no',isset($customer_details['tin_no'])?$customer_details['tin_no']:'',['class' => ['form-control'],'id'=>'tin_no'] ) ?>
    </div>
    </div>
 

    <div class="form-group" >
    <?= Html::label('Contact person', '',['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'first_name',isset($customer_details['first_name'])?$customer_details['first_name']:'',['class' => ['form-control'],'id'=>'fname'] ) ?>
    <?= Html::Input('hidden', 'id',isset($id)?$id:'','' ) ?>
    
    <?= Html::Input('hidden', 'tbl_name','tbl_complaint','' ) ?>
    <?= Html::Input('hidden', 'where','complaint_id','' ) ?>
    <?= Html::Input('hidden', 'render','complaint/complaint','' ) ?>
    </div>
    
    <?= Html::label('Address', 'Address', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::textarea('address',isset($customer_details['Address'])?$customer_details['Address']:'',['class' => ['form-control'],'id'=>'address']) ?>
    </div>
    
   <?php /*?> <?= Html::label('Last Name','last_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'last_name',isset($customer_details['last_name'])?$customer_details['last_name']:'',['class' => ['form-control'],'id'=>'lname'] ) ?>
    </div><?php */?>
    </div>
  
    <div class="form-group" >
      <?= Html::label('Office No', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'office_no',isset($customer_details['office_no'])?$customer_details['office_no']:'',['class' => ['form-control'],'id'=>'office_no']) ?>
    </div>
    
    <?= Html::label('City', 'City', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'city',isset($customer_details['city'])?$customer_details['city']:'',['class' => ['form-control'],'id'=>'city'] ) ?>
    </div>
    </div>
    
     <div class="form-group" >
  <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'contact_phone',isset($customer_details['contact_phone'])?$customer_details['contact_phone']:'',$options = ['class' => ['form-control'],'required'=>'required','id'=>'mob'] ) ?>
    
    <span><button type="button" onclick="check_phone()" style="margin: -57px 0 0 330px;" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button></span>
    
    <?= Html::img('@web/uploads/ajax-loader.gif', ['id'=>'loading','style'=>'display:none;']);?>
    <span id="error_msg" style="color:#F00; display:none;">Unavailable!</span>
    </div>
   
    <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'contact_email',isset($customer_details['email_address'])?$customer_details['email_address']:'',['class' => ['form-control'],'id'=>'email_address'] ) ?>
    </div>
    </div>
    
    
     
   <h3>Product Details</h3>
   
    <div class="form-group" >
    <?= Html::label('Model of Product', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'lap_model',isset($laptop_details['lap_model'])?$laptop_details['lap_model']:'',['class' => ['form-control'],'id'=>'lap_model','required'=>'required']) ?>
    </div>
    <?= Html::label('S.no of Product', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'lap_sno',isset($laptop_details['lap_sno'])?$laptop_details['lap_sno']:'',['class' => ['form-control'],'id'=>'lap_sno','required'=>'required'] ) ?>
    </div>
    </div>
     
    <div class="form-group" >
    <?= Html::label('User name ', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'lap_user',isset($laptop_details['lap_user'])?$laptop_details['lap_user']:'',['class' => ['form-control'],'id'=>'lap_user']) ?>
    </div>
    
    <?= Html::label('Password', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= Html::Input('text', 'lap_pass',isset($laptop_details['lap_pass'])?$laptop_details['lap_pass']:'',['class' => ['form-control'],'id'=>'lap_pass'] ) ?>
    </div>
    </div>
    
    
    
    <h3>Priority</h3>
    
    <div class="form-group" >
   
    <div class="col-sm-4">
   <?php 
   $priority=array('1'=>'Normal','2'=>'Urgent','3'=>'Very urgent');
   
    echo Select2::widget([
			'name' => 'priority',
			'id' => 'priority',
			'value'=>isset($Complaint['priority'])?$Complaint['priority']:1,
			'data' => $priority,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Priority ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); ?>
    </div>
    
     </div>
    
    
     
     <div><h4>Warranty Details</h4></div>
     <br />
     
     
          <div>
           <table style="max-width:420px;" class="table table-striped">
        <thead>
         
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		 foreach($warranty_details as $value): 
		 
		 if($i%2 == 0)
		 {
			 $style='style="float:right;width:49%;text-align:right;"';
		 }
		 else
		 {
			$style='style="float:left;width:49%;text-align:left;"'; 
		 }
		 ?>
       
       
          <tr <?php echo $style; ?>>
            <td><?php echo $i ?></td>
             <td><?php echo ucfirst($value); ?></td>
            <td>
			<?php
			$val=0;
			if(isset($warranty))
			{
				$val=$warranty[$value];
			}
			
			
			 echo 	CheckboxX::widget([
						'name'=>'warranty['.$value.']',
						'value'=>$val,
						//'options'=>$options,
						'pluginOptions'=>['threeState'=>false]
					]);
			
			 ?></td>
          
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
        </div>
     
    <h4>Product list</h4>
    
    <div class="form-group" >
   
    <div class="col-sm-4">
   <?php  echo Select2::widget([
			'name' => 'access_cat',
			'id' => 'access_cat',
			'value'=>isset($Complaint['access_cat'])?$Complaint['access_cat']:'',
			'data' => $category_accessories,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Category ...','onchange'=>'cat_type()'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); ?>
    </div>
    
     </div>
    
  
     <div>
           <table class="table table-striped">
        <thead>
          <tr>
            
            <th>Part Description</th>
            <th>Presence</th>
            <th>S.no</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		$warning_list=array();
		$warning_list=Yii::$app->mycomponent->Checked_accessoried();
		//print_r($warning_list);
		 foreach($Accessories_list as $list):
		 
		 $category=json_decode($list['category'],true);
		 $class='';
		 if(isset($category))
		 {
			 foreach($category as $cat=>$val)
			 {
				if($val==1) 
				{
					$class.='show_'.$cat.' ';
				}
			 }
		 }
		  ?>
       
       
          <tr class="access_cat <?php echo $class; ?>">
           
             <td><?php echo ucfirst($list['acc_name']); ?></td>
            <td>
			<?php
			$val=0;
			if(isset($prsence) && isset($prsence[$list['id']]))
			{
				$val=$prsence[$list['id']];
			}
			$check='';
			$state=false;
			if(in_array($list['id'],$warning_list))
			{
				//$check=Yii::t('yii', 'Are you sure you want to delete?');
				$check="get_alert(".$list['id'].")";
				$state=true;
			}
		// echo Html::Input($model, 'accesories','')->widget(CheckboxX::classname(), []); 
			 echo 	CheckboxX::widget([
						'name'=>'prsence['.$list['id'].']',
						'value'=>$val,
						'initInputType' => CheckboxX::INPUT_CHECKBOX,
						'options'=>[ //'data-confirm' => $check
						'id'=>'warning_'.$list['id'],
						//'id'=>'s_1',
						'onchange'=>$check
						],
						'pluginOptions'=>['threeState'=>$state]
					]);
					
			
			 ?>
             <div style="color:#FF0000; display:none;" class="<?php echo 'warning_'.$list['id']?>" ><?php echo $list['description']; ?></div>
             
             </td>
            <td> <input type="text" name="acc_sno[]" class="form-control" value="<?php echo isset($acc_sno[$j])?$acc_sno[$j]:''; ?>" /></td>
            <td><input type="text" name="acc_remarks[]" class="form-control" value="<?php echo isset($acc_remarks[$j])?$acc_remarks[$j]:''; ?>" /></td>
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
        </div>
        <div><h4>Physical condition of the Laptop</h4></div>
     <br />
     <?php //echo $Complaint['physical_type']?>
      <input type="radio" name="phycical_type" class="physical_type" value="GOOD" <?php if(isset($Complaint['physical_type']) && ($Complaint['physical_type']=='GOOD' )) { echo 'checked="checked"'; }?> onclick="phycical_enable('GOOD')" checked="checked"  />GOOD
      <input type="radio" name="phycical_type" class="physical_type" value="BAD" <?php if(isset($Complaint['physical_type']) && ($Complaint['physical_type']=='BAD' )) { echo 'checked="checked"'; }?> onclick="phycical_enable('BAD')"/>BAD
     <div style="float:right; margin:0 208px;" class="phy_bad" > 
    
         <p><a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span></a></p>
        
        </div><br />
        
         <?php
		if(isset($Complaint['phycical']))
		{
		
		 $products=json_decode($Complaint['phycical'],true);
		// print_r(array_keys($products));
		// exit();
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs phy_bad">
        <?php foreach($products as $phycical): ?>
       
       
          <tr id="remove<?php echo $i; ?>" class="field1">
            <td> <input type="text" name="phycical[]" class="form-control" value="<?php echo $phycical ?>" /></td>
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
          </tr>
      
		<?php
			$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { ?>
     
     
  
        
     <table class="table table-striped">
    <thead>
      
    </thead>
    <tbody class="inputs phy_bad">
   
      <tr id="remove0">
        <td><input type="text" name="phycical[]" class="form-control" /></td>
       <td><a class="btn btn-danger" onclick="remove(0)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
      </tr>
       
      
     </tbody>
  </table>
  <?php } ?>
     
     <div><h4>Problem reported by customer</h4></div>
     <br /> 
   <div style="float:right; margin: 0 208px;" > 
    
            <p><a class="btn btn-success"  id="add1" ><span class="glyphicon glyphicon-plus-sign"></span></a></p>
        
        </div><br />
     <?php   
        if(isset($Complaint['problem']))
		{
		
		 $products=json_decode($Complaint['problem'],true);
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs1">
        <?php foreach($products as $problem): ?>
       
       
          <tr id="pr_remove<?php echo $i; ?>" class="field">
              <td> <input type="text" name="problem[]" class="form-control" value="<?php echo $problem ?>" /></td>
              <td><a class="btn btn-danger" onclick="pr_remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
            
          </tr>
      
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { ?>
     <table class="table table-striped">
    <thead>
      
    </thead>
    <tbody class="inputs1">
   
      <tr id="pr_remove0">
        <td><input type="text" name="problem[]" class="form-control" /></td>
        <td><a class="btn btn-danger" onclick="pr_remove(0)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td> 
      </tr>
       
      
     </tbody>
  </table>
  <?php } ?>
  

 <?= Html::submitButton('SAVE' , ['class'=>'btn btn-success']) ?>
 
 <?= Html::endForm() ?>
 </div>
 </div>
 </div>
 
 
 
 <script>
 
 function phycical_enable(condition)
 {
	 if(condition=='GOOD')
	 {
		$('.phy_bad').hide(); 
	 }
	 else
	 {
		$('.phy_bad').show();  
	 }
 }
 $(document).ready(function()
 {
   cat_type();
   var condition=$('input[name=phycical_type]:checked').val(); 
  // alert(condition);
   phycical_enable(condition)
 });
  function get_alert(id)
  {
	  $('.warning_'+id).hide();
	var warning_id= $('#warning_'+id).val();
	if(warning_id=='')
	{
		$('.warning_'+id).show();
	}
	  
	  
	   
  }
 function cat_type() 
 {
	 var cat=$('#access_cat').val();
	// alert(cat);
	 $('.access_cat').hide();
	 $('.show_'+cat).show();
	 //alert(cat);
 }
 
 
function check_phone() {
	var mob=$('#mob').val();
	      $('#fname').val('');
		  $('#lname').val('');
		  $('#address').val('');
		  $('#city').val('');
		  $('#city').val('');
		  $('#email_address').val('');
		  $('#company_name').val('');
		  $('#tin_no').val('');
		  $('#office_no').val('');
		 
//alert(mob);
    $('#loading').show();
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/mob_validation' ?>',
	data:'mob='+mob,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	success: function(response){
		//var first_name=response.results.first_name;
		 //alert(JSON.stringify(response));
		 $('#loading').hide();
		var status=response.status;
		if(parseInt(status)==1)
		{
		  $('#company_name').val(response.results.company_name);
		  $('#tin_no').val(response.results.tin_no);
		  $('#office_no').val(response.results.office_no);
		  $('#fname').val(response.results.first_name);
		  $('#lname').val(response.results.last_name);
		  $('#address').val(response.results.Address);
		  $('#city').val(response.results.city);
		  $('#city').val(response.results.city);
		  $('#email_address').val(response.results.email_address);
		  
		}
		else
		{
			$("#error_msg").show();
           setTimeout(function() { $("#error_msg").hide(); }, 5000);
		}
	
	}
	});
}
</script>
 
 
 <script>
 function remove(id)
{
	//alert(id);
	$('#remove'+id).remove();
}
function pr_remove(id)
{
	//alert(id);
	$('#pr_remove'+id).remove();
}
 
 $(document).ready(function(){

var i = $('.field').size() + 1;
var j = $('.field1').size() + 1;

$('#add').click(function() {
	$('<tr id="remove'+j+'" class="field1">'+
	'<td><input type="text" class="form-control" name="phycical[]" value="" /></td>'+
	'<td><a  onclick="remove('+j+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>').fadeIn('slow').appendTo('.inputs');

i++;
});

$('#add1').click(function() {
	$('<tr id="pr_remove'+i+'" class="field">'+
	'<td><input type="text" class="form-control" name="problem[]" value="" /></td>'+
	'<td><a  onclick="pr_remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>').fadeIn('slow').appendTo('.inputs1');

i++;
});

/*$('#remove').click(function() {
if(i > 1) {
$('.field:last').remove();
i--;
}
});*/


$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
 
 