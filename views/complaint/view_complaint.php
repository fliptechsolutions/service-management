<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use app\models\Accesories;
use app\models\Customer;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use app\models\Cataccesories;
//yii\db\BaseActiveRecord; 

$this->title = 'COMPLAINT | MR infotech';
$Accessories_list = Accesories::find()->all();
$warranty_details=array('TRC','Warranty','AMC');
$category_accessories = ArrayHelper::map(Cataccesories::find()->all(), 'id', 'cat_name');
//print_r($model[0]['acc_name']);
//$acc_sno='';
if(isset($_GET['id']))
{
	$id=$_GET['id'];
	$where_con='complaint_id='.$id;
	$Complaint=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_complaint');
	if($Complaint['laptop_details'])
	{
	  $laptop_details=json_decode($Complaint['laptop_details'],true);
	}
	if($Complaint['prsence'])
	{
	  $prsence=json_decode($Complaint['prsence'],true);
	}
	if($Complaint['acc_sno'])
	{
	  $acc_sno=json_decode($Complaint['acc_sno'],true);
	}
	if($Complaint['acc_remarks'])
	{
	  $acc_remarks=json_decode($Complaint['acc_remarks'],true);
	}
	if($Complaint['warranty'])
	{
	  $warranty=json_decode($Complaint['warranty'],true);
	}
	
	$customer_details = Customer::findOne(['id' =>$Complaint['customer_id']]);
}

 ?>
 
  
 
 <h2>COMPLAINT</h2>
<div class="panel panel-default">
<div class="panel-body">
       
        <div> <h3>Customer Details</h3></div>
        <br />  
<div class="user-form">
 <?= Html::beginForm(['complaint/create'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ;
 
 $options = ['class' => ['form-control'],'required'=>'required'];
 ?>
 
    <div class="form-group" >
    <?= Html::label('Company name', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['company_name'])?$customer_details['company_name']:''; ?>
    </div>
    
    
    <?= Html::label('Tin No.', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['tin_no'])?$customer_details['tin_no']:''; ?>
    </div>
    </div>
 

    <div class="form-group" >
    <?= Html::label('First Name', '',['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['first_name'])?$customer_details['first_name']:''; ?>
    </div>
  
    
    <?= Html::label('Last Name','last_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['last_name'])?$customer_details['last_name']:''; ?>
    </div>
    </div>
  
    <div class="form-group" >
    <?= Html::label('Address', 'Address', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['Address'])?$customer_details['Address']:''; ?>
    </div>
    
    <?= Html::label('City', 'City', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['city'])?$customer_details['city']:'';?>
    </div>
    </div>
    
     <div class="form-group" >
    <?= Html::label('Office No', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['office_no'])?$customer_details['office_no']:''; ?>
    </div>
   
    <?= Html::label('Contact Email', 'email', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['email_address'])?$customer_details['email_address']:''; ?>
    </div>
    </div>
    
    
    <div class="form-group" >
    <?= Html::label('Contact Phone', 'contactphone', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($customer_details['contact_phone'])?$customer_details['contact_phone']:''; ?>
     
    <span id="error_msg" style="color:#F00; display:none;">Unavailable!</span>
    </div>
    </div>
     
  
     
   <h3>Product Details</h3>
   
    <div class="form-group" >
    <?= Html::label('Model of Product', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($laptop_details['lap_model'])?$laptop_details['lap_model']:''; ?>
    </div>
    <?= Html::label('S.no of Product', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($laptop_details['lap_sno'])?$laptop_details['lap_sno']:''; ?>
    </div>
    </div>
     
    <div class="form-group" >
    <?= Html::label('User name ', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($laptop_details['lap_user'])?$laptop_details['lap_user']:''; ?>
    </div>
    
    <?= Html::label('Password', '', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
    <?= isset($laptop_details['lap_pass'])?$laptop_details['lap_pass']:''; ?>
    </div>
    </div>
    
    
    
    <h3>Priority</h3>
    
    <div class="form-group" >
   
    <div class="col-sm-4">
   <?php 
   $priority=array('1'=>'Normal','2'=>'Urgent','3'=>'Very urgent');
   
    echo Select2::widget([
			'name' => 'priority',
			'id' => 'priority',
			'value'=>isset($Complaint['priority'])?$Complaint['priority']:'',
			'data' => $priority,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Priority ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); ?>
    </div>
    
     </div>
    
    
     
     <div><h4>Warranty Details</h4></div>
     <br />
     
     
          <div>
           <table style="max-width:420px;" class="table table-striped">
        <thead>
         
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		 foreach($warranty_details as $value): 
		 
		 if($i%2 == 0)
		 {
			 $style='style="float:right;width:49%;text-align:right;"';
		 }
		 else
		 {
			$style='style="float:left;width:49%;text-align:left;"'; 
		 }
		 ?>
       
       
          <tr <?php echo $style; ?>>
            <td><?php echo $i ?></td>
             <td><?php echo ucfirst($value); ?></td>
            <td>
			<?php
			$val=0;
			if(isset($warranty))
			{
				$val=$warranty[$value];
			}
			
			
			 echo 	CheckboxX::widget([
						'name'=>'warranty['.$value.']',
						'value'=>$val,
						//'options'=>$options,
						'pluginOptions'=>['threeState'=>false]
					]);
			
			 ?></td>
          
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
        </div>
     
    <h4>Accessories list</h4>
    
    <div class="form-group" >
   
    <div class="col-sm-4">
   <?php  echo Select2::widget([
			'name' => 'access_cat',
			'id' => 'access_cat',
			'value'=>isset($Complaint['access_cat'])?$Complaint['access_cat']:'',
			'data' => $category_accessories,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Category ...','onchange'=>'cat_type()'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]); ?>
    </div>
    
     </div>
    
  
     <div>
           <table class="table table-striped">
        <thead>
          <tr>
            
            <th>Part Description</th>
            <th>Presence</th>
            <th>S.no</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		$warning_list=array();
		$warning_list=Yii::$app->mycomponent->Checked_accessoried();
		//print_r($warning_list);
		 foreach($Accessories_list as $list):
		 
		 $category=json_decode($list['category'],true);
		 $class='';
		 if(isset($category))
		 {
			 foreach($category as $cat=>$val)
			 {
				if($val==1) 
				{
					$class.='show_'.$cat.' ';
				}
			 }
		 }
		  ?>
       
       
          <tr class="access_cat <?php echo $class; ?>">
           
             <td><?php echo ucfirst($list['acc_name']); ?></td>
            <td>
			<?php
			$val=0;
			if(isset($prsence) && isset($prsence[$list['id']]))
			{
				$val=$prsence[$list['id']];
			}
			$check='';
			$state=false;
			if(in_array($list['id'],$warning_list))
			{
				//$check=Yii::t('yii', 'Are you sure you want to delete?');
				$check="get_alert(".$list['id'].")";
				$state=true;
			}
		// echo Html::Input($model, 'accesories','')->widget(CheckboxX::classname(), []); 
			 echo 	CheckboxX::widget([
						'name'=>'prsence['.$list['id'].']',
						'value'=>$val,
						'initInputType' => CheckboxX::INPUT_CHECKBOX,
						'options'=>[ //'data-confirm' => $check
						'id'=>'warning_'.$list['id'],
						//'id'=>'s_1',
						'onchange'=>$check
						],
						'pluginOptions'=>['threeState'=>$state]
					]);
					
			
			 ?>
             <div style="color:#FF0000; display:none;" class="<?php echo 'warning_'.$list['id']?>" >Have you veryfied this accessory?</div>
             
             </td>
            <td> <input type="text" name="acc_sno[]" class="form-control" value="<?php echo isset($acc_sno[$j])?$acc_sno[$j]:''; ?>" /></td>
            <td><input type="text" name="acc_remarks[]" class="form-control" value="<?php echo isset($acc_remarks[$j])?$acc_remarks[$j]:''; ?>" /></td>
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
        </div>
        <div><h4>Physical condition of the Laptop</h4></div>
    
        
         <?php
		if(isset($Complaint['phycical']))
		{
		
		 $products=json_decode($Complaint['phycical'],true);
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs">
        <?php foreach($products as $phycical): ?>
       
       
          <tr id="remove<?php echo $i; ?>" class="field1">
            <td> <?php echo $phycical ?></td>
            
          </tr>
      
		<?php
			$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		else { ?>
   
  <?php } ?>
     
     <div><h4>Problem reported by customer</h4></div>
    
     <?php   
        if(isset($Complaint['problem']))
		{
		
		 $products=json_decode($Complaint['problem'],true);
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs1">
        <?php foreach($products as $problem): ?>
       
       
          <tr id="pr_remove<?php echo $i; ?>" class="field">
              <td> <?php echo $problem ?></td>
              
            
          </tr>
      
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php } ?>
  

 
 </div>
 </div>
 </div>
 
 
 
 