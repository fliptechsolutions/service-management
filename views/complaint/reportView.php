<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
use app\models\Accesories;
use kartik\checkbox\CheckboxX;
use app\models\Customer;
$Accessories_list = Accesories::find()->all();

if(isset($model))
{
	$id=$model;
	$where_con='complaint_id='.$id;
	$Complaint=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_complaint');
	if($Complaint['laptop_details'])
	{
	  $laptop_details=json_decode($Complaint['laptop_details'],true);
	}
	if($Complaint['prsence'])
	{
	  $prsence=json_decode($Complaint['prsence'],true);
	}
	if($Complaint['acc_sno'])
	{
	  $acc_sno=json_decode($Complaint['acc_sno'],true);
	}
	if($Complaint['acc_remarks'])
	{
	  $acc_remarks=json_decode($Complaint['acc_remarks'],true);
	}
	if($Complaint['warranty'])
	{
	  $warranty=json_decode($Complaint['warranty'],true);
	}
}
$customer_details = Customer::findOne(['id' =>$Complaint['customer_id']]);
$date_cstm = date_create($Complaint['date_created']);
$warranty_details=array('TRC','Warranty','AMC');
?> 
<h1 style="font-family: Tahoma; text-align:center; font-size: 12px; font-weight: bold;">JOB CARD</h1>
 <table cellspacing="0" cellpadding="0" style="border:0; width: 100%;">    
    <tbody>
      <tr>
        <td style="border:0px solid #F00; width: 65%;">
            <hgroup>
                <h2 style="border:0px solid #F00; font-family: Times New Roman; font-size:24px; font-weight: bold;">MR INFOTECH</h2>
                <h3 style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight: bold;">(Expert in Chip level service)</h3>
                <h3 style="border:0px solid #F00; font-family: Arial; font-size:12px; font-weight: bold;">(Used laptop & accessories sale)</h3>
            </hgroup>
            <hr style="line-height:1; margin:5px auto 0; color:#fff;" />
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">No.20A,Puthur High Road,(Opp. Aruna Theatre),Trichy-17.</p>
            <p style="border:0px solid #F00; font-family: Arial; font-size:12px;">www.laptopservicetrichy.com / e-mail:mrinfoservice@gmail.com</p>
            <hr style="line-height:1; margin:5px auto 0; color:#fff;" />
            <h4 style="border:0px solid #F00; font-family: Arial; font-size:14px;">MOBILE : 95436 66362,90255 34526, Service:97867 68786</h4>
        </td>
        <td style="border:0px solid #F00;">
        	<table cellspacing="0" cellpadding="5" style="border:0; width: 100%;">
            	<tbody>
                	<tr>
                    	<td style="border:1px solid #000;">
                        	Job Card No. :&nbsp;&nbsp;<?php echo $id; ?>
                        </td>
                    </tr>
                    <tr>
                    	<td style="border:1px solid #000; border-top:0;">
                        	Date. :&nbsp;&nbsp;<?php echo date_format($date_cstm, "d/m/Y"); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>       
      </tr>
     
    </tbody>
  </table>

    <table cellspacing="0" cellpadding="5" style="width: 100%; margin:5px auto 0;">        
        <tbody>
            <tr>
                <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; width:170px;">Name of the Organization :</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;"><?php echo ucfirst($customer_details['company_name']); ?></td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; text-align: center;" colspan="2">Warranty Details</td>                
            </tr>
            <tr>
                <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px;">Contact Person :</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;"><?php echo $customer_details['first_name'].' '.$customer_details['last_name']; ?></td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; width: 80px;">TRC</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; width: 80px; text-align:center;"><?php if($warranty['TRC']==1) echo '&#x2714;'; ?></td>                                
            </tr>
            <tr>
                <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; vertical-align:top;" rowspan="3">Address :</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; vertical-align:top;" rowspan="3"><?php echo $customer_details['Address']; ?></td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;">Warranty</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; text-align:center;"><?php if($warranty['Warranty']==1) echo '&#x2714;'; ?></td>                                
            </tr>
            <tr>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;">AMC</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; text-align:center;"><?php if($warranty['AMC']==1) echo '&#x2714;'; ?></td>                                
            </tr>
            <tr>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;">&nbsp;</td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;"></td>                                
            </tr>
            <?php /*?><tr>
                <td>Tin No.</td>
                <td class="customer-field"><b><?php echo $customer_details['tin_no']; ?></b></td>
            </tr>
            <tr>
                <td>Name</td>
                <td class="customer-field"><b><?php echo $customer_details['first_name'].''.$customer_details['last_name']; ?></b></td>
            </tr>
            <tr>
                <td>City</td>
                <td class="customer-field"><b><?php echo $customer_details['city']; ?></b></td>
            </tr> 
            <tr>
                <td>Address</td>
                <td class="customer-field"><b><?php echo $customer_details['Address']; ?></b></td>
            </tr>   
            <tr> 
                <td>Office No</td>
                <td class="customer-field"><b><?php echo $customer_details['office_no']; ?></b></td>
            </tr>
            <tr>  
                <td>Contact Email</td>
                <td class="customer-field"><b><?php echo $customer_details['email_address']; ?></b></td>
            </tr>
            <tr>   
                <td>Contact Phone</td>
                <td class="customer-field"><b><?php echo $customer_details['contact_phone']; ?></b></td>
            </tr><?php */?>
        </tbody>
    </table>
    
    <table cellspacing="0" cellpadding="5" style="width: 100%;">
    	<tbody>
        	<tr>
                <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; width: 56px;">e-mail : </td>
                <td style="border: 1px solid #000; border-left:0; border-right:0; border-bottom:0; font-size:13px;"><?php echo $customer_details['email_address']; ?></td>
                <td style="border: 1px solid #000; border-left:0; border-right:0; border-bottom:0; font-size:13px; width: 60px;">Fax : </td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px;"><?php echo $customer_details['contact_phone']; ?></td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; width: 80px;"></td>
                <td style="border: 1px solid #000; border-left:0; border-bottom:0; font-size:13px; width: 80px;"></td>                                
            </tr>
            <tr>
                <td style="border: 1px solid #000; border-right:0; font-size:13px;">Phone : </td>
                <td style="border: 1px solid #000; border-left:0; border-right:0; font-size:13px;"><?php echo $customer_details['contact_phone']; ?></td>
                <td style="border: 1px solid #000; border-left:0; border-right:0; font-size:13px;">Mobile : </td>
                <td style="border: 1px solid #000; border-left:0; font-size:13px;"><?php echo $customer_details['contact_phone']; ?></td>
                <td style="border: 1px solid #000; border-left:0; font-size:13px;"></td>
                <td style="border: 1px solid #000; border-left:0; font-size:13px;"></td>                                
            </tr>
        </tbody>
    </table>
	<table style="width: 100%; cellspacing="0" margin:5px auto 0;">
    	<tbody>
        	<tr>
                <td style="border: 0px solid #000;" colspan="2">
                    <h3 style="border:0px solid #000; border-bottom-width: 1px; width: 112px; margin:10px auto 15px 5px; font-family: Arial; font-size:13px; font-weight: bold;">PRODUCT DETAILS</h3>
                </td>
            </tr>
            <tr>
                <td style="border: 0px solid #000; width: 120px;">
                    <p style="border:0px solid #F00; margin:0px auto 0px 5px; font-family: Arial; font-size:13px;">Model of Product :</p>
                </td>
                <td style="border: 0px solid #000;">
                    <p style="border:0px solid #F00; margin:0px auto 0px 5px; font-family: Arial; font-size:13px;">NSR15000</p>
				</td>
            </tr>
            <tr>
                <td style="border: 0px solid #000; padding: 5px 2px;">
                    <p style="border:0px solid #F00; margin:0px auto 0px 5px; font-family: Arial; font-size:13px;">S.No. of Product :</p>
                </td>
                <td style="border: 1px solid #000;">
                    <p style="border:0px solid #F00; margin:0px auto 0px 5px; font-family: Arial; font-size:13px;">85745R541</p>
				</td>
            </tr>
        </tbody>
    </table>

	<h3 style="border:0px solid #000; border-bottom-width: 1px; width: 90px; margin:10px auto 5px 5px; font-family: Arial; font-size:13px;">Accessories list</h3>
    <table cellspacing="0" cellpadding="5" style="width: 100%; border-bottom: 1px solid #000;">
    	<thead>
        	<tr>
                <th style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; width: 30px; text-align: center;">S.No.</th>
                <th style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; width: 140px; text-align: center;">PART DESCRIPTION</th>
                <th style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; width: 80px; text-align: center;">PRESENCE</th>
                <th style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; text-align: center;">SERIAL NO.</th>
                <th style="border: 1px solid #000; border-bottom:0; font-size:13px; width: 100px; text-align: center;">REMARKS</th>                               
            </tr>
        </thead>
        <tbody>
        <input type="text" value="<?php echo isset($Complaint['access_cat'])?$Complaint['access_cat']:'' ?>" id="access_cat" />
			<?php
			$cat_id=isset($Complaint['access_cat'])?$Complaint['access_cat']:'';
				$i=1;
				$j=0;
				
				
				foreach($Accessories_list as $list):
				$category=json_decode($list['category'],true);
				//$style='';
				         if($category[$cat_id]==1)
						 {
						
					?>           
            		<tr >
                        <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; text-align: center;"><?php echo $i ?></td>
                        <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px;"><?php echo ucfirst($list['acc_name']).$category[$cat_id]; ?></td>
                        <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px; text-align: center;">
                            <?php
                            $val='Nil';
                            if(isset($prsence[$list['id']])) {
                                if($prsence[$list['id']]==1) {
                                    $val='&#x2714;';
                                }
                            }      
                            ?>
                            <p><?php echo $val; ?></p>
                        </td>
                        <td style="border: 1px solid #000; border-right:0; border-bottom:0; font-size:13px;"><p><?php echo isset($acc_sno[$j])?$acc_sno[$j]:''; ?></p></td>
                        <td style="border: 1px solid #000; border-bottom:0; font-size:13px;"><p><?php echo isset($acc_remarks[$j])?$acc_remarks[$j]:''; ?></p></td>
            		</tr>            
           			<?php
                    $i++;
					$j++;
					}
				endforeach;
			?>        
        </tbody>
    </table>
    
    <?php /*?><table class="table table-bordered">
        <thead>
          <tr>
            <th>No.</th>
            <th>Part Description</th>
            <th>Presence</th>
            <th>S.no</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		 foreach($Accessories_list as $list): ?>
       
       
          <tr>
            <td><?php echo $i ?></td>
             <td><?php echo ucfirst($list['acc_name']); ?></td>
            <td>
			<?php
			$val='Nil';
			if(isset($prsence[$list['id']]))
			{
				if($prsence[$list['id']]==1)
				{
					$val='<i class="fa fa-check"></i>';
				}
			}
	
			
			 ?>
             <p><?php echo $val; ?></p>
             </td>
            <td><p><?php echo isset($acc_sno[$j])?$acc_sno[$j]:''; ?></p></td>
            <td><p><?php echo isset($acc_remarks[$j])?$acc_remarks[$j]:''; ?></p></td>
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table><?php */?>
		
        <h3 style="border:0px solid #000; margin:10px auto 5px 0px; font-family: Arial; font-size:13px; text-indent: 7px;">Physical condition of the Laptop :</h3>
        <h3 style="border:0px solid #000; margin:10px auto 5px 0px; font-family: Arial; font-size:13px; text-indent: 7px;"><?php echo $Complaint['physical_type']; ?></h3>
		<?php
			if(isset($Complaint['phycical'])) {			
				$products=json_decode($Complaint['phycical'],true);
				$i=1;
				?>
				<table cellspacing="0" cellpadding="0" style="width: 100%;">
                    <tbody>
						<?php
							foreach($products as $phycical):
								?>			
								<tr>
									<td style="width:20px; font-size:13px; padding-left: 7px; padding-bottom:3px;"><?php echo $i ?>)</td>
									<td style="font-size:13px; padding-left: 7px; padding-bottom:3px;"><?php echo $phycical ?></td>			
								</tr>				
								<?php
								$i++;
							endforeach;
						?>
                    </tbody>
				</table>
				<?php
            }
		?>
        
        <h3 style="border:0px solid #000; margin:10px auto 5px 0px; font-family: Arial; font-size:13px; text-indent: 7px;">Problem Reported by customer :</h3>
		<?php
			if(isset($Complaint['problem'])) {			
				$products=json_decode($Complaint['problem'],true);
				$i=1;
				?>
				<table cellspacing="0" cellpadding="0" style="width: 100%;">
                    <tbody>
						<?php
							foreach($products as $problem):
								?>			
								<tr>
									<td style="width:20px; font-size:13px; padding-left: 7px; padding-bottom:3px;"><?php echo $i ?>)</td>
									<td style="font-size:13px; padding-left: 7px; padding-bottom:3px;"><?php echo $problem ?></td>			
								</tr>				
								<?php
								$i++;
							endforeach;
						?>
                    </tbody>
				</table>
				<?php
            }
		?>

    <table cellspacing="0" cellpadding="10" style="width: 100%; margin-top: 15px;">
    	<tbody>
        	<tr>
                <td style="border: 0px solid #000; font-size:13px; width: 50%;">Customer Signature</td>
                <td style="border: 0px solid #000; font-size:13px;">Signature of the Cell Co-ordinator</td>                           
            </tr>
            <tr>
                <td style="border: 0px solid #000; font-size:13px; width: 50%;">Name of the Customer</td>
                <td style="border: 0px solid #000; font-size:13px;">Name of the Cell Co-ordinator</td>                                
            </tr>
        </tbody>
    </table>
    <p style="text-align: center; font-size: 12px; position: fixed; bottom: 10px; width: 100%;">
    	Note : Kindly return the job card while taking the delivery of the notebook.
    </p>
    
    
    
   <?php /*?> 
   
   <pagebreak />
   <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 190px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Checking Department Status : </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">Kumaravel Kumaran</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 112px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Engineer Name : </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">Kumaravel Kumaran</h3>
                </td>
                <td style="border:0px solid #F00; width: 30px;">
                </td>
                <td style="border:0px solid #F00; width: 45px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Date : </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000; width: 75px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">09/02/17</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <h3 style="border:0px solid #000; margin:10px auto 5px 0px; font-family: Arial; font-size:15px; text-indent: 7px; font-weight: bold;">REPORT</h3>
    
    <table cellspacing="0" cellpadding="10" style="width: 100%; border: 0px solid #000;">
    	<thead>
        	<tr>
            	<th style="border:1px solid #000; border-right:0; width: 50px;">
              	    SL.NO.
                </th>
                <th style="border:1px solid #000;">
              	    DETAILS
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php for($x=0; $x<12; $x++) { ?>      
            <tr>
            	<td style="border:1px solid #000; border-top:0; border-right:0; text-align: center; width: 50px;">
              	    <?php echo $x+1; ?>
                </td>
                <td style="border:1px solid #000; border-top:0;">
              	    Please fill the details about the reports here...
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000; margin-top:20px;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 100px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Job Waiting For </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">Kumaravel Kumaran</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 125px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Job Commited Date </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">09/02/2017</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 130px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px;">Job Completed Date </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">09/02/2017</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <h3 style="border:0px solid #000; border-bottom: 1px solid #000; width: 100px; margin:20px auto 15px; font-family: Arial; font-size:18px; font-weight: bold;">Check List</h3>
    
    <table cellspacing="5" cellpadding="3" style="width: 100%; border: 0px solid #000; margin-bottom: 15px;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Display</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Camera</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">USB</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Charging</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="3" style="width: 100%; border: 0px solid #000; margin-bottom: 15px;">
    	<tbody>
            <tr>
            	<td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Battery</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">RAM</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Hinges</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Out look</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
            </tr>
        </tbody>
    </table>
    
    <table cellspacing="5" cellpadding="3" style="width: 100%; border: 0px solid #000; margin-bottom: 15px;">
    	<tbody>
            <tr>
            	<td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Key Board</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Audio</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Panel lock</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Void Seal</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
            </tr>
        </tbody>
    </table>
    <table cellspacing="5" cellpadding="3" style="width: 100%; border: 0px solid #000; margin-bottom: 15px;">
    	<tbody>
            <tr>
            	<td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">HDD</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;">Disp. Color</h3>
                </td>
                <td style="border:1px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;">CE</h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;"></h3>
                </td>
                <td style="border:0px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;"></h3>
                </td>
                <td style="border:0px solid #F00; width: 12.5%; text-align: center;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:13px; font-weight: bold;"></h3>
                </td>
                <td style="border:0px solid #000; width: 12.5%;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:13px;"></h3>
                </td>
            </tr>
        </tbody>
    </table>  
    
    <table cellspacing="5" cellpadding="2" style="width: 100%; border: 0px solid #000; margin-top: 20px;">
    	<tbody>
        	<tr>
            	<td style="border:0px solid #F00; width: 290px;">
              	    <h3 style="border:0px solid #000; font-family: Arial; font-size:15px; font-weight: bold;">Laptop assembled and Checked by : </h3>
                </td>
                <td style="border:0px solid #F00; border-bottom:1px dotted #000;">
              	    <h3 style="border:0px solid #000; width: 100%; font-family: Arial; font-size:14px;">Bhuvaneshwaran</h3>
                </td>
            </tr>
        </tbody>
    </table>      <?php */?>
    <script>
	$(document).ready(function()
	 {
	   cat_type();
	 });
	 function cat_type() 
 {
	 var cat=$('#access_cat').val();
	// alert(cat);
	 $('.access_cat').hide();
	 $('.show_'+cat).show();
	 //alert(cat);
 }
 </script>