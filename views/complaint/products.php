<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\BaseHtml;
use yii\helpers\Url;
use app\models\Accesories;
 use kartik\checkbox\CheckboxX;

$Accessories_list = Accesories::find()->all();

if(isset($model))
{
	$id=$model;
	$where_con='complaint_id='.$id;
	$Complaint=Yii::$app->mycomponent->Get_user_details($where_con,'tbl_complaint');
	if($Complaint['laptop_details'])
	{
	  $laptop_details=json_decode($Complaint['laptop_details'],true);
	}
	if($Complaint['prsence'])
	{
	  $prsence=json_decode($Complaint['prsence'],true);
	}
	if($Complaint['acc_sno'])
	{
	  $acc_sno=json_decode($Complaint['acc_sno'],true);
	}
	if($Complaint['acc_remarks'])
	{
	  $acc_remarks=json_decode($Complaint['acc_remarks'],true);
	}
	if($Complaint['warranty'])
	{
	  $warranty=json_decode($Complaint['warranty'],true);
	}
}

/*?> <h1>JOB CARD</h1>
 <table class="table" style="border:0;">
    
    <tbody>
      <tr>
        <td>
            <hgroup>
        	<h2>MR INFOTECH</h2>
            <h3>(Expert in Chip level service)</h3>
            <h3>(Used laptop & accessories sale)</h3>
            </hgroup>
            <p>
            
            </p>
        </td>
        <td>Doe</td>
       
      </tr>
     
    </tbody>
  </table><?php */?>
  <h3>Laptop Details</h3>
<form class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-sm-3">Model of Laptop</label>
    <div class="col-sm-9">
      <p class="form-control-static"><?php echo isset($laptop_details['lap_model'])?$laptop_details['lap_model']:''; ?></p>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3">S.No. of Laptop</label>
    <div class="col-sm-9">
      <p class="form-control-static"><?php echo isset($laptop_details['lap_sno'])?$laptop_details['lap_sno']:''; ?></p>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3">User name</label>
    <div class="col-sm-9">
      <p class="form-control-static"><?php echo isset($laptop_details['lap_user'])?$laptop_details['lap_user']:''; ?></p>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-3">Password</label>
    <div class="col-sm-9">
      <p class="form-control-static"><?php echo isset($laptop_details['lap_pass'])?$laptop_details['lap_pass']:''; ?></p>
    </div>
  </div>
</form>  


<h3>Accessories list</h3>
    
           <table class="table table-bordered">
        <thead>
          <tr>
            <th>No.</th>
            <th>Part Description</th>
            <th>Presence</th>
            <th>S.no</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <tbody>
        <?php  $i=1; $j=0;
		 foreach($Accessories_list as $list): ?>
       
       
          <tr>
            <td><?php echo $i ?></td>
             <td><?php echo ucfirst($list['acc_name']); ?></td>
            <td>
			<?php
			$val='Nil';
			if(isset($prsence[$list['id']]))
			{
				if($prsence[$list['id']]==1)
				{
					$val='<i class="fa fa-check"></i>';
				}
			}
	
			
			 ?>
             <p><?php echo $val; ?></p>
             </td>
            <td><p><?php echo isset($acc_sno[$j])?$acc_sno[$j]:''; ?></p></td>
            <td><p><?php echo isset($acc_remarks[$j])?$acc_remarks[$j]:''; ?></p></td>
          </tr>
         
		<?php  $i++;
		$j++;
		endforeach; ?>
        </tbody>
        </table>
        
         <h3>Physical condition of the Laptop</h3>
         <?php
		if(isset($Complaint['phycical']))
		{
		
		 $products=json_decode($Complaint['phycical'],true);
		 $i=1;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs">
        <?php foreach($products as $phycical): ?>
       
       
          <tr>
           <td style="width:30px;"> <p><?php echo $i ?>)</p></td>
            <td> <p><?php echo $phycical ?></p></td>
           
          </tr>
      
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
           <?php } ?>
           
           
            <h3>Problem reported by customer</h3>
         <?php
		if(isset($Complaint['problem']))
		{
		
		 $products=json_decode($Complaint['problem'],true);
		 $i=1;
		  ?>
           <table class="table table-striped">
        <thead>
         
        </thead>
        <tbody class="inputs">
        <?php foreach($products as $problem): ?>
       
       
          <tr>
           <td style="width:30px;"> <p><?php echo $i ?>)</p></td>
            <td> <p><?php echo $problem ?></p></td>
           
          </tr>
      
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
           <?php } ?>

  
 <!-- <table class="table table-bordered">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>-->