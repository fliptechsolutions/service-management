<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 use kartik\date\DatePicker;
 use kartik\daterange\DateRangePicker;
 use kartik\select2\Select2;
 use app\models\Inventory;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
 use webvimark\modules\UserManagement\models\User;
//print_r( User::getCurrentUser(25));
//$addon = <<< HTML

$this->title = 'Complaint | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus=''; ?>
<?php $Rolls=Yii::$app->mycomponent->GetRolls();
//print_r($Rolls);
foreach($Rolls as $roll)
{
	$filter_roll[$roll['item_name']]=$roll['item_name'];
}
?>

<?php

if(isset($_GET['id']))
{
	$url_open=Yii::$app->request->baseUrl.'/complaint/report?id='.$_GET['id'];
  /*echo "<script>window.open('https://secure.brosix.com/webclient/?nid=4510', 'width=710,height=555,left=160,top=170')</script>";*/
  echo "<script>window.open('".$url_open."')</script>";
  
} ?>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['register'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Complaints'],
    'columns' => [
        //'payment_type',
      //  'contact_phone',
       /* 'Title',
        'Published:date',*/
		 /*['class'=>'kartik\grid\SerialColumn',
		 'header' => 'S No',],*/
		 
		/* [
			'class'=>'kartik\grid\ExpandRowColumn',
			'width'=>'50px',
			'value'=>function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail'=>function ($model, $key, $index, $column) {
				if ($model->complaint_id!='')
				 {
                    $model = \app\models\Complaint::findOne($model->complaint_id);
					
                    return Yii::$app->controller->renderPartial('products', ['model'=>$model->complaint_id]);
				 } 
				 else 
				 {
					return '<div class="alert alert-danger">No data found</div>';
				 }
				
				
				//return Yii::$app->controller->renderPartial('products', ['model'=>$model,'id'=>1]);
			},
			'headerOptions'=>['class'=>'kartik-sheet-style'] 
			//'expandOneOnly'=>true,
		],*/
		
		
		
		 [
            'attribute'=>'complaint_id',
			'headerOptions' => ['style' => 'width:10px'],
            //'pageSummary'=>'Complaint id',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
         ],
		 [
		     'label' => 'Customer name',
            'attribute'=>'first_name',
			'value'=>'customerdetails.first_name',
            //'pageSummary'=>'Page Summary',
           // 'pageSummaryOptions'=>['class'=>'text-right text-warning'],
        ],
		[
		     'label' => 'Contact phone',
            'attribute'=>'contact_phone',
			'value'=>'customerdetails.contact_phone',
			
        ],
		[
		    'label' => 'Address',
			'attribute'=>'address',
            'value'=>'customerdetails.Address',
          
         ],
		  
		 /*[
		    'label' => 'Email Address',
			'attribute'=>'email_address',
            'value'=>'customerdetails.email_address',
			
        ],*/
		 [
            'label'=>'Department',
			'attribute'=>'to_dpt',
			'filter'=>$filter_roll,
		 'value'=> function($model)
              { 
			   $model = \app\models\Job::findOne(['complaint_id'=>$model->complaint_id,'status'=>'pending']);
			 //  $result='Completed'
			    $name= Yii::$app->mycomponent->Get_user($model['to_id']); 
			    $roll= Yii::$app->mycomponent->Get_Roll($model['to_id']);
				$result=ucfirst($name['firstname']).' '.$name['last_name'].'<br> <span class="label label-success">'.ucfirst($roll).'</span>'; 
			    return $result;
             
              },
     'format' => 'raw'
],
		
		
		[
    'attribute'=>'status',
	'filter'=>array('pending'=>'pending','Completed'=>'Completed','Rejected'=>'Rejected'),
    'value'=> function($model)
              { 
			   if($model->status == "pending")
              {
                 $status_class = "label-warning";
              }
              else if($model->status == "Completed")
              {
                 $status_class = "label-success";
              }
              else if($model->status == "Rejected")
              {
                 $status_class = "label-danger";
              }
              else
              {
                 $status_class = "label-default";
              }
			  
                  // return  Html::a('<span class="label '.$status_class.'">'.$model->status.'</span>', ['complaint/status_change','complaint_id'=>$model->complaint_id], ['class' => 'popupModal']);  
				   
				   return  Html::a('<span class="label '.$status_class.'">'.$model->status.'</span>', ' ', [
                    //'onclick' => 'edit_complaint('.$model->complaint_id.')',
					 'onclick' => 'ass_status('.$model->complaint_id.')',
					/*'data-target'=>"#myModal",
					'data-toggle'=>"modal",*/
                   
                ]);     
              },
     'format' => 'raw'
],
		
		
		
		
		[

				//'class'=>'kartik\grid\EditableColumn',
				
				'attribute'=>'date_created',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'date_created',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d H:m:s'],
				
				'opens'=>'left'
				
				]
				
				]),
				
				/*'editableOptions'=> [
				
				'header' => 'term start',
				
				'inputType' => \kartik\editable\Editable::INPUT_TEXT

],
*/



	            'format' => 'raw'		,
			
		
      ],
	  
	  [ 'class' => 'kartik\grid\ActionColumn',
              'header' => 'Spares',
              'template' => '{edit} {print}',
              'buttons' => [
                 
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-plus">Spares</span>',['complaint/spares', 'id' =>$model->complaint_id,'customer_id'=>$model->customer_id,'action'=>'complaint']);
                 },
				 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },
                 
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/complaint/register', 'id' =>$model->complaint_id]);
                }
				if ($action === 'print') {
					$sales = Inventory::find()->where(['=', 'complaint_id',$model->complaint_id])->one();
                    $url = Url::to(['/complaint/bill', 'id' =>$model->complaint_id,'sales_id'=>isset($sales->sales_id)?$sales->sales_id:'','customer_id' =>$model->customer_id]);
                } 
                                 
                return $url;
            }
          ],
		
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
              'template' => '{assign} {edit} {delete} {view} {print}',
              'buttons' => [
                 'assign' => function ($url, $model) {
					// $row = \app\models\Job::findOne(['complaint_id' => $model->complaint_id,'status'=>'pending']);
					 $row = \app\models\Job::findOne(['complaint_id' => $model->complaint_id]);
					$row_id=0;
					 if(isset($row->id))
					 {
						 $row_id=$row->id;
					 }
                   return Html::a('<span class="glyphicon glyphicon-user"></span> ', $url,[
                    //'onclick' => 'edit_complaint('.$model->complaint_id.')',
					 'onclick' => 'assign_complaint('.$model->complaint_id.','.$row_id.')',
					/*'data-target'=>"#myModal",
					'data-toggle'=>"modal",*/
                   
                ]);
                 },
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                ]);
                 },
				 'view' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                 },
				 
				 'print' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-print"></span>', $url,['target'=>'_blank', 'data-pjax'=>"0"]);
                 },
				 'format' => 'raw'
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/complaint/register', 'id' =>$model->complaint_id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/complaint/delete', 'id' =>$model->complaint_id]);
                } 
				if ($action === 'view') {
                    $url = Url::to(['/complaint/view_complaint', 'id' =>$model->complaint_id,]);
                }   
				 if ($action === 'print') {
                    $url = Url::to(['/complaint/report', 'id' =>$model->complaint_id,]);
                }                   
                return $url;
            }
          ],
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
<?php //\yii\widgets\Pjax::end(); ?>

 

<?php
$this->registerJs("$(function() {
   $('.popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});"); ?>
 

<div id="state_list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
<input type="hidden" data-toggle="modal" data-target="#stc" class="stc"/>
<input type="hidden" data-toggle="modal" data-target="#stat_change" class="stat_change"/>
	<script>
   function assign_complaint(complaint_id,job_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(complaint_id);
		$("#job_id").val(job_id);
		$(".assign_to").trigger('click');
        
    }
	 function ass_status(complaint_id) 
    {
    
       // alert(complaint_id);
		$("#sts_cid").val(complaint_id);
		status_change(complaint_id)
		$(".stat_change").trigger('click');
        
    }
    </script>


<script>
function status_change(complaint_id) {
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_change' ?>',
	data:'complaint_id='+complaint_id,
	success: function(data){
	//alert(data);
		$("#compl_status").html(data);
		//$("#stc").trigger('click');
	}
	});
}
</script>


<script>
function edit_complaint(order_id) {

//alert(order_id);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/register' ?>',
	data:'order_id='+order_id,
	success: function(data){
	alert(data);
		$("#receipt").html(data);
		$(".receipt_trigg").trigger('click');
	}
	});
}
</script>

<script>
function assign_status() {

var complaint_id=$('#sts_cid').val();
var status=$('#compl_status').val();
var status_notes=$('#status_notes').val();
//alert(status);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_update' ?>',
	data:'status='+status+'&complaint_id='+complaint_id+'&notes='+status_notes,
	success: function(data){
	//alert("#status_"+val);
	
		$('#modal').modal('hide');
		$.pjax.reload({container:'#w0'});
		
	}
	});
}


</script>


<div class="modal fade" id="stat_change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='sts_cid' name="sts_cid"  />
                
             
               
               
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Status </label>
               <div class="col-sm-8"> 
               <select name="compl_status" id="compl_status" class="team_member form-control" style="width:375px;"   ></select>
            
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="status_notes" id="status_notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="assign_status()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                  <input type="hidden" id='job_id' name="job_id"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Assign to </label>
               <div class="col-sm-8"> 
                <?php foreach($Rolls as $rolle) { $rolls1[$rolle['item_name']]=$rolle['item_name'];	} ?>
                <?php
				
					echo Select2::widget([
					'name' => 'test',
					'id' => 'role',
					'data' => $rolls1,
					'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
					'options' => ['placeholder' => 'Select Rolls ...','class'=>'roll-name','onchange'=>'get_team()'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]);
				
		?>
            
			   </div> 
               </div>
               
               
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Team Member </label>
               <div class="col-sm-8"> 
               <select name="team_member" id="team_member" class="team_member form-control" style="width:375px;"   ></select>
            
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="notes" id="notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function(){
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
});



function get_team()
{
	//console.log($('textarea[name*="s_no[]"]').val());
	
	//JSON.parse();
	var team_name=$('.roll-name').val();
	      
		  
		  $('.team_member option').each(function() {
    
           $(this).remove();
    
           });
		 

	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/get_team_member' ?>',
	data:'team_name='+team_name,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	success: function(response){
		
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var result=response.team_users;
			for(var item in result)
			{
				
			  $('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('#team_member');
			}
		
		  
		}
		else
		{
			alert(response.results);
		}
	
	}
	});

	
}

function st_assign()
 {

var cid=$('#cid').val();
var notes=$('#notes').val();
var role=$('#role').val();
var job_id=$('#job_id').val();
var team_member=$('#team_member').val();
//alert(job_id);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/roleassign' ?>',
	data:'cid='+cid+'&notes='+notes+'&role='+role+'&job_id='+job_id+'&team_member='+team_member,
	success: function(data){
		if(data.trim()!='updated')
		{
	      alert(data);
		}
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
		$.pjax.reload({container:'#w0'});
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->