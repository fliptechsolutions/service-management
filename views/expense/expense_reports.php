<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 use app\models\Expense;
 use yii\data\SqlDataProvider;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Expense | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_expense'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['expense'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Expense Reports'],
    'columns' => [
       
		  [
		    'label'=>'Expense Type',
		    'attribute'=>'expense_name',
			'value'=>
			function($model)
			{
			  if(isset($model->expense_id))
              { 
			  
			       $Expense= Expense::find()->where(['id'=>$model->expense_id])->one();
				    $search_date=date("Y-m-01").' - '.date("Y-m-t");
				   if(isset($_GET['Expense_reports']['exp_date']) && $_GET['Expense_reports']['exp_date']!='')
				   {
					  $search_date= $_GET['Expense_reports']['exp_date'];
				   }
				  
                   return  Html::a('<span>'.$Expense->expense.'</span>', 
				   ['/expense/expense','Expense_details[expense_name]'=>$Expense->expense,'Expense_details[exp_date]'=>$search_date]);      
              }
			},
			'format'=>'raw'
			
          ],
		   
		  
		  [
		    'label'=>'Amount',
		    'attribute'=>'amount',
			'value'=>'amount',
			// 'width'=>'20%',
          ],
		  
		  
		  [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Expense date',
				
				'attribute'=>'exp_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'exp_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
		 
		  
		
		
		
    ],
]);


?>
 
 

