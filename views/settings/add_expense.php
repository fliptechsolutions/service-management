<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Expense | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/settings/add_vat'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['settings/vat'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Expense type</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'expense', ['label'=>'Expense type', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'expense',['showLabels'=>false])->textInput(['placeholder'=>'Expense']); ?>
    </div>
    </div>
    <div class="form-group">
    <?= Html::activeLabel($model, 'Description', ['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'description',['showLabels'=>false])->textArea(['placeholder'=>'description']); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
