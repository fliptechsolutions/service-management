<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'VAT | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/settings/add_vat'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['settings/vat'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Vat settings</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'vat_name', ['label'=>'Vat Name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'vat_name',['showLabels'=>false])->textInput(['placeholder'=>'Vat Name']); ?>
    </div>
    </div>
    <div class="form-group">
    <?= Html::activeLabel($model, 'vat', ['label'=>'Vat', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'vat',['showLabels'=>false])->textInput(['placeholder'=>'vat(%)']); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
