<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Accounts | MR IMPEX'; 
use app\models\Accounts;
?>

<?php 
      $type='bank';
      if(isset($accounts->payment_type)) 
      {
		   $type=$accounts->payment_type;
		   
	  }
	  $url=['settings/add_accounts'];
	  if(isset($_GET['id']))
	  {
		$url=['settings/update_accounts','id'=>$_GET['id']];  
	  }
	  
	  ?>

<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/settings/add_accounts'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/settings/accounts'], ['class'=>'btn btn-success']) ?> 
            
        </div>
        <div> <h3>ADD ACCOUNTS</h3></div>
        <br />  
<div class="user-form">
<?= Html::beginForm($url, 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ; ?>
    
   
   
    <div class="form-group">
     <?= Html::label('Payment type', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <input type="radio" name="payment_type" value="bank" <?php if(isset($accounts->payment_type) && ($accounts->payment_type=='bank' )) { echo 'checked="checked"'; }?> onclick="ac_enable('bank')" checked="checked"  />Bank
      <input type="radio" name="payment_type" value="cash" <?php if(isset($accounts->payment_type) && ($accounts->payment_type=='cash' )) { echo 'checked="checked"'; }?> onclick="ac_enable('cash')"/>Cash
      <input type="radio" name="payment_type" value="cheque" <?php if(isset($accounts->payment_type) && ($accounts->payment_type=='cheque' )) { echo 'checked="checked"'; }?> onclick="ac_enable('cheque')"/>Cheque
      <input type="radio" name="payment_type" value="credit" <?php if(isset($accounts->payment_type) && ($accounts->payment_type=='credit' )) { echo 'checked="checked"'; }?> onclick="ac_enable('credit')"/>Credit
    </div>
    </div>
    
    <div class="form-group" >
     <?= Html::label('Account name', 'Account name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'account_name',isset($accounts->account_name)?$accounts->account_name:'',['class' => ['form-control']]) ?>
    </div>
    </div>

     <div class="form-group credit">
     <?= Html::label('Days', 'bank_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('number', 'days',isset($accounts->days)?$accounts->days:'',['class' => ['form-control bank_account']]) ?>
    </div>
    </div>
    

    
     <div class="form-group bank">
     <?= Html::label('Bank name', 'bank_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'bank_name',isset($accounts->bank_name)?$accounts->bank_name:'',['class' => ['form-control bank_account']]) ?>
    </div>
    </div>
    
    
     <div class="form-group bank">
     <?= Html::label('Account Number', 'account_number', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'account_number',isset($accounts->account_number)?$accounts->account_number:'',['class' => ['form-control bank_account']]) ?>
    </div>
    </div>
    <div class="form-group bank">
     <?= Html::label('Branch', 'branch', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'branch',isset($accounts->branch)?$accounts->branch:'',['class' => ['form-control  bank_account']]) ?>
    </div>
    </div>
    
     <div class="form-group bank">
     <?= Html::label('IFSC', 'ifsc', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'ifsc',isset($accounts->ifsc)?$accounts->ifsc:'',['class' => ['form-control bank_account ']]) ?>
    </div>
    </div>
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?= Html::endForm() ?>
</div>
</div>
</div>
<script>
$(document).ready(function(){
	var type="<?php echo $type;?>";
	 ac_enable(type)
	
});
function ac_enable(type)
{
	//alert('.'+type);
	$('.credit').hide();
	if(type=='cash' || type=='cheque')
	{
		$('.bank').hide();
		//$('.bank_account').val('');
	}
	else if(type=='credit')
	{
		$('.cash').hide();
		$('.bank').hide();
		//$('.cash_account').val('');
	}
	else
	{
		$('.cash').hide();
		//$('.cash_account').val('');
	}
	$('.'+type).show();
}
</script>
