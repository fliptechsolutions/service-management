<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Income | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/settings/add_vat'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['settings/vat'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Income settings</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'income_name', ['label'=>'Income Name', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
        <?= $form->field($model, 'income_name',['showLabels'=>false])->textInput(['placeholder'=>'Income Name']); ?>
    </div>
    </div>
    <div class="form-group">
    <?= Html::activeLabel($model, 'Description', ['label'=>'Description', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'description',['showLabels'=>false])->textInput(['placeholder'=>'Description']); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
