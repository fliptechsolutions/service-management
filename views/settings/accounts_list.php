<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Accounts | MR IMPEX'; 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_accounts'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['accounts'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'ACCOUNTS'],
    'columns' => [
	
			[
			  'label'=>'Payment type',
			  'attribute'=>'payment_type',
			  'filter'=>array('Cash'=>'Cash','Bank'=>'Bank','Cheque'=>'Cheque','credit'=>'Credit'),
			],
       
	      [
		    'label'=>'Account name',
		    'attribute'=>'account_name',
			'value'=>'account_name',
			
          ],
		  [
		    'label'=>'Bank name',
		    'attribute'=>'bank_name',
			'value'=>'bank_name',
			
          ],
		 
		  [
		    'label'=>'Account number',
            'attribute'=>'account_number',          
          ],
		  [
		    'label'=>'Branch',
            'attribute'=>'branch',           
          ],
		  
		  [
		    'label'=>'IFSC',
            'attribute'=>'ifsc',        
          ],
		  
		  [
		    'label'=>'Credit Days',
            'attribute'=>'days',        
          ],
		
		
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
			  
              'template' => '{edit} {delete}',
              'buttons' => [
                
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/settings/update_accounts', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/settings/delete_accounts', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);


?>
 
 

