<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Settings | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">

        <div> <h3>GENERAL SETTINGS</h3></div>
        <br />  
<div class="user-form">
<?= Html::beginForm(['settings/settings'], 'post', ['enctype' => 'multipart/form-data' ,'class'=>'form-horizontal']) ; ?>
    
   
     <div class="form-group">
     <?= Html::label('Company name', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'company_name',Yii::$app->mycomponent->Get_settings('company_name'),['class' => ['form-control'],'required'=>'required']) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('Title', 'title', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::textArea('title',Yii::$app->mycomponent->Get_settings('title'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
    <div class="form-group">
     <?= Html::label('Company Address', 'Address', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::textArea('address',Yii::$app->mycomponent->Get_settings('address'),['class' => ['form-control'],'required'=>'required']) ?>
    </div>
    </div>
    
    <div class="form-group">
     <?= Html::label('Contact number', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'contact_number1',Yii::$app->mycomponent->Get_settings('contact_number1'),['class' => ['form-control'],'required'=>'required']) ?>
    </div>
    </div>
     <div class="form-group">
     <?= Html::label('Mobile number', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'contact_number2',Yii::$app->mycomponent->Get_settings('contact_number2'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('Tin Number', 'bank_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'tin_number',Yii::$app->mycomponent->Get_settings('tin_number'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('CST Number', 'bank_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'cst_number',Yii::$app->mycomponent->Get_settings('cst_number'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
    <div class="form-group">
     <?= Html::label('Service Mobile No.', 'Company_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'contact_number3',Yii::$app->mycomponent->Get_settings('contact_number3'),['class' => ['form-control']]) ?>
    </div>
    </div>
    <div class="form-group">
     <?= Html::label('City', 'City', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'city',Yii::$app->mycomponent->Get_settings('city'),['class' => ['form-control'],'required'=>'required']) ?>
    </div>
    </div>
    <div class="form-group">
     <?= Html::label('State', 'State', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'state',Yii::$app->mycomponent->Get_settings('state'),['class' => ['form-control'],'required'=>'required']) ?>
    </div>
    </div>
     <div class="form-group">
     <?= Html::label('Email', 'Email', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'email',Yii::$app->mycomponent->Get_settings('email'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('Website', 'website', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'website',Yii::$app->mycomponent->Get_settings('website'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('Bank name', 'bank_name', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'bank_name',Yii::$app->mycomponent->Get_settings('bank_name'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
    
     <div class="form-group">
     <?= Html::label('Account Number', 'account_number', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'account_number',Yii::$app->mycomponent->Get_settings('account_number'),['class' => ['form-control']]) ?>
    </div>
    </div>
    <div class="form-group">
     <?= Html::label('Branch', 'branch', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'branch',Yii::$app->mycomponent->Get_settings('branch'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
     <div class="form-group">
     <?= Html::label('IFSC', 'ifsc', ['class'=>'control-label col-sm-2']) ?>
    <div class="col-sm-4">
      <?= Html::Input('text', 'ifsc',Yii::$app->mycomponent->Get_settings('ifsc'),['class' => ['form-control']]) ?>
    </div>
    </div>
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?= Html::endForm() ?>
</div>
</div>
</div>
