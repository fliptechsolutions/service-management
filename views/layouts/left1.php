<?php 
 use yii\widgets\Pjax;

//Pjax::begin(); 
       
			$menus=Yii::$app->mycomponent->menu();
		


?>
<aside class="main-sidebar">

<?php
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\components\GhostNav;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\models\User;
echo GhostMenu::widget([
    'encodeLabels'=>true,
    'activateParents'=>true,
    'items' => [
        [
           // 'label' => 'Backend routes',
            //'items'=>UserManagementModule::menuItems()
        ],
        [
           // 'label' => 'Frontend routes',
            'items'=>[
               
				['label'=>'Users',  'icon' => 'fa fa-user','url'=>['/admin/users'], 'active' => in_array(Yii::$app->controller->action->id,['users','newuser','updateuser'])],
				['label'=>'Job Cards',  'icon' => 'fa fa-user','url'=>['/complaint/'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['complaint/index','complaint/register'])],
				['label'=>'My Jobs',  'icon' => 'fa fa-user','url'=>['/complaint/newcomplaint']],
				['label'=>'Customers',  'icon' => 'fa fa-user','url'=>['/complaint/customers'], 'active' => in_array(Yii::$app->controller->action->id,['customers','newcustomer','updatecustomer'])],
                ['label'=>'Vendors', 'url'=>['/complaint/vendor'], 'active' => in_array(Yii::$app->controller->action->id,['vendor','newvendor','updatevendor'])],
				
				['label' => 'Accessories', 'items' => [
				['label'=>'Category of Accessories', 'url'=>['/inventory/cataccessories'], 'active' => in_array(Yii::$app->controller->action->id,['cataccessories','newcataccesories','updatecateaccessories'])],
				['label'=>'Accessories', 'url'=>['/inventory/accessories'], 'active' => in_array(Yii::$app->controller->action->id,['accessories','newaccesories','updateaccessories'])]
				]],
				['label'=>'Shops', 'url'=>['/inventory/shop'], 'active' => in_array(Yii::$app->controller->action->id,['shop','newshop','updateshop'])],
				['label'=>'Products', 'url'=>['/inventory/products'], 'active' => in_array(Yii::$app->controller->action->id,['products','newproduct','updateproducts'])],
				['label'=>'Purchase', 'url'=>['/inventory/purchase'] , 'active' => in_array(Yii::$app->controller->action->id,['purchase','newpurchase','updatepurchase'])],
				['label'=>'Store', 'url'=>['/store/'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['store/index','store/newsales','store/updatesales'])],
				['label'=>'Sales', 'url'=>['/inventory/sales'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/sales','inventory/newsales','inventory/updatesales'])],
				['label'=>'Stock',  'icon' => 'fa fa-user','url'=>['/store/stock']],
				['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
                /*['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
                ['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],*/
            ],
        ],
    ],
]);
//echo Yii::$app->controller->action->id;
?>

   

    </section>

</aside>

<?php 
//print_r(Yii::$app->authManager->getRoles());

/*?><ul class='sidebar-menu'>
<?php 

 foreach($menus as $menu):
 
 $check_url=User::canRoute($menu['url']);
 if($check_url==1)
 {
	 
	   if (strpos($menu['active'], Yii::$app->controller->action->id) !== false) 
		{
			$class='class="active"';
			
		}
		else
		{
			$class='';
			
		}
		$url=array($menu['url']);

	  ?>
 <li <?php echo $class; ?>><a href="<?php echo ($url[0]);?>"><i class="<?php echo $menu['icon'];?>"></i> <span><?php echo $menu['label'];?></span></a></li>
	 
<?php } endforeach; ?>
     </ul>
     </section>
     </aside><?php */?>