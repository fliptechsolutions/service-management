<?php 
 use yii\widgets\Pjax;

 use webvimark\modules\UserManagement\models\User;
//Pjax::begin(); ?>
<aside class="main-sidebar">

   

        <?php
		if((User::canRoute(['/admin/users'])==1))
		{
			$items[]=['label'=>'Staffs',  'icon' => 'fa fa-user','url'=>['/admin/users'], 'active' => in_array(Yii::$app->controller->action->id,['users','newuser','updateuser'])];
		}
		
		if((User::canRoute(['/complaint/customers'])==1) && (User::canRoute(['/complaint/vendor'])==1))
        {
	         $items[]=['label' => 'Users','url'=>'#','icon' => 'fa fa-user', 'items' => [
				['label'=>'Customers',  'icon' => 'fa fa-user','url'=>['/complaint/customers'], 'active' => in_array(Yii::$app->controller->action->id,['customers','newcustomer','updatecustomer'])],
                ['label'=>'Vendors', 'icon' => 'fa fa-user','url'=>['/complaint/vendor'], 'active' => in_array(Yii::$app->controller->action->id,['vendor','newvendor','updatevendor'])],
				]];
         }
		
        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' =>
				 [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
					['label' => 'Merchant Add','icon' => 'fa fa-cutlery', 'url' => ['admin/register']],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Same tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>
    </section>

</aside>
<?php //Pjax::end(); ?>