<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
   

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    copyrights@2017 | MR INFOTECH | Developed by <a href="http://www.fliptech.co.in" target="_blank">Fliptech Solutions</a>
</footer>