<style>

.scroll_fix {
	overflow: scroll;
    overflow-x: hidden;
    height: 550px;
    margin-right: -17px;
    /*padding-right: 84px;*/
	
	
  /*  position: fixed;
    width: 248px;
    overflow-y: scroll;
    top: 0;
    bottom: 0;*/
}
</style>
<?php 
 use kartik\sidenav\SideNav;
 use webvimark\modules\UserManagement\components\GhostNav;
use webvimark\modules\UserManagement\UserManagementModule;
 use webvimark\modules\UserManagement\models\User;
$permission=['/complaint/'];
//echo "kk".$superAdminAllowed;
//echo User::canRoute($permission);
 $type='SideNav::TYPE_PRIMARY';
 $item='1';
use yii\helpers\Url;
echo '<aside class="main-sidebar">';


$items=array();
$Customers='';
$Vendors='';
$Categories='';
$Accessories='';
$Shops='';
$Purchase='';
$Sales='';
$Store='';
$Selling='';
$complaints='';
$products='' ;
$product_type='';
$manufacture='';
$voucher=''; 
$General='';
$General_income='';
$receipt='';
$vat='' ;
$expense='';
$income='';
$general_settings='';
$main_accessories='';
$adminattendance='';
$userattendance='';
$attendance_reports='';
$reportsattanance='';
 $salesReports='';
 $purchaseReports='';
 $receiptReports='';
 $voucherReports='';
 $expenseReports='';
 $incomeReports='';
 $stock='';
  $accounts='';
if((User::canRoute(['/admin/users'])==1))
{
	$items[]=['label'=>'Staffs', 'icon' => 'user','url'=>['/admin/users', 'type'=>SideNav::TYPE_SUCCESS], 'active' => in_array(Yii::$app->controller->action->id,['users','newuser','updateuser']), 'class' => 'treeview'];
}
if((User::canRoute(['/complaint/complaint'])==1))
{
	$complaints=['label'=>'Complaints',  'url'=>['/complaint/complaint'], 'active' => in_array(Yii::$app->controller->action->id,['complaint','spares','register'])];
}
if((User::canRoute(['/complaint/estimate'])==1))
{
	$estimate=['label'=>'Complaint estimates', 'url'=>['/complaint/estimate'], 'active' => in_array(Yii::$app->controller->action->id,['estimate'])];
}

if($complaints!='')
{
	$items[]=['label'=>'Job Cards',  'icon' => 'tag','items' => array_filter([$complaints,$estimate])];
}

if((User::canRoute(['/admin/adminattendance'])==1))
{
	$adminattendance=['label'=>'Attendance', 'url'=>['/admin/adminattendance'], 'active' => in_array(Yii::$app->controller->action->id,['adminattendance'])];
}
if((User::canRoute(['/admin/userattendance'])==1))
{
	$userattendance=['label'=>'User attendance', 'url'=>['/admin/userattendance'], 'active' => in_array(Yii::$app->controller->action->id,['userattendance'])];
}
if((User::canRoute(['/admin/attendance_reports'])==1))
{
	$attendance_reports=['label'=>'Attendance reports', 'url'=>['/admin/attendance_reports'], 'active' => in_array(Yii::$app->controller->action->id,['attendance_reports'])];
}
if((User::canRoute(['/admin/reportsattanance'])==1) && Yii::$app->user->identity->firstname!='superadmin')
{
	$reportsattanance=['label'=>'Attendance reports', 'url'=>['/admin/reportsattanance'], 'active' => in_array(Yii::$app->controller->action->id,['reportsattanance'])];
}

if($adminattendance!='' || $userattendance!='' || $attendance_reports!='' || $reportsattanance!='')
{
	$items[]=['label'=>'Attendance',  'icon' => 'user','items' => array_filter([$adminattendance,$userattendance,$attendance_reports,$reportsattanance])];
}

if((User::canRoute(['/complaint/newcomplaint'])==1) && Yii::$app->user->identity->firstname!='superadmin')
{
	$items[]=['label'=>'My Jobs',  'icon' => 'tag','url'=>['/complaint/newcomplaint']];
}
if((User::canRoute(['/complaint/customers'])==1))
{
	$Customers=['label'=>'Customers',  'icon' => 'user','url'=>['/complaint/customers'], 'active' => in_array(Yii::$app->controller->action->id,['customers','newcustomer','updatecustomer'])];
}
if((User::canRoute(['/complaint/vendor'])==1))
{
	$Vendors=['label'=>'Suppliers', 'icon' => 'user','url'=>['/complaint/vendor'], 'active' => in_array(Yii::$app->controller->action->id,['vendor','newvendor','updatevendor'])];
}
if((User::canRoute(['/inventory/selling'])==1))
{
	$Selling=['label'=>'Discount', 'url'=>['/inventory/selling'], 'active' => in_array(Yii::$app->controller->action->id,['selling','addselling','updateselling'])];
}
if($Customers!='' || $Vendors!='' || $Selling!='')
{
	$items[]=['label' => 'Users','icon' => 'user', 'items' => array_filter([$Customers,$Vendors,$Selling])];
}
if((User::canRoute(['/inventory/cataccessories'])==1))
{
	$Categories=['label'=>'Category of Accessories', 'url'=>['/inventory/cataccessories'], 'active' => in_array(Yii::$app->controller->action->id,['cataccessories','newcataccesories','updatecateaccessories'])];
}
if((User::canRoute(['/inventory/cataccessories'])==1) || (User::canRoute(['/inventory/accessories'])==1))
{
	$Accessories=['label'=>'Accessories', 'url'=>['/inventory/accessories'], 'active' => in_array(Yii::$app->controller->action->id,['accessories','newaccesories','updateaccessories'])];
}

if($Categories!='' || $Accessories!='')
{
	$main_accessories=['label' => 'Accessories','icon' => 'cog', 'items' => array_filter([$Categories,$Accessories])];
}

if((User::canRoute(['/inventory/products'])==1))
{
	$products=['label'=>'Products', 'url'=>['/inventory/products'], 'active' => in_array(Yii::$app->controller->action->id,['products','newproduct','updateproducts'])];
}

if((User::canRoute(['/inventory/product_type'])==1))
{
	$product_type=['label'=>'Product type', 'url'=>['/inventory/product_type'], 'active' => in_array(Yii::$app->controller->action->id,['product_type','addproduct','updateprotype'])];
}

if((User::canRoute(['/inventory/manufacture'])==1))
{
	$manufacture=['label'=>'Manufacture', 'url'=>['/inventory/manufacture'], 'active' => in_array(Yii::$app->controller->action->id,['manufacture','add_manufact','updatemanufact'])];
}


if($products!='' || $product_type!='' || $manufacture!='')
{
	$items[]=['label' => 'Products','icon' => 'wrench', 'items' => array_filter([$product_type,$manufacture,$products])];
}

if((User::canRoute(['/inventory/shop'])==1))
{
	$Shops=['label'=>'Warehouse', 'url'=>['/inventory/shop'], 'active' => in_array(Yii::$app->controller->action->id,['shop','newshop','updateshop'])];
}

if((User::canRoute(['/inventory/opening_stock'])==1))
{
	$stock=['label'=>'Opening Stock', 'url'=>['/inventory/opening_stock'], 'active' => in_array(Yii::$app->controller->action->id,['opening_stock','update_stock','add_stock'])];
}

if((User::canRoute(['/inventory/purchase'])==1))
{
	$Purchase=['label'=>'Purchase' , 'url'=>['/inventory/purchase'] , 'active' => in_array(Yii::$app->controller->action->id,['purchase','newpurchase','updatepurchase'])];
}
if((User::canRoute(['/inventory/sales'])==1))
{
	$Sales=['label'=>'Sales', 'url'=>['/inventory/sales'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/sales','inventory/newsales','inventory/updatesales'])];
}
if((User::canRoute(['/store/index'])==1))
{
	$Store=['label'=>'Spares', 'url'=>['/store/index'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['store/index','store/newsales','store/updatesales'])];
}


if((User::canRoute(['/inventory/voucher'])==1))
{
	$voucher=['label'=>'Payments', 'url'=>['/inventory/voucher'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/voucher','inventory/add_voucher','inventory/update_voucher'])];
}
if((User::canRoute(['/inventory/receipt'])==1))
{
	$receipt=['label'=>'Receipt', 'url'=>['/inventory/receipt'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/receipt','inventory/add_receipt','inventory/update_receipt'])];
}


if($Shops!='' || $Purchase!='' || $Sales!='' || $Store!='' || $stock!='')
{
	$items[]=['label' => 'Inventory','icon' => 'briefcase', 'items' => array_filter([$Shops,$Purchase,$Sales,$stock,$Store])];
}

$General =['label'=>'General Expense','url'=>['/expense/expense'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['expense/expense','expense/add_expense','expense/update_expense'])];
$General_income=['label'=>'General Income','url'=>['/income/income'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['income/income','income/add_income','income/update_income'])];


if($voucher!='')
{
	$items[]=['label' => 'Expense','icon' => 'tasks', 'items' => array_filter([$General,$voucher])];
}

if($receipt!='')
{
	$items[]=['label' => 'Income','icon' => 'tasks', 'items' => array_filter([$General_income,$receipt])];
}

if((User::canRoute(['/store/stock'])==1))
{
	$items[]=['label'=>'Stock',  'icon' => 'tasks','url'=>['/store/stock']];
}

if((User::canRoute(['/settings/settings'])==1))
{
	$general_settings=['label'=>'General Settings','url'=>['/settings/settings'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['settings/settings'])];
}

if((User::canRoute(['/settings/accounts'])==1))
{
	$accounts=['label'=>'Accounts','url'=>['/settings/accounts'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['settings/accounts','settings/add_accounts'])];
}


if((User::canRoute(['/inventory/salesreports'])==1))
{
	$salesReports=['label'=>'Sales Reports', 'url'=>['/inventory/salesreports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/salesreports'])];
}
if((User::canRoute(['/inventory/purchasereports'])==1))
{
	$purchaseReports=['label'=>'Purchase Reports', 'url'=>['/inventory/purchasereports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/purchasereports'])];
}
if((User::canRoute(['/inventory/receiptreports'])==1))
{
	$receiptReports=['label'=>'Receipt Reports', 'url'=>['/inventory/receiptreports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/receiptreports'])];
}
if((User::canRoute(['/inventory/voucherreports'])==1))
{
	$voucherReports=['label'=>'Voucher Reports', 'url'=>['/inventory/voucherreports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['inventory/voucherreports'])];
}
if((User::canRoute(['/expense/expensereports'])==1))
{
	$expenseReports=['label'=>'Expense Reports', 'url'=>['/expense/expensereports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['expense/expensereports'])];
}
if((User::canRoute(['/income/incomereports'])==1))
{
	$incomeReports=['label'=>'Income Reports', 'url'=>['/income/incomereports'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['income/incomereports'])];
}



if($salesReports!='' || $purchaseReports!='' || $receiptReports!='' || $voucherReports!='')
{
	$items[]=['label' => 'Reports','icon' => 'th-list', 'items' => array_filter([$salesReports,$purchaseReports,$receiptReports,$voucherReports,$expenseReports,$incomeReports])];
}


if((User::canRoute(['/settings/vat'])==1))
{
	$vat=['label'=>'Vat', 'url'=>['/settings/vat'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['settings/add_vat','settings/vat','settings/update_vat'])];
}
if((User::canRoute(['/settings/expense'])==1))
{
	$expense=['label'=>'Expense Type', 'url'=>['/settings/expense'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['settings/expense','settings/add_expense','settings/update_expense'])];
}
if((User::canRoute(['/settings/income'])==1))
{
	$income=['label'=>'Income Type', 'url'=>['/settings/income'], 'active' => in_array(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id,['settings/income','settings/add_income','settings/update_income'])];
}


if($vat!=''|| $expense!='' || $general_settings!='' || $accounts!='')
{
	$items[]=['label' => 'Settings','icon' => 'wrench', 'items' => array_filter([$general_settings,$accounts,$vat,$expense,$income,$main_accessories])];
}

$items[]=['label'=>'Change own password', 'icon' => 'lock','url'=>['/user-management/auth/change-own-password']];

echo SideNav::widget([
    'type' => $type,
    'encodeLabels' => false,
    'heading' => false,
    'items'=>$items	
]);        
?>
 </section>

</aside>

