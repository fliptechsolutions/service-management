<?php
use yii\helpers\Html;
use lajax\translatemanager\helpers\Language;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 

    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> 
   
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 
    
    <!-- Load SCRIPT.JS which will create datepicker for input field  -->
   

<header class="main-header">

    <?= Html::a('<span class="logo-mini">MR</span><span class="logo-lg">' . Yii::$app->mycomponent->Get_settings('company_name'). '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    
    <?php 
	
	 //echo"kk". \lajax\translatemanager\widgets\ToggleTranslate::widget();?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                
                
                <!-- Tasks: style can be found in dropdown.less -->
                
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <!--  <img src="<?= $directoryAsset;
					 
					   ?>
                      
                       /img/user2-160x160.jpg" class="user-image" alt="User Image"/>-->
                        <span class="hidden-xs"><?php echo ucfirst(Yii::$app->user->identity->firstname); ?><br>
						<?php $roll= Yii::$app->mycomponent->Get_Roll(Yii::$app->user->identity->id); 
						 echo ucfirst($roll);?></span>
                       
                    </a>
                   
                        <!-- Menu Body -->
                       
                        <!-- Menu Footer-->
                       
                              
                </li>

              <li>
      <?= Html::a( '<i class="fa fa-power-off"></i>',['/user-management/auth/logout']) ?>
      </li>
            </ul>
        </div>
    </nav>
</header>