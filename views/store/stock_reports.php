<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
  use app\models\Inventory;


$this->title = 'Stock Reports | '.Yii::$app->mycomponent->Get_settings('company_name');; 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/



$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>

<?php
//print_r($dataProvider);
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	//'options'=>[],
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	
	    'toolbar'=> [
		'<div style="float:left; margin-right: 600px;" >PRODUCT ID : <b> '.$_GET['id'].'</b>
		</div>',
		
        ['content'=>
		
                   Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['stockreports'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Stock Reports'],
    'columns' => [
	
	   
		 [
            'label'=>'Product Name',
			'attribute' => 'product_name',
			'value' => 'product.product_name',
			
           
         ],
		  [
            'label'=>'Product Model',
			'attribute' => 'product_model',
			'value' => 'product.product_model',
			
           
         ],
		 [
            'label'=>'Manufacturer',
			'attribute' => 'manufacturer',
			'value' => 'product.manufacturer',
			
           
         ],
		 
		 [
            'label'=>'S.NO',
			'attribute' => 'sno',
			'value' => 'sno',
			
           
         ],
    
		 [
		    'label' => 'Selling Price',
            'attribute'=>'selling_price',
			//'contentOptions' => ['class' => 'text-center'],
			//'width'=>'10%',
         ],
		
	
		
    ],
]);
?>
