<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 //use app\models\Orders;
 use yii\data\SqlDataProvider;
  use app\models\Inventory;


$this->title = 'Stock | '.Yii::$app->mycomponent->Get_settings('company_name');; 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/



$bgStatus=''; ?>
<?php //\yii\widgets\Pjax::begin(); ?>

<?php
//print_r($dataProvider);
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	//'options'=>[],
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	
	    'toolbar'=> [
		
		
		/*.Html::a('<i class="glyphicon glyphicon-search"></i>', ['stock'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'style'=>'margin-top: -34px;
    float: right;', 'title'=> 'Reset Grid']).'</div>',*/
        ['content'=>
		
                   Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 
	'heading'=>'<form class="form-inline" method="get">
  <div class="form-group">
   
    <input type="text" class="form-control" style="width:250px;"  value="'.$description.'" placeholder="Description" name="description" >
  </div>

  <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
  
</form>'],
	
	
	/*'STOCK   <form method="get"><input type="text" class="form-control" value="'.$description.'" placeholder="Description" name="description" style="width: 300px;" />
		<button type="submit" class="btn btn-default" style="margin: -55px 264px 0;"><i class="glyphicon glyphicon-search"></i></button></form>'],*/
    'columns' => [
	
	    [
		     'header' => 'Product ID',
            'attribute'=>'product_id',
			//'headerOptions' => ['style' => 'width:10px'],
			'width'=>'10%',
			
         ],
		 [
            'label'=>'Product Name',
			//'placeholder'=>'Select company',
			'attribute' => 'product_name',
			//'filterType'=>GridView::FILTER_SELECT2,
			'value' => 'product.product_name',
			 'options'=>['placeholder'=>'Select company'],
			'format'=>'raw'
           
         ],
		  [
            'label'=>'Product Model',
			'attribute' => 'product_model',
			'value' => 'product.product_model',
			
           
         ],
		 
		 [
            'label'=>'Product Manufacturer',
			'attribute' => 'manufacturer',
			'value' => 'product.manufacturer',
			
           
         ],
    
		 [
		    'label' => 'Remaining Stock',
            'attribute'=>'my_sum',
			'value' =>function($model)
				{
				  if(isset($model->my_sum))
				  { 
				  
					   return  Html::a('<span>'.$model->my_sum.'</span>', 
					   ['/store/stockreports','id'=>$model->product_id]);      
				  }
				},
			'format'=>'raw',
			
			
			'contentOptions' => ['class' => 'text-center'],
			'width'=>'10%',
         ],
		
	
		
    ],
]);
?>
<?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
?>
<?php //\yii\widgets\Pjax::end(); ?>
<?php
$Rolls=Yii::$app->mycomponent->GetRolls();

?>
 

<?php
$this->registerJs("$(function() {
   $('.popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});"); ?>
 

<div id="state_list"></div>

<div id="receipt"></div>
<input type="hidden" data-toggle="modal" data-target="#show_receipt" class="receipt_trigg"/>

<input type="hidden" data-toggle="modal" data-target="#assign_to" class="assign_to"/>
<input type="hidden" data-toggle="modal" data-target="#stc" class="stc"/>
	<script>
    function assign_complaint(complaint_id) 
    {
    
       // alert(complaint_id);
		$("#cid").val(complaint_id);
		$(".assign_to").trigger('click');
        
    }
    </script>


<script>
function status_change(complaint_id) {
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_change' ?>',
	data:'complaint_id='+complaint_id,
	success: function(data){
	//alert(data);
		$("#state_list").html(data);
		$("#stc").trigger('click');
	}
	});
}
</script>


<script>
function edit_complaint(order_id) {

//alert(order_id);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/register' ?>',
	data:'order_id='+order_id,
	success: function(data){
	alert(data);
		$("#receipt").html(data);
		$(".receipt_trigg").trigger('click');
	}
	});
}
</script>

<script>
function assign_status(val) {

var status=$('#st').val();
//alert(status);
	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/complaint/status_update' ?>',
	data:'status='+status+'&complaint_id='+val,
	success: function(data){
	//alert("#status_"+val);
	
		$('#modal').modal('hide');
		$.pjax.reload({container:'#w0'});
		
	}
	});
}


</script>


<div class="modal fade" id="assign_to" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div id="tableContainer-1">
    <div id="tableContainer-2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
		    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times"></i>
            </button>
          </div>          
          <div id="div-forms" class="dropmenu-div" style="padding:10px;">
            <form class="form form-horizontal" name="" method="post"  >            
                
                 <input type="hidden" id='cid' name="cid"  />
                            
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Assign to </label>
               <div class="col-sm-8"> 
			   <select name="role" id="role" class="form-control" id="week-list">
               <?php foreach($Rolls as $rolle) { ?>
                  <option value="<?php echo $rolle['item_name'] ?>"><?php echo ucfirst($rolle['item_name']) ?></option>
                  
                  <?php } ?>
               </select>
			   </div> 
               </div>
               
               <div class="form-group row">
               <label class="control-label col-sm-4"  value="Schema Name">Notes</label>
               <div class="col-sm-8"> 
			  <textarea name="notes" id="notes" style="width: 100%;min-height: 100px;"></textarea>
			   </div> 
               </div>
              
            <div class="form-group row">
              <div class="text-center">
               
           <input class="btn btn-success" type="button" onclick="st_assign()" name="smt"  value="Assign" />
           <input class="btn btn-danger" type="submit"  data-dismiss="modal" value="Cancel" aria-label="Close"/>
            
           </div>
           </div>
            </form>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
function st_assign()
 {

var cid=$('#cid').val();
var notes=$('#notes').val();
var role=$('#role').val();
//alert(status);
	$.ajax({
	type: "POST",
	url: '<?php echo Yii::$app->request->baseUrl. '/admin/roleassign' ?>',
	data:'cid='+cid+'&notes='+notes+'&role='+role,
	success: function(data){
	//alert(data);
	
		//$("#status_"+val).html(data);
		$(".close").trigger('click');
	}
	});
}


</script>
        



<!-- View receipt button -->



<!--<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#show_receipt">View Receipt</a>-->