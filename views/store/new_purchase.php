<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
//use app\models\Inventory;
 use yii\helpers\Url;
 use kartik\select2\Select2;
  use app\models\Vat;
 use yii\helpers\ArrayHelper;
$this->title = 'Purchase | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div>
             <?= Html::a('Create', ['/store/newsales'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['/store/index'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <h3>COMPLAINT ID : <?php echo $_GET['id']; ?></h3>
        <h3>Sales</h3>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
 
    <div class="form-group">
    <?= Html::activeLabel($model, 'Shop',['label'=>'Shop', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
       <?= $form->field($model, 'shop',['showLabels'=>false])->dropDownList($items1) ?>
        
    </div>
    </div>
    <input type="hidden" name="complaint_id" value="<?php echo $_GET['id']; ?>" />
     <input type="hidden" name="customer_id" value="<?php echo $_GET['customer_id']; ?>" />
     <input type="hidden" name="action" value="<?php echo $_GET['action']; ?>" />
    
     <?php
		
		
		
		if(isset($_GET['id']))
		{
			
			
                    $Sales = \app\models\Sales::find()->where(['=', 'complaint_id', $_GET['id']])->one();
		}
					
                
		  ?>
    
    
     <div class="form-group">
    <label  class='col-sm-2 control-label'>Discount</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="discount" id="discount"  class="form-control" value="<?php echo isset($Sales->discount)?$Sales->discount:''?>" />
    
    </div>
    </div>
    
       
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Vat (%)</label>
   
    <div class="col-sm-4">
    <?php $vat_details = ArrayHelper::map(Vat::find()->all(), 'vat', 'vat_name');  ?> 
    
     <?php
	// print_r($product_details);
	 
				echo Select2::widget([
			'name' => 'vat',
			'data' => $vat_details,
			'value'=>isset($Sales->vat)?$Sales->vat:'',
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Vat ...','id'=>'vat','onchange'=>'Vat_calc()'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
 
     <?php /*?><input type="text" name="vat" id="vat" class="form-control" onkeyup="Vat_calc()" value="<?php echo isset($model->vat)?$model->vat:''?>" required="required"/><?php */?>
    
    </div>
    </div>
    
    
    <div class="form-group">
    <label  class='col-sm-2 control-label'>Transport</label>
   
    <div class="col-sm-4">
 
     <input type="text" name="trans" id="trans" class="form-control" value="<?php echo isset($Sales->trans)?$Sales->trans:''?>" />
    
    </div>
    </div>
     
   
        <h3>Product Details</h3>
        <div style="float:right;margin: 0 35px  0 0;" > 
            <p><a class="btn btn-success"  id="add" ><span class="glyphicon glyphicon-plus-sign"></span> Add</a></p>
        </div>
  
  

        
        
        <?php
		
		
		
		if(isset($_GET['id']))
		{
			
			
			if ($_GET['id']!='')
				 {
                    $product_all = \app\models\Inventory::find()->where(['=', 'complaint_id', $_GET['id']])->all();
					
                    
				 } 
				 
				// print_r($product_all);
				// exit();
		
		
		 $i=0;
		  ?>
           <table class="table table-striped">
        <thead>
       <tr>
           <th width="30%">Product</th>
              <th width="30%">S.NO.</th>
               <th width="10%">Price</th>
               <th width="5%">Qty</th>
              <th width="10%">Sub Total</th>
            
              <th width="5%">VAT Amt</th>
           
              <th width="20%">Total</th>
          </tr>
        </thead>
        <tbody class="inputs">
     
       <?php foreach($product_all as $product): ?>
         <?php   $group_table = \app\models\Group_sales::find()->where(['=', 'group_id', $product['group_id_sales']])->one(); ?>
         <?php
		 
		 $sub_total=($group_table['qty']*$product['price']);
		//$vat_total=$sub_total-($sub_total* $group_table['vat']/100);
		$vat=isset($Sales->vat)?$Sales->vat:'';
		$vat_amt=($sub_total* $vat/100);
		  ?>
       
       
          <tr id="remove<?php echo $i; ?>" class="field">
            <td>
            
             <?php
				echo Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'value'=>$product['product_id'],
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...','class'=>'product_0','onchange'=>'get_serial_number(0)'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
            
           </td>
        <?php $serial_num=Yii::$app->mycomponent->actionCheck_product_available($product['product_id'])?>
            
          
           <td><select name="s_no[]" id="s_no_0" class="form-control s_no_0"  placeholder="S.NO." style="width:250px;" onchange='Get_serial_details(0)'>
           <option value="<?php echo $product['sno']; ?>" ><?php echo $product['sno']; ?></option>
		   <?php foreach($serial_num as $key=>$val)
		
		{ ?>
			<option value="<?php echo $val; ?>" ><?php echo $val; ?></option>
		<?php }?>
           </select></td>
            <td> <input type="text" name="price[]" class="form-control price_<?php echo $i;?>" value="<?php echo $product['price'] ?>" onkeyup="get_subtotal(<?php echo $i; ?>)" required="required"></td>
           
            <td> <input type="text" name="qty[]" data-id="<?php echo $i;?>" class="form-control qty_<?php echo $i;?> check_qty" value="1" onkeyup="get_subtotal(<?php echo $i; ?>)" readonly="readonly"></td>
            
            <td><input type="text" name="subtotal[]" class="form-control total_<?php echo $i;?>" onkeyup="get_price(<?php echo $i; ?>)" value="<?php echo $sub_total;?>"  required="required"></td>
            <td> <input type="text" name="vat_amt[]" style="width:50px;" class="form-control vat_amt_<?php echo $i;?>" value="<?php echo $vat_amt; ?>"  readonly="readonly" ></td>
            <td><input type="text" name="total[]" style="width:77px;" class="form-control all_total_<?php echo $i;?>"  value="<?php echo $group_table['total'];?>" readonly="readonly"></td>
            <td><a class="btn btn-danger" onclick="remove(<?php echo $i; ?>)" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
          </tr>
           
          
           

		
		
		<?php
		$i++;
		 endforeach; ?>
         </tbody>
           </table>
		<?php }
		
		
		$test= Select2::widget([
			'name' => 'product_id[]',
			'data' => $product_details,
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Products ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
			 ?>

    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
<script>


function Vat_calc()
  {
   
    $('.check_qty').each(function(){
     var id=$(this).data('id'); //
	 var qty=$(this).val();
	 var price=$('.price_'+id).val();
				if(qty!='' && price!='')
				{
					 get_price(id);
					 get_subtotal(id);
				}
				else
				{
					
					
				}
	
	
	
    
      });

  }


function get_serial_number(id)
{
	//console.log($('textarea[name*="s_no[]"]').val());
	
	//JSON.parse();
	var product_id=$('.product_'+id).val();
	      
		  
		  $('.s_no_'+id+' option').each(function() {
    
           $(this).remove();
    
           });
		  $('.price_'+id).val('');
		  $('.discont_'+id).val('');
		  $('.vat_'+id).val('');
		  $('.trans_'+id).val('');
		  $('.total_'+id).val(''); 
		  $('.all_total_'+id).val('');
		  $('.vat_amt_'+id).val('');

	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/check_product_available' ?>',
	data:'product_id='+product_id+'&id='+id,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	success: function(response){
		
		var status=response.status;
		if(parseInt(status)==1)
		{
			var result=response.sno;
			for(var item in result)
			{
				
			  $('<option value=""></option>'+'<option value="'+result[item]+'">'+result[item]+'</option>').appendTo('#s_no_'+id);
			}
		
		  
		}
		else
		{
			alert('Items unavilable');
		}
	
	}
	});

	
}


function Get_serial_details(id)
{
	//console.log($('textarea[name*="s_no[]"]').val());
	
	//JSON.parse();
	var sno=$('.s_no_'+id).val();
	   
		  $('.price_'+id).val('');
		  $('.total_'+id).val(''); 
		  $('.all_total_'+id).val('');
		  $('.vat_amt_'+id).val('');

	$.ajax({
	type: "GET",
	url: '<?php echo Yii::$app->request->baseUrl. '/inventory/get_serial_details' ?>',
	data:'sno='+sno+'&id='+id,
	 contentType: "application/json; charset=utf-8",
      dataType: "json",
	  success: function(response){
		
		var status=response.status;
		if(parseFloat(status)==1)
		{
			var tot=parseFloat(response.sell_price);
		  $('.all_total_'+id).val(tot.toFixed(2));
		  $('.price_'+id).val(tot.toFixed(2));
		    get_price(id);
		    get_subtotal(id);
		  
		  
		}
		else
		{
			alert('Items unavilable');
		}
	
	}
	});

	
}


function get_subtotal(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
	if (qty > 0 && price>0)
	 {
			if(qty!='' && price!='')
			{
				var total=parseFloat(qty)*parseFloat(price);
				
				$('.total_'+row).val(total.toFixed(2));
				$('.all_total_'+row).val(total.toFixed(2));
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt.toFixed(2));
					$('.all_total_'+row).val(total.toFixed(2));
				}
				
				
			}
	 }
	 else
	 {
		 $('.qty_'+row).val('');
		 $('.total_'+row).val('');
	 }
}

function get_price(row)
{ 
    $('.vat_amt_'+row).val('');
	var qty=$('.qty_'+row).val();
	var total=$('.total_'+row).val();
	var price=$('.price_'+row).val();
	var vat=$('#vat').val();
	
		if(qty!='' && total!='')
		{
			var price=(Math.abs(parseFloat(total))/Math.abs(parseFloat(qty)));
			var d_price=Math.round(price * 100) / 100;
			$('.price_'+row).val(d_price);
			$('.all_total_'+row).val(total);
			  if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=parseFloat(total)+parseFloat(vat_amt);
				    $('.vat_amt_'+row).val(vat_amt);
					$('.all_total_'+row).val(total.toFixed(2));
					
				}
				
		}
		else if(qty!='' && total=='' && price!='')
		{
			    var total=parseFloat(qty)*parseFloat(price);
				$('.all_total_'+row).val(total);
				
				
				
				if(vat!='')
				{
					
					var vat_amt=(total*(parseFloat(vat) / 100));
				    total=total+vat_amt;
				    $('.vat_amt_'+row).val(vat_amt);
					$('.all_total_'+row).val(total);
				}
				
		}
	 
}
 function remove(id)
{
	//alert(id);
	$('#remove'+id).remove();
}
 
 
 $(document).ready(function(){
$.fn.select2.defaults.set("theme", "krajee");
$("select").select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select  ...",
 
  allowClear: true
});
var i = $('.field').size() + 1;
var result = <?php echo json_encode($product_details)?>;

$('#add').click(function() {
	
	$('<tr id="remove'+i+'"><td><select id="product_options'+i+'" name="product_id[]" onchange="get_serial_number('+i+')" class="form-control product_'+i+'"></select></td>'+
	'<td><select name="s_no[]" id="s_no_'+i+'" class="form-control s_no_'+i+'"   style="width:250px;" onchange="Get_serial_details('+i+')"></select></td>'+
	'<td><input type="text" class="form-control price_'+i+'" name="price[]" required="required" onkeyup="get_subtotal('+i+')"/></td>'+
	'<td><input type="text" class="form-control qty_'+i+' check_qty" value="1" readonly="readonly" data-id="'+i+'" name="qty[]" required="required" onkeyup="get_subtotal('+i+')" /></td>'+
	'<td><input type="text" class="form-control total_'+i+'" name="subtotal[]" required="required" onkeyup="get_price('+i+')"  /></td>'+
	'<td><input type="text" class="form-control vat_amt_'+i+'" style="width:40px;" name="vat_amt[]" readonly="readonly"  /></td>'+
	'<td><input type="text" class="form-control all_total_'+i+'" style="width:77px;" name="total[]" readonly="readonly"  /></td>'+
	'<td><a  onclick="remove('+i+')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>').fadeIn('slow').appendTo('.inputs');

$("#product_options"+i).select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Products ...",
 
  allowClear: true
});

$("#s_no_"+i).select2({
	// templateSelection: template,
  tags: "true",
  class:'selection_arrow',
  placeholder: "Select Sno ...",
 
  allowClear: true
});
for(var item in result)
{
	
  $('<option value=""></option>'+'<option value="'+item+'">'+result[item]+'</option>').appendTo('#product_options'+i);
}
i++;
});

/*$('#remove').click(function() {
if(i > 1) {
$('.field:last').remove();
i--;
}
});*/


$('#reset').click(function() {
while(i > 2) {
$('.field:last').remove();
i--;
}
});

// here's our click function for when the forms submitted

$('.submit').click(function(){

var answers = [];
$.each($('.field'), function() {
answers.push($(this).val());
});

if(answers.length == 0) {
answers = "none";
}

alert(answers);

return false;

});

});
 </script>
 