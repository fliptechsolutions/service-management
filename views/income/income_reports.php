<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 use app\models\Income_type;
 use yii\data\SqlDataProvider;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Income | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_income'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['income'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Income Reports'],
    'columns' => [
       
		  [
		    'label'=>'Income Type',
		    'attribute'=>'income_name',
			'value'=>function($model)
				{
				  if(isset($model->income_id))
				  { 
				  
					   $Expense= Income_type::find()->where(['id'=>$model->income_id])->one();
						$search_date=date("Y-m-01").' - '.date("Y-m-t");
					   if(isset($_GET['Income_reports']['exp_date']) && $_GET['Income_reports']['exp_date']!='')
					   {
						  $search_date= $_GET['Income_reports']['exp_date'];
					   }
					  
					   return  Html::a('<span>'.$Expense->income_name.'</span>', 
					   ['/income/income','Income[income_name]'=>$Expense->income_name,'Income[exp_date]'=>$search_date]);      
				  }
				},
			'format'=>'raw'
          ],
		 
		  
		  [
		    'label'=>'Amount',
		    'attribute'=>'amount',
			'value'=>'amount',
			// 'width'=>'20%',
          ],
		 
		  
		  
		  [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Income Date',
				
				'attribute'=>'exp_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'exp_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
		
	
		
		
		
    ],
]);


?>
 
 

