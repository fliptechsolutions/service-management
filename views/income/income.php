<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 use yii\helpers\BaseHtml;
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use kartik\grid\GridView;
 use yii\data\ActiveDataProvider;
 use \app\models\Articles;
 use app\models\Income_type;
 use yii\data\SqlDataProvider;

$Rolls=Yii::$app->mycomponent->GetRolls();
$this->title = 'Income | '.Yii::$app->mycomponent->Get_settings('company_name'); 
/*$merchant_id= Yii::$app->user->identity->merchant_id;
$users=Yii::$app->mycomponent->All_users();
$Orders=Yii::$app->mycomponent->Orders($merchant_id);*/

$bgStatus='';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'showPageSummary'=>true,
    'pjax'=>true,
    'striped'=>true,
    'hover'=>true,
	'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
	    'toolbar'=> [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['add_income'],['data-pjax'=>0,  'title'=> 'Add', 'class'=>'btn btn-success', ]) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['income'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        //'{export}',
        '{toggleData}',
    ],
    'panel'=>['type'=>'primary', 'heading'=>'Income Ledger'],
    'columns' => [
       
		  [
		    'label'=>'Income Type',
		    'attribute'=>'income_name',
			'value'=>//'income_type.income_name',
			function($model)
			{
				$ex=Income_type::findOne(['id' =>$model->income_id ]);
				return isset($ex->income_name)?$ex->income_name:'';
			},
			// 'width'=>'30%',
          ],
		 
		  
		  [
		    'label'=>'Amount',
		    'attribute'=>'amount',
			'value'=>'amount',
			// 'width'=>'20%',
          ],
		 
		  [
		    'label'=>'Description',
            'attribute'=>'description',
			 //'width'=>'20%',
           
          ],
		  
		  [

				//'class'=>'kartik\grid\EditableColumn',
				'label'=>'Income Date',
				
				'attribute'=>'exp_date',
				
				//'pageSummary'=>true,
				
				'filterType' => GridView::FILTER_DATE_RANGE,
				
				'filterWidgetOptions' =>([
				
				//'model'=>$model,
				
				'attribute'=>'exp_date',
				
				'presetDropdown'=>TRUE,
				
				'convertFormat'=>true,
				
				'pluginOptions'=> [
				
				'locale'=>['format' => 'Y-m-d'],
				
				'opens'=>'left'
				
				]
				
				]),
				
			


	            'format' => 'raw'		,
			
		
      ],
		
	
		
		['class' => 'kartik\grid\ActionColumn',
              'header' => 'Actions',
			  'width'=>'30%',
              'template' => '{edit} {delete}',
              'buttons' => [
                
                 'edit' => function ($url, $model) {
                   return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                 },
                 'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,[
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure  want to delete?'),
                    'data-method' => 'post',
                ]);
                 },
              ],
              'urlCreator' => function ($action, $model, $key, $index) {

                if ($action === 'assign') {
                    $url ='' ;
                }
                if ($action === 'edit') {
                    $url = Url::to(['/income/update_income', 'id' =>$model->id]);
                }
                if ($action === 'delete') {
                    $url = Url::to(['/income/delete_income', 'id' =>$model->id]);
                }                    
                return $url;
            }
          ],
		
    ],
]);


?>
 
 

