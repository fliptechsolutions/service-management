<?php
use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\date\DatePicker;
$Rolls=Yii::$app->mycomponent->GetRolls();
use app\models\Income_type;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
$this->title = 'Income | MR Infotech'; 
?>
<div class="panel panel-default">
<div class="panel-body">
<div><?= Html::a('Create', ['/income/add_income'],['class'=>'btn btn-success']) ?>
             <?= Html::a('List', ['income/income'], ['class'=>'btn btn-success']) ?> 
            
        </div><br />
        <div> <h3>Income Type</h3></div>
        <br />  
<div class="user-form">
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-horizontal', 
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]); ?>
    
   
     <div class="form-group">
    <?= Html::activeLabel($model, 'type',['label'=>'Income Type', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
       <?php /*?><?= $form->field($model, 'type',['showLabels'=>false])->textInput() ?><?php */?>
       
       
        <?php
				echo Select2::widget([
			'name' => 'income_id',
			'value' => isset($model->income_id)?$model->income_id:'',
			'data' =>  ArrayHelper::map(Income_type::find()->all(), 'id', 'income_name'),
			'theme' => Select2::THEME_KRAJEE, // this is the default if theme is not set
			'options' => ['placeholder' => 'Select Income type ...'],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);
		
		?>
        
    </div>
    </div>
  <div class="form-group">
    <?= Html::activeLabel($model, 'date',['label'=>'Income Date', 'class'=>'col-sm-2 control-label']) ?>
    <div class="col-sm-4">
	<?php   
                echo DatePicker::widget([
                'name' => 'exp_date', 
                'value' => isset($model->exp_date)?$model->exp_date:date('Y-m-d'),
                'options' => ['placeholder' => 'Select date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]);
            ?>
        
    </div>
    </div>
    
     <div class="form-group">
    <?= Html::activeLabel($model, 'Amount', ['label'=>'Amount', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'amount',['showLabels'=>false])->textInput(['placeholder'=>'Amount']); ?>
    </div>
    </div>
    
    
    <div class="form-group">
    <?= Html::activeLabel($model, 'Description', ['label'=>'Notes', 'class'=>'col-sm-2 control-label']) ?>
   <div class="col-sm-4">
        <?= $form->field($model, 'description',['showLabels'=>false])->textArea(['placeholder'=>'Notes']); ?>
    </div>
    </div>
    
    
    
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
