-- phpMyAdmin SQL Dump
-- version 4.0.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2020 at 07:56 PM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mrinfotech_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Admin', 4, 1477047723),
('BGA', 42, NULL),
('BGA', 43, NULL),
('BGA', 46, NULL),
('Card', 33, NULL),
('Chip', 45, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  KEY `fk_auth_item_group_code` (`group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`, `group_code`) VALUES
('/*', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/admin/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/about', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/adminattendance', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/attendance-reports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/bga', 3, NULL, NULL, NULL, 1477121493, 1477121493, NULL),
('/admin/bgaadd', 3, NULL, NULL, NULL, 1477121493, 1477121493, NULL),
('/admin/captcha', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/card', 3, NULL, NULL, NULL, 1477121493, 1477121493, NULL),
('/admin/cardadd', 3, NULL, NULL, NULL, 1477121492, 1477121492, NULL),
('/admin/checking', 3, NULL, NULL, NULL, 1477049114, 1477049114, NULL),
('/admin/checkingadd', 3, NULL, NULL, NULL, 1477116123, 1477116123, NULL),
('/admin/chip', 3, NULL, NULL, NULL, 1477118977, 1477118977, NULL),
('/admin/chipadd', 3, NULL, NULL, NULL, 1477118977, 1477118977, NULL),
('/admin/contact', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/delete-user', 3, NULL, NULL, NULL, 1477059938, 1477059938, NULL),
('/admin/deleteuser', 3, NULL, NULL, NULL, 1477060023, 1477060023, NULL),
('/admin/deleteusers', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/admin/error', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/front', 3, NULL, NULL, NULL, 1477049772, 1477049772, NULL),
('/admin/front-desc', 3, NULL, NULL, NULL, 1477048267, 1477048267, NULL),
('/admin/frontadd', 3, NULL, NULL, NULL, 1477055871, 1477055871, NULL),
('/admin/index', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/login', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/logout', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/admin/newuser', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/admin/register', 3, NULL, NULL, NULL, 1477039264, 1477039264, NULL),
('/admin/reportsattanance', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/role', 3, NULL, NULL, NULL, 1477130834, 1477130834, NULL),
('/admin/roleassign', 3, NULL, NULL, NULL, 1479384656, 1479384656, NULL),
('/admin/roleassign2', 3, NULL, NULL, NULL, 1509434006, 1509434006, NULL),
('/admin/saveattendance', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/updateuser', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/admin/upload-settings', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/userattendance', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/admin/usercreate', 3, NULL, NULL, NULL, 1477054761, 1477054761, NULL),
('/admin/users', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/*', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/bill', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/complaint/captcha', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/complaint', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/create', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/customers', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/delete', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/deletecustomer', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/deletevendor', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/error', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/estimate', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/complaint/get-team-member', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/complaint/index', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/jobassign', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/mob-validation', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/newcomplaint', 3, NULL, NULL, NULL, 1479552634, 1479552634, NULL),
('/complaint/newcustomer', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/newvendor', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/register', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/complaint/report', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/complaint/spares', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/complaint/status-change', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/status-update', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/updatecustomer', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/updatevendor', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/vendor', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/complaint/view-complaint', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/complaint/viewcomplaint', 3, NULL, NULL, NULL, 1509432358, 1509432358, NULL),
('/expense/*', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/add-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/captcha', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/delete-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/error', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/expensereports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/expense/update-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/gridview/*', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/gridview/export/*', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/gridview/export/download', 3, NULL, NULL, NULL, 1477559983, 1477559983, NULL),
('/income/*', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/add-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/captcha', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/delete-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/error', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/incomereports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/income/update-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/*', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/inventory/accessories', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/add-manufact', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/add-receipt', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/add-stock', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/add-voucher', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/addproduct', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/addselling', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/allvendors', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/cancel-sales', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/captcha', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/cataccessories', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/check-product-available', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/deleteaccess', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/inventory/deletecataccess', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/deletemanufact', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/deleteparchase', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/inventory/deleteproducts', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/inventory/deleteprotype', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/deletesales', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/deleteselling', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/deleteshop', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/deletestock', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/error', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/get-pay-type', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/get-paylist', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/get-purchaselist', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/get-serial-details', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/manufacture', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/newaccesories', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/newcataccesories', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/newproduct', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/newpurchase', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/newsales', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/newshop', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/opening-stock', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/product-type', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/products', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/purchase', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/purchasereports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/receipt', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/receiptreports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/return-sales', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/sales', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/salesreports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/selling', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/shop', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/sold-check', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/update-manufacids', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/update-receipt', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/update-stock', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/update-typeids', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/update-voucher', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/updateaccessories', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/updatecateaccessories', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/updatemanufact', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/updateparchase', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/updateproducts', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/updateprotype', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/updatepurchase', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/updatesales', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/updateselling', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/inventory/updateshop', 3, NULL, NULL, NULL, 1482818896, 1482818896, NULL),
('/inventory/voucher', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/inventory/voucherreports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/*', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/accounts', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/add-accounts', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/add-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/add-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/add-vat', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/captcha', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/delete-accounts', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/delete-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/delete-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/delete-vat', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/error', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/settings', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/update-accounts', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/update-expense', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/update-income', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/update-vat', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/settings/vat', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/site/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/about', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/captcha', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/contact', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/error', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/index', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/login', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/site/logout', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/store/*', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/store/captcha', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/store/error', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/store/index', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/store/newsales', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/store/stock', 3, NULL, NULL, NULL, 1488370966, 1488370966, NULL),
('/store/stockreports', 3, NULL, NULL, NULL, 1509431500, 1509431500, NULL),
('/store/updatesales', 3, NULL, NULL, NULL, 1482818895, 1482818895, NULL),
('/user-management/*', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/auth-item-group/*', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/bulk-activate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/bulk-deactivate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/bulk-delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/create', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/grid-page-size', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/grid-sort', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/index', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/toggle-attribute', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/update', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth-item-group/view', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth/*', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/auth/captcha', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/change-own-password', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/auth/confirm-email', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/confirm-email-receive', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/confirm-registration-email', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/login', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/logout', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/password-recovery', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/password-recovery-receive', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/auth/registration', 3, NULL, NULL, NULL, 1477039055, 1477039055, NULL),
('/user-management/permission/*', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/bulk-activate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/bulk-deactivate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/bulk-delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/create', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/grid-page-size', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/grid-sort', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/index', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/refresh-routes', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/set-child-permissions', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/set-child-routes', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/toggle-attribute', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/update', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/permission/view', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/role/bulk-activate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/bulk-deactivate', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/bulk-delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/create', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/delete', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/grid-page-size', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/role/grid-sort', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/role/index', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/set-child-permissions', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/set-child-roles', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/toggle-attribute', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/update', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/role/view', 3, NULL, NULL, NULL, 1477039054, 1477039054, NULL),
('/user-management/user-permission/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-permission/set', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user-permission/set-roles', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user-visit-log/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/bulk-activate', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/bulk-deactivate', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/bulk-delete', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/create', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/delete', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/grid-page-size', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/grid-sort', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/index', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/toggle-attribute', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/update', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user-visit-log/view', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user/*', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user/bulk-activate', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/bulk-deactivate', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/bulk-delete', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/change-password', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/create', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/delete', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/grid-page-size', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/grid-sort', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user/index', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/toggle-attribute', 3, NULL, NULL, NULL, 1477039053, 1477039053, NULL),
('/user-management/user/update', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('/user-management/user/view', 3, NULL, NULL, NULL, 1477030356, 1477030356, NULL),
('Admin', 1, 'Admin', NULL, NULL, 1477030356, 1477030356, NULL),
('assignRolesToUsers', 2, 'Assign roles to users', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('BGA', 1, 'BGA dept', NULL, NULL, 1477119197, 1477119197, NULL),
('BGA team permission', 2, 'BGA', NULL, NULL, 1479885364, 1479885364, NULL),
('bindUserToIp', 2, 'Bind user to IP', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('Card', 1, 'card level team', NULL, NULL, 1477120408, 1477120408, NULL),
('card team access', 2, 'card', NULL, NULL, 1479885463, 1479885463, NULL),
('changeOwnPassword', 2, 'Change own password', NULL, NULL, 1477030356, 1477030356, 'userCommonPermissions'),
('changeUserPassword', 2, 'Change user password', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('checking', 1, 'Checking team', NULL, NULL, 1477115426, 1477115426, NULL),
('Checking users managemet', 2, 'Checking', NULL, NULL, 1477049321, 1477049321, NULL),
('Chip', 1, 'Chiplevel team', NULL, NULL, 1477117539, 1477117539, NULL),
('chiplevel', 2, 'chip', NULL, NULL, 1479885106, 1479885106, NULL),
('comlaint details', 2, 'complaint', NULL, NULL, 1477560097, 1477560097, NULL),
('commonPermission', 2, 'Common permission', NULL, NULL, 1477030352, 1477030352, NULL),
('createUsers', 2, 'Create users', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('deleteUsers', 2, 'Delete users', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('editUserEmail', 2, 'Edit user email', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('editUsers', 2, 'Edit users', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('Front', 1, 'Front office', NULL, NULL, 1477054246, 1477054246, NULL),
('front team access', 2, 'front office', NULL, NULL, 1479885553, 1479885553, NULL),
('Front users managemet', 2, 'Admin', NULL, NULL, 1477039018, 1477056027, 'userManagement'),
('store', 1, 'Store', NULL, NULL, 1477035037, 1477035037, NULL),
('Stores', 2, 'Store module', NULL, NULL, 1482818861, 1482818861, NULL),
('viewRegistrationIp', 2, 'View registration IP', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('viewUserEmail', 2, 'View user email', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('viewUserRoles', 2, 'View user roles', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('viewUsers', 2, 'View users', NULL, NULL, 1477030356, 1477030356, 'userManagement'),
('viewVisitLog', 2, 'View visit log', NULL, NULL, 1477030356, 1477030356, 'userManagement');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Front users managemet', '/admin/bga'),
('Front users managemet', '/admin/bgaadd'),
('Front users managemet', '/admin/card'),
('Front users managemet', '/admin/cardadd'),
('Checking users managemet', '/admin/checking'),
('Front users managemet', '/admin/checking'),
('Front users managemet', '/admin/checkingadd'),
('Front users managemet', '/admin/chip'),
('Front users managemet', '/admin/chipadd'),
('Front users managemet', '/admin/deleteuser'),
('Front users managemet', '/admin/front'),
('Front users managemet', '/admin/frontadd'),
('changeOwnPassword', '/admin/index'),
('Front users managemet', '/admin/index'),
('card team access', '/admin/register'),
('Front users managemet', '/admin/register'),
('BGA team permission', '/admin/roleassign'),
('card team access', '/admin/roleassign'),
('Checking users managemet', '/admin/roleassign'),
('chiplevel', '/admin/roleassign'),
('comlaint details', '/admin/roleassign'),
('front team access', '/admin/roleassign'),
('Stores', '/admin/roleassign'),
('BGA team permission', '/admin/roleassign2'),
('card team access', '/admin/roleassign2'),
('Checking users managemet', '/admin/roleassign2'),
('chiplevel', '/admin/roleassign2'),
('comlaint details', '/admin/roleassign2'),
('front team access', '/admin/roleassign2'),
('Stores', '/admin/roleassign2'),
('Front users managemet', '/admin/usercreate'),
('front team access', '/complaint/bill'),
('comlaint details', '/complaint/captcha'),
('front team access', '/complaint/captcha'),
('front team access', '/complaint/complaint'),
('comlaint details', '/complaint/create'),
('front team access', '/complaint/create'),
('front team access', '/complaint/customers'),
('BGA team permission', '/complaint/delete'),
('front team access', '/complaint/delete'),
('front team access', '/complaint/deletecustomer'),
('Stores', '/complaint/deletevendor'),
('comlaint details', '/complaint/error'),
('front team access', '/complaint/error'),
('front team access', '/complaint/index'),
('BGA team permission', '/complaint/jobassign'),
('card team access', '/complaint/jobassign'),
('Checking users managemet', '/complaint/jobassign'),
('chiplevel', '/complaint/jobassign'),
('comlaint details', '/complaint/jobassign'),
('front team access', '/complaint/jobassign'),
('front team access', '/complaint/mob-validation'),
('BGA team permission', '/complaint/newcomplaint'),
('card team access', '/complaint/newcomplaint'),
('Checking users managemet', '/complaint/newcomplaint'),
('chiplevel', '/complaint/newcomplaint'),
('comlaint details', '/complaint/newcomplaint'),
('front team access', '/complaint/newcomplaint'),
('front team access', '/complaint/newcustomer'),
('Stores', '/complaint/newvendor'),
('front team access', '/complaint/register'),
('front team access', '/complaint/report'),
('front team access', '/complaint/status-change'),
('front team access', '/complaint/status-update'),
('front team access', '/complaint/updatecustomer'),
('Stores', '/complaint/updatevendor'),
('Stores', '/complaint/vendor'),
('BGA team permission', '/complaint/view-complaint'),
('card team access', '/complaint/view-complaint'),
('Checking users managemet', '/complaint/view-complaint'),
('chiplevel', '/complaint/view-complaint'),
('comlaint details', '/complaint/view-complaint'),
('front team access', '/complaint/view-complaint'),
('Stores', '/complaint/view-complaint'),
('BGA team permission', '/complaint/viewcomplaint'),
('card team access', '/complaint/viewcomplaint'),
('Checking users managemet', '/complaint/viewcomplaint'),
('chiplevel', '/complaint/viewcomplaint'),
('comlaint details', '/complaint/viewcomplaint'),
('front team access', '/complaint/viewcomplaint'),
('Stores', '/complaint/viewcomplaint'),
('Stores', '/inventory/allvendors'),
('Stores', '/inventory/deleteparchase'),
('Stores', '/inventory/deleteproducts'),
('Stores', '/inventory/newproduct'),
('Stores', '/inventory/newpurchase'),
('Stores', '/inventory/products'),
('Stores', '/inventory/purchase'),
('Stores', '/inventory/updateparchase'),
('Stores', '/inventory/updateproducts'),
('Stores', '/inventory/updatepurchase'),
('Stores', '/store/captcha'),
('Stores', '/store/error'),
('Stores', '/store/index'),
('Stores', '/store/newsales'),
('Stores', '/store/stock'),
('Stores', '/store/updatesales'),
('changeOwnPassword', '/user-management/auth/change-own-password'),
('assignRolesToUsers', '/user-management/user-permission/set'),
('assignRolesToUsers', '/user-management/user-permission/set-roles'),
('editUsers', '/user-management/user/bulk-activate'),
('editUsers', '/user-management/user/bulk-deactivate'),
('deleteUsers', '/user-management/user/bulk-delete'),
('changeOwnPassword', '/user-management/user/change-password'),
('changeUserPassword', '/user-management/user/change-password'),
('createUsers', '/user-management/user/create'),
('deleteUsers', '/user-management/user/delete'),
('viewUsers', '/user-management/user/grid-page-size'),
('viewUsers', '/user-management/user/index'),
('editUsers', '/user-management/user/update'),
('viewUsers', '/user-management/user/view'),
('BGA', 'BGA team permission'),
('Card', 'card team access'),
('Admin', 'changeOwnPassword'),
('BGA', 'changeOwnPassword'),
('Card', 'changeOwnPassword'),
('checking', 'changeOwnPassword'),
('Chip', 'changeOwnPassword'),
('Front', 'changeOwnPassword'),
('store', 'changeOwnPassword'),
('Stores', 'changeOwnPassword'),
('checking', 'changeUserPassword'),
('Front', 'changeUserPassword'),
('Admin', 'Checking users managemet'),
('Front users managemet', 'Checking users managemet'),
('Chip', 'chiplevel'),
('checking', 'comlaint details'),
('Front', 'front team access'),
('Admin', 'Front users managemet'),
('store', 'Front users managemet'),
('Admin', 'store'),
('store', 'Stores'),
('editUserEmail', 'viewUserEmail'),
('assignRolesToUsers', 'viewUserRoles'),
('assignRolesToUsers', 'viewUsers'),
('changeUserPassword', 'viewUsers'),
('createUsers', 'viewUsers'),
('deleteUsers', 'viewUsers'),
('editUsers', 'viewUsers');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_group`
--

CREATE TABLE IF NOT EXISTS `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_group`
--

INSERT INTO `auth_item_group` (`code`, `name`, `created_at`, `updated_at`) VALUES
('userCommonPermissions', 'User common permission', 1477030356, 1477030356),
('userManagement', 'User management', 1477030356, 1477030356);

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1477030330),
('m140608_173539_create_user_table', 1477030348),
('m140611_133903_init_rbac', 1477030350),
('m140808_073114_create_auth_item_group_table', 1477030351),
('m140809_072112_insert_superadmin_to_user', 1477030352),
('m140809_073114_insert_common_permisison_to_auth_item', 1477030352),
('m141023_141535_create_user_visit_log', 1477030353),
('m141116_115804_add_bind_to_ip_and_registration_ip_to_user', 1477030354),
('m141121_194858_split_browser_and_os_column', 1477030355),
('m141201_220516_add_email_and_email_confirmed_to_user', 1477030356),
('m141207_001649_create_basic_user_permissions', 1477030356);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_accessories`
--

CREATE TABLE IF NOT EXISTS `tbl_accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `category` text,
  `warning_menu` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_accessories`
--

INSERT INTO `tbl_accessories` (`id`, `acc_name`, `description`, `category`, `warning_menu`) VALUES
(9, 'Hard Disk', 'laptop hard drive', '{"1":"1","2":"0","3":"0"}', 0),
(10, 'adapter', '', '{"1":"1","2":"0","3":"0"}', 0),
(11, 'power cable', '', '{"1":"1","2":"0","3":"0"}', 0),
(12, 'battery', '', '{"1":"1","2":"0","3":"0"}', 0),
(13, 'screen', '', '{"1":"1","2":"0","3":"0"}', 0),
(14, 'dvd drive', '', '{"1":"1","2":"0","3":"0"}', 0),
(15, 'keyboard', '', '{"1":"1","2":"0","3":"0"}', 0),
(16, 'carry case', '', '{"1":"1","2":"0","3":"0"}', 0),
(17, 'ram', '', '{"1":"1","2":"0","3":"0"}', 0),
(18, 'wifi card', '', '{"1":"1","2":"0","3":"0"}', 0),
(19, 'cpu fan', '', '{"1":"0","2":"1","3":"0"}', 0),
(20, 'processor', '', '{"1":"0","2":"1","3":"0"}', 0),
(21, 'ram', 'testing', '{"1":"0","2":"1","3":"0","4":"0"}', 1),
(22, 'harddisk', '', '{"1":"0","2":"1","3":"0"}', 0),
(23, 'smps', '', '{"1":"0","2":"1","3":"0"}', 0),
(24, 'full cpu', '', '{"1":"0","2":"1","3":"0"}', 0),
(26, 'adapter', '', '{"1":"0","2":"0","3":"1"}', 0),
(27, 'pouch', '', '{"1":"0","2":"0","3":"1"}', 1),
(28, 'datacable', '', '{"1":"0","2":"0","3":"1"}', 0),
(29, 'cable wire', '', '{"1":"0","2":"0","3":"0","4":"1"}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_accounts`
--

CREATE TABLE IF NOT EXISTS `tbl_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `account_name` varchar(60) DEFAULT NULL,
  `bank_name` varchar(60) DEFAULT NULL,
  `account_number` varchar(60) DEFAULT NULL,
  `branch` varchar(60) DEFAULT NULL,
  `ifsc` varchar(60) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_accounts`
--

INSERT INTO `tbl_accounts` (`id`, `payment_type`, `account_name`, `bank_name`, `account_number`, `branch`, `ifsc`, `days`, `created_date`) VALUES
(1, 'bank', 'bank2', 'TMB BANK', '02315648954565446', 'trichy', 'TMBL12345689', NULL, '2017-04-06 12:28:44'),
(3, 'bank', 'Bank1', 'IOB', '12346889945654489', 'Trichy', 'IOB12345897', NULL, '2017-04-06 12:29:57'),
(4, 'cash', 'Cash', '', '', '', '', NULL, '2017-04-06 13:19:52'),
(5, 'cheque', 'cheque1', '', '', '', '', NULL, '2017-04-19 16:06:22'),
(6, 'credit', 'Seven days', '', '', '', '', 7, '2017-04-22 12:50:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE IF NOT EXISTS `tbl_attendance` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `attendance_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) NOT NULL,
  `attendance` int(2) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`id`, `attendance_date`, `user_id`, `attendance`, `created_date`) VALUES
(3, '2017-04-20 08:51:12', 43, 1, '2017-04-20'),
(4, '2017-04-20 08:51:12', 42, 1, '2017-04-20'),
(5, '2017-04-20 08:58:31', 41, 1, '2017-04-20'),
(6, '2017-04-20 11:47:55', 1, 1, '2017-04-20'),
(7, '2017-04-21 10:24:01', 1, 1, '2017-04-21'),
(8, '2017-04-21 11:48:07', 41, 1, '2017-04-21'),
(9, '2017-10-27 01:51:37', 1, 1, '2017-10-27'),
(10, '2017-10-28 05:13:39', 45, 1, '2017-10-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cancelled`
--

CREATE TABLE IF NOT EXISTS `tbl_cancelled` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `price` varchar(30) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `sno` text,
  `shop` varchar(30) DEFAULT NULL,
  `type` varchar(40) DEFAULT NULL,
  `complaint_id` varchar(10) DEFAULT NULL,
  `sold_price` varchar(10) DEFAULT NULL,
  `customer_id` varchar(10) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `group_id_sales` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `selling_price` int(11) DEFAULT NULL,
  `group_stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `tbl_cancelled`
--

INSERT INTO `tbl_cancelled` (`id`, `purchase_id`, `product_id`, `sales_id`, `stock_id`, `qty`, `price`, `total`, `sno`, `shop`, `type`, `complaint_id`, `sold_price`, `customer_id`, `group_id`, `group_id_sales`, `sub_total`, `selling_price`, `group_stock_id`) VALUES
(133, NULL, 63, 39, NULL, -1, '1500.00', 1500, '12345', NULL, 'sales', NULL, NULL, '13', NULL, 58, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cat_accessories`
--

CREATE TABLE IF NOT EXISTS `tbl_cat_accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_cat_accessories`
--

INSERT INTO `tbl_cat_accessories` (`id`, `cat_name`, `description`) VALUES
(1, 'Laptop', 'All laptops models'),
(2, 'Desktop', 'All desktop categories'),
(3, 'Tablet', 'All tabs'),
(4, 'tv', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complaint`
--

CREATE TABLE IF NOT EXISTS `tbl_complaint` (
  `complaint_id` int(14) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `date_created` date NOT NULL,
  `laptop_details` text,
  `prsence` text,
  `acc_sno` text,
  `acc_remarks` text,
  `problem` text,
  `phycical` text,
  `warranty` text,
  `priority` int(11) DEFAULT '0',
  `access_cat` text,
  `physical_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `tbl_complaint`
--

INSERT INTO `tbl_complaint` (`complaint_id`, `customer_id`, `status`, `date_created`, `laptop_details`, `prsence`, `acc_sno`, `acc_remarks`, `problem`, `phycical`, `warranty`, `priority`, `access_cat`, `physical_type`) VALUES
(62, 8, 'pending', '2017-04-13', '{"lap_sno":"hhhhdsfsfds","lap_user":"admin","lap_model":"123456","lap_pass":"1234"}', '{"19":"1","20":"1"}', '["","","","","","","","","","","sadadsdsad120","sadadsdsad120ghjghjgh","","","","","","","",""]', '["","","","","","","","","","","ddfdfd","ddfdfd","","","","","","","",""]', '["keys not working"]', '["keys not working"]', '{"TRC":"1","Warranty":"0","AMC":"0"}', 2, '', 'GOOD'),
(63, 9, 'pending', '2017-04-13', '{"lap_sno":"lxe540y01082123a322000","lap_user":"vinth","lap_model":"acer extensa 5630","lap_pass":"1234"}', '{"9":"1","10":"1","12":"1","13":"1","14":"1","15":"1","17":"1","18":"1"}', '["qghs98bj","","","bt0060402691101025b101","","","good","","","","","","","","","","","","",""]', '["HITACHI 500GB","acer og","","compartable ","good","good","","","spidia 1gb","internal","","","","","","","","","",""]', '["automatic resstart in 5min ","os install","anti virus install","fan replace","battery problem","top panel broken"]', '["vnvfghfghfg","gfhfghfghfghfh"]', '{"TRC":"1","Warranty":"0","AMC":"0"}', 3, '1', 'BAD'),
(71, 10, 'pending', '2017-03-06', '{"lap_sno":"7ad67h","lap_user":"hai","lap_model":"dell 5010","lap_pass":"55556"}', '{"9":"1","10":"1","11":"1","12":"1","13":"1","14":"1","15":"1","16":"0","17":"0","18":"1","19":"0","20":"0","21":"0","22":"0","23":"0","24":"0","26":"0","27":"0","28":"0"}', '["09099808876","nwxlnlkwmn","lkfml","nkjhjhowihdoh","","","","nil","","inside","","","","","","","","",""]', '["500 gb","90 watt","2meter","","broken on corner","","1 key missing","","","","","","","","","","","",""]', '["nodisplay"]', '["good"]', '{"TRC":"1","Warranty":"0","STEP":"0","AMC":"0","ITW":"0","Trade":"0"}', NULL, NULL, NULL),
(72, 11, 'Completed', '2017-03-08', '{"lap_sno":"132132163223132","lap_user":"rrr","lap_model":"lenovo l410 ","lap_pass":"123"}', '{"9":"1","10":"1","11":"1","12":"1","13":"1","14":"1","15":"1","16":"1","17":"1","18":"1","19":"0","20":"0","21":"0","22":"0","23":"0","24":"0","26":"0","27":"0","28":"0"}', '["dfgbdhdf","dfgdf","gdfg","df","dfgd","dfg","dfg","dfg","gdf","gdf","","","","","","","","",""]', '["dfgdfg","gdf","dfgdfg","gdfg","fgdfg","dfg","dfg","dfg","gdf","g","","","","","","","","",""]', '["dull display"]', '["GOOD"]', '{"TRC":"1","Warranty":"0","STEP":"0","AMC":"0","ITW":"0","Trade":"0"}', NULL, NULL, NULL),
(73, 8, 'pending', '2017-03-29', '{"lap_sno":"","lap_user":"","lap_model":"laptop","lap_pass":""}', NULL, '["fghjffgfgj","fgjfgjgj","","","","","","","","","","","","","","","","","",""]', '["fgjfg","fgjfg","","","","","","","","","","","","","","","","","",""]', '["keys not working"]', '["good working stage"]', '{"TRC":"1","Warranty":"0","AMC":"1"}', 1, '1', NULL),
(74, 11, 'pending', '2017-03-29', '{"lap_sno":"","lap_user":"","lap_model":"fdgdfg","lap_pass":""}', '{"19":"1","20":"1"}', '["","","","","","","","","","","dfghdf","dfhdf","","","","","","","",""]', '["","","","","","","","","","","fdh","fdh","","","","","","","",""]', '[""]', '[""]', '{"TRC":"1","Warranty":"0","AMC":"1"}', 2, '2', NULL),
(75, 9, 'pending', '2017-10-27', '{"lap_sno":"sdkfjhbkjnbl","lap_user":"","lap_model":"hp 550","lap_pass":""}', '{"9":"1","10":"1","11":"1","12":"1","13":"1","14":"1","15":"1","16":"1","17":"1","18":"1"}', '["fdrsdfsdaf","sdfsdf","sdfafdsafadsf","sdfsdf","sdfs","dsfsdfsadf","fdgdfgdfh","fghjfgdh","gfdh","gh,kjflogvh","","","","","","","","","",""]', '["sdf","","sdfsadf","sdf","sdfsdfsdfsda","sdfasdfsadf","sdfsadgfdfshtfjyhzsdefv","fghfgh","fnghmurfytlt79 ","xzcfarweyrtukl,","","","","","","","","","",""]', '["no poewr on"]', '["hinges broken ","panel broken"]', '{"TRC":"1","Warranty":"0","AMC":"0"}', 2, '1', 'BAD');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `subject` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `Address` text,
  `contact_phone` varchar(20) DEFAULT NULL,
  `company_name` varchar(30) DEFAULT NULL,
  `tin_no` varchar(30) DEFAULT NULL,
  `office_no` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `selling_id` int(10) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `cst_number` varchar(20) DEFAULT NULL,
  `opening_balance` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `first_name`, `last_name`, `email_address`, `city`, `Address`, `contact_phone`, `company_name`, `tin_no`, `office_no`, `date_created`, `selling_id`, `state`, `cst_number`, `opening_balance`, `paid`, `pincode`, `notes`) VALUES
(8, 'Kumaravel', '', 'kumaran2489@gmail.com', 'Trichy', '02 2nd street samy nagar', '9994996019', 'fliptech solution', '1234', '99965466', '2017-04-13 11:35:19', NULL, 'Tamil nadu', '2534', 0, NULL, '621305', NULL),
(9, 'vinothan', '', 'winway@gmail.com', 'trichy', 'chatram', '1212121212', 'winway computer', '1111111111', '9999999999', '2017-10-27 13:47:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'venothan', 'khc', 'winway@gmail.com', 'trichy', 'no  5 chatram', '989898765', 'winway', '1234567890', '986598658', '2017-03-06 06:51:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'krishna', 'kk', 'krish@gmail.com', 'trichy', 'ikdjbnko', '5555555555', 'krishnainfotech', '111111212121', '9865540404', '2017-03-29 14:13:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'riyas', 'ahmed', 'riyasinfotech', 'tambaram', 'no:20-a,puthur high road ', '9879879879', 'riyas infotech', '', '9898989898', '2017-03-20 11:27:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'guru', 'sdfg', '45@gmail.com', 'trichy', 'xdsn', '7777777777', 'gkgk', 'sdfg', '454545', '2017-03-30 11:32:29', 4, 'tamilnadu', '12356', 500, NULL, '22522', NULL),
(14, 'Kumaravel', '', 'kk2489@gmail.com', 'Trichy', 'Trichy', '9994996017', 'MR IMPEX', '1234', '', '2017-05-18 06:46:56', NULL, '', '12356', 500, 300, '', 'testing'),
(15, 'testuser', '', 'test@gmail.com', 'Trichy', 'Trichy', '124565965665', 'test', '6598456', NULL, '0000-00-00 00:00:00', NULL, NULL, '56899', NULL, NULL, NULL, NULL),
(16, 'Kumaravel', '', 'test256@gmail.com', 'Trichy', 'Trichy', '8778522458', 'ASSISTICUM', '1234', NULL, '0000-00-00 00:00:00', NULL, NULL, '123456', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_expense`
--

CREATE TABLE IF NOT EXISTS `tbl_expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_id` int(11) DEFAULT NULL,
  `amount` varchar(30) DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_expense`
--

INSERT INTO `tbl_expense` (`id`, `expense_id`, `amount`, `exp_date`, `description`) VALUES
(1, 2, '100', '2017-03-28', 'testing'),
(3, 1, '200', '2017-03-31', 'gdfhdfhd'),
(4, 2, '500', '2017-03-28', 'SMS Charges. Entered this to see how this column in the display fits in\r\n'),
(5, 2, '200', '2017-04-05', 'ICICI'),
(6, 6, '100', '2017-03-28', 'Riyas'),
(7, 6, '100', '2017-03-28', 'Raja');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_expense_type`
--

CREATE TABLE IF NOT EXISTS `tbl_expense_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expense` varchar(40) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_expense_type`
--

INSERT INTO `tbl_expense_type` (`id`, `expense`, `description`) VALUES
(1, 'Advertisement', 'asdsdsadsadsadsa'),
(2, 'Bank Charges', 'Bank Chargesdfsfdsfds dsfrewrewew'),
(3, 'Cleaning & Maintenance', 'fdsfdsfdsfsfsfs'),
(4, 'Conveyance & Travel', 'Test notes'),
(6, 'Petrol', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_front_office`
--

CREATE TABLE IF NOT EXISTS `tbl_front_office` (
  `front_id` int(14) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `Address` text,
  `city` text,
  `contact_phone` varchar(20) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  `photo` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`front_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tbl_front_office`
--

INSERT INTO `tbl_front_office` (`front_id`, `first_name`, `last_name`, `email_address`, `username`, `password`, `Address`, `city`, `contact_phone`, `status`, `photo`) VALUES
(34, 'Ganesh kumar', 'karthick', 'ganesh@gmail.com', 'ganesh@gmail.com', '$2y$13$mU8bcr09eYNj3Z6tDm3zg..j/Sw35KKWYRc5dyNS6GcQQtAIIMNpe', 'sweden', 'sweden', '9994996019', 'active', NULL),
(35, 'kumaravel', 'kumaran', 'kk@gmail.com', 'kk@gmail.com', '$2y$13$EcEZtCcCaGsdTi8tjpat/./eq52383ojQ2l4tH8gmjrZKBQsaDKQ2', 'manapparai', 'Trichy', '9994996019', 'active', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_purchase`
--

CREATE TABLE IF NOT EXISTS `tbl_group_purchase` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `vat` varchar(10) DEFAULT NULL,
  `trans` varchar(10) DEFAULT NULL,
  `total` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `tbl_group_purchase`
--

INSERT INTO `tbl_group_purchase` (`group_id`, `purchase_id`, `discount`, `vat`, `trans`, `total`, `qty`) VALUES
(18, 18, '', '4', '50', '5250', 1),
(19, 18, '0', '4', '10', '1882', 2),
(20, 19, '', '4', '', '4160', 10),
(21, 20, '', '5', '', '1749.3', 1),
(22, 21, '', '5', '', '6825', 10),
(23, 22, NULL, NULL, NULL, '1917.00', 1),
(24, 23, NULL, NULL, NULL, '199.50', 2),
(25, 24, NULL, NULL, NULL, '4550.00', 1),
(26, 24, NULL, NULL, NULL, '11999.99', 1),
(27, 25, NULL, NULL, NULL, '2100.00', 2),
(28, 26, NULL, NULL, NULL, '1000.00', 1),
(33, 27, NULL, NULL, NULL, '1000', 1),
(34, 27, NULL, NULL, NULL, '500', 1),
(43, 29, NULL, NULL, NULL, '6000', 1),
(45, 30, NULL, NULL, NULL, '6000', 1),
(57, 32, NULL, NULL, NULL, '600', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_sales`
--

CREATE TABLE IF NOT EXISTS `tbl_group_sales` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `vat` varchar(10) DEFAULT NULL,
  `trans` varchar(10) DEFAULT NULL,
  `total` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `complaint_id` int(11) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `tbl_group_sales`
--

INSERT INTO `tbl_group_sales` (`group_id`, `sales_id`, `discount`, `vat`, `trans`, `total`, `qty`, `complaint_id`, `notes`) VALUES
(49, 37, NULL, NULL, NULL, '1575.00', 1, 63, NULL),
(55, 36, NULL, NULL, NULL, '1575.00', 1, NULL, NULL),
(56, 38, NULL, NULL, NULL, '2100.00', 1, NULL, NULL),
(57, 38, NULL, NULL, NULL, '997.50', 1, NULL, NULL),
(58, 39, NULL, NULL, NULL, '1575.00', 1, NULL, NULL),
(65, 42, NULL, NULL, NULL, '1500', 1, NULL, 'test notes'),
(73, 44, NULL, NULL, NULL, '800', 1, NULL, 'hjfjfgfjj'),
(74, 45, NULL, NULL, NULL, '150', 1, NULL, 'testing'),
(76, 46, NULL, NULL, NULL, '150', 1, NULL, 'dfgdfgdfgdfgdf'),
(77, 46, NULL, NULL, NULL, '150', 1, NULL, 'dfgdfgdfgdfgdf'),
(78, 46, NULL, NULL, NULL, '150', 1, NULL, 'dfgdfgdfgdfgdf'),
(87, 41, NULL, NULL, NULL, '500', 1, NULL, 'test1'),
(88, 41, NULL, NULL, NULL, '2000', 1, NULL, 'test2'),
(93, 40, NULL, NULL, NULL, '1575.00', 1, NULL, ''),
(95, 47, NULL, NULL, NULL, '150', 1, NULL, 'fghffgfgjfg'),
(96, 43, NULL, NULL, NULL, '4550', 1, NULL, 'test warranty notes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group_stock`
--

CREATE TABLE IF NOT EXISTS `tbl_group_stock` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `total` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_group_stock`
--

INSERT INTO `tbl_group_stock` (`group_id`, `stock_id`, `total`, `qty`) VALUES
(9, 6, '1000.00', 1),
(10, 6, '1500.00', 1),
(11, 7, '500.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_income`
--

CREATE TABLE IF NOT EXISTS `tbl_income` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `income_id` int(11) DEFAULT NULL,
  `amount` varchar(30) DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_income`
--

INSERT INTO `tbl_income` (`id`, `income_id`, `amount`, `exp_date`, `description`) VALUES
(1, 1, '500', '2017-03-28', 'fgfgjggfj'),
(2, 2, '300', '2017-04-28', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_income_type`
--

CREATE TABLE IF NOT EXISTS `tbl_income_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `income_name` varchar(40) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_income_type`
--

INSERT INTO `tbl_income_type` (`id`, `income_name`, `description`) VALUES
(1, 'Discount Received', 'dsfsfdsfdsf'),
(2, 'Mis. Income', 'sdgsdgdsg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inventory`
--

CREATE TABLE IF NOT EXISTS `tbl_inventory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `price` varchar(30) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `sno` text,
  `shop` varchar(30) DEFAULT NULL,
  `type` varchar(40) DEFAULT NULL,
  `complaint_id` varchar(10) DEFAULT NULL,
  `sold_price` varchar(10) DEFAULT NULL,
  `customer_id` varchar(10) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `group_id_sales` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `selling_price` int(11) DEFAULT NULL,
  `group_stock_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=202 ;

--
-- Dumping data for table `tbl_inventory`
--

INSERT INTO `tbl_inventory` (`id`, `purchase_id`, `product_id`, `sales_id`, `stock_id`, `qty`, `price`, `total`, `sno`, `shop`, `type`, `complaint_id`, `sold_price`, `customer_id`, `group_id`, `group_id_sales`, `sub_total`, `selling_price`, `group_stock_id`) VALUES
(55, 18, 53, NULL, NULL, 1, '5000', 5000, 'dfjhgbkjdsbkjbdfa', '6', 'purchase', NULL, NULL, NULL, 18, NULL, NULL, 5500, NULL),
(56, 18, 48, NULL, NULL, 1, '900', 900, 'adsfsadfafads', '6', 'purchase', NULL, NULL, NULL, 19, NULL, NULL, 950, NULL),
(57, 19, 49, NULL, NULL, 1, '400', 400, 'dfjdyjdfj', '5', 'purchase', NULL, NULL, NULL, 20, NULL, NULL, 500, NULL),
(58, 20, 55, NULL, NULL, 1, '1666', 1666, 'SDFSDFDS', '6', 'purchase', NULL, NULL, NULL, 21, NULL, NULL, 2000, NULL),
(59, 21, 57, NULL, NULL, 1, '650', 650, 'fgfgdg', '6', 'purchase', NULL, NULL, NULL, 22, NULL, NULL, 750, NULL),
(60, 22, 58, NULL, NULL, 1, '1825.71', 1825, 'xcvvdsf', '6', 'purchase', NULL, NULL, NULL, 23, NULL, NULL, 2200, NULL),
(85, 23, 59, NULL, NULL, 1, '95', 95, '123475', '6', 'purchase', NULL, NULL, NULL, 24, NULL, NULL, 150, NULL),
(86, 23, 59, NULL, NULL, 1, '95', 95, '124545', '6', 'purchase', NULL, NULL, NULL, 24, NULL, NULL, 150, NULL),
(87, 24, 46, NULL, NULL, 1, '4333.33', 4333, '1235455', '5', 'purchase', NULL, NULL, NULL, 25, NULL, NULL, 4550, NULL),
(88, 24, 47, NULL, NULL, 1, '11428.56', 11428, '516165', '5', 'purchase', NULL, NULL, NULL, 26, NULL, NULL, 4500, NULL),
(112, 25, 61, NULL, NULL, 1, '1000', 1000, '2489naramuk', '5', 'purchase', NULL, NULL, NULL, 27, NULL, NULL, 1500, NULL),
(113, 25, 61, NULL, NULL, 1, '1000', 1000, '25689946', '5', 'purchase', NULL, NULL, NULL, 27, NULL, NULL, 1500, NULL),
(118, 26, 61, NULL, NULL, 1, '1000', 1000, '6589456kkjkj', '5', 'purchase', NULL, NULL, NULL, 28, NULL, NULL, 2000, NULL),
(121, NULL, 61, 37, NULL, -1, '1500.00', 1500, '25689946', '5', 'sales', '63', NULL, '9', NULL, 49, NULL, NULL, NULL),
(127, NULL, 61, 36, NULL, -1, '1500.00', 1500, '2489naramuk', NULL, 'sales', NULL, NULL, '8', NULL, 55, NULL, NULL, NULL),
(128, NULL, 61, 38, NULL, -1, '2000.00', 2000, '6589456kkjkj', NULL, 'sales', NULL, NULL, '8', NULL, 56, NULL, NULL, NULL),
(129, NULL, 48, 38, NULL, -1, '950.00', 950, 'adsfsadfafads', NULL, 'sales', NULL, NULL, '8', NULL, 57, NULL, NULL, NULL),
(131, NULL, 63, NULL, 6, 1, '1000', 1000, '12345', NULL, 'opening_stock', NULL, NULL, NULL, NULL, NULL, NULL, 1500, 9),
(132, NULL, 62, NULL, 6, 1, '1500', 1500, '123456', NULL, 'opening_stock', NULL, NULL, NULL, NULL, NULL, NULL, 2000, 10),
(142, NULL, 62, 42, NULL, -1, '1428.57', 1428, '1235ghjgjghjghhj', NULL, 'sales', NULL, NULL, '14', NULL, 65, NULL, NULL, NULL),
(145, 27, 62, NULL, NULL, 1, '952.38', 952, '1235ghjgjghjghhj', '5', 'purchase', NULL, NULL, NULL, 33, NULL, NULL, 1500, NULL),
(146, 27, 46, NULL, NULL, 1, '476.19', 476, 'kkkuuuummm', '5', 'purchase', NULL, NULL, NULL, 34, NULL, NULL, 800, NULL),
(154, NULL, 46, 44, NULL, -1, '761.90', 761, 'kkkuuuummm', NULL, 'sales', NULL, NULL, '8', NULL, 73, NULL, NULL, NULL),
(163, 29, 48, NULL, NULL, 1, '5714.29', 5714, 'dfgdfdfhkj14555555', '5', 'purchase', NULL, NULL, NULL, 43, NULL, NULL, 7000, NULL),
(164, NULL, 59, 45, NULL, -1, '142.86', 142, '124545', NULL, 'sales', NULL, NULL, '8', NULL, 74, NULL, NULL, NULL),
(177, NULL, 49, 41, NULL, -1, '476.19', 476, 'dfjdyjdfj', NULL, 'sales', NULL, NULL, '13', NULL, 87, NULL, NULL, NULL),
(178, NULL, 62, 41, NULL, -1, '1904.76', 1904, '123456', NULL, 'sales', NULL, NULL, '13', NULL, 88, NULL, NULL, NULL),
(183, NULL, 63, 40, NULL, -1, '1500.00', 1500, '12345', NULL, 'sales', NULL, NULL, '13', NULL, 93, NULL, NULL, NULL),
(185, 30, 49, NULL, NULL, 1, '5714.29', 5714, '1555fgdfgdfg5255', '5', 'purchase', NULL, NULL, NULL, 45, NULL, NULL, 7000, NULL),
(198, 32, 48, NULL, NULL, 1, '571.43', 571, '122333fgdfgdfgdf', '5', 'purchase', NULL, NULL, NULL, 57, NULL, NULL, 700, NULL),
(199, NULL, 59, 47, NULL, -1, '142.86', 142, '123475', NULL, 'sales', NULL, NULL, '16', NULL, 95, NULL, NULL, NULL),
(200, NULL, 46, 43, NULL, -1, '4333.33', 4333, '1235455', NULL, 'sales', NULL, NULL, '15', NULL, 96, NULL, NULL, NULL),
(201, NULL, 66, NULL, 7, 1, '500', 500, 'fghjjjk125555', NULL, 'opening_stock', NULL, NULL, NULL, NULL, NULL, NULL, 600, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_assign`
--

CREATE TABLE IF NOT EXISTS `tbl_job_assign` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `complaint_id` int(10) DEFAULT NULL,
  `from_id` int(10) DEFAULT NULL,
  `to_id` int(10) DEFAULT NULL,
  `from_dpt` varchar(30) DEFAULT NULL,
  `to_dpt` varchar(30) DEFAULT NULL,
  `details` text,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tbl_job_assign`
--

INSERT INTO `tbl_job_assign` (`id`, `complaint_id`, `from_id`, `to_id`, `from_dpt`, `to_dpt`, `details`, `status`, `date_created`) VALUES
(22, 63, 5, 7, 'Chip', 'checking', NULL, 'completed', '2017-10-31 07:40:15'),
(23, 63, 1, 33, NULL, 'Card', '', 'completed', '2017-10-31 08:36:53'),
(24, 63, 1, 46, NULL, 'BGA', '', 'completed', '2017-10-31 08:37:22'),
(25, 63, 1, 45, NULL, 'Chip', '', 'pending', '2017-10-31 11:02:53'),
(26, 63, 1, 45, NULL, 'Chip', 'display complaint', 'pending', '2017-10-31 11:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_manufacture`
--

CREATE TABLE IF NOT EXISTS `tbl_manufacture` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `manufacture_name` varchar(10) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_manufacture`
--

INSERT INTO `tbl_manufacture` (`id`, `manufacture_name`, `description`) VALUES
(6, 'Dell', 'Dell'),
(7, 'acer', ''),
(8, 'hp', ''),
(9, 'sony', ''),
(10, 'h-l data', ''),
(11, 'de', ''),
(12, 'renols', ''),
(13, 'Sjhjhgjgh', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_opening_stock`
--

CREATE TABLE IF NOT EXISTS `tbl_opening_stock` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `stock_date` date NOT NULL,
  `total` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_opening_stock`
--

INSERT INTO `tbl_opening_stock` (`id`, `stock_date`, `total`) VALUES
(1, '2017-03-25', '12100'),
(6, '2017-04-08', '2500'),
(7, '2017-05-09', '500');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_open_balance_customer`
--

CREATE TABLE IF NOT EXISTS `tbl_open_balance_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_open_balance_customer`
--

INSERT INTO `tbl_open_balance_customer` (`id`, `customer_id`, `transaction_id`, `amount`) VALUES
(1, 14, 20, 200),
(2, 14, 21, 300);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_open_balance_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_open_balance_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_open_balance_supplier`
--

INSERT INTO `tbl_open_balance_supplier` (`id`, `supplier_id`, `transaction_id`, `amount`) VALUES
(1, 5, 14, 600);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `pid` int(10) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `product_model` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `manufacturer` varchar(100) DEFAULT NULL,
  `description` text,
  `type_id` int(11) DEFAULT NULL,
  `manufacture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  FULLTEXT KEY `searchproduct` (`product_name`,`description`) COMMENT 'searchproduct'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`pid`, `product_name`, `product_model`, `type`, `manufacturer`, `description`, `type_id`, `manufacture_id`) VALUES
(43, 'tps51125`', 'laptop ic''s', 'stepdown', 'Tps', NULL, NULL, NULL),
(44, 'tps51125a', 'laptop ic''s', 'stepdown', 'Tps', NULL, NULL, NULL),
(46, 'bq727', 'laptop ic''s', 'charging ic', 'bq`', NULL, NULL, NULL),
(47, '320gb hdd', 'wd320gb', 'laptop', 'western digital', NULL, NULL, NULL),
(48, 'ram', '1gb', 'laptop', '`adata', NULL, NULL, NULL),
(49, 'laptop  fan', '4739', 'laptop fan', 'acer', NULL, NULL, 7),
(50, 'samsung 1gb ram ddr2', 'ddr2 ram', 'ram', 'samsung', NULL, 5, NULL),
(51, 'dvdwriter', 'lg12345``', 'desktop', 'lg', NULL, NULL, NULL),
(52, 'Dell Monitor 1234', '1234', 'Monitor', 'Dell', NULL, 1, 6),
(53, 'Dell Monitor e5112', 'e5112', 'Monitor', 'Dell', NULL, 1, 6),
(55, 'acer keyboard 4736', '4736', 'keyboard', 'acer', NULL, 2, 7),
(56, 'acer Monitor 1234', '1234', 'Monitor', 'acer', NULL, 1, 7),
(57, 'h-l data dvdwriter laptop sata', 'laptop sata', 'dvdwriter', 'h-l data', NULL, 6, 10),
(58, 'Dell top panel 1525 a+b+h', '1525 a+b+h', 'top panel', 'Dell', NULL, 7, 6),
(59, 'de camera 3521', '3521', 'camera', 'de', '', 8, 11),
(61, 'renols Monitor h323', 'h323', 'Monitor', 'renols', 'bhuvajdfdjfhdfjhdj', 1, 12),
(62, 'hp Mouse testxxnnnx', 'testxxnnnx', 'Mouse', 'hp', 'bharath', 3, 8),
(63, 'hp Mouse 2345658', '2345658', 'Mouse', 'hp', 'kumar', 3, 8),
(64, 'acer Monitor test', 'test', 'Monitor', 'acer', 'hhhdhdhh', 1, 7),
(65, 'hp1 Monitor H2O', 'H2O', 'Monitor', 'hp1', 'ghjfgjfgjg', 1, 8),
(66, 'acer ram 18.5v 3.5a', '18.5v 3.5a', 'ram', 'acer', 'test', 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_type`
--

CREATE TABLE IF NOT EXISTS `tbl_product_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_type` varchar(10) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_product_type`
--

INSERT INTO `tbl_product_type` (`id`, `product_type`, `description`) VALUES
(1, 'Monitor', 'Led monitor'),
(2, 'keyboard', 'keyboardadsfff safafsaffsa'),
(3, 'Mouse', 'fgdfgdfgdfgdfg'),
(4, 'fan', 'laptop fan'),
(5, 'ram', 'laptop'),
(6, 'dvdwriter', 'laptop sata '),
(7, 'top panel', 'Abh'),
(8, 'camera', 'Laptop');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_purchase`
--

CREATE TABLE IF NOT EXISTS `tbl_purchase` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) NOT NULL,
  `purchase_date` date NOT NULL,
  `shop` varchar(30) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `invoice_number` varchar(50) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `vat` varchar(10) DEFAULT NULL,
  `trans` varchar(10) DEFAULT NULL,
  `paid` float NOT NULL DEFAULT '0',
  `tax_type` int(1) NOT NULL DEFAULT '1',
  `credit_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `tbl_purchase`
--

INSERT INTO `tbl_purchase` (`id`, `vendor_id`, `purchase_date`, `shop`, `total`, `sub_total`, `invoice_number`, `discount`, `vat`, `trans`, `paid`, `tax_type`, `credit_type`) VALUES
(18, 4, '2017-03-20', '6', 7132, NULL, '11321', NULL, NULL, NULL, 0, 1, NULL),
(19, 5, '2017-03-20', '5', 4160, NULL, '1321', NULL, NULL, NULL, 4160, 1, NULL),
(20, 6, '2017-03-20', '6', 1749.3, NULL, '20876', NULL, NULL, NULL, 0, 1, NULL),
(21, 6, '2017-03-21', '6', 6825, NULL, '12212', NULL, NULL, NULL, 0, 1, NULL),
(22, 6, '2017-03-21', '6', 1917, NULL, '45554', '0', '5', '50', 0, 1, NULL),
(23, 5, '2017-03-26', '6', 199.5, NULL, '5522', '', '5', '50', 90, 1, NULL),
(24, 4, '2017-03-28', '5', 16550, NULL, '1234', '150', '5', '', 0, 1, NULL),
(25, 6, '2017-04-07', '5', 2100, NULL, '15689', '100', '5', '', 0, 1, NULL),
(26, 4, '2017-04-14', '5', 1000, NULL, '2147483647', '', '', '', 0, 1, NULL),
(27, 6, '2017-04-17', '5', 1500, NULL, '99956466', '10', '5', '10', 0, 2, NULL),
(28, 6, '2017-04-15', '5', 6000, NULL, '12548954', '', '5', '', 0, 2, NULL),
(29, 6, '2017-04-15', '5', 6000, NULL, '12548954', '', '5', '', 0, 2, 6),
(30, 6, '2017-04-15', '5', 6000, NULL, '12548954', '', '5', '', 1000, 2, NULL),
(32, 6, '2017-05-03', '5', 640, 600, 'VI/COM/00128/17-18', '10', '5', '50', 500, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_purchase_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_purchase_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `notes` text,
  `pay_type` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `purchase_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_purchase_transaction`
--

INSERT INTO `tbl_purchase_transaction` (`id`, `vendor_id`, `notes`, `pay_type`, `amount`, `purchase_date`, `modified_date`) VALUES
(3, 5, 'test', 3, 4000, '2017-04-25 18:34:14', '2017-04-25 18:34:14'),
(4, 5, 'test   ', 1, 250, '2017-04-25 18:35:40', '2017-04-25 18:35:40'),
(5, 6, NULL, 1, 1000, '2017-05-08 13:04:25', '2017-05-08 13:04:25'),
(6, 6, NULL, 1, 1000, '2017-05-08 13:17:06', '2017-05-08 13:17:06'),
(13, 6, ' ', 1, 500, '2017-05-08 13:45:47', '2017-05-08 13:45:47'),
(14, 5, 'dsfgsfdsfsdf       ', 1, 600, '2017-05-18 16:39:08', '2017-05-18 16:39:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_receipt`
--

CREATE TABLE IF NOT EXISTS `tbl_receipt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `pay_type` varchar(30) DEFAULT NULL,
  `amount` varchar(30) DEFAULT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` text,
  `payon_sales` int(11) NOT NULL DEFAULT '2',
  `sales_id` int(11) DEFAULT NULL,
  `bank_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tbl_receipt`
--

INSERT INTO `tbl_receipt` (`id`, `transaction_id`, `pay_type`, `amount`, `sales_date`, `notes`, `payon_sales`, `sales_id`, `bank_date`) VALUES
(29, 4, '3', '500', '2017-04-25 08:47:57', 'test      ', 2, 40, NULL),
(30, 5, '4', '50', '2017-04-25 08:49:46', 'gfgdfgdfg ', 2, 40, NULL),
(31, 6, '4', '500', '2017-04-25 10:17:41', 'ghjfghjghjghjjhjj ', 2, 42, NULL),
(32, 7, '4', '900', '2017-04-25 10:43:24', 'hgjghjghjhj ', 2, 42, NULL),
(33, 8, '4', '1025', '2017-04-25 10:44:49', 'vhfghfhfh ', 2, 40, NULL),
(34, 8, '4', '975', '2017-04-25 10:44:49', 'vhfghfhfh ', 2, 41, NULL),
(35, 9, '1', '1500', '2017-04-25 10:45:33', 'hjjgjghj', 2, 41, NULL),
(38, 11, '1', '110', '2017-05-08 08:42:46', ' ', 1, 47, '2017-05-03'),
(39, NULL, '1', '100', '2017-05-04 12:58:41', ' ', 1, NULL, NULL),
(40, NULL, '1', '100', '2017-05-04 12:59:11', ' ', 1, NULL, NULL),
(41, 14, '3', '2000', '2017-05-04 01:32:33', ' ', 1, 41, NULL),
(42, 15, '1', '1000', '2017-05-04 01:44:23', ' ', 1, 40, NULL),
(43, 16, '1', '500', '2017-05-08 01:56:22', '', 2, 37, '2017-05-08'),
(44, 17, '1', '540', '2017-05-09 05:43:59', ' ', 1, 43, '2017-05-08'),
(45, 21, '3', '100', '2017-05-18 10:49:50', ' yuuyi', 2, 42, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales`
--

CREATE TABLE IF NOT EXISTS `tbl_sales` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `complaint_id` int(11) DEFAULT NULL,
  `sales_date` date NOT NULL,
  `total` float DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `vat` varchar(10) DEFAULT NULL,
  `trans` varchar(10) DEFAULT NULL,
  `paid` float NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `service_sno` varchar(20) DEFAULT NULL,
  `sales_sno` varchar(20) DEFAULT NULL,
  `tax_type` int(1) NOT NULL DEFAULT '1',
  `credit_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `tbl_sales`
--

INSERT INTO `tbl_sales` (`id`, `customer_id`, `complaint_id`, `sales_date`, `total`, `sub_total`, `discount`, `vat`, `trans`, `paid`, `status`, `service_sno`, `sales_sno`, `tax_type`, `credit_type`) VALUES
(36, 8, NULL, '2017-04-05', 1575, NULL, '50', '5', '5', 0, 1, NULL, '1', 1, NULL),
(37, 9, 63, '2017-04-03', 1575, NULL, '10', '5', '', 500, 1, '1', NULL, 1, NULL),
(38, 8, NULL, '2017-04-05', 3097.5, NULL, '50', '5', '5', 0, 1, NULL, '1', 1, NULL),
(40, 13, NULL, '2017-05-04', 1575, NULL, '10', '5', '', 1000, 1, NULL, '2', 1, NULL),
(41, 13, NULL, '2017-05-04', 2500, NULL, '10', '5', '5', 2000, 1, NULL, '3', 2, NULL),
(42, 14, NULL, '2017-04-18', 1500, NULL, '10', '5', '10', 1500, 1, NULL, '4', 2, NULL),
(43, 15, NULL, '2017-05-09', 4540, 4550, '10', '5', '', 540, 1, NULL, '5', 2, NULL),
(44, 8, NULL, '2017-04-22', 800, NULL, '', '5', '', 0, 1, NULL, '6', 2, 6),
(45, 8, NULL, '2017-05-01', 150, NULL, '', '5', '', 0, 1, NULL, '7', 2, 0),
(47, 16, NULL, '2017-05-08', 200, 150, '', '5', '50', 110, 1, NULL, '8', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_sales_transaction` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` text,
  `pay_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_sales_transaction`
--

INSERT INTO `tbl_sales_transaction` (`id`, `customer_id`, `amount`, `sales_date`, `notes`, `pay_type`) VALUES
(4, 13, 500, '2017-04-25 11:39:18', 'test      ', 3),
(5, 13, 50, '2017-04-25 14:18:46', 'gfgdfgdfg ', 4),
(6, 14, 500, '2017-04-25 15:47:27', 'ghjfghjghjghjjhjj ', 4),
(7, 14, 900, '2017-04-25 15:48:42', 'hgjghjghjhj ', 4),
(8, 13, 2000, '2017-04-25 16:14:29', 'vhfghfhfh ', 4),
(9, 13, 1500, '2017-04-25 16:15:33', 'hjjgjghj', 1),
(11, 16, 110, '2017-05-01 13:44:55', NULL, 1),
(14, 13, 2000, '2017-05-04 18:54:21', NULL, 3),
(15, 13, 1000, '2017-05-04 19:05:38', NULL, 1),
(16, 9, 500, '2017-05-08 19:26:22', '', 1),
(17, 15, 540, '2017-05-09 11:13:59', NULL, 1),
(20, 14, 200, '2017-05-18 13:44:10', 'hfghfghg        ', 3),
(21, 14, 400, '2017-05-18 16:17:50', ' yuuyi', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_selling_price`
--

CREATE TABLE IF NOT EXISTS `tbl_selling_price` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `selling_name` varchar(30) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_selling_price`
--

INSERT INTO `tbl_selling_price` (`id`, `selling_name`, `price`) VALUES
(4, 'DISC5', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `option_name` varchar(30) DEFAULT NULL,
  `option_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `service_id`, `option_name`, `option_value`) VALUES
(1, 1, 'company_name', 'MR IMPEX'),
(2, 1, 'address', 'No.20A,Puthur High Road,(Opp. Aruna Theatre),Trichy-17.'),
(3, 1, 'contact_number', '95436 66362'),
(4, 1, 'contact_number1', '95436 66362'),
(5, 1, 'contact_number2', '90255 34526'),
(6, 1, 'contact_number3', '97867 68786'),
(7, 1, 'city', 'Trichy'),
(8, 1, 'state', 'Tamil Nadu'),
(9, 1, 'title', '(EXPERT IN CHIP LEVEL SERVICE)\r\n(Used Laptop & Accessories Sale)'),
(10, 1, 'email', 'mrinfoservice@gmail.com'),
(11, 1, 'website', 'www.laptopservicetrichy.com'),
(12, 1, 'tin_number', '12345'),
(13, 1, 'cst_number', '654321'),
(14, 1, 'bank_name', 'TMB BANK'),
(15, 1, 'account_number', '02315648954565446'),
(16, 1, 'ifsc', 'TMBL12345689'),
(17, 1, 'branch', 'trichy');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shop`
--

CREATE TABLE IF NOT EXISTS `tbl_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` varchar(40) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_shop`
--

INSERT INTO `tbl_shop` (`id`, `shop`, `description`) VALUES
(5, 'Mr infotech', 'No.20/A, Puthur High Road, \r\nNear Aruna Theater busstop,\r\nPuthur,Trichy-17.'),
(6, 'Mr Impex', 'No.20/A, Puthur High Road, \nNear Aruna Theater busstop,\nPuthur,Trichy-17.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_history`
--

CREATE TABLE IF NOT EXISTS `tbl_status_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `complaint_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `notes` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_status_history`
--

INSERT INTO `tbl_status_history` (`id`, `complaint_id`, `user_id`, `status`, `notes`, `created_date`) VALUES
(1, 74, 1, 'Completed', 'fsgssggdsg', '2017-03-30 07:50:10'),
(2, 74, 1, 'pending', 'fdegdfgdfg', '2017-03-30 07:50:26'),
(3, 62, 1, 'Rejected', 'dsfsdsdsfdsf', '2017-03-30 07:51:34'),
(4, 62, 1, 'pending', 'textfhhf', '2017-03-30 07:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_user_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_user_settings`
--

INSERT INTO `tbl_user_settings` (`id`, `user_id`, `document`) VALUES
(6, 42, 'e9da78dee8db7a6b2d691c049c531ce4.txt'),
(7, 42, 'ad08b21be7123bf68d0550cc4f32479b.txt'),
(8, 42, '941c618fb1b0e9e80d76f08e1526d92c.pdf'),
(9, 42, 'fb895c3cba536d7f3c42e69ad3125f62.jpg'),
(10, 43, 'ad5686fe0e89b8c28a1242e121d7eed3.jpg'),
(11, 43, 'fc9ec8564467f5baa21e0b015bb94349.jpg'),
(12, 43, '1050d68d9a60b8a32c26d8fc68878646.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vat`
--

CREATE TABLE IF NOT EXISTS `tbl_vat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `vat_name` varchar(40) DEFAULT NULL,
  `vat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_vat`
--

INSERT INTO `tbl_vat` (`id`, `vat_name`, `vat`) VALUES
(1, 'VAT @ 14.5', 15),
(2, 'VAT @ 5%', 5),
(3, 'Service Tax @ 14%', 14),
(5, 'VAT @10%', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor`
--

CREATE TABLE IF NOT EXISTS `tbl_vendor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `company_name` text,
  `tin_no` varchar(30) DEFAULT NULL,
  `office_no` varchar(30) DEFAULT NULL,
  `Address` text,
  `contact_phone` varchar(20) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(30) DEFAULT NULL,
  `cst_number` varchar(30) DEFAULT NULL,
  `opening_balance` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `pincode` varchar(30) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_vendor`
--

INSERT INTO `tbl_vendor` (`id`, `first_name`, `last_name`, `email_address`, `city`, `company_name`, `tin_no`, `office_no`, `Address`, `contact_phone`, `date_created`, `state`, `cst_number`, `opening_balance`, `paid`, `pincode`, `notes`) VALUES
(4, 'kannan', 'r', 'comcare@gmail.com', 'chennai', 'comcare', '88888888888', '1231231231', 'ritche st', '6666666666', '2017-03-01 14:58:57', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'rajagopalanm', 'k', 'SWASTIK@GMAIL.COM', 'chennai', 'swastil', '12312312312', '', 'thambardam', '1212121222122', '2017-03-04 09:06:49', '', '', 1000, 600, '', ''),
(6, 'kumara', 'r', 'laptec@gmail.com', 'maurai', 'lap tech', '33866257428', '04524250444', 'salai road', '9999944444', '2017-03-20 11:53:25', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'antony', 'n', 'ww@gmail.com', 'chennai', 'ddd', '12345678912', '621305', 'walaja', '987654321', '2017-03-28 08:29:47', 'tamilnadu', '1234', 150, NULL, '621305', 'fgdssfssssssssssssssssssssssssssssssssssssscxvcxcxcxcxcxcxbxcb');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher`
--

CREATE TABLE IF NOT EXISTS `tbl_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `pay_type` varchar(30) DEFAULT NULL,
  `amount` varchar(30) DEFAULT NULL,
  `voucher_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notes` text,
  `payon_purchase` int(2) NOT NULL DEFAULT '2',
  `purchase_id` int(11) DEFAULT NULL,
  `bank_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tbl_voucher`
--

INSERT INTO `tbl_voucher` (`id`, `transaction_id`, `pay_type`, `amount`, `voucher_date`, `notes`, `payon_purchase`, `purchase_id`, `bank_date`) VALUES
(52, 3, '3', '4000', '2017-04-25 01:04:14', 'test', 2, 19, NULL),
(53, 4, '1', '160', '2017-04-25 01:13:09', 'test   ', 2, 19, NULL),
(54, 4, '1', '90', '2017-04-25 01:13:09', 'test   ', 2, 23, NULL),
(55, 5, '1', '1000', '2017-05-08 07:34:25', '', 1, 30, '2017-05-05'),
(64, 13, '1', '500', '2017-05-09 05:42:48', ' ', 1, 32, '2017-05-03');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `roll` varchar(30) DEFAULT NULL,
  `contact_phone` varchar(30) DEFAULT NULL,
  `address` text,
  `city` text,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `superadmin` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT '0',
  `date_of_join` date DEFAULT NULL,
  `qulification` varchar(20) DEFAULT NULL,
  `gaurdian_mob` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `aathar_no` varchar(20) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `profile` varchar(100) DEFAULT NULL,
  `document1` varchar(100) DEFAULT NULL,
  `document2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `last_name`, `roll`, `contact_phone`, `address`, `city`, `auth_key`, `password_hash`, `confirmation_token`, `status`, `superadmin`, `created_at`, `updated_at`, `registration_ip`, `bind_to_ip`, `email`, `email_confirmed`, `date_of_join`, `qulification`, `gaurdian_mob`, `state`, `aathar_no`, `blood_group`, `profile`, `document1`, `document2`) VALUES
(1, 'superadmin', 'superadmin', NULL, NULL, NULL, NULL, NULL, 'KWhYMciqr5cpsSvNrYLTBuUOGcwUlLtR', '$2y$13$LEg9S/aaqzNzBks7D5MWD.jQGcUBpPFr2WRnfV6dwaE9Rd5vCSoSC', NULL, 1, 1, 1477030352, 1477058099, NULL, '', 'bhu@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'admin', 'Admin', NULL, NULL, NULL, NULL, NULL, 'YvyQBVTmdy2RubQiM6QOcOfJVnfK2HlB', '$2y$13$t87vEoO/pvk/JFsWrvIbp.ic/HE7p7uSy6dVb7ecLpWffLFJ64KQO', NULL, 1, 0, 1477047606, 1477139061, '::1', '', 'bhu@gmail.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'kbk2489@gmail.com', 'dfghfgh', 'ghfgh', 'Card', '96328655655', 'fghfgh', 'fgfhfg', NULL, '$2y$10$AMJAOAygOhTQd4f4rTiSVOIMJBA5GhX2PApJ5iPhFrkybQWtAeHHy', NULL, 1, 0, NULL, NULL, NULL, NULL, 'kbk2489@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'nagoor1@gmail.com', 'bhuvanesh', '', 'BGA', '666666666611', 'trichy21,2nd street', 'Trichy', '', '$2y$10$Ydb9ubYEniVsj793uGbd5ufbTgy..F5hCwpJ5B9Uakc/q/oqoACrG', NULL, 1, 0, NULL, NULL, NULL, NULL, 'nagoor1@gmail.com', 0, NULL, 'MCA', '9629483006', 'Tamilnadu', '1234', 'B+', '880images.jpg', NULL, NULL),
(43, 'kumaran2489@gmail.com', 'Kumaravel', '', 'BGA', '9994996019', 'Samy nagar,2nd street ', 'Namakkal', '', '$2y$10$djjqD18tYWBCVW/ODTGIwe3OmFA.qPDwYL0sYBp0RIibKZByeB2mm', NULL, 1, 0, NULL, NULL, NULL, NULL, 'kumaran2489@gmail.com', 0, NULL, 'MCA', '8778522458', 'Tamil nadu', '125', 'B+', '767kk.jpg', NULL, NULL),
(45, 'kanchana@gmail.com', 'Kanchana', NULL, 'Chip', '9898989898', 'No-50-a,ahamed colony first cross', 'Trichy', '', '$2y$10$6/tLfDqtNPMcW31N4ODAIuKRLCC8f7G4qYLq3m4pb7X5FGoRS2D5u', NULL, 1, 0, NULL, NULL, NULL, NULL, 'kanchana@gmail.com', 0, NULL, 'B.E', '8787878787', 'Tamilnadu', '654656546', 'o+', NULL, NULL, NULL),
(46, 'vivekpalanisamy@gmail.com', 'vivek', NULL, 'BGA', '09894300131', 'Namakkal', 'Namakkal', '', '$2y$10$HfCElb7jZDHUJIY/fGb.JOKxjTwcdPos.YJkkO35N8tPSfG/ZXjHC', NULL, 1, 0, NULL, NULL, NULL, NULL, 'vivekpalanisamy@gmail.com', 0, NULL, 'BTECH', '09894300131', 'tamil nadu', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_visit_log`
--

CREATE TABLE IF NOT EXISTS `user_visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_auth_item_group_code` FOREIGN KEY (`group_code`) REFERENCES `auth_item_group` (`code`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  ADD CONSTRAINT `user_visit_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
